export default {
	props: {
		title: {
			required: true,
		},
		message: {
			protected: true,
		},
		createdAt: {
			type: "datetime",
			required: true,
			readonly: true,
		},
		dueAt: {
			type: "datetime",
			required: true,
			index: true,
		}
	},
	hooks: {
		afterCreate() {
			if ( this.$isNew ) {
				this.createdAt = new Date();
			}
		},
	},
};
