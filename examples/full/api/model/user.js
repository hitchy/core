/* eslint-disable jsdoc/require-jsdoc */

// extends the user model that comes included with @hitchy/plugin-auth
export default function( options, User ) {
	return {
		...User,
		props: {
			...User.props,
			mail: {},
			interests: {},
		},
	};
}
