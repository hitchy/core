import HTTP from "node:http";

/**
 * Implements service for running a fake backend to evaluate the reverse proxy
 * feature of @hitchy7plugin-proxy.
 */
export default class FakeBackend {
	static #server;

	/**
	 * Starts an HTTP service for listening to requests locally forwarded by the
	 * reverse proxy of @hitchy/plugin-proxy.
	 */
	static startService() {
		this.#server = HTTP.createServer( ( req, res ) => {
			const chunks = [];

			req.on( "data", chunk => chunks.push( chunk ) );
			req.on( "end", () => {
				const body = Buffer.concat( chunks );

				const data = body.length ? JSON.parse( body.toString( "utf8" ) ) : {};

				data.method = req.method;
				data.query = req.url;
				data.processedByFakeBackend = true;

				res.setHeader( "content-type", "application/json" );
				res.end( JSON.stringify( data ) );
			} );
		} );

		this.#server.listen( 3001, "127.0.0.1" );
	}

	/**
	 * Stops the HTTP service set up before for listening to requests locally
	 * forwarded by the reverse proxy of @hitchy/plugin-proxy.
	 */
	static stopService() {
		this.#server.close();
	}
}
