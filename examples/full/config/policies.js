export default function() { // eslint-disable-line jsdoc/require-jsdoc
	const api = this;

	return {
		policies: {
			"GET /ping/deferring": api.service.RateLimiter.limitPerTime( 3, { timeframe: 10, queueSize: 1000 } ),
			"GET /ping/dropping": api.service.RateLimiter.limitPerTime( 3, { timeframe: 10, queueSize: 0 } ),
			"GET /ping/mixed": api.service.RateLimiter.limitPerTime( 3, { timeframe: 10, queueSize: 5 } ),
		}
	};
}
