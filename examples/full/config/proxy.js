export const proxy = [
	{
		prefix: "/api/backend",
		target: "http://127.0.0.1:3001/"
	},
	{
		prefix: "/lib/htmx",
		target: "https://unpkg.com/htmx.org@2.0.3/dist/htmx.min.js",
		opaque: true
	},
	{
		prefix: "/lib/cst",
		target: "https://unpkg.com/htmx.org@1.9.12/dist/ext/client-side-templates.js",
		opaque: true
	},
	{
		prefix: "/lib/handlebars",
		target: "https://cdn.jsdelivr.net/npm/handlebars@latest/dist/handlebars.js",
		opaque: true
	},
	{
		prefix: "/lib/tailwind",
		target: "https://cdn.tailwindcss.com/3.4.15",
		opaque: true
	},
	{
		prefix: "/lib/alpinejs",
		target: "https://unpkg.com/alpinejs@3.14.3",
		opaque: true
	},
	{
		prefix: "/lib/vue",
		target: "https://unpkg.com/vue@3/dist/vue.global.js",
		opaque: true
	},
	{
		prefix: "/lib/vue-reactivity",
		target: "https://cdn.jsdelivr.net/npm/@vue/reactivity@latest/dist/reactivity.global.min.js",
		opaque: true
	},
	{
		prefix: "/lib/lit-html/",
		target: "https://unpkg.com/lit-html@latest/",
		opaque: true
	},
	{
		prefix: "/lib/lit/",
		target: "https://cdn.jsdelivr.net/gh/lit/",
		opaque: true
	},
];
