export const routes = {
	"GET /ping{/:value}": ( req, res ) => setTimeout( () => res.json( { pong: req.params.value } ), 50 ),
};
