export default {
	static: [
		{
			prefix: "/",
			folder: "frontend",
			fallback: "not-found.html",
		}
	]
};
