/* eslint-disable jsdoc/require-jsdoc */

import { LitElement, html } from "/lib/lit/dist@3/core/lit-core.min.js";
import { addGlobalStylesToShadowRoot } from "../lib/styles.js";
import { get } from "../lib/query.js";
import { AuthenticationMixin } from "../lib/authenticated.js";

export class AuthControlElement extends AuthenticationMixin( LitElement ) {
	render() {
		let action;

		if ( this.authenticated?.user ) {
			action = html`<button @click="${this.logout}">Logout</button>`;
		} else if ( this.authenticated?.user != null ) {
			action = html`<x-login-form></x-login-form>`;
		}

		return html`
			<div class="flex row center-across gap wrap">
				<x-auth-state class="greedy"></x-auth-state>
				${action}
			</div>
		`;
	}

	createRenderRoot() {
		return addGlobalStylesToShadowRoot( super.createRenderRoot() );
	}

	async logout() {
		await get( "/api/auth/logout" );

		this.fetchUser();
	}
}

customElements.define( "x-auth-control", AuthControlElement );
