/* eslint-disable jsdoc/require-jsdoc */

import { LitElement, html } from "/lib/lit/dist@3/core/lit-core.min.js";
import { AuthenticationMixin } from "../lib/authenticated.js";

export class AuthStateElement extends AuthenticationMixin( LitElement ) {
	render() {
		if ( this.authenticated?.user?.name ) {
			return html`Welcome, ${this.authenticated.user.name}!`;
		}

		if ( this.authenticated?.user ) {
			return html`Welcome!`;
		}

		return html`Please log in!`;
	}
}

customElements.define( "x-auth-state", AuthStateElement );
