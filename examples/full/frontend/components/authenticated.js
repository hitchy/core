/* eslint-disable jsdoc/require-jsdoc */
import { LitElement, html } from "/lib/lit/dist@3/core/lit-core.min.js";
import { AuthenticationMixin } from "../lib/authenticated.js";

export class AuthenticatedElement extends AuthenticationMixin( LitElement ) {
	render() {
		return html`
			<slot></slot>
			${this.authenticated.user ? html`<slot name="authenticated"></slot>` : undefined}
			${this.authenticated.user ? undefined : html`<slot name="unauthenticated"></slot>`}
		`;
	}
}

customElements.define( "x-authenticated", AuthenticatedElement );
