/* eslint-disable jsdoc/require-jsdoc */

import { LitElement, html, css } from "/lib/lit/dist@3/core/lit-core.min.js";
import { addGlobalStylesToShadowRoot } from "../lib/styles.js";

export class CardElement extends LitElement {
	static styles = css`
		.card {
			padding: 1rem;
			margin: 1rem;
			background: skyblue;
			border: 1px solid deepskyblue;
			border-radius: 5px;
		}
	`;

	render() {
		return html`
			<div class="card flex column gap">
				<slot></slot>
			</div>
		`;
	}

	createRenderRoot() {
		return addGlobalStylesToShadowRoot( super.createRenderRoot() );
	}
}

customElements.define( "x-card", CardElement );
