/* eslint-disable jsdoc/require-jsdoc */
import { LitElement, html } from "/lib/lit/dist@3/core/lit-core.min.js";

export class IfElement extends LitElement {
	static properties = {
		condition: {}
	};

	render() {
		return html`<div>${this.condition ? html`<slot></slot>` : ""}</div>`;
	}
}

customElements.define( "x-if", IfElement );
