/* eslint-disable jsdoc/require-jsdoc */

import { LitElement, html } from "/lib/lit/dist@3/core/lit-core.min.js";
import { post } from "../lib/query.js";
import { AuthenticationMixin } from "../lib/authenticated.js";
import { FormMixin } from "../lib/form.js";
import { addGlobalStylesToShadowRoot } from "../lib/styles.js";
import { ToastsMixin } from "../lib/toasts.js";

export class LoginFormElement extends ToastsMixin( FormMixin( AuthenticationMixin( LitElement ) ) ) {
	static properties = {
		username: { state: true },
		password: { state: true },
	};

	render() {
		return html`
			<form class="flex row center-across gap-sm wrap" @submit=${this.login}>
				<input placeholder="your username..." name="username" value=${this.username} @input=${this.value()}>
				<input placeholder="your password..." name="password" value=${this.password} @input=${this.value()} type="password">
				<button type="submit" ?disabled=${!this.username || !this.password}>Login</button>
			</form>
		`;
	}

	createRenderRoot() {
		return addGlobalStylesToShadowRoot( super.createRenderRoot() );
	}

	async login( event ) {
		event.preventDefault();

		if ( this.username && this.password ) {
			try {
				const result = await post( "/api/auth/login", {
					username: this.username,
					password: this.password,
				} );

				if ( result.authenticated ) {
					this.fetchUser();
					return;
				}
			} catch {}

			this.errorToast( "Login has failed." );
		}
	}
}

customElements.define( "x-login-form", LoginFormElement );
