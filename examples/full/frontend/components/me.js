/* eslint-disable jsdoc/require-jsdoc */
import { LitElement, html } from "/lib/lit/dist@3/core/lit-core.min.js";
import { addGlobalStylesToShadowRoot } from "../lib/styles.js";
import { FormMixin } from "../lib/form.js";
import { post, patch, get } from "../lib/query.js";
import { AuthenticationMixin } from "../lib/authenticated.js";
import { ToastsMixin } from "../lib/toasts.js";

export class MeElement extends ToastsMixin( FormMixin( AuthenticationMixin( LitElement ) ) ) {
	static properties = {
		current: "",
		next: "",
		repetition: "",
		mail: "",
		interests: "",
	};

	#loaded = false;

	render() {
		return html`
			<h2>Change password</h2>
			<div>
				<form class="flex row center-across gap-sm wrap" @submit="${this.changePassword}">
					<input type="password" name="current" @input="${this.value()}" placeholder="current password">
					<input type="password" name="next" @input="${this.value()}" placeholder="new password">
					<input type="password" name="repetition" @input="${this.value()}" placeholder="new password again">
					<button type="submit" ?disabled="${!this.current || !this.next || this.next !== this.repetition}">Submit</button>
				</form>
			</div>
			<h2>Manage your profile</h2>
			<ul>
				<li>The default user model of <b>@hitchy/plugin-auth</b> has been augmented to contain custom data per user.</li>
			</ul>
			<div style="margin-top: 1em">
				<form class="flex row center-across gap-sm wrap" @submit="${this.saveAccount}">
					<input type="email" name="mail" value=${this.mail} @input="${this.value()}" placeholder="your mail address">
					<input type="text" name="interests" value=${this.interests} @input="${this.value()}" placeholder="your interests">
					<button type="submit">Save account</button>
				</form>
			</div>`;
	}

	createRenderRoot() {
		return addGlobalStylesToShadowRoot( super.createRenderRoot() );
	}

	async willUpdate() {
		if ( !this.#loaded && this.authenticated.user ) {
			this.#loaded = true;

			const record = await get( `/api/user/${this.authenticated.user.uuid}` );

			this.mail = record.mail;
			this.interests = record.interests;
		}
	}

	async changePassword( event ) {
		const form = event.target;

		event.preventDefault();

		if ( this.current && this.next && this.next === this.repetition ) {
			const { error } = await post( "/api/auth/password", {
				current: this.current,
				next: this.next,
			} );

			if ( error ) {
				this.errorToast( "Changing password has failed." );
			} else {
				this.current = this.next = this.repetition = "";
				form.reset();
			}
		}
	}

	async saveAccount( event ) {
		event.preventDefault();

		const result = await patch( `/api/user/${this.authenticated.user.uuid}`, {
			mail: this.mail,
			interests: this.interests,
		} );

		if ( result.error ) {
			this.errorToast( "Updating account has failed." );
		}
	}
}

customElements.define( "x-me", MeElement );
