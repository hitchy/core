/* eslint-disable jsdoc/require-jsdoc */
import { LitElement, html, css } from "/lib/lit/dist@3/core/lit-core.min.js";
import { get } from "../lib/query.js";
import { addGlobalStylesToShadowRoot } from "../lib/styles.js";
import { ContextualMixin } from "../lib/contextual.js";

export class PingerElement extends ContextualMixin( LitElement ) {
	static properties = {
		value: {},
		tip: {},
		okay: { type: Number, state: true },
		fail: { type: Number, state: true },
	};

	static styles = css`
		.flex {
			line-height: 1.5em;
			min-width: 0;
			width: min-content;
			font-weight: bold;
			cursor: help;
		}
		
		.disabled {
			opacity: 0.5;
		}

		.label {
			flex: 1 100 1px;
			background: darkred;
			color: white;
			border-top-left-radius: 0.75em;
			border-bottom-left-radius: 0.75em;
			border-top: 3px solid darkred;
			border-left: 3px solid darkred;
			border-bottom: 3px solid darkred;
			padding: 0 0.5em;
		}

		.fail {
			flex: 1 100 1px;
			min-width: 2em;
			background: white;
			color: red;
			text-align: right;
			border-top: 3px solid darkred;
			border-bottom: 3px solid darkred;
			padding: 0 0.2em;
		}

		.okay {
			flex: 1 100 1px;
			min-width: 2em;
			background: white;
			color: green;
			border-top-right-radius: 0.75em;
			border-bottom-right-radius: 0.75em;
			text-align: right;
			border-top: 3px solid darkred;
			border-right: 3px solid darkred;
			border-bottom: 3px solid darkred;
			padding: 0 0.5em;
		}
	`;

	#timer;

	constructor() {
		super( {
			namespace: "pinger",
			initialize: () => {
				this.pinger.enabled = localStorage.getItem( "pinger" ) || this.value;
			}
		} );

		this.okay = 0;
		this.fail = 0;
	}

	createRenderRoot() {
		return addGlobalStylesToShadowRoot( super.createRenderRoot() );
	}

	render() {
		return html`
			<div class="flex row center-across start ${this.pinger.enabled === this.value ? "" : "disabled"}" title="${this.tip}" @click="${this.enable}">
				<div class="label">${this.value}</div>
				<div class="fail">${this.fail}</div>
				<div class="okay">${this.okay}</div>
			</div>`;
	}

	connectedCallback() {
		super.connectedCallback();

		this.ping();
	}

	disconnectedCallback() {
		super.disconnectedCallback();

		clearTimeout( this.#timer );
		this.#timer = undefined;
	}

	enable() {
		localStorage.setItem( "pinger", this.pinger.enabled = this.value );
	}

	willUpdate() {
		if ( this.pinger.enabled === this.value ) {
			if ( !this.#timer ) {
				this.ping();
			}
		} else {
			clearTimeout( this.#timer );
			this.#timer = undefined;
		}
	}

	async ping() {
		if ( this.pinger.enabled !== this.value ) {
			return;
		}

		this.#timer = setTimeout( () => this.ping(), 1000 );

		const { pong } = await get( `/ping/${this.value}` );

		if ( pong === this.value ) {
			this.okay++;
		} else {
			this.fail++;
		}
	}
}

customElements.define( "x-pinger", PingerElement );
