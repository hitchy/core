/* eslint-disable jsdoc/require-jsdoc */

import { LitElement, html } from "/lib/lit/dist@3/core/lit-core.min.js";
import { get, patch, post } from "../lib/query.js";
import { AuthenticationMixin } from "../lib/authenticated.js";
import { FormMixin } from "../lib/form.js";
import { addGlobalStylesToShadowRoot } from "../lib/styles.js";
import { TasksMixin } from "../lib/tasks.js";
import { ToastsMixin } from "../lib/toasts.js";

export class TaskEditorElement extends ToastsMixin( FormMixin( TasksMixin( AuthenticationMixin( LitElement ) ) ) ) {
	static properties = {
		uuid: {},
		title: { state: true },
		message: { state: true },
		dueAt: { state: true },
	};

	render() {
		return html`
			<form class="flex row center-across gap-sm wrap stretch" @submit=${this.save}>
				<input placeholder="title" name="title" value=${this.title} @input=${this.value()}>
				<input placeholder="description" name="message" value=${this.message} @input=${this.value()} class="greedy">
				<input placeholder="due date" name="dueAt" value=${this.dueAt} @input=${this.value()} type="date">
				<button type="submit" ?disabled=${!this.title || !this.dueAt}>${this.uuid ? "Update" : "Create"}</button>
				${this.uuid ? html`<button @click="${this.cancel}">X</button>` : undefined}
			</form>
		`;
	}

	createRenderRoot() {
		return addGlobalStylesToShadowRoot( super.createRenderRoot() );
	}

	async connectedCallback() {
		super.connectedCallback();

		if ( this.uuid ) {
			const record = await get( `/api/task/${this.uuid}` );

			this.title = record.title;
			this.message = record.message;
			this.dueAt = new Date( record.dueAt ).toISOString().replace( /T.*$/, "" );
		}
	}

	async save( event ) {
		const form = event.target;

		event.preventDefault();

		if ( this.title && this.dueAt ) {
			const result = await ( this.uuid ? patch : post )( this.uuid ? `/api/task/${this.uuid}` : "/api/task", {
				title: this.title,
				message: this.message,
				dueAt: this.dueAt,
			} );

			if ( result.uuid ) {
				this.title = this.message = "";
				this.dueAt = undefined;
				form.reset();

				this.dispatchEvent( new CustomEvent( "x-editor-close", { bubbles: true, composed: true } ) );

				await this.fetchTasks();
			} else {
				this.errorToast( this.uuid ? "Updating task has failed." : "Creating task has failed." );
			}
		}
	}

	cancel() {
		this.dispatchEvent( new CustomEvent( "x-editor-close", { bubbles: true, composed: true } ) );
	}
}

customElements.define( "x-task-editor", TaskEditorElement );
