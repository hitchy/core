/* eslint-disable jsdoc/require-jsdoc */

import { LitElement, html, css } from "/lib/lit/dist@3/core/lit-core.min.js";
import { AuthenticationMixin } from "../lib/authenticated.js";
import { FormMixin } from "../lib/form.js";
import { addGlobalStylesToShadowRoot } from "../lib/styles.js";
import { TasksMixin } from "../lib/tasks.js";
import { remove } from "../lib/query.js";
import { ToastsMixin } from "../lib/toasts.js";

export class TasksListElement extends ToastsMixin( FormMixin( TasksMixin( AuthenticationMixin( LitElement ) ) ) ) {
	static properties = {
		editingUuid: { state: true },
	};

	static styles = css`
		.grid {
			display: grid;
			grid-template-areas: 
				"title title actions"
				"description description actions"
				"created due -";
			grid-template-rows: 1fr minmax(1fr, auto) 1fr;
			grid-template-columns: 1fr 1fr minmax(0, auto);
			border: 1px solid #00000040;
			padding: 0.5rem;
			border-radius: 5px;
		}
		
		.grid .title {
			grid-area: title;
			font-weight: bold;
		}
		
		.grid .description {
			grid-area: description;
		}
		
		.grid .createdAt {
			grid-area: created;
			font-size: 0.7em;
		}
		
		.grid .dueAt {
			grid-area: due;
			font-size: 0.7em;
		}
		
		.grid .actions {
			grid-area: actions;
		}
	`;

	render() {
		if ( this.tasks?.items?.length > 0 ) {
			const items = this.tasks.items.map( task => {
				if ( task.uuid === this.editingUuid ) {
					return html`
						<x-task-editor uuid="${task.uuid}" @x-editor-close="${this.closeEditor}"></x-task-editor>
					`;
				}

				return html`
					<div class="grid">
						<div class="title">${task.title}</div>
						<div class="description">${task.message}</div>
						<div class="createdAt">
							created at ${isNaN( new Date( task.createdAt ) ) ? html`-` : new Date( task.createdAt ).toISOString()}
						</div>
						<div class="dueAt">
							due at ${isNaN( new Date( task.dueAt ) ) ? html`-` : new Date( task.dueAt ).toDateString()}
						</div>
						<div class="actions">
							<button @click="${() => this.editTask( task.uuid )}">Edit</button>
							<button @click="${() => this.deleteTask( task.uuid )}">Delete</button>
						</div>
					</div>
				`;
			} );

			return html`
				<div class="flex column gap">${items}</div>
			`;
		}

		return html`
			<div class="flex row center no-tasks-message"><i>no tasks exist, yet</i></div>
		`;

	}

	createRenderRoot() {
		return addGlobalStylesToShadowRoot( super.createRenderRoot() );
	}

	editTask( uuid ) {
		if ( !this.editingUuid ) {

			this.editingUuid = uuid;
		}
	}

	async deleteTask( uuid ) {
		const result = await remove( `/api/task/${uuid}` );

		if ( result.error ) {
			this.errorToast( "Removing task has failed." );
		} else {
			await this.fetchTasks();
		}
	}

	closeEditor() {
		this.editingUuid = undefined;
	}
}

customElements.define( "x-tasks", TasksListElement );
