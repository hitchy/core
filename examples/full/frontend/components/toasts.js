/* eslint-disable jsdoc/require-jsdoc */

import { LitElement, html, css } from "/lib/lit/dist@3/core/lit-core.min.js";
import { addGlobalStylesToShadowRoot } from "../lib/styles.js";
import { ToastsMixin } from "../lib/toasts.js";

export class ToastsElement extends ToastsMixin( LitElement ) {
	static styles = css`
		.toasts {
			position: fixed;
			top: 1rem;
			right: 1rem;
			z-index: 100;
		}

		.toast {
			padding: 1rem;
			margin: 1rem;
			background: skyblue;
			border: 1px solid deepskyblue;
			border-radius: 5px;
			cursor: pointer;
		}
		
		.toast.error {
			background: orangered;
			border: 1px solid red;
			color: white;
		}
		
		.toast + .toast {
			margin-top: 1rem;
		}
	`;

	render() {
		const toasts = this.toasts.messages.map( toast => {
			return html`
				<div class="toast ${toast.type}"
				     @click="${event => this.dismissToast( event, toast )}"
				     @dblclick="${event => this.dismissToast( event, toast )}"
				>${toast.message}
				</div>
			`;
		} ).reverse();

		return html`<div class="toasts">${toasts}</div>`;
	}

	createRenderRoot() {
		return addGlobalStylesToShadowRoot( super.createRenderRoot() );
	}

	dismissToast( event, toast ) {
		event.preventDefault();
		event.stopPropagation();
		event.stopImmediatePropagation();

		const index = this.toasts.messages.findIndex( i => i.id === toast.id );

		if ( index > -1 ) {
			const copy = [...this.toasts.messages];
			copy.splice( index, 1 );
			this.toasts.messages = copy.map( c => ( {
				id: c.id,
				type: c.type,
				message: c.message,
			} ) );
		}
	}
}

customElements.define( "x-toasts", ToastsElement );
