import { get } from "./query.js";
import { ContextualMixin } from "./contextual.js";

/**
 * Implements component mixin for managing currently authenticated user.
 *
 * @constructor
 */
export const AuthenticationMixin = superClass => class extends ContextualMixin( superClass ) {
	/** @internal */
	constructor() {
		super( {
			namespace: "authenticated",
			initialize: () => this.fetchUser(),
		} );
	}

	/**
	 * Updates information on currently authenticated user by querying the
	 * backend.
	 *
	 * @returns {Promise<void>} promise settled once the information has been updated
	 */
	async fetchUser() {
		const result = await get( "/api/auth/current" );

		if ( result.success && result.authenticated ) {
			this.authenticated.user = result.authenticated;
		} else {
			this.authenticated.user = false;
		}
	}
};
