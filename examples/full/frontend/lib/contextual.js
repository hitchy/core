/**
 * Implements mixin for components to gain access on a shared context providing
 * reactive data.
 *
 * @constructor
 */
export const ContextualMixin = superClass => class extends superClass {
	/**
	 * Names context to manage.
	 *
	 * it is used to distinguish between multiple independent contexts on the
	 * same container.
	 *
	 * @type {string}
 	 */
	#namespace;

	/**
	 * CSS-selects container element of current component managed context is
	 * managed at.
	 *
	 * @type {string}
	 */
	#rootSelector;

	/**
	 * Names the local property of current component found context's has been
	 * assigned to.
	 *
	 * @type {string}
	 */
	#localName;

	/**
	 * initializes the context's data on first component context is attached to.
	 *
	 * @type {function():void}
	 */
	#initialize;

	/**
	 * Detects if a property's value has actually changed or not.
	 *
	 * Invocation properties are
	 *  - the property's old value
	 *  - the property's new value
	 *  - the sequence of property names leading from the context's data to the
	 *    property compared here
	 *
	 * @type {function(any, any, string[]):boolean}
	 */
	#compareFn;

	/**
	 * @param namespace unique name of context to gain access to
	 * @param rootSelector CSS selector of closest container element this context will be managed at
	 * @param localName name of local property the gathered context's data pool is assigned to
	 * @param initialize custom function invoked on first time the context is attached to a component
	 * @param compareFn custom function used to discover if a property is considered changing or not
	 */
	constructor( { namespace, rootSelector, localName, initialize, compareFn } = {} ) {
		super();

		this.#namespace = namespace;
		this.#rootSelector = rootSelector;
		this.#localName = localName;
		this.#initialize = initialize;
		this.#compareFn = compareFn;
	}

	/**
	 * Traverses up the DOM tree looking for closest container element matching
	 * provided CSS selector.
	 *
	 * @param {string} cssSelector CSS selector of container to find
	 * @param {HTMLElement} startAt element to start checking for containers at
	 * @returns {HTMLElement|undefined} closest container matching provided selector, undefined if none is matching
	 */
	#closestContext( cssSelector, startAt ) {
		const found = startAt.closest( cssSelector );
		if ( found ) {
			return found;
		}

		const root = startAt.getRootNode();
		if ( root === document || !( root instanceof ShadowRoot ) ) {
			return undefined;
		}

		return this.#closestContext( cssSelector, root.host );
	}

	/**
	 * Gains access on container element meant to hold the context to manage.
	 *
	 * @returns {object} found context
	 */
	#getContext() {
		const root = this.#closestContext( this.#rootSelector, this ) || document;

		return root.$context?.[this.#namespace];
	}

	/**
	 * Implements a deep monitor for an object requesting updates of all
	 * connected hosts of provided context whenever any of the properties is
	 * changing.
	 *
	 * @param {string[]} prefix sequence of properties you need to follow to get to the monitored object
	 * @param {object} data object to be monitored for changes
	 * @param {any} context context in which the object is monitored
	 * @returns {object} a proxy exposing the provided object's data and monitoring its properties for changes
	 */
	#monitor( prefix, data, context ) {
		return new Proxy( data, {
			get: ( target, p, receiver ) => {
				if ( target[p] && typeof target[p] === "object" ) {
					return this.#monitor( [ ...prefix, p ], target[p], context );
				}

				return Reflect.get( target, p, receiver );
			},
			set: ( target, p, newValue ) => {
				const changed = this.#compareFn ? this.#compareFn( target[p], newValue, [ ...prefix, p ] ) : target[p] !== newValue;

				target[p] = newValue;

				if ( changed ) {
					for ( const host of context.hosts.values() ) {
						host.requestUpdate();
					}
				}

				return true;
			}
		} );
	}

	/** @internal */
	connectedCallback() {
		super.connectedCallback();

		const root = this.#closestContext( this.#rootSelector, this ) || document;

		if ( !root.$context ) {
			root.$context = {};
		}

		let context;

		if ( root.$context[this.#namespace] ) {
			context = root.$context[this.#namespace];

			this[this.#localName || this.#namespace] = context.data;
		} else {
			context = root.$context[this.#namespace] = {
				hosts: new Set(),
			};

			context.data = this.#monitor( [], {}, context );

			this[this.#localName || this.#namespace] = context.data;

			this.#initialize?.();
		}

		context.hosts.add( this );
	}

	/** @internal */
	disconnectedCallback() {
		super.disconnectedCallback();

		this.#getContext().hosts.delete( this );
	}
};
