/**
 * Implements utilities for form-like components as a mixin.
 *
 * @constructor
 */
export const FormMixin = superClass => class extends superClass {
	/**
	 * Puts value of provided event's target to named property of local
	 * component.
	 *
	 * @param {string} name name of local component's property to write value to, omit to use name of event's target
	 * @param {Event} event event addressing a target to read value from
	 * @returns {function(Event):void} function reading value of given event's to the named property of the component
	 */
	value( name = undefined ) {
		return event => {
			this[name || event.target.name] = String( event.target.value || "" ).trim();
		};
	}
};
