/**
 * Sends GET request to the backend.
 *
 * @param {string} url request URL
 * @returns {Promise<any>} promise settled with response data parsed as JSON
 */
export async function get( url ) {
	const res = await fetch( new URL( url, "http://127.0.0.1:3000" ) );

	return await res.json();
}

/**
 * Sends POST request to the backend.
 *
 * @param {string} url request URL
 * @param {object} data data to include JSON-encoded in request body
 * @returns {Promise<any>} promise settled with response data parsed as JSON
 */
export async function post( url, data ) {
	const res = await fetch( new URL( url, "http://127.0.0.1:3000" ), {
		method: "POST",
		headers: {
			"content-type": "application/json",
		},
		body: JSON.stringify( data ),
	} );

	return await res.json();
}

/**
 * Sends PUT request to the backend.
 *
 * @param {string} url request URL
 * @param {object} data data to include JSON-encoded in request body
 * @returns {Promise<any>} promise settled with response data parsed as JSON
 */
export async function put( url, data ) {
	const res = await fetch( new URL( url, "http://127.0.0.1:3000" ), {
		method: "PUT",
		headers: {
			"content-type": "application/json",
		},
		body: JSON.stringify( data ),
	} );

	return await res.json();
}

/**
 * Sends PATCH request to the backend.
 *
 * @param {string} url request URL
 * @param {object} data data to include JSON-encoded in request body
 * @returns {Promise<any>} promise settled with response data parsed as JSON
 */
export async function patch( url, data ) {
	const res = await fetch( new URL( url, "http://127.0.0.1:3000" ), {
		method: "PATCH",
		headers: {
			"content-type": "application/json",
		},
		body: JSON.stringify( data ),
	} );

	return await res.json();
}

/**
 * Sends DELETE request to the backend.
 *
 * @param {string} url request URL
 * @returns {Promise<any>} promise settled with response data parsed as JSON
 */
export async function remove( url ) {
	const res = await fetch( new URL( url, "http://127.0.0.1:3000" ), {
		method: "DELETE",
	} );

	return await res.json();
}
