/* eslint-disable jsdoc/require-jsdoc */
let cache = null;

export function cloneGlobalStyleSheets() {
	if ( !cache ) {
		cache = Array.from( document.styleSheets )
			.map( stylesheet => {
				const clone = new CSSStyleSheet();
				const css = Array.from( stylesheet.cssRules )
					.map( rule => rule.cssText )
					.join( " " );

				clone.replaceSync( css );

				return clone;
			} );
	}

	return cache;
}

export function addGlobalStylesToShadowRoot( shadowRoot ) {
	shadowRoot.adoptedStyleSheets.push( ...cloneGlobalStyleSheets() );

	return shadowRoot;
}
