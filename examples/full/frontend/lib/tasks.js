import { get } from "./query.js";
import { ContextualMixin } from "./contextual.js";

/**
 * Implements component mixin for managing list of existing tasks.
 *
 * @constructor
 */
export const TasksMixin = superClass => class extends ContextualMixin( superClass ) {
	/** @internal */
	constructor() {
		super( {
			namespace: "tasks",
			initialize: () => this.fetchTasks(),
		} );
	}

	/**
	 * Fetches list of existing tasks from backend.
	 *
	 * @returns {Promise<void>} promise settled once the tasks have been fetched
	 */
	async fetchTasks() {
		const query = { gte: { dueAt: new Date( Math.floor( Date.now() / 86400000 ) * 86400000 ).toISOString() } };
		const result = await get( `/api/task?sortBy=dueAt&query=${JSON.stringify(query)}` );

		if ( Array.isArray( result.items ) ) {
			this.tasks.items = result.items;
		} else {
			this.tasks.items = [];
		}
	}
};
