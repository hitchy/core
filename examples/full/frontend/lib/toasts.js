import { ContextualMixin } from "./contextual.js";

let nextId = 1;

/**
 * Implements component mixin for managing currently authenticated user.
 *
 * @constructor
 */
export const ToastsMixin = superClass => class extends ContextualMixin( superClass ) {
	/** @internal */
	constructor() {
		super( {
			namespace: "toasts",
			initialize: () => {
				this.toasts.messages = [];
			},
		} );
	}

	/**
	 * Enqueues error toast to display.
	 */
	errorToast( message ) {
		this.toasts.messages = [ ...this.toasts.messages.map( t => ( {
			id: t.id,
			type: t.type,
			message: t.message,
		} ) ), {
			id: nextId++, type: "error", message,
		} ];
	}
};
