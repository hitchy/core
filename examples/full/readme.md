# Full-feature example hitchy application

This folder contains an example web application based on Hitchy covering most official plugins available for Hitchy today. It is used to evaluate Hitchy in a series of E2E tests.

## Overview

* Folder **api** contains all the backend components.
* Folder **config** consists of files customizing the backend's runtime configuration.
* Folder **frontend** contains a static implementation of a simple frontend based on [lit](https://lit.dev).
* Files **initialize.js** and **shutdown.js** are used to set up and tear down a separate HTTP service suitable for testing Hitchy's reverse proxying capabilities.

## Configuration

The backend has been configured

* to persistently manage data in a local subfolder named **data**.
* to expose all files in folder **frontend**.
* to apply different kinds of rate limiting to a custom endpoint.
* to forward certain requests to a remote CDN.
