export default {
	lang: "en",
	base: "/",
	outDir: "../../public",
	title: "Hitchy Core Manual",
	head: [
		[ "link", {
			rel: "icon",
			href: "/favicon.svg",
			type: "image/svg"
		} ],
	],
	themeConfig: {
		sidebar: "auto",
		displayAllHeaders: true,
		repo: "https://gitlab.com/hitchy/core",
		repoLabel: "Contribute!",
		outline: {
			level: [2,4]
		},
		footer: {
			copyright: 'created by <a target="_blank" href="https://cepharum.de/en/">cepharum GmbH</a>',
			message: '<a target="_blank" href="https://cepharum.de/en/imprint.html">Imprint</a>'
		},
		nav: [
			{ text: "Home", link: "/" },
			{ text: "Tutorials", link: "/tutorials/" },
			{ text: "API", items: [
					{ text: "Hitchy's API", link: "/api/hitchy" },
					{ text: "Plugins API", link: "/api/plugins" },
					{ text: "Core Components", link: "/api/components/" },
			] },
			{ text: "Internals", items: [
					{ text: "Architecture", link: "/internals/architecture-basics.html" },
					{ text: "Components", link: "/internals/components.html" },
					{ text: "Patterns", link: "/internals/patterns.html" },
					{ text: "Plugins", link: "/internals/bootstrap.html" },
					{ text: "Routing", link: "/internals/routing-basics.html" },
			] },
			{ text: "Hitchy", items: [
					{ text: "Core", link: "/" },
					{ text: "Plugins", items: [
							{ text: "Odem", link: "https://odem.hitchy.org/" },
							{ text: "Auth", link: "https://auth.hitchy.org/" },
						] },
					{ text: "Tools", items: [
							{ text: "SDT", link: "https://sdt.hitchy.org/" },
						] }
				] },
		],
	},
}
