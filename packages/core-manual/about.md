---
sidebar: false
---

# About

Hitchy is a server-side application framework written in Javascript. In its current state, it is designed to work with Node.js.

The framework starts lean and relies on [plugins](plugins) extending its rather scarce but powerful set of core features:

* [integrate with a service capable of receiving requests](internals/architecture-basics.html#integrating-with-services)
* [discover plugins](internals/architecture-basics.md#discovering-plugins)
* [dispatch incoming requests based on a routing configuration](internals/routing-basics)

This design of Hitchy is in compliance with our [basic design principles](principles).


## Why "Hitchy"?

We've started developing this framework while using SailsJS for creating applications. We basically were great fans of ExpressJS back then. But SailsJS was not quite as convincing.

That's why we started developing our own framework to provide what was missing in ExpressJS and make it integrate as a middleware there. We considered this middleware to hitch itself on to ExpressJS. That's why we called it _hitchy_. Aside from that this name was sticking and available.

Even today, Hitchy works great on top of ExpressJS. However, it is also great on its own. That's why we usually run it without another framework involved.


