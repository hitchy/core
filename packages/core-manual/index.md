---
layout: home

hero:
  name: Hitchy
  text: extensible server-side application framework
  tagline: Create lightweight backends with Javascript for rich web applications.
  image:
    src: /logo-reduced.svg
    alt: Hitchy logo
  actions:
    - theme: brand
      text: About
      link: about.md
    - theme: brand
      text: Getting started
      link: tutorials/
    - theme: brand
      text: API reference
      link: api/hitchy.md
    - theme: brand
      text: Routing basics
      link: internals/routing-basics.md
---

### MIT License

Copyright &copy; 2017 cepharum GmbH

:::details Click to see the full text ...
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
:::
