# Hitchy Plugins

This is a moderated list of currently existing plugins for Hitchy:

* [**@hitchy/plugin-auth**](https://www.npmjs.com/package/@hitchy/plugin-auth) - adds support for user authentication and request authorization incl. OIDC and SAML
* [**@hitchy/plugin-cookies**](https://www.npmjs.com/package/@hitchy/plugin-cookies) - simplified cookie handling
* [**@hitchy/plugin-odem**](https://www.npmjs.com/package/@hitchy/plugin-odem) - a document-oriented database system
* [**@hitchy/plugin-odem-etcd**](https://www.npmjs.com/package/@hitchy/plugin-odem-etcd) - an adapter for storing data in an etcd cluster
* [**@hitchy/plugin-odem-rest**](https://www.npmjs.com/package/@hitchy/plugin-odem-rest) - a REST API for accessing data managed by @hitchy/plugin-odem
* [**@hitchy/plugin-odem-socket.io**](https://www.npmjs.com/package/@hitchy/plugin-odem-socket.io) - accessing data managed by @hitchy/plugin-odem via websocket 
* [**@hitchy/plugin-proxy**](https://www.npmjs.com/package/@hitchy/plugin-proxy) - reverse proxy transparently forwarding requests to separate/remote service
* [**@hitchy/plugin-rate-limiter**](https://www.npmjs.com/package/@hitchy/plugin-rate-limiter) - generates customizable policy routing handlers for limiting request rates
* [**@hitchy/plugin-session**](https://www.npmjs.com/package/@hitchy/plugin-session) - server-side session support
* [**@hitchy/plugin-socket.io**](https://www.npmjs.com/package/@hitchy/plugin-socket.io) - integrates [socket.io](https://socket.io/) with Hitchy incl. request dispatching via websocket
* [**@hitchy/plugin-static**](https://www.npmjs.com/package/@hitchy/plugin-static) - publishing static files in a folder

## Writing Plugins

See the description of [Hitchy's powerful plugin API](api/plugins.md) for integrating plugins to learn how to write your own plugins.
