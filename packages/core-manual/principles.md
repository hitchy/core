# Hitchy design principles

Hitchy is meant to comply with many common paradigms such as [DRY](https://de.wikipedia.org/wiki/Don%E2%80%99t_repeat_yourself) and [KISS](https://en.wikipedia.org/wiki/KISS_principle). In addition Hitchy developers are asked to comply with the following set of custom principles and paradigms as good as possible:


## Convention over configuration

In Hitchy there are many conventions simplifying day to day software development when obeyed. And many features are not meant to depend on some configuration. So, do not try to bend Hitchy e.g. for matching a naming scheme or project hierarchy you prefer but accept conventions laid out by Hitchy and coding will be fun for sure.

For example, features implemented in plugins usually work simply by adding either plugin as a dependency to your project.

## Keep it lean!

npm offers a huge amount of ready-to-use packages and most of them tend to rely on this offer by depending on more packages. This quite often results in tons of packages fetched from npm for running your application even when it is kept rather simple.

We fight this discrepancy by preventing use of dependencies for getting some feature that might take just 10-20 lines of code. Omit dependencies unless they make sufficient sense and offer substantial benefits. There are some rules for guiding:

* Always have a look into either used dependency. 
* Assess the code yourself.
* Try relying on popular and well-established dependencies, only.
* Always check the dependencies of either dependency as well.
* Avoid stale dependencies.
* Decide if it's worth adding tens, hundreds or even thousands of new files just to get some feature that requires some tens of lines of code.


## Speak promises! 

Using _natively supported_ promises is highly beneficial over using callbacks with regards to handling asynchronous processing and related error handling. Hitchy does not try to support different ways of handling asynchronous processing but sticks with promises by convention to keep the code as simple as possible.


## Performance over code simplicity

This principle appears to contradict others listed here for sure. However, Hitchy is implemented in Javascript and there are no tools transforming easy-to-read code into highly performing code. Thus, authoring Hitchy is preferring better performance over simpler code.

We stick to these rules:

* Work synchronously as long as possible.
* Take a local copy of some expensively accessible information.

  ```javascript
  // BAD
  for ( let i = 0; i < array.length; i++ ) { ... }

  // BETTER
  for ( let i = 0, num = array.length; i < num; i++ ) { ... }

  // BAD
  some.data[x].result = fn( some.data[x].info, some.data[x].additional );

  // BETTER
  const ref = some.data[x];
  ref.result = fn( ref.info, ref.additional );
  ```

* In addition to that, use references on repeatedly working with nested information.

  ```javascript
  const ref = some.data[x];
  ref.result = fn( ref.info, ref.additional );
  ```

* Avoid helper methods with callbacks when processing potentially huge sets of data.

  ```javascript
  // BAD
  const result = hugeArrayOfIntegers
      .filter( i => i > 0 )
      .map( i => `prefix-${i}` );
  
  // BETTER
  const count = hugeArrayOfIntegers.length;
  const result = new Array( count );
  let write = 0;

  for ( let read = 0; read < count; read++ ) {  
    const item = hugeArrayOfIntegers[read];

    if ( item > 0 ) {
      result[write++] = `prefix-${item}`;
    }
  }
  
  result.splice( write );
  ```

* Always work asynchronously when starting some action that might block even for a moment.
* Embrace streams to limit consumption of memory.
* Always keep an eye on what might happen under the hood of Javascript engine.
* Use [JSPerf](https://jsperf.app) or a similar tool for comparing different approaches:
  * [iterating arrays](https://jsperf.app/yodapo)
  * [iterating objects](https://jsperf.app/nuwosi/2)
  * [arrays vs. collections](https://jsperf.app/hizaru)
