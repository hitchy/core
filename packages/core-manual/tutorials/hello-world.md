---
next: advanced-routing.md
---

# Tutorial: Hello World!

## Prerequisites

Hitchy is implemented in Javascript and requires [Node.js](https://nodejs.org/) as a runtime. In addition, it relies on a tool called [npm](https://www.npmjs.com/) which is used to access a vast amount of ready-to-use packages. Hitchy is just one of those. **npm** is included with Node.js.

## Create Project

* Create and enter new folder for your project:

  ```bash
  mkdir hello-world && cd hello-world
  ```

* Initialize the project:

  ```bash
  npm init
  ```

* Install Hitchy:

  ```bash
  npm i @hitchy/core
  ```

## Configure Router

Create a sub-folder named **config**. Put a file named **routes.js** with the following content there:

:::code-group
```javascript [config/routes.js]
exports.routes = {
	"/": ( req, res ) => res.send( "Hello World!" ),
};
```
:::

::: warning Important  
The file's name does not matter much. The key `routes` used for exporting in first line of file's content is essential though. As a convention, we suggest to name the file just like the exported configuration key to support long-term maintenance of code.  
:::

:::warning CommonJS or ES module?
Examples in this tutorial use CommonJS module syntax as this is the created project's default.

Hitchy is capable of dealing with either kind of project out of the box. It also recognizes files with extensions **.cjs** and **.mjs** to explicitly contain either a CommonJS module or an ES module regardless of the containing project's type.
:::


## Run Project

When in project folder enter 

```sh
hitchy start
``` 

for running the project. This will display some URL to be opened in a web browser, like [http://127.0.0.1:3000](http://127.0.0.1:3000). Click on the URL or copy-n-paste it into your browser to get the desired output.

:::tip Stopping Hitchy
After starting hitchy the service is running in foreground. Log messages are printed on screen. If you want to stop hitchy just press Ctrl+C. This will gracefully shut down Hitchy.
:::

### Dockerize Your Project

For production setup, we strongly suggest to put your project in a [Docker](https://docs.docker.com/engine/reference/commandline/cli/) container.

1. Create a file named **Dockerfile** in root folder of your project containing:

   :::code-group
   ```dockerfile [Dockerfile]
   FROM node:lts-alpine
   COPY . /app
   RUN npm ci
   CMD ["npm", "run", "start:docker"]
   EXPOSE 3000
   ```
   :::

2. Create another file named **.dockerignore** in same folder with content:

   ```
   node_modules
   ```
   
   This improves build time of the Docker image and prevents potential issues at runtime.

3. Open your project's **package.json** file. Look for the `scripts` section and add script `start:docker` as demonstrated:

   :::code-group
   ```json [package.json]
   {
     ...
     "scripts": {
       ...
       "start:docker": "hitchy start --ip=0.0.0.0"
     }
     ...
   }
   ```
   :::
   
4. Build the image to use on creating containers:

   ```bash
   docker build -t name-of-your-project .
   ```

5. **Optional:** Publish your project by pushing the created image to a remote registry of Docker images:

   ```bash
   docker push name-of-your-project
   ```

After that you may run your service at any machine with docker engine installed. Create and run a container with:

```bash
docker run -d --restart always -p 3000:3000 name-of-your-project
```

