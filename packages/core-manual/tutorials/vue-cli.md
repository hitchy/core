# Combining Hitchy with Vue v2

## Prerequisites

First, a local installation of [Node.js](https://nodejs.org/en/) is required.

Vue development probably depends on packages that require additional installation of [Python](https://www.python.org/downloads/) on your computer. Current versions of Node.js installer offer installation of additionally required build tools. When accepting that offer it will cover Python installation. 

:::tip Python Required?
This step is basically optional as long as there are no dependencies that rely on binary libraries to be compiled on your computer. You may control this by disabling certain features such as CSS pre-processors on creating your Vue application in next section.
:::

Vue comes with a CLI that must be installed globally using `npm` which in turn has been installed as part of Node.js. 

```bash
npm install -g @vue/cli
```

In addition, we suggest installing [git](https://git-scm.com/) and [docker](https://www.docker.com/) on your computer.

## Create a Vue Project

:::tip Vue Project Exists Already? 
Just skip this part!
:::

Start with creating a Vue project using Vue CLI:

```bash
vue create myapp
```

This will ask questions about features and tools you like to use with your application before creating a new sub-folder named like your app: **myapp**. This process may take a while.

Enter that folder:

```bash
cd myapp
```

Open file **.gitignore** and append these lines:

:::code-group
``` [.gitignore]
data
server/public
server/config/local.js
```
:::

## Add Hitchy

Hitchy will be integrated with your Vue application. Its code is going to reside
in sub-folder named **server**:

```bash
mkdir server
```

Additional dependencies are required:

```bash
npm install @hitchy/core @hitchy/plugin-static @hitchy/plugin-odem @hitchy/plugin-odem-rest
npm install -D @hitchy/plugin-proxy
```

* **@hitchy/core** is the core of Hitchy capable of handling requests and
  processing routing tables.
* **@hitchy/plugin-static** is extending Hitchy for statically exposing files in
  a local folder. It will be used in a production setup for serving files of
  your application as built by Vue.
* **@hitchy/plugin-proxy** is another extension to Hitchy implementing a basic
  reverse proxy. It is used to expose Vue's development server instead of the
  built files during development.
* **@hitchy/plugin-odem** is rather optional here. As an extension to Hitchy, it
  is implementing a document-oriented database for managing data on server-side.
* **@hitchy/plugin-odem-rest** is an addition to plugin-odem exposing that one's
  models over REST API without any extra line of code.

Adjust the file **package.json** for adding scripts named `start` and `start:dev` in `scripts`:

:::code-group
```json [package.json]
{
	...
	"scripts": {
		...
		"start": "hitchy --project=server --plugins=. --ip=0.0.0.0",
		"start:dev": "hitchy --project=server --plugins=. --ip=0.0.0.0 --development --debug"
	},
	...
}
```
:::

## Add Code Quality Tools

We suggest using these tools for testing your code:

```bash
npm install -D eslint-plugin-promise eslint-config-cepharum mocha should @hitchy/server-dev-tools
```

You should create a file named **server/.eslintrc** containing:

:::code-group
```json [server/.eslintrc]
{
	"extends": [
		"eslint-config-cepharum"
	]
}
```
:::

Optionally add scripts named `server:test` and `server:lint` in **package.json** file:

:::code-group
```json [package.json]
{
	...
	"scripts": {
		...
		"server:lint": "eslint server",
		"server:test": "mocha --ui tdd --recursive 'server/test/**/*.spec.js'"
	},
	...
}
```
:::

## Wrap Vue in Hitchy

Hitchy is meant to deliver output of Vue. 

### Development Setup

When developing Vue applications, there is a local server presenting current state of your application as a live preview. It is started with

```bash
npm run serve
```

You must start this server in addition to Hitchy yourself.

Create a file named **myapp/server/config/proxy.js** containing:

:::code-group
```javascript [myapp/server/config/proxy.js]
export default function (options) {
	return {
		proxy: options.arguments.development ? [{
			prefix: "/",
			target: "http://127.0.0.1:8080/"
		}] : [],
	};
}
```
:::

:::tip Remarks
This configuration file [complies with a special pattern](../internals/patterns.md#common-module-pattern) to configure server in different ways depending on [argument](../api/hitchy.md#options-arguments) `--development` provided on command line or not.
:::

:::warning Different syntax
Examples in this tutorial use ES module syntax based on the assumption that the Vue project created in the beginning [has been marked accordingly](https://nodejs.org/docs/latest/api/packages.html#type).

Hitchy is capable of dealing with either kind of project by default. It also recognizes files with extensions **.cjs** and **.mjs** to explicitly contain either a CommonJS module or an ES module regardless of the containing project's type.
:::

This file is setting up reverse proxy plugin to forward all requests that do not match more specific routes declared in Hitchy to that development server of Vue.

### Production Setup

In production setup, there are static files built with Vue, only. Hitchy is required to serve those instead of some local development server's output. 

Create a file **myapp/server/config/static.js** containing

:::code-group
```javascript [myapp/server/config/static.js]
export default function( options ) {
	return {
		static: options.arguments.development ? [] : [{
			prefix: "/",
			folder: "public",
			fallback: "index.html"
		}],
	};
}
```
:::

This causes Hitchy to statically expose all files in the folder **myapp/server/public**.

Edit file named **myapp/vue.config.js** to customize option **outputDir** so that Vue is putting built files of application into the desired folder. Create the file if it's missing.

:::code-group
```javascript [myapp/vue.config.js]
export default {
  lintOnSave: false,
  outputDir: "server/public"
}
```
:::

## Test Your Setup

### Development Setup

Invoke

```bash
npm run serve
```

for starting development server of Vue and

```bash
npm run start:dev
```

for starting Hitchy. Observe the latter script's output for picking URL to open in a browser. This should present home screen of Vue skeleton created before.

### Production Setup

Build application files with

```bash
npm run build
```

On exit, build output files have been written to folder **myapp/server/public**. 

This time, start Hitchy by invoking

```bash
npm run start
```

Just like in development setup, open URL presented after a few moments in your browser. It will display the same home screen of Vue as before.


## Customize Hitchy

### Use persistent data backend

For Hitchy's document-oriented database persistently saving data you need to set up an adapter. By default, it is using an in-memory backend, thus loosing all
data on restarting the server.

Create a file **myapp/server/config/database.js** with following content:

:::code-group
```javascript [myapp/server/config/database.js]
import Path from "node:path";
import { fileURLToPath } from "node:url";

export default function() {
	return {
		database: {
			default: new this.services.OdemAdapterFile( {
				dataSource: Path.resolve( fileURLToPath( import.meta.url ), "../../../data" ),
			} ),
		},
	};
}
```
:::

This is selecting another backend included with the @hitchy/plugin-odem plugin for storing
data in the local file system. The configured folder will be **myapp/data** and
that's why it has been listed in **.gitignore** file created earlier.

:::warning Production Use  
Using this backend might be an option for production setup, as well.

* You should not use it if you expect a huge number of records and/or a lot of requests accessing them. 

* You can not use it on running single instance of your application in a cluster unless it has reliable and exclusive access on a particular filesystem at any point in time (like a network share or similar). 

* Eventually, you can not use it on running multiple instances of your application in a cluster even though all instances are accessing the same set of files. 

Please refer to [@hitchy/plugin-odem-etcd](https://www.npmjs.com/package/@hitchy/plugin-odem-etcd) for using an etcd cluster instead of local files.  
:::

