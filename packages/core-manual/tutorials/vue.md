# Getting Started: Hitchy & Vue

Hitchy as a framework is suitable for implementing a server-side business logic.
Even though you could also have some server-side rendering of views,
implementing separate client-side application is rather common nowadays.

In this tutorial we'll combine Hitchy with [Vue](https://vuejs.org/) to achieve
that.

:::tip Benefits
By combining Hitchy with a client-side framework such as [Vue](https://vuejs.org/), your project is reducing load on server by moving GUI logics to the client. This is beneficial with regard to handling lots of requests on server side and to implementing stateful GUIs e.g. by managing cookies or similar. Your project will run faster even on smaller servers.
:::

With the advent of Vue v3 and its preferred build tool Vite, there are two ways to set up a project combining Hitchy and Vue:

* [Hitchy and Vue v2](vue-cli.md)
* [Hitchy and Vue v3](vue-vite.md)
