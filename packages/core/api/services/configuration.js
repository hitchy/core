import File from "node:fs";
import Path from "node:path";

export default function( options ) { // eslint-disable-line jsdoc/require-jsdoc
	const api = this;

	const logDebug = api.log( "hitchy:bootstrap:debug" );
	const logInfo = api.log( "hitchy:bootstrap:info" );

	/**
	 * Implements code for gathering configuration of current runtime.
	 */
	class Configuration {
		/**
		 * Asks every discovered plugin for its configuration.
		 *
		 * @param {Hitchy.Core.PluginHandle[]} plugins descriptions of discovered plugins
		 * @returns {Promise<void>} promise for having configured core, all discovered plugins and the application
		 * @private
		 */
		static async gather( plugins ) {
			// read configuration files of every discovered plugin
			for ( let i = 0; i < plugins.length; i++ ) {
				const plugin = plugins[i];

				logDebug( "* checking configuration of plugin %s", plugin.name );

				const folder = Path.resolve( plugin.folder, "config" );
				const config = await this._readConfigurationFiles( folder, true ); // eslint-disable-line no-await-in-loop

				plugin.config = plugin.api.$config = config;
			}

			// read configuration files of current application
			logDebug( "* checking configuration of current application" );

			const folder = Path.resolve( options.projectFolder, "config" );
			const config = await this._readConfigurationFiles( folder, false );

			Object.defineProperties( api.config, {
				/**
				 * Silently injects application's individual configuration
				 * into resulting global configuration object for use
				 * by other stages of bootstrap like exposure.
				 *
				 * @private
				 */
				$appConfig: { value: config },
			} );

			// invoke configure() handler optionally exposed by either plugin's API
			for ( let i = 0; i < plugins.length; i++ ) {
				const plugin = plugins[i];
				const fn = plugin.api.configure;

				if ( typeof fn === "function" ) {
					await fn.call( api, options, plugin ); // eslint-disable-line no-await-in-loop
				}
			}
		}

		/**
		 * Reads all files with extension ".js" found in provided folder and injects
		 * their exported API into api.config.
		 *
		 * If any configuration file is exporting function instead of object this
		 * function is invoked according to the pattern used to load plugins etc.
		 * in hitchy expecting function to return object to be injected actually. On
		 * returning Promise injection is delayed until promise is resolved.
		 *
		 * @param {string} configFolder path name of folder containing files to be read
		 * @param {boolean} isPlugin true if provided folder is part of some discovered plugin
		 * @returns {Promise<object>} promises configuration read and merged from all files in selected folder
		 * @private
		 */
		static async _readConfigurationFiles( configFolder, isPlugin ) {
			const compiledConfiguration = {};
			let files;

			// shallowly get names of all files in provided folder
			try {
				files = await File.promises.readdir( configFolder, { withFileTypes: true } );
			} catch ( cause ) {
				if ( cause.code === "ENOENT" ) {
					// there is no configuration folder -> skip
					if ( !isPlugin ) {
						logInfo( "missing configuration folder, thus using empty configuration" );
					}

					return compiledConfiguration;
				}

				throw cause;
			}

			// collect names of actual configuration files, only
			const filtered = new Array( files.length );
			let write = 0;

			for ( let i = 0; i < files.length; i++ ) {
				const file = files[i];

				if ( file.isDirectory() ) {
					continue;
				}

				const fileName = file.name;

				if ( !fileName || fileName[0] === "." ) {
					continue;
				}

				if ( fileName.endsWith( ".js" ) ) {
					filtered[write++] = {
						name: fileName.slice( 0, -3 ),
						withExtension: fileName,
					};
				} else if ( fileName.endsWith( ".cjs" ) ) {
					filtered[write++] = {
						name: fileName.slice( 0, -4 ),
						withExtension: fileName,
					};
				} else if ( fileName.endsWith( ".mjs" ) ) {
					filtered[write++] = {
						name: fileName.slice( 0, -4 ),
						withExtension: fileName,
					};
				}
			}

			filtered.splice( write );

			// sort all configuration files by name
			filtered.sort( ( l, r ) => l.name.localeCompare( r.name ) );

			// make sure any config/local.js is processed second to last
			const localIndex = filtered.findIndex( i => i.name === "local" );
			if ( localIndex > -1 ) {
				filtered.push( filtered.splice( localIndex, 1 )[0] );
			}

			// make sure any config/final.js is processed last
			const finalIndex = filtered.indexOf( "final" );
			if ( finalIndex > -1 ) {
				filtered.push( filtered.splice( finalIndex, 1 )[0] );
			}

			const strategyFn = ( segments, fallback ) => this.strategySelector( segments, fallback );

			// successively read discovered files and merge what they expose
			for ( let i = 0; i < filtered.length; i++ ) {
				const { withExtension } = filtered[i];
				const fullName = Path.resolve( configFolder, withExtension );

				logDebug( "  - reading file config/%s", withExtension );

				const config = await api.cmp( fullName, compiledConfiguration ); // eslint-disable-line no-await-in-loop

				api.utility.object.merge( compiledConfiguration, config || {}, strategyFn );
			}

			// eventually merge this folder's configuration into the global configuration, too
			api.utility.object.merge( api.config, compiledConfiguration, strategyFn );

			return compiledConfiguration;
		}

		/**
		 * Selects strategy for deeply merging configuration objects.
		 *
		 * This callback is used to exclusively concatenate lists of targets
		 * assigned to the same combination of HTTP method and URL prefix in
		 * policy routing definitions by different plugins and the application.
		 *
		 * In all other cases of defining some configuration, different sources
		 * should blend/replace each other when using arrays.
		 *
		 * @param {string[]} segments breadcrumb of property names leading to current property to be merged
		 * @param {string} strategy default strategy for merging selected property based on its values to merge
		 * @returns {"blend"|"concat"|"keep"|"replace"} strategy to use
		 */
		static strategySelector( segments, strategy ) {
			const numSegments = segments.length;
			if ( numSegments < 2 ) {
				return strategy;
			}

			const [ major, minor ] = segments;
			const isPolicy = major === "policies";

			if ( isPolicy ) {
				switch ( minor ) {
					case "early" :
					case "before" :
					case "after" :
					case "late" :
						if ( numSegments === 3 ) {
							// this case is about merging lists of targets
							// assigned to same routing source in a policy
							// routing slot
							return "concat";
						}
				}
			}

			return strategy;
		}
	}

	return Configuration;
}
