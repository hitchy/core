import FS from "node:fs/promises";
import { resolve } from "node:path";
import { parseDotEnv, resolveVariables } from "../../lib/misc/dotenv.js";

/**
 * Implements service Dotenv providing API for conveniently considering .env
 * file in current project's base folder.
 */
export default function( options ) {
	let read;

	return {
		/**
		 * Reads content of selected file and parses its .env-like syntax.
		 *
		 * @param {string} filename name of file to read
		 * @param {Object<string,string>} mergeWith existing set of variables to merge parsed content of read file with
		 * @returns {Promise<Object<string,string>>} promise for content of named file being read, parsed and optionally merged with provided values
		 */
		async readFile( filename, mergeWith = undefined ) {
			try {
				return parseDotEnv( await FS.readFile( filename, "utf-8" ), ( local, name, value ) => {
					return resolveVariables( value, local, mergeWith );
				} );
			} catch ( cause ) {
				if ( cause.code !== "ENOENT" ) {
					throw cause;
				}

				return {};
			}
		},

		/**
		 * Reads .env file of local project and merges its definitions with
		 * existing environment variables of current process.
		 *
		 * @returns {Promise<Object<string, string>>} promise for content of local .env file merged with environment variables
		 */
		read() {
			read ??= this.readFile( resolve( options.projectFolder, ".env" ), options.environment || {} );

			return read;
		}
	};
}
