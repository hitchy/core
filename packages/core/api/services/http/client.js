import Http from "node:http";
import Https from "node:https";
import { Readable } from "node:stream";

/**
 * @typedef {IncomingMessage} ClientResponse
 * @property {function():Promise<Buffer>} body fetches body of response
 * @property {function():Promise<object>} json fetches body of response as JSON and provides parsed object
 */

export default function() { // eslint-disable-line jsdoc/require-jsdoc
	const api = this;

	const logDebug = api.log( "hitchy:http-client:debug" );
	const logError = api.log( "hitchy:http-client:error" );

	const agents = {
		http: new Http.Agent( {
			keepAlive: true,
			maxFreeSockets: 10,
		} ),
		https: new Https.Agent( {
			keepAlive: true,
			maxFreeSockets: 10,
		} ),
	};

	/**
	 * Implements client for conveniently sending HTTP(S) requests.
	 *
	 * @alias that.service.HttpClient
	 */
	class HttpClient {
		/**
		 * Fetches resource via HTTP(S).
		 *
		 * @param {string} method HTTP request method, e.g. "GET"
		 * @param {string|URL} url URL to fetch
		 * @param {string|object|ReadableStream} body request payload to be transmitted
		 * @param {Object<string,*>} headers custom request headers
		 * @param {int} timeout timeout in seconds
		 * @returns {Promise<ClientResponse>} promises available response
		 */
		static fetch( method, url, body = null, headers = {}, { timeout = Infinity } = {} ) {
			const start = Date.now();
			const _headers = Object.assign( {}, headers );

			logDebug( `${method} ${url}` );

			return new Promise( ( resolve, reject ) => {
				const _url = url instanceof URL ? url : new URL( url );
				let resendDelay = 250;

				if ( timeout < Infinity ) {
					setTimeout( timeoutHandler, ( timeout * 1000 ) + 1000 );
				}

				sendRequest();

				/**
				 * Rejects promise with error due to running into a timeout.
				 *
				 * @returns {void}
				 */
				function timeoutHandler() {
					reject( new Error( `request for ${_url} timed out after ${Date.now() - start} ms` ) );
				}

				/**
				 * Attempts to send request.
				 *
				 * @returns {void}
				 */
				function sendRequest() {
					const hasContentType = Object.keys( _headers ).some( name => name.toLowerCase() === "content-type" );
					if ( !hasContentType && body && body.constructor === Object ) {
						_headers["content-type"] = "application/json; charset=UTF-8";
					}

					const protocolName = _url.protocol === "https:" ? "https" : "http";
					const Protocol = protocolName === "https" ? Https : Http;
					const client = Protocol.request( _url, {
						method,
						headers: _headers,
						timeout: 2000,
						agent: agents[protocolName],
					} );

					if ( timeout < Infinity ) {
						client.setTimeout( timeout * 1000 );
						client.once( "timeout", timeoutHandler );
					}

					client.once( "error", error => {
						switch ( error.code ) {
							case "ECONNREFUSED" :
							case "ENOTFOUND" :
							case "EAI_AGAIN" :
								if ( resendDelay > 0 ) {
									logDebug( `connection failed due to ${error.code} ... retrying in ${resendDelay}ms` );

									setTimeout( sendRequest, resendDelay );
									resendDelay += Math.min( resendDelay, 5000 );
									break;
								}

							// falls through
							default :
								reject( error );
						}
					} );

					client.once( "response", response => {
						let _bodyFetcher = null;

						response.body = () => {
							if ( !_bodyFetcher ) {
								if ( timeout < Infinity ) {
									response.setTimeout( timeout * 1000 );

									response.once( "timeout", () => {
										const msg = `response from ${_url} timed out after ${Date.now() - start} ms`;
										reject( Object.assign( new Error( msg ), { code: "ETIMEDOUT" } ) );
									} );
								}

								_bodyFetcher = new Promise( ( _resolve, _reject ) => {
									const chunks = [];

									response.once( "error", _reject );
									response.on( "data", chunk => chunks.push( chunk ) );
									response.once( "end", () => _resolve( Buffer.concat( chunks ) ) );
								} );
							}

							return _bodyFetcher;
						};

						response.json = () => {
							return response.body().then( buffer => JSON.parse( buffer.toString( "utf8" ) ) );
						};

						resolve( response );
					} );

					if ( body ) {
						if ( body instanceof Readable ) {
							resendDelay = NaN;
							body.pipe( client );
						} else if ( body instanceof Buffer ) {
							client.end( body );
						} else if ( typeof body === "object" && !Array.isArray( body ) ) {
							client.end( JSON.stringify( body ) );
						} else if ( typeof body === "string" ) {
							client.end( body, "utf8" );
						} else {
							client.end();
						}
					} else {
						client.end();
					}
				}
			} )
				.then( response => {
					logDebug( `${method} ${url} -> ${response.statusCode}` );

					return response;
				} )
				.catch( error => {
					logError( error.message );

					throw error;
				} );
		}
	}

	return HttpClient;
}

export const appendFolders = false;
