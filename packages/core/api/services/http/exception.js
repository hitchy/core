export default function() { // eslint-disable-line jsdoc/require-jsdoc
	/**
	 * Implements exception associated with an HTTP status code.
	 */
	class HttpException extends Error {
		/**
		 * @param {int} status HTTP status code
		 * @param {string} message description of issue
		 * @param {string} code errno-compatible code
		 */
		constructor( status, message, code = null ) {
			super( message, code );

			Object.defineProperties( this, {
				statusCode: { value: parseInt( status ) || 500 },
			} );
		}

		/**
		 * Converts provided exception into object e.g. for sending to HTTP
		 * client in a response's body.
		 *
		 * @returns {object} representation of exception as object
		 */
		toResponseJSON() {
			return {
				error: this.message,
			};
		}
	}

	return HttpException;
}

export const appendFolders = false;
