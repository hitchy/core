import { PassThrough } from "node:stream";

export default function() { // eslint-disable-line jsdoc/require-jsdoc
	const api = this;

	/**
	 * Implements client to create and locally dispatch request.
	 */
	class LocalClientRequest extends PassThrough {
		static FakeServiceSocket = {
			port: 8080,
			family: "IPv4",
			address: "127.0.0.1",
		};

		static FakeClientSocket = {
			port: 12345,
			family: "IPv4",
			address: "127.0.0.1",
		};

		/**
		 * @param {string} url URL path and query to request
		 * @param {string} method name of request method to simulate
		 * @param {Object<string,string>} headers request headers
		 */
		constructor( { url, method = "GET", headers = {} } = {} ) {
			if ( !url || typeof url !== "string" || !/^\/\S+$/.test( url ) ) {
				throw new TypeError( "invalid request path" );
			}

			super();

			const normalizedHeaders = {};
			const rawHeaders = [];

			Object.keys( headers ).forEach( name => {
				normalizedHeaders[name.toLowerCase()] = headers[name];

				rawHeaders.push( name );
				rawHeaders.push( headers[name] );
			} );

			const { FakeServiceSocket: localSocket, FakeClientSocket: peerSocket } = this.constructor;

			Object.defineProperties( this, {
				/**
				 * Indicates current request being dispatched local, only.
				 *
				 * @name LocalClientRequest#internal
				 * @property {boolean}
				 * @readonly
				 */
				internal: { value: true, enumerable: true },

				/**
				 * Provides request URL path and query.
				 *
				 * @name LocalClientRequest#url
				 * @property {string}
				 * @readonly
				 */
				url: { value: url, enumerable: true },

				/**
				 * Provides request method.
				 *
				 * @name LocalClientRequest#method
				 * @property {string}
				 * @readonly
				 */
				method: { value: method || "GET", enumerable: true },

				/**
				 * Provides fake list of raw header elements.
				 *
				 * @name LocalClientRequest#rawHeaders
				 * @property {string[]}
				 * @readonly
				 */
				rawHeaders: { value: rawHeaders, enumerable: true },

				/**
				 * Provides request headers.
				 *
				 * @name LocalClientRequest#headers
				 * @property {Object<string,string>}
				 * @readonly
				 */
				headers: { value: normalizedHeaders, enumerable: true },

				/**
				 * Provides fake list of raw trailers.
				 *
				 * @name LocalClientRequest#rawTrailers
				 * @property {string[]}
				 * @readonly
				 */
				rawTrailers: { value: [], enumerable: true },

				/**
				 * Provides fake set of trailers.
				 *
				 * @name LocalClientRequest#trailers
				 * @property {Object<string,string>}
				 * @readonly
				 */
				trailers: { value: {}, enumerable: true },

				/**
				 * Provides fake socket mostly for exposing some peer address.
				 *
				 * @name LocalClientRequest#socket
				 * @property {object}
				 * @readonly
				 */
				socket: {
					value: Object.create( {}, {
						address: { value: () => localSocket },
						localAddress: { value: localSocket.address, },
						localPort: { value: localSocket.port, },
						remoteAddress: { value: peerSocket.address, },
						remotePort: { value: peerSocket.port, },
						remoteFamily: { value: peerSocket.family, },
					} ),
					enumerable: true,
				},

				/**
				 * Exposes response associated with request on dispatching the
				 * latter.
				 *
				 * @name LocalClientRequest#response
				 * @property {LocalClientResponse}
				 * @readonly
				 */
				response: {
					value: new api.service.LocalClientResponse(),
				},

				/**
				 * Provides fake HTTP version.
				 *
				 * @name LocalClientRequest#httpVersion
				 * @property {string}
				 * @readonly
				 */
				httpVersion: { value: "1.0", enumerable: true },
			} );
		}

		/**
		 * Dispatches request using routing feature of current Hitchy instance.
		 *
		 * @returns {Promise<LocalClientResponse>} promises response
		 */
		dispatch() {
			return new Promise( ( resolve, reject ) => {
				/** @type {Hitchy.Core.RequestContext} */
				const context = {
					request: this,
					response: this.response,
					done: _error => {
						if ( _error ) {
							console.error( `got error on locally dispatching ${this.method} ${this.url}: ${_error.message}` );
						}
					},
					local: {},
					consumed: {
						byPolicy: false,
						byController: false,
					},
				};

				api.utility.introduce( context );

				api.service.Router.normalize( context );
				api.service.Responder.normalize( context );

				return api.service.Router.dispatch( context )
					.then( () => {
						if ( context.consumed.byController || this.response.finished ) {
							resolve( this.response );
						} else {
							reject( Object.assign( new Error( "forwarded request did not match any routing" ), { status: 404 } ) );
						}
					} )
					.catch( reject );
			} );
		}
	}

	return LocalClientRequest;
}

export const appendFolders = false;
