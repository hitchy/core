import { PassThrough } from "node:stream";
import { STATUS_CODES } from "node:http";

export default function() { // eslint-disable-line jsdoc/require-jsdoc
	const api = this;

	/**
	 * Implements client to create and locally dispatch request.
	 */
	class LocalClientResponse extends PassThrough {
		/** */
		constructor() {
			super();

			/**
			 * Caches volatile set of headers.
			 *
			 * @type {Object<string,(string|string[])>}
			 * @private
			 */
			this.headers = {};

			/**
			 * Fakes current status code of response.
			 *
			 * @name LocalClientResponse#statusCode
			 * @type {int}
			 */
			this.statusCode = 200;


			const { FakeServiceSocket: localSocket, FakeClientSocket: peerSocket } = api.service.LocalClientRequest;
			let message = null;

			Object.defineProperties( this, {
				/**
				 * Exposes message related to current status code.
				 *
				 * @name LocalClientResponse#statusMessage
				 * @property {string}
				 */
				statusMessage: {
					get: () => message || STATUS_CODES[parseInt( this.statusCode ) || 500],
					set( value ) { message = value; },
				},

				/**
				 * Indicates if response has been written completely.
				 *
				 * @name LocalClientResponse#finished
				 * @property {boolean}
				 * @readonly
				 */
				finished: {
					get: () => this.writableFinished,
				},

				/**
				 * Provides fake socket of response.
				 *
				 * @name LocalClientResponse#socket
				 * @property {object}
				 * @readonly
				 */
				socket: {
					value: Object.create( {}, {
						address: { value: () => localSocket },
						localAddress: { value: localSocket.address, },
						localPort: { value: localSocket.port, },
						remoteAddress: { value: peerSocket.address, },
						remotePort: { value: peerSocket.port, },
						remoteFamily: { value: peerSocket.family, },
					} ),
					enumerable: true,
				},

				/**
				 * Indicates whether are assumed to be sent already.
				 *
				 * @name LocalClientResponse#headersSent
				 * @property {boolean}
				 * @readonly
				 */
				headersSent: {
					get: () => Object.isFrozen( this.headers ),
				}
			} );

			Object.defineProperties( this, {
				/**
				 * Provides alias for socket of response.
				 *
				 * @name LocalClientResponse#connection
				 * @property {object}
				 * @readonly
				 */
				connection: {
					value: this.socket,
					enumerable: true,
				},
			} );
		}

		/**
		 * Fakes addTrailers() of ServerResponse.
		 *
		 * @param {object} headers headers to be appended, get discarded here
		 * @returns {void}
		 */
		addTrailers( headers ) {} // eslint-disable-line no-unused-vars,no-empty-function

		/**
		 * Defines response header.
		 *
		 * @param {string} name name of header to set
		 * @param {string|string[]} value header value(s)
		 * @returns {void}
		 */
		setHeader( name, value ) {
			this.headers[name.toLowerCase()] = value;
		}

		/**
		 * Fetches previously defined response header.
		 *
		 * @param {string} name name of header to set
		 * @returns {string|string[]} selected header's value
		 */
		getHeader( name ) {
			return this.headers[name.toLowerCase()];
		}

		/**
		 * Removes response header.
		 *
		 * @param {string} name name of header to remove
		 * @returns {void}
		 */
		removeHeader( name ) {
			delete this.headers[name.toLowerCase()];
		}

		/**
		 * Checks if response header exists.
		 *
		 * @param {string} name name of header to remove
		 * @returns {boolean} true if header has been defined before, false otherwise
		 */
		hasHeader( name ) {
			return this.headers.hasOwnProperty( name.toLowerCase() );
		}

		/**
		 * Fetches defined set of response headers.
		 *
		 * @returns {Object<string,string>} defined response headers
		 */
		getHeaders() {
			return Object.assign( {}, this.headers );
		}

		/**
		 * Fetches names of defined response headers.
		 *
		 * @returns {string[]} names of defined response headers
		 */
		getHeaderNames() {
			return Object.keys( this.headers );
		}

		/**
		 * Fakes flushing headers.
		 *
		 * @return {void}
		 */
		flushHeaders() {
			if ( !Object.isFrozen( this.headers ) ) {
				Object.defineProperties( this, {
					headers: { value: Object.freeze( Object.assign( {}, this.headers ) ) },
				} );
			}
		}

		/**
		 * Fakes ServerResponse#writeContinue().
		 *
		 * @returns {void}
		 */
		writeContinue() {
			throw new Error( "HTTP's Continue not supported when dispatching internally" );
		}

		/**
		 * Fakes ServerResponse#writeProcessing().
		 *
		 * @returns {void}
		 */
		writeProcessing() {
			throw new Error( "HTTP's Processing not supported when dispatching internally" );
		}

		/**
		 * Fakes ServerResponse#writeHead().
		 *
		 * @param {int} statusCode status code to set
		 * @param {string} statusMessage status message to set explicitly
		 * @param {Object<string,(string|string[])>} headers response headers to set
		 * @returns {void}
		 */
		writeHead( statusCode, statusMessage, headers ) {
			if ( this.headersSent ) {
				throw new Error( "headers sent already" );
			}

			this.statusCode = statusCode;

			let _headers = headers;

			if ( typeof statusMessage === "string" ) {
				this.statusMessage = statusMessage;
			} else {
				_headers = statusMessage;
			}

			Object.keys( _headers ).forEach( name => {
				this.setHeader( name, _headers[name] );
			} );

			this.flushHeaders();
		}

		/**
		 * Simplifies retrieval of response body.
		 *
		 * @returns {Promise<Buffer>} promises response body
		 */
		body() {
			if ( !this.__bodyFetched ) {
				this.__bodyFetched = new Promise( ( resolve, reject ) => {
					const buffers = [];

					this.on( "data", chunk => buffers.push( chunk ) );

					this.once( "error", reject );
					this.once( "end", () => {
						resolve( Buffer.concat( buffers ) );
					} );
				} );
			}

			return this.__bodyFetched;
		}
	}

	return LocalClientResponse;
}

export const appendFolders = false;
