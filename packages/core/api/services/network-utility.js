import Network from "node:net";

export default function() { // eslint-disable-line jsdoc/require-jsdoc
	/**
	 * Implements network-related utility functions.
	 */
	class NetworkUtility {
		/**
		 * Parses optional information on request forwarded by at least one
		 * reverse proxy.
		 *
		 * @param {string} header raw header value to parse
		 * @returns {object} parsed components of provided header
		 */
		static parseKeyValueHeader( header ) {
			const parsed = {};

			if ( header ) {
				let source = header;
				const ptn = /^\s*([^="\s]+)\s*=\s*(?:"([^"\r\n]*)"|([^\s;]+))\s*(?:;(.+))?$/;
				let match;

				while ( source && ( match = source.match( ptn ) ) != null ) {
					parsed[match[1].toLowerCase()] = match[3] || match[2];
					source = match[4];
				}
			}

			return parsed;
		}

		/**
		 * Parses optional information on request forwarded by at least one
		 * reverse proxy.
		 *
		 * @param {Hitchy.Core.IncomingMessage} request request descriptor
		 * @param {object} forwarded parsed components of `Forwarded` request header
		 * @returns {string} parsed components of provided header
		 */
		static extractClientIp( request, forwarded ) {
			const normalized = request || {};
			const { headers = {} } = normalized;
			const sources = [ forwarded.for, headers["x-forwarded-for"], headers["x-real-ip"] ];

			for ( let i = 0; i < sources.length; i++ ) {
				const source = sources[i];
				const trimmed = source == null ? "" : String( source ).trim();

				if ( trimmed ) {
					const single = trimmed.replace( /,.*$/, "" ).trim();

					if ( Network.isIP( single ) ) {
						return single;
					}
				}
			}

			return ( normalized.socket || {} ).remoteAddress;
		}

		/**
		 * Compiles absolute URL of incoming request according to its
		 * information.
		 *
		 * @param {Hitchy.Core.IncomingMessage} request request descriptor
		 * @param {object} forwarded parsed components of `Forwarded` request header
		 * @returns {URL} compiled URL
		 */
		static compileRequestUrl( request, forwarded ) {
			const normalized = request || {};
			const { headers = {} } = normalized;
			let protocol;

			if ( forwarded.proto ) {
				protocol = forwarded.proto;
			} else if ( headers && headers["x-forwarded-proto"] ) {
				protocol = headers["x-forwarded-proto"];
			} else if ( normalized.socket && normalized.socket.encrypted ) {
				protocol = "https";
			} else {
				protocol = "http";
			}

			return new URL( normalized.url, `${protocol}://${forwarded.host || headers.host}` );
		}
	}

	return NetworkUtility;
}
