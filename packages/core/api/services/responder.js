export default function() { // eslint-disable-line jsdoc/require-jsdoc
	const api = this;

	const logInfo = api.log( "hitchy:request:info" );

	/**
	 * Implements tools for qualifying and managing the API for controlling
	 * response to a routed request.
	 */
	class Responder {
		/**
		 * Extends response manager to provide some assumed API at least.
		 *
		 * @param {Hitchy.Core.RequestContext} requestContext context of request to be handled
		 * @returns {Hitchy.Core.RequestContext} provided request context with contained response manager extended
		 */
		static normalize( requestContext ) {
			const { request, response } = requestContext;
			const polyfills = this.ResponsePolyfills;

			for ( let i = 0; i < polyfills.length; i++ ) {
				const { name, handler } = polyfills[i];

				if ( !( name in response ) ) {
					response[name] = api.service.RoutingPolyfillFactory[handler]( requestContext );
				}
			}

			switch ( request.method ) {
				case "HEAD" :
					this.disableResponse( response );
					break;
			}

			if ( !response._originalEnd ) {
				response._originalEnd = response.end;

				response.end = function( ...args ) {
					response._originalEnd.apply( this, args );

					logInfo( "%s %s %s %dms", request.method, request.url, response.statusCode, Math.round( Date.now() - requestContext.startTime ) );
				};
			}

			return requestContext;
		}

		static ResponsePolyfills = [
			{
				name: "format",
				handler: "responseFormat",
			},
			{
				name: "json",
				handler: "responseJson",
			},
			{
				name: "redirect",
				handler: "responseRedirect",
			},
			{
				name: "send",
				handler: "responseSend",
			},
			{
				name: "set",
				handler: "responseSet",
			},
			{
				name: "status",
				handler: "responseStatus",
			},
			{
				name: "type",
				handler: "responseType",
			},
		];

		/**
		 * Wraps provided ServerResponse in a proxy that is preventing actions meant
		 * to describe some actual response sent back.
		 *
		 * @note This method is adjusting provided response object.
		 *
		 * @param {ServerResponse} response response to be disabled
		 * @returns {ServerResponse} disabled response
		 */
		static disableResponse( response ) {
			const end = response.end;
			response.end = () => end.call( response );

			const setHeader = response.setHeader;
			response.setHeader = ( name, value ) => {
				if ( !/^\s*content-type\s*$/i.test( name ) ) {
					setHeader.call( response, name, value );
				}
			};

			const writeHead = response.writeHead;
			response.writeHead = ( code, text, headers ) => {
				const hasText = headers != null || !text || typeof text !== "object";
				const copy = filterHeader( hasText ? headers : text );

				return hasText ? writeHead.call( response, code, text, copy ) : writeHead.call( response, code, copy );
			};

			const set = response.set;
			response.set = ( name, value ) => {
				if ( name && typeof name === "object" ) {
					set.call( response, filterHeader( name ) );
				} else if ( !/^\s*content-type\s*$/i.test( name ) ) {
					set.call( response, name, value );
				}

				return response;
			};

			response.json = response.send = () => {
				end.call( response );
				return response;
			};

			return response;

			/**
			 * Creates shallow copy of provided response headers with properties
			 * describing content removed.
			 *
			 * @param {object} headers original set of response headers
			 * @returns {object} copy of provided response headers omitting certain fields
			 */
			function filterHeader( headers ) {
				if ( !headers ) {
					return headers;
				}

				const names = Object.keys( headers );
				const numNames = names.length;
				const copy = {};

				for ( let i = 0; i < numNames; i++ ) {
					const key = names[i];

					if ( !/^\s*content-type\s*$/i.test( key ) ) {
						copy[key] = headers[key];
					}
				}

				return copy;
			}
		}
	}

	return Responder;
}
