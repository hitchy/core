import RoutingPolyfillFactory from "./routing-polyfill-factory.js";

export default function( options ) { // eslint-disable-line jsdoc/require-jsdoc
	/**
	 * Implements fallback responses for selected scenarios.
	 */
	class Response {
		/**
		 * Renders fallback response e.g. on encountering error while routing a
		 * request or passes encountered error back into context of framework
		 * Hitchy has been injected into, usually ExpressJS.
		 *
		 * @param {Hitchy.Core.RequestContext} requestContext context of request rendered fallback response is related to
		 * @param {Error=} error error to be handled
		 * @returns {void}
		 */
		static renderFallback( requestContext, error = undefined ) {
			if ( !error ) {
				this.splash( requestContext );
			} else if ( options?.handleErrors ) {
				this.error( requestContext, error );
			} else {
				requestContext.done( error );
			}
		}

		/**
		 * Renders splash screen while hitchy framework is booting.
		 *
		 * @param {Hitchy.Core.RequestContext} requestContext context of routed request rendered response is related to
		 * @returns {void}
		 */
		static splash( requestContext ) {
			const format = RoutingPolyfillFactory.responseFormat( requestContext );
			const status = RoutingPolyfillFactory.responseStatus( requestContext );
			const send = RoutingPolyfillFactory.responseSend( requestContext );

			status( 423 );

			format( {
				html() {
					send( `<!doctype html>
<html lang="en">
<head>
<title>Service is starting ...</title>
</head>
<body>
<h1>Welcome!</h1>
<p>We kindly ask for your patience as this service is not available, yet.</p>
</body>
</html>` );
				},
				json() {
					send( {
						softError: "Welcome! This service is not available, yet."
					} );
				},
				default() {
					send( "Welcome! This service is not available, yet." );
				}
			} );
		}

		/**
		 * Renders simple view describing the error captured on routing some
		 * request.
		 *
		 * @param {Hitchy.Core.RequestContext} requestContext context of routed request the rendered response is related to
		 * @param {Error} error captured error to be described in view
		 * @returns {void}
		 */
		static error( requestContext, error ) {
			const format = RoutingPolyfillFactory.responseFormat( requestContext );
			const status = RoutingPolyfillFactory.responseStatus( requestContext );
			const send = RoutingPolyfillFactory.responseSend( requestContext );

			requestContext.api?.log( "hitchy:injector:debug" )( "internally rendering error:", error );

			const isDev = process.env.NODE_ENV === "development";
			const _error = {
				status: parseInt( error.statusCode ) || parseInt( error.status ) || parseInt( error.code ) || 500,
				message: error.message || "unknown error"
			};

			status( _error.status );

			format( {
				html() {
					send( `<!doctype html>
<html lang="en">
<head>
<title>Error</title>
</head>
<body>
<h1>Request failed</h1>
<p>An error occurred while processing your request and there is not any more specific response provided by this application.</p>
<p${isDev ? ' style="white-space: pre"' : ""}>${isDev ? error.stack || _error.message : _error.message}</p>
</body>
</html>` );
				},
				json() {
					send( {
						error: _error.message,
						code: _error.status,
					} );
				},
				text() {
					send( "Error: " + ( _error.message || "unknown error" ) );
				},
				default() {
					send( "Error: " + ( _error.message || "unknown error" ) );
				}
			} );
		}
	}

	return Response;
}
