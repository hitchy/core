export default function() { // eslint-disable-line jsdoc/require-jsdoc
	const api = this;

	const CommonStages = [ "before", "after" ];
	const AllStages = [ "early", ...CommonStages, "late" ];

	/** @name RouteDefinitionNormalizer */
	return {
		Plugin: ( ...args ) => _normalizeDefinition( true, false, ...args ),
		Custom: ( ...args ) => _normalizeDefinition( true, true, ...args ),
		Blueprint: ( ...args ) => _normalizeDefinition( false, false, ...args ),
	};

	/**
	 * Commonly implements normalization of routing definitions.
	 *
	 * @param {boolean} supportStages true to support any stage in raw definition,
	 *        e.g. blueprints do not support any stage
	 * @param {boolean} supportAllStages true to support stages "early" and "late"
	 *        as used with custom routes of current application
	 * @param {object} rawDefinition raw definition of routes to be normalized
	 * @returns {HitchyRoutePluginTablesNormalized|HitchyRouteDescriptorSet} normalized routing definitions
	 * @private
	 */
	function _normalizeDefinition( supportStages, supportAllStages, rawDefinition ) {
		switch ( typeof rawDefinition ) {
			case "object" :
				if ( rawDefinition && !Array.isArray( rawDefinition ) ) {
					let expectDefinitionsPerStage = Boolean( supportStages );
					let hasDefinitionsPerStage = false;
					let hasUnknownStages = false;

					if ( expectDefinitionsPerStage ) {
						// expect up to four stages and at most one property not selecting one of those stages
						const propertiesToCheck = Object.keys( rawDefinition ).slice( 0, 5 );

						for ( let i = 0; i < propertiesToCheck.length; i++ ) {
							const name = propertiesToCheck[i];

							if ( AllStages.includes( name ) ) {
								hasDefinitionsPerStage = true;

								if ( !supportAllStages && !CommonStages.includes( name ) ) {
									hasUnknownStages = true;
								}
							} else {
								expectDefinitionsPerStage = false;
							}
						}

						if ( hasDefinitionsPerStage && !expectDefinitionsPerStage ) {
							// simple routing declarations are mixed with subordinated
							// sets of declarations for particular routing slot
							throw new TypeError( "invalid mix of routing definitions with and without routing slot selection" );
						}
					}

					if ( !expectDefinitionsPerStage ) {
						// at least one property does not use name matching any
						// known stage -> consider whole definition as implicit
						break;
					}

					if ( hasUnknownStages ) {
						// all properties of definition match names of known stages,
						// but some of the stages are not expected thus throw
						// exception to notify user (to support in debugging)
						throw new TypeError( "got one or more unexpected stages in route definition" );
					}


					// create new object containing maps of routes per expected stage
					const result = {};
					const stages = supportAllStages ? AllStages : CommonStages;

					for ( let i = 0; i < stages.length; i++ ) {
						const stage = stages[i];

						result[stage] = normalizeRoutes( rawDefinition[stage] );
					}

					return result;
				}
				break;

			case "undefined" :
				break;

			default :
				throw new TypeError( `invalid route definition: ${rawDefinition}` );
		}

		return supportStages ? supportAllStages ? {
			early: new Map(),
			before: normalizeRoutes( rawDefinition || {} ),
			after: new Map(),
			late: new Map()
		} : {
			before: normalizeRoutes( rawDefinition || {} ),
			after: new Map()
		} : normalizeRoutes( rawDefinition || {} );
	}

	/**
	 * Normalizes single set of routes provided as object, array or map.
	 *
	 * @param {Array|object|Map} rawSetOfRoutes raw set of routes to be normalized
	 * @returns {Map} normalized routing definitions grouped by their source
	 * @private
	 */
	function normalizeRoutes( rawSetOfRoutes ) {
		if ( !rawSetOfRoutes ) {
			return new Map();
		}

		if ( rawSetOfRoutes instanceof Map ) {
			return rawSetOfRoutes;
		}

		const { Route } = api.service;

		if ( Array.isArray( rawSetOfRoutes ) ) {
			// create Map from provided array
			const mapping = new Map();

			for ( let i = 0; i < rawSetOfRoutes.length; i++ ) {
				const route = rawSetOfRoutes[i];
				let source, target;

				if ( Array.isArray( route ) ) {
					if ( route.length === 2 ) {
						[ source, target ] = route;
					} else {
						throw new TypeError( "invalid array-based definition of a route" );
					}
				} else {
					const { method, url } = Route.preparseSource( route );

					source = `${method} ${url}`;
					target = route;
				}

				if ( mapping.has( source ) ) {
					const existing = mapping.get( source );

					if ( Array.isArray( source ) ) {
						existing.push( target );
					} else {
						mapping.set( source, [ existing, target ] );
					}
				} else {
					mapping.set( source, target );
				}
			}

			return mapping;
		}

		if ( typeof rawSetOfRoutes === "object" ) {
			const mapping = new Map();

			for ( const key of Object.keys( rawSetOfRoutes ) ) {
				const value = rawSetOfRoutes[key];
				const { method, url, prefix, exact } = Route.preparseSource( key );

				mapping.set( `${method} ${exact ? "=" : ""}${prefix ? "~" : ""}${url}`, value );
			}

			return mapping;
		}

		throw new TypeError( "invalid list of raw routing definitions" );
	}
}
