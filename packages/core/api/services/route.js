import Util from "node:util";

import { pathToRegexp as Parser, match as Matcher, compile as Compiler } from "path-to-regexp";

export default function() { // eslint-disable-line jsdoc/require-jsdoc
	const api = this;
	const { utility: { case: Case } } = api;

	/**
	 * Implements generic code for handling route definition and the resulting route
	 * information required for efficiently processing it on request dispatching.
	 *
	 * @name Route
	 * @property {HitchyRouteSource} source refers to unparsed definition of route's source
	 * @property {HitchyRouteTarget} target refers to unparsed definition of route's target
	 * @property {string} method HTTP method route is bound to (might be ALL)
	 * @property {string} path path name pattern as provided in route's definition
	 * @property {string} prefix static mandatory prefix of current route
	 * @property {RegExp} pattern pattern matching path names to be handled
	 * @property {HitchyRouteParameter[]} parameters descriptions of parameters defined in route's source
	 * @property {?function} handler refers to function to be invoked on processing
	 *           route, might be null on a route addressing missing target
	 * @property {Array} args arguments defined to be passed in addition to regular signature (req,res,[next]) on invoking route's handler
	 * @property {boolean} isValid set true if route is valid
	 * @property {?string} warning describes warning regarding current route
	 * @property {function(Object<string,(string|array)>):string} compile creates example path matching current route's source using some provided data
	 */
	class Route {
		/**
		 * @param {HitchyRouteSource} source source of route, e.g. a request method and a pathname to match
		 * @param {HitchyRouteTarget} target handler function or selector for handler function
		 * @param {boolean} isMatchingPrefix if true route is matching prefixes of request URLs, otherwise whole URL must be matching
		 */
		constructor( source, target, { isMatchingPrefix = false } = {} ) {
			const compiledSource = this.constructor.parseSource( source, isMatchingPrefix );
			const compiledTarget = this.constructor.parseTarget( target );

			// expose indestructible information on resulting route
			Object.defineProperties( this, {
				source: { value: source, enumerable: true },
				target: { value: target, enumerable: true },
				method: { value: compiledSource.method, enumerable: true },
				path: { value: compiledSource.definition, enumerable: true },
				prefix: { value: compiledSource.prefix, enumerable: true },
				pattern: { value: compiledSource.pattern, enumerable: true },
				parameters: { value: compiledSource.parameters, enumerable: true },
				matcher: { value: compiledSource.matcher, enumerable: true },
				compile: { value: compiledSource.render, enumerable: true },
				context: { value: compiledTarget.context, enumerable: true },
				handler: { value: compiledTarget.handler, enumerable: true },
				args: { value: compiledTarget.args, enumerable: true },
				warning: { value: compiledTarget.warning || null, enumerable: true },
			} );

			// detach definition of isValid so its implementation can rely on
			// properties defined before
			Object.defineProperties( this, {
				isValid: { value: this._isValid(), enumerable: true },
			} );
		}

		/**
		 * Indicates if current route is considered valid and may be processed for
		 * request dispatching.
		 *
		 * @returns {boolean} true if route is considered valid
		 * @protected
		 */
		_isValid() {
			return typeof this.handler === "function";
		}

		/**
		 * Parses and validates provided definition of a route's source.
		 *
		 * @param {HitchyRouteSource} definition definition of routing source
		 * @returns {{method: string, url: string, prefix: boolean, exact: boolean}} normalized description of routing source
		 */
		static preparseSource( definition ) {
			let method, url;

			switch ( typeof definition ) {
				case "string" : {
					// extract source part preceding the => in strings as complex as this:
					//     /some/url/foo      => someController::someMethod
					//     GET ~/some/url/foo => someController::someMethod
					//     * =/some/url/foo   => someController::someMethod
					//     = /some/url/foo    => someController::someMethod
					const normalized = String( definition || "" ).trim().replace( /\s+/g, " " );

					[ , method, url ] = /^(?:(\*|[a-z]{1,16}) ?)?([=~]? ?\{?\/.*?) ?(?:=>|$)/i.exec( normalized ) || [];

					if ( method == null ) {
						method = "ALL";
					}

					break;
				}

				case "object" :
					if ( definition && typeof definition.url === "string" && definition.url.trim().length > 0 ) {
						switch ( typeof definition.type ) {
							case "undefined" :
							case "string" :
								break;

							default :
								throw new TypeError( Util.format( "invalid type on route for URL %s", definition.url ) );
						}

						method = definition.hasOwnProperty( "type" ) ? definition.type : "ALL";
						url = definition.url;

						break;
					}

				// falls through
				default :
					throw new TypeError( Util.format( "invalid route %s", definition ) );
			}


			if ( typeof method !== "string" ) {
				throw new TypeError( Util.format( "route w/ invalid type %s", definition ) );
			}

			method = method.trim().toUpperCase();
			if ( !method.length || method === "*" ) {
				method = "ALL";
			}


			if ( typeof url !== "string" ) {
				throw new TypeError( Util.format( "invalid URL in route %s", definition ) );
			}

			const exact = url[0] === "=";
			const prefix = url[0] === "~";

			if ( exact || prefix ) {
				url = url.slice( 1 ).trim();
			}

			if ( !/^\{?\//.test( url ) ) {
				throw new TypeError( `path of route must begin with a slash, but got ${url}` );
			}

			if ( /\/\/|\/:(?:[^"$_\p{ID_Start}]|$)|\(\)/u.test( url ) ) {
				throw new TypeError( `invalid segment or separator in path of route: ${url}` );
			}


			return { method, url, prefix, exact };
		}

		/**
		 * Parses source definition of current route.
		 *
		 * @param {HitchyRouteSource} source definition of routing source
		 * @param {boolean} isMatchingPrefix set true to get pattern matching prefix of request URL instead of whole URL
		 * @returns {HitchyRouteCompiledSource} compiled description of routing source
		 * @throws TypeError on providing invalid argument
		 */
		static parseSource( source, isMatchingPrefix = false ) {
			const { method, url, exact, prefix: matchPrefix } = this.preparseSource( source );
			const parseOptions = {
				sensitive: true,
				end: !isMatchingPrefix,
			};

			if ( matchPrefix ) {
				parseOptions.end = false;
			}

			if ( exact ) {
				parseOptions.end = true;
			}

			const { regexp: pattern, keys: parameters } = Parser( url.replace( /\/$/, "" ), parseOptions );

			// extract the longest leading part of URL that does not contain any
			// character used for parameters, repetitions (supported in previous
			// versions, only) etc.
			let [ , prefix ] = /^([^*+?(:[{]*)/.exec( url );

			// trim final slash
			prefix = prefix.replace( /\/$/, "" );
			if ( !prefix.length ) {
				prefix = "/";
			}

			// extract optionally given type of parameter value
			for ( let i = 0; i < parameters.length; i++ ) {
				const parameter = parameters[i];

				if ( parameter.type === "param" ) {
					const segments = parameter.name.split( "." );

					if ( segments.length > 1 ) {
						parameter.valueType = segments.pop().trim().toLowerCase();
						parameter.source = parameter.name;
						parameter.name = segments.join( "." );
					}
				}
			}

			return {
				method,
				definition: url,
				pattern,
				parameters,
				prefix,
				matcher: Matcher( url.replace( /\/$/, "" ), parseOptions ),
				render: Compiler( url ),
			};
		}

		/**
		 * Selects function address as target of route in its target definition.
		 *
		 * @param {HitchyRouteTarget} target routing target definition
		 * @returns {?HitchyRouteCompiledTarget} compiled description of routing target
		 */
		static parseTarget( target ) {
			let _target = target;

			switch ( typeof _target ) {
				case "string" : {
					// target might be given as string
					// -> convert to object selecting controller and its method by name
					const match = /^(?:.+?=>\s*)?([^.:#]+)(?:(?:\.|::|#)(.+))?$/.exec( _target.trim() );
					if ( !match ) {
						throw new TypeError( Util.format( "invalid routing target selector %s", _target ) );
					}

					_target = {
						module: match[1],
						method: ( match[2] || "index" ).replace( /\s*\(\s*\)\s*$/, "" ),
					};
					break;
				}

				case "function" :
					// target might be given as function reference to invoke as-is
					return {
						handler: _target,
						args: []
					};

				case "object" :
					// target is selected by object naming a method and its containing controller/policy as "module"
					if ( _target && ( _target.module || _target.controller || _target.policy ) && _target.method ) {
						if ( !_target.module ) {
							const original = _target;

							_target = {
								module: _target.controller || _target.policy,
								method: _target.method.replace( /\s*\(\s*\)\s*$/, "" ),
							};

							if ( original.hasOwnProperty( "args" ) ) {
								_target.args = original.args;
							}
						}

						break;
					}

				// falls through
				default :
					throw new TypeError( Util.format( "invalid routing target descriptor %s", _target ) );
			}

			// at this point there is an object describing controller and method
			// of routing target

			const result = {
				handler: null,
				args: [],
				warning: null,
			};

			let context;
			let module;

			if ( _target.module != null ) {
				if ( typeof _target.module === "string" ) {
					// drop optional suffix in name of module
					module = _target.module = _target.module.replace( this.tailPattern, "" );

					// check if selected module exists
					const implementations = api[this.collectionPluralName];
					const implementName = [
						module,
						Case.kebabToPascal( module ),
						Case.camelToPascal( module ),
					]
						.find( name => ( implementations ? implementations.hasOwnProperty( name ) : null ) );

					if ( implementName ) {
						context = implementations[implementName];
					}
				} else if ( typeof _target.module === "object" ) {
					module = "<implicit>";
					context = _target.module;
				}
			}

			if ( context ) {
				// selected method must be provided by found controller
				result.context = context;
				result.handler = context[_target.method];

				if ( typeof result.handler === "function" ) {
					if ( Array.isArray( _target.args ) ) {
						result.args = _target.args.slice();
					} else if ( _target.hasOwnProperty( "args" ) ) {
						result.args = [_target.args];
					}
				} else {
					result.warning = `invalid route to missing ${this.collectionSingularName} action ${module}.${_target.method}`;
				}
			} else {
				result.warning = `invalid route to missing ${this.collectionSingularName} ${module} (to contain action ${_target.method})`;
			}

			return result;
		}

		/**
		 * Indicates route partially matching some tested prefix.
		 *
		 * @see Route#selectProbablyCoveredPrefixes()
		 * @returns {number} unique indicator value
		 * @constructor
		 */
		static get MATCH_PARTIAL() { return 1; }

		/**
		 * Indicates route fully matching some tested prefix.
		 *
		 * @see Route#selectProbablyCoveredPrefixes()
		 * @returns {number} unique indicator value
		 * @constructor
		 */
		static get MATCH_FULL() { return 2; }

		/**
		 * Retrieves all given prefixes considered probably covered by current
		 * route.
		 *
		 * @example cases of probably covering routes:
		 *  - `/test` probably covers `/test/name`
		 *  - `/te` probably covers `/test/name`
		 *  - `/` probably covers `/test/name`
		 *  - `/test?` probably covers `/test/name`
		 *  - `/test*` probably covers `/test/name`
		 *  - `/test+` probably covers `/test/name`
		 *  - `/(test)?` probably covers `/test/name`
		 *  - `/(test)*` probably covers `/test/name`
		 *  - `/(test)+` probably covers `/test/name`
		 *  - `/(test)?/test/name/` probably covers `/test/name`
		 *  - `/(test)+/name` probably covers `/test/name`
		 *  - `/test/:minor` probably covers `/test/name`
		 *  - `/:major/name` probably covers `/test/name`
		 *  - `/:major?/name` probably covers `/test/name`
		 *  - `/*major/name` probably covers `/test/name`
		 *  - `/:major/:minor` probably covers `/test/name`
		 *  - `/test{/:minor}/name` probably covers `/test/name`
		 *  - `/test{/*minor}` probably covers `/test/name`
		 *  - `/test/*minor` probably covers `/test/name`
		 *  - `{/*major}` probably covers `/test/name`
		 *  - `/*major` probably covers `/test/name`
		 *
		 * @example cases of non-covering routes:
		 *  - `/teste` does not cover `/test/name`
		 *  - `/tee` does not cover `/test/name`
		 *  - `/s` does not cover `/test/name`
		 *  - `/tas?s` does not cover `/test/name`
		 *  - `/tas*` does not cover `/test/name`
		 *  - `/tas+` does not cover `/test/name`
		 *  - `/(tast)?` does not cover `/test/name`
		 *  - `/(tast)*` does not cover `/test/name`
		 *  - `/(tast)+` does not cover `/test/name`
		 *  - `/tast/:minor` does not cover `/test/name`
		 *  - `/:major/neme` does not cover `/test/name`
		 *
		 * @param {string[]} prefixCandidates list of prefixes to test
		 * @returns {Object<string,int>} maps all basically covered prefixes into indicator whether matching partially or fully
		 */
		selectProbablyCoveredPrefixes( prefixCandidates ) {
			if ( !Array.isArray( prefixCandidates ) || prefixCandidates.some( i => typeof i !== "string" || i.trim() === "" ) ) {
				throw new TypeError( "invalid set of prefix candidates to filter" );
			}

			const covered = {};

			if ( prefixCandidates.length > 0 ) {
				const parsedPath = Parser( this.path.replace( /\/$/, "" ), {
					sensitive: true,
					end: false,
					trailing: true,
				} );

				// find and categorize all provided candidates for being a valid
				// prefix of current route's path or even matching it fully
				for ( let i = 0; i < prefixCandidates.length; i++ ) {
					const prefixCandidate = prefixCandidates[i];
					const match = parsedPath.regexp.exec( prefixCandidate );

					if ( match ) {
						covered[prefixCandidate] = match[0] === prefixCandidate && match[1] == null ? Route.MATCH_FULL : Route.MATCH_PARTIAL;
					}
				}
			}

			return covered;
		}

		/**
		 * Provides pattern matching suffix supported in name of component in a
		 * route's target.
		 *
		 * This is used to drop "Controller" in a component name like "UserController"
		 * to select the basic name of component in related collection.
		 *
		 * @type {RegExp}
		 */
		static get tailPattern() {
			return /(?<=.)Controller$/i;
		}

		/**
		 * Names singular of collection exposing components this route is capable of
		 * addressing.
		 *
		 * @type {string}
		 */
		static get collectionSingularName() {
			return "";
		}

		/**
		 * Names collection exposing components this route is capable of addressing
		 * (expecting plural form).
		 *
		 * @type {string}
		 */
		static get collectionPluralName() {
			return "";
		}
	}

	return Route;
}
