export default function() { // eslint-disable-line jsdoc/require-jsdoc
	const api = this;
	const { service } = api;

	/**
	 * Implements wrapper for compiling and providing description of a
	 * route to be dispatched to a controller.
	 */
	class ControllerRoute extends service.Route {
		/** @inheritDoc */
		constructor( source, target ) {
			super( source, target, { isMatchingPrefix: false } );
		}

		/** @inheritDoc */
		static get tailPattern() {
			return /(?<=.)Controller$/i;
		}

		/** @inheritDoc */
		static get collectionSingularName() {
			return "controller";
		}

		/** @inheritDoc */
		static get collectionPluralName() {
			return "controllers";
		}
	}

	return ControllerRoute;
}
