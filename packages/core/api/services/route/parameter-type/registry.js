export default function() { // eslint-disable-line jsdoc/require-jsdoc
	const api = this;

	const logDebug = api.log( "hitchy:router:debug" );
	const logError = api.log( "hitchy:router:error" );

	const registry = new Map();

	/**
	 * Implements service for globally registering parameter type validators
	 * used by Hitchy's router to check actual values provided as parameters in
	 * routes to be dispatched.
	 */
	class RouteParameterTypeRegistry {
		/**
		 * Adds or replaces a callback for validating values of a named type.
		 *
		 * @param {string} typeName case-insensitive name of parameter type validator is for
		 * @param {function(value):boolean} validatorFn callback invoked to check a value assumed to be of named type
		 * @returns {RouteParameterTypeRegistry} current registration service for daisy-chaining registrations
		 */
		static register( typeName, validatorFn ) {
			const normalized = String( typeName || "" ).trim().toLowerCase();

			if ( !normalized ) {
				throw new TypeError( "invalid parameter type name: " + typeName );
			}

			if ( typeof validatorFn !== "function" ) {
				throw new TypeError( "invalid parameter type validation callback: " + typeName );
			}

			logDebug( `registering validation callback for route parameters of type "${normalized}" provided as "${typeName}"` );

			registry.set( normalized, validatorFn );

			return this;
		}

		/**
		 * Provides a message describing why given value does not comply with
		 * named type of parameter values.
		 *
		 * @param {string} typeName case-insensitive name of parameter type to check for
		 * @param {string|string[]} value one or more values extracted from a (repeated) route parameter
		 * @returns {undefined|string} description of issue found in provided value, falsy if value is fine
		 */
		static getValidationError( typeName, value ) {
			const normalized = String( typeName || "" ).trim().toLowerCase();

			if ( !normalized ) {
				// no type has been named -> assuming no type has been defined
				// on the parameter, hence no validation is possible and every
				// value is fine
				return undefined;
			}

			if ( !registry.has( normalized ) ) {
				logError( `missing support for route parameter values of type ${normalized} provided as ${typeName}` );

				return `can not validate parameter of type ${typeName}`;
			}

			try {
				if ( !registry.get( normalized )( value ) ) {
					return `invalid parameter value of type ${typeName}`;
				}

				return "";
			} catch ( cause ) {
				return cause.message;
			}
		}
	}

	RouteParameterTypeRegistry
		.register( "uuid", value => api.service.ValueParser.isUuid( value ) )
		.register( "integer", value => api.service.ValueParser.isInteger( value ) )
		.register( "number", value => api.service.ValueParser.isNumber( value ) )
		.register( "keyword", value => api.service.ValueParser.isKeyword( value ) );

	return RouteParameterTypeRegistry;
}

export const appendFolders = false;
