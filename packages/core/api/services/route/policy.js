export default function() { // eslint-disable-line jsdoc/require-jsdoc
	const api = this;
	const { service } = api;

	/**
	 * Implements wrapper for compiling and providing description of a
	 * policy-based non-terminal route.
	 */
	class PolicyRoute extends service.Route {
		/** @inheritDoc */
		constructor( source, target ) {
			super( source, target, { isMatchingPrefix: true } );
		}

		/** @inheritDoc */
		static get tailPattern() {
			return /(?<=.)Policy$/i;
		}

		/** @inheritDoc */
		static get collectionSingularName() {
			return "policy";
		}

		/** @inheritDoc */
		static get collectionPluralName() {
			return "policies";
		}
	}

	return PolicyRoute;
}
