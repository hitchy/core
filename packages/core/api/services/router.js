import PromiseUtils from "promise-essentials";

export default function( options ) { // eslint-disable-line jsdoc/require-jsdoc
	const api = this;
	const { service } = api;

	const logDebug = api.log( "hitchy:router:debug" );
	const logWarning = api.log( "hitchy:router:warning" );
	const logError = api.log( "hitchy:router:error" );

	const debug = options.debug;

	/**
	 * Internally counts processed requests assigning unique numeric ID to every
	 * request this way.
	 *
	 * This "ID" is primarily used in logging requests.
	 *
	 * @type {number}
	 */
	let requestCounter = 0;

	const trim = ( input, maxLength = 120 ) => {
		const normalized = String( input ).replace( /\s+/g, " " );

		if ( normalized.length > maxLength ) {
			return normalized.substring( 0, maxLength - 3 ) + "...";
		}

		return normalized;
	};

	/**
	 * Implements service for routing incoming requests based on custom routes
	 * read from runtime configuration.
	 */
	class Router {
		static policies = {};

		static controllers = {};

		static RequestPolyfills = [
			{
				name: "path",
				handler: "requestPath",
			},
			{
				name: "query",
				handler: "requestQuery",
			},
			{
				name: "body",
				apiName: "fetchBody",
				handler: "requestBody",
			},
			{
				header: "accept",
				name: "accept",
				handler: "requestAccept",
				fallback: [],
			},
			{
				name: "is",
				handler: "requestIs",
			},
			{
				name: "res",
				handler: "requestRes",
			},
			{
				name: "ignore",
				handler: "requestIgnore",
			},
		];

		/**
		 * Sets up router according to configuration gathered from plugins, from
		 * application and due to blueprint routing.
		 *
		 * @param {Hitchy.Core.PluginHandle[]} plugins list of discovered plugins
		 * @returns {Promise<void>} promise for having compiled routing tables
		 */
		static async configure( plugins ) {
			const Normalize = service.RouteDefinitionNormalizer;

			const [ policies, routes, blueprints ] = await Promise.all( [
				PromiseUtils.map( plugins, this.createGetter( "policies", Normalize.Plugin ) ),
				PromiseUtils.map( plugins, this.createGetter( "routes", Normalize.Plugin ) ),
				PromiseUtils.map( plugins, this.createGetter( "blueprints", Normalize.Blueprint ) ),
			] );

			const customPolicies = await this.extractDefinition( api.config.$appConfig.policies || {}, Normalize.Custom );
			const customControllers = await this.extractDefinition( api.config.$appConfig.routes || {}, Normalize.Custom );

			// finally configure routing including all routes collected above
			this.compile( plugins, policies, routes, blueprints, customPolicies, customControllers );
		}

		/**
		 * Generates getter for commonly fetching routing configuration per plugin.
		 *
		 * @param {string} propertyName name of plugin's API property containing
		 *        definition of routes to process
		 * @param {function} normalizerFn callback function for normalizing definitions exposed by plugin
		 * @returns {function(handle):*} getter function
		 */
		static createGetter( propertyName, normalizerFn ) {
			return plugin => {
				if ( plugin.api.hasOwnProperty( propertyName ) ) {
					return this.extractDefinition( plugin.api[propertyName], normalizerFn, plugin );
				}

				return this.extractDefinition( plugin.config[propertyName], normalizerFn, plugin );
			};
		}

		/**
		 * Extracts raw definition of routes from provided dealing with getters and
		 * promises there before passing resulting definition to provided normalizer
		 * function.
		 *
		 * @param {Object|function():Object|Promise<Object>} source a raw map of routes, a promise for such a map or a function returning either one
		 * @param {function(Object):Object} normalizerFn function mapping some raw map of routes into a normalized map of routes
		 * @param {Hitchy.Core.PluginHandle} pluginHandle a plugin's handle extracted routing map is originating from
		 * @returns {Promise<Object>} promise resolved with normalized map of routes extracted from provided source
		 */
		static async extractDefinition( source, normalizerFn, pluginHandle = undefined ) {
			if ( typeof source === "function" ) {
				source = source.call( api, options, pluginHandle );
			}

			return normalizerFn( await source );
		}

		/**
		 * Initializes sorted routing table from routes provided in configuration
		 *
		 * @param {Hitchy.Core.PluginHandle[]} plugins lists discovered plugins
		 * @param {HitchyRoutePluginTablesNormalized[]} policiesPerPlugin lists defined policy routes per plugin
		 * @param {HitchyRoutePluginTablesNormalized[]} controllersPerPlugin lists defined terminal controller routes per plugin
		 * @param {HitchyRouteDescriptorSet[]} blueprintsPerPlugin lists defined blueprint routes per plugin
		 * @param {HitchyRoutePluginTablesNormalized[]} appPolicies lists policy routes of current application
		 * @param {HitchyRoutePluginTablesNormalized[]} appControllers lists terminal controller routes of current application
		 * @returns {void}
		 */
		static compile( plugins, policiesPerPlugin, controllersPerPlugin, blueprintsPerPlugin, appPolicies, appControllers ) {
			// put all defined routes into proper order
			let { policies, controllers } = this.sortDefinitions(
				plugins,
				policiesPerPlugin, controllersPerPlugin, blueprintsPerPlugin,
				appPolicies, appControllers
			);

			// convert definitions into instances of Route grouped by method and by prefix
			policies = this._compileRoutes( policies, service.PolicyRoute );
			controllers = this._compileRoutes( controllers, service.ControllerRoute );

			if ( debug ) {
				logDebug( `COLLECTED early policies:\n${policies.before.dump()}` );
				logDebug( `COLLECTED early controllers:\n${controllers.before.dump()}` );
				logDebug( `COLLECTED late controllers:\n${controllers.after.dump()}` );
				logDebug( `COLLECTED late policies:\n${policies.after.dump()}` );
			}

			policies.before.optimizeByPrefix();
			policies.after.optimizeByPrefix( true );

			this.policies = policies;
			this.controllers = controllers.before.concat( controllers.after ).optimizeByPrefix( true );

			if ( debug ) {
				logDebug( `CONFIGURED early policies:\n${this.policies.before.dump()}` );
				logDebug( `CONFIGURED controllers:\n${this.controllers.dump()}` );
				logDebug( `CONFIGURED late policies:\n${this.policies.after.dump()}` );
			}
		}

		/**
		 * Collects all provided definitions of routes sorted properly.
		 *
		 * @param {Hitchy.Core.PluginHandle[]} plugins lists discovered plugins
		 * @param {HitchyRoutePluginTablesNormalized[]} policiesPerPlugin lists defined policy routes per plugin
		 * @param {HitchyRoutePluginTablesNormalized[]} controllersPerPlugin lists defined terminal controller routes per plugin
		 * @param {HitchyRouteDescriptorSet[]} blueprintsPerPlugin lists defined blueprint routes per plugin
		 * @param {HitchyRoutePluginTablesNormalized[]} appPolicies lists policy routes of current application
		 * @param {HitchyRoutePluginTablesNormalized[]} appControllers lists terminal controller routes of current application
		 * @returns {{policies: OrderedRoutingQueue, controllers: OrderedRoutingQueue}} grouped and sorted definitions of policy and controller routes
		 */
		static sortDefinitions( plugins, policiesPerPlugin, controllersPerPlugin, blueprintsPerPlugin, appPolicies, appControllers ) {
			const numPlugins = plugins.length;

			const policies = new service.OrderedRoutingQueue( numPlugins );
			const controllers = new service.OrderedRoutingQueue( numPlugins );

			for ( let i = 0; i < numPlugins; i++ ) {
				const policyMaps = policiesPerPlugin[i];
				const controllerMaps = controllersPerPlugin[i];
				const blueprintMap = blueprintsPerPlugin[i];

				this.assignMap( policies.getOnPlugin( i, "before", new Map() ), policyMaps.before );
				this.assignMap( policies.getOnPlugin( i, "after", new Map() ), policyMaps.after );

				this.assignMap( controllers.getOnPlugin( i, "before", new Map() ), controllerMaps.before );
				this.assignMap( controllers.getOnPlugin( i, "after", new Map() ), controllerMaps.after );

				this.assignMap( controllers.getInnerSlot( new Map() ), blueprintMap );
			}

			const stages = [ "early", "before", "after", "late" ];
			for ( let i = 0; i < stages.length; i++ ) {
				const stage = stages[i];

				this.assignMap( policies.getCustomSlot( stage, new Map() ), appPolicies[stage] );
				this.assignMap( controllers.getCustomSlot( stage, new Map() ), appControllers[stage] );
			}

			return {
				policies: policies.compact( map => map?.size > 0 ),
				controllers: controllers.compact( map => map?.size > 0 ),
			};
		}

		/**
		 * Copies all elements from source map to target map.
		 *
		 * @param {Map} target target map
		 * @param {Map} source source map
		 * @returns {void}
		 */
		static assignMap( target, source ) {
			for ( const [ key, value ] of source.entries() ) {
				target.set( key, value );
			}
		}

		/**
		 * Compiles route definitions into sorted lists of compiled routes grouped
		 * by HTTP method.
		 *
		 * @param {OrderedRoutingQueue} stages routing tables managers per routing stage
		 * @param {Type<Route>} RouteClass class of routes to compile
		 * @returns {{before:RoutesPerMethod, after:RoutesPerMethod}} routes grouped per method and split into stages accordingly
		 */
		static _compileRoutes( stages, RouteClass ) {
			const result = {
				before: new service.RoutesPerMethod(),
				after: new service.RoutesPerMethod(),
			};

			let defined = 0,
				valid = 0;

			const names = [ "before", "after" ];

			for ( let i = 0; i < names.length; i++ ) {
				const name = names[i];
				const maps = stages[name];

				for ( let length = maps.length, m = 0; m < length; m++ ) {
					const map = maps[m];

					for ( const [ source, target ] of map.entries() ) {
						const isList = Array.isArray( target );
						const targets = isList ? target : [target];
						const numTargets = targets.length;

						if ( RouteClass === service.ControllerRoute && numTargets > 1 ) {
							throw new TypeError( "invalid definition of multiple targets for single controller route" );
						}

						for ( let j = 0; j < targets.length; j++ ) {
							const currentTarget = targets[j];

							defined++;

							const route = new RouteClass( source, currentTarget );

							if ( route.warning ) {
								logWarning( route.warning );
							}

							if ( route.isValid ) {
								valid++;

								result[name].append( route );
							}
						}
					}
				}
			}

			if ( debug ) {
				logDebug( `${valid} valid routes out of ${defined} defined ones` );
			}

			return result;
		}

		/**
		 * Dispatches request according to configured routes.
		 *
		 * @param {Hitchy.Core.RequestContext} requestContext description of current request's context
		 * @returns {Promise<void>} promises request dispatched and handled
		 */
		static async dispatch( requestContext ) {
			const { request, response } = requestContext;
			const { method, path } = request;
			const { policies, controllers } = this;

			request.id = String( ++requestCounter, 16 ).padStart( 8, "0" );
			request.context = requestContext;
			request.hitchy = requestContext.api;


			if ( debug ) {
				logDebug( `${request.id}: routing ${method} ${path}` );
			}


			const stages = [
				{
					label: "early policy",
					isPolicy: true,
					late: false,
					prefixedRoutes: policies.before.onMethod( method )
				},
				{
					label: "responder",
					isPolicy: false,
					late: false,
					prefixedRoutes: controllers.onMethod( method )
				},
				{
					label: "late policy",
					isPolicy: true,
					late: true,
					prefixedRoutes: policies.after.onMethod( method )
				},
			];

			try {
				for ( let i = 0; i < stages.length; i++ ) {
					await dispatchRouteInStage( stages[i] ); // eslint-disable-line no-await-in-loop
				}
			} catch ( cause ) {
				logError( `${request.id}: routing failed: ${cause.statusCode ? cause.message : cause.stack}` );

				throw cause;
			}


			/**
			 * Looks up routing stage for matching route(s).
			 *
			 * @param {string} label label of routing stage
			 * @param {boolean} isPolicy indicates whether set of routes describes policy routes instead of controller ones
			 * @param {boolean} late indicates whether routes belong to late processing or not
			 * @param {RoutesPerPrefix} prefixedRoutes set of defined routes to be tested for matching request
			 * @returns {Promise|undefined} promises having processed some matching routes, undefined if there are no matching routes in current stage
			 * @private
			 */
			async function dispatchRouteInStage( { label, isPolicy, late, prefixedRoutes } ) {
				let matching, write = 0;


				/**
				 * step 1: find all routes actually matching current request
				 */
				if ( prefixedRoutes ) {
					// got some routes related this request's method
					const routes = prefixedRoutes.onPrefix( path );
					const length = routes.length;

					if ( length < 1 ) {
						return;
					}

					if ( debug ) {
						logDebug( `${request.id}: using prefix "${prefixedRoutes.getLongestMatchingPrefix( path )}" on processing ${label} routing` );
					}

					matching = new Array( length );
					write = 0;

					for ( let i = 0; i < length; i++ ) {
						const route = routes[i];

						if ( debug ) {
							logDebug( `${request.id}: CHECK ${label} route "${route.source}" using pattern ${route.pattern}` );
						}

						let match = route.matcher( path );

						if ( match ) {
							const params = Object.create( null );
							const found = match.params;
							const defined = route.parameters;

							outer: // eslint-disable-line no-labels
							for ( const source of Object.keys( found ) ) {
								const value = found[source];

								for ( let j = 0; j < defined.length; j++ ) {
									const parameter = defined[j];

									if ( parameter.source === source && parameter.valueType ) {
										const error = service.RouteParameterTypeRegistry.getValidationError( parameter.valueType, value );

										if ( error ) { // eslint-disable-line max-depth
											logDebug( `${request.id}: IGNORE ${label} route "${route.source}" due to "${trim( value, 40 )}" does not match type ${parameter.valueType}: ${error}` ); // eslint-disable-line max-len
											match = false;
											break outer; // eslint-disable-line no-labels
										}

										params[parameter.name] = value;
										break;
									}
								}

								params[source] = value;
							}

							if ( match ) {
								match.params = params;

								matching[write++] = {
									label,
									isPolicy,
									late,
									route,
									match,
								};

								if ( debug ) {
									logDebug( `${request.id}: OBEY ${label} route "${route.source}" handled by ${trim( route.target )}` );
								}

								if ( !isPolicy ) {
									// processing controller routes we need only the
									// first matching one
									break;
								}
							}
						}
					}
				}


				/**
				 * step 2: process routes found matching request before
				 */
				if ( write > 0 ) {
					if ( debug ) {
						logDebug( `${request.id}: matching ${write} ${label} route(s)` );
					}

					matching.splice( write );

					for ( let i = 0; i < matching.length; i++ ) {
						await _processMatchingRoute( matching[i] ); // eslint-disable-line no-await-in-loop
					}

					if ( debug ) {
						logDebug( `${request.id}: passed ${label} routing` );
					}
				}
			}

			/**
			 * Invokes single matching route.
			 *
			 * @param {string} label human-readable label of routing stage this route belongs to
			 * @param {boolean} isPolicy true if current route is a policy
			 * @param {boolean} late indicates whether routes belong to late processing or not
			 * @param {Route} route route to be invoked
			 * @param {object} match result of matching some pattern on current request
			 * @returns {Promise<void>} promise for having checked and invoked any handler matching current request
			 * @private
			 */
			async function _processMatchingRoute( { label, isPolicy, late, route, match } ) {
				if ( requestContext.ignored ) {
					logDebug( `${request.id}: IGNORE ${label} route "${route.source}" handled by ${trim( route.target )}` );

					return;
				}

				// collect parameters passed to handler according to current route
				const localParams = match.params;
				const hasProperty = {}.hasOwnProperty;
				const source = request.params || {};

				for ( const name of Object.keys( source ) ) {
					if ( !hasProperty.call( localParams, name ) ) {
						localParams[name] = source[name];
					}
				}

				if ( debug ) {
					logDebug( `${request.id}: MATCH ${label} route "${route.source}" handled by ${trim( route.target )} w/ parameters %j`, localParams );
				}


				// wrap proxy around request object for injecting some route-specific properties
				const requestProxy = new Proxy( request, {
					get: ( target, prop, receiver ) => {
						switch ( prop ) {
							case "params" :
								return localParams;

							case "api" :
								return target[prop] || api;

							default :
								return Reflect.get( target, prop, receiver );
						}
					}
				} );

				// use proxy so that using `this` in a handler is accessing either
				// actual properties and methods of handler's component or
				// properties of request context as fallback
				const contextProxy = new Proxy( route.context || {}, {
					get: ( target, name, receiver ) => {
						if ( requestContext.hasOwnProperty( name ) ) {
							return requestContext[name];
						}

						return Reflect.get( target, name, receiver );
					},
				} );


				if ( isPolicy ) {
					// invoking a policy ...
					if ( late ) {
						requestContext.consumed.byPolicy = true;
					}

					await new Promise( ( resolve, reject ) => {
						const nextFn = error => {
							if ( error ) {
								reject( error );
							} else {
								resolve();
							}
						};

						const result = route.handler.call( contextProxy, requestProxy, response, nextFn, ...route.args );

						if ( result instanceof Promise ) {
							result.then( resolve ).catch( reject );
						}
					} );
				} else {
					// invoking a controller endpoint ...
					requestContext.consumed.byController = true;

					await route.handler.call( contextProxy, requestProxy, response, ...route.args );
				}
			}
		}

		/**
		 * Injects additional functions and information into provided request
		 * descriptor.
		 *
		 * @param {Hitchy.Core.RequestContext} requestContext some request's descriptor (usually exposed as `req` in a request handler)
		 * @returns {Hitchy.Core.RequestContext} provided request context with additional features injected
		 */
		static normalize( requestContext ) {
			const { request } = requestContext;
			const { headers } = request;
			const polyfills = this.RequestPolyfills;

			// synchronously inject instantly available properties
			for ( let i = 0; i < polyfills.length; i++ ) {
				const { header, name, apiName = name, handler, fallback } = polyfills[i];

				if ( apiName in request ) {
					// do not inject a polyfill for something existing
					continue;
				}

				if ( !header || header in headers ) {
					// either does not require some request header or required header exists
					// -> inject missing polyfill
					request[apiName] = api.service.RoutingPolyfillFactory[handler]( requestContext );
				} else if ( Array.isArray( fallback ) ) {
					request[apiName] = fallback.slice();
				} else if ( fallback != null ) {
					request[apiName] = fallback;
				}
			}

			return requestContext;
		}

		/**
		 * Provides alias for service exposed as api.service.LocalClientRequest.
		 *
		 * @returns {Hitchy.Core.ServiceComponent} client service for locally dispatching request
		 */
		static get client() {
			return service.LocalClientRequest;
		}
	}

	return Router;
}

export const appendFolders = false;
