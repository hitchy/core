export default function() { // eslint-disable-line jsdoc/require-jsdoc
	const api = this;
	const { service: Service } = api;

	/**
	 * Implements route collector managing separate sorted lists of routes per
	 * method either set of routes are bound to.
	 *
	 * @name RoutesPerMethod
	 * @property {Object<string,(Route[]|RoutesPerPrefix)>} methods maps name of method into sorted list of routes bound to it
	 * @property {boolean} isAdjustable indicates if further routes may be added or not
	 */
	class RoutesPerMethod {
		/** */
		constructor() {
			Object.defineProperties( this, {
				methods: { value: {} },
				isAdjustable: { value: true, configurable: true },
			} );
		}

		/**
		 * Adds route to proper lists according to the route's method.
		 *
		 * @param {Route} route route to be appended
		 * @returns {Route} provided route
		 */
		append( route ) {
			if ( !( route instanceof Service.Route ) ) {
				throw new TypeError( "invalid route to be added" );
			}

			if ( !this.isAdjustable ) {
				throw new Error( "must not append route to write-protected collector" );
			}


			const lists = this.methods;
			let method = route.method.trim().toUpperCase();

			if ( method === "*" ) {
				method = "ALL";
			}

			if ( !lists.hasOwnProperty( method ) ) {
				// got some new method -> always initialize with all previously
				// collected routes to be used with every method
				lists[method] = ( lists.ALL || [] ).slice( 0 );
			}

			lists[method].push( route );


			if ( method === "ALL" ) {
				// got a route not bound to any particular request method
				// -> bind this route to every other method's list of routes as well
				for ( const methodName of Object.keys( this.methods ) ) {
					if ( methodName !== "ALL" ) {
						this.methods[methodName].push( route );
					}
				}
			}


			return route;
		}

		/**
		 * Appends all routes managed in provided instance.
		 *
		 * @param {RoutesPerMethod} remote grouped routes to be merged into current set
		 * @returns {RoutesPerMethod} fluent interface
		 */
		concat( remote ) {
			if ( !( remote instanceof RoutesPerMethod ) ) {
				throw new TypeError( "invalid set of routes per method" );
			}

			if ( !remote.isAdjustable ) {
				throw new TypeError( "remote instance has been optimized before" );
			}

			if ( !this.isAdjustable ) {
				throw new TypeError( "can not add to write-protected instance" );
			}


			const sources = remote.methods;
			const targets = this.methods;

			for ( const methodName of Object.keys( sources ) ) {
				if ( !targets.hasOwnProperty( methodName ) ) {
					// got some new method -> always initialize with all previously
					// collected routes to be used with every method
					targets[methodName] = ( targets.ALL || [] ).slice( 0 );
				}

				targets[methodName] = targets[methodName].concat( sources[methodName] );
			}

			return this;
		}

		/**
		 * Retrieves list of all previously appended routes matching provided HTTP
		 * method.
		 *
		 * @note This method implicitly invokes optimizeByPrefix() if current
		 *       instance is still adjustable making it non-adjustable afterwards.
		 *
		 * @param {string} method name of HTTP method
		 * @returns {?RoutesPerPrefix} all previously appended routes to be obeyed on processing selected HTTP method
		 */
		onMethod( method ) {
			if ( typeof method !== "string" || method.trim().length < 1 ) {
				throw new TypeError( "invalid method to look up" );
			}

			if ( this.isAdjustable ) {
				this.optimizeByPrefix();
			}

			let _method = method.toUpperCase();

			switch ( _method ) {
				case "*" :
					_method = "ALL";

				// falls through
				default :
					return this.methods[_method] || this.methods.ALL || null;
			}
		}

		/**
		 * Replaces routes of every previously collected method with sets of routes
		 * grouped by prefix.
		 *
		 * @param {boolean} preferSpecific if true, routes with more specific prefix are preferred over those with more generic prefix
		 * @returns {RoutesPerMethod} fluent interface
		 */
		optimizeByPrefix( preferSpecific = false ) {
			if ( this.isAdjustable ) {
				for ( const methodName of Object.keys( this.methods ) ) {
					const source = this.methods[methodName];
					const target = new Service.RoutesPerPrefix( this._getPrefixes( source ) );

					for ( let i = 0, length = source.length; i < length; i++ ) {
						target.append( source[i] );
					}

					target.sortPerPrefix( preferSpecific );

					this.methods[methodName] = target;
				}

				Object.defineProperties( this, {
					isAdjustable: { value: false },
				} );
			}

			return this;
		}

		/**
		 * Extracts unique set of prefixes from provided list of routes.
		 *
		 * @param {Route[]} routes list of routes
		 * @returns {string[]} unique list of prefixes used in  provided routes
		 * @private
		 */
		_getPrefixes( routes ) {
			const prefixes = {};

			if ( Array.isArray( routes ) ) {
				routes.forEach( route => ( prefixes[route.prefix] = true ) );
			}

			return Object.keys( prefixes );
		}

		/**
		 * Returns description of currently managed groups of routes for debugging.
		 *
		 * @returns {string} description of currently collected routes
		 */
		dump() {
			const set = this.methods;
			const fix = !this.isAdjustable;

			return Object.keys( set )
				.sort()
				.map( name => {
					const prefix = `on ${name} requests (${fix ? "optimized" : "adjustable"}):\n`;

					if ( fix ) {
						return prefix + set[name].dump();
					}

					return prefix + set[name].map( route => ` - ${route.path}` ).join( "\n" );
				} )
				.join( "\n" );
		}
	}

	return RoutesPerMethod;
}

export const appendFolders = false;
