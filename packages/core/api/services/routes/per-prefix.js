export default function() { // eslint-disable-line jsdoc/require-jsdoc
	const api = this;
	const { service } = api;

	/**
	 * @name RoutesPerPrefix
	 * @property {boolean} extensible true if appending routes is probably extending prefixes
	 * @property {Object<string,Route[]>} prefixes
	 */
	class RoutesPerPrefix {
		/**
		 * @param {string[]} prefixes set of prefixes for initializing internally managed groups of routes per prefix
		 */
		constructor( prefixes = null ) {
			const value = {};

			if ( Array.isArray( prefixes ) ) {
				prefixes.forEach( name => ( value[name.length ? name : "/"] = [] ) );
			}

			Object.defineProperties( this, {
				extensible: { value: !Array.isArray( prefixes ) },
				prefixes: { value: value },
			} );
		}

		/**
		 * Fetches longest prefix of internally managed prefixes matching given
		 * one.
		 *
		 * @param {string} prefix prefix to be matched
		 * @returns {?string} longest prefix of locally managed groups sharing start with provided one
		 */
		getLongestMatchingPrefix( prefix ) {
			if ( typeof prefix !== "string" || prefix.trim().length < 1 ) {
				throw new TypeError( "invalid prefix to look up" );
			}

			const prefixLength = prefix.length;
			let longestLength = 0;
			let longest = null;

			for ( const item of Object.keys( this.prefixes ) ) {
				const itemLength = item.length;

				if ( itemLength <= prefixLength && itemLength > longestLength ) {
					if ( prefix.slice( 0, itemLength ) === item ) {
						longestLength = itemLength;
						longest = item;

						if ( itemLength === prefixLength ) {
							break;
						}
					}
				}
			}

			return longest;
		}

		/**
		 * Collects provided route in all prefix-related groups covering this
		 * route.
		 *
		 * @param {Route} route route to be collected
		 * @return {RoutesPerPrefix} fluent interface
		 */
		append( route ) {
			if ( !( route instanceof service.Route ) ) {
				throw new TypeError( "invalid route to append" );
			}

			const isPolicy = route instanceof service.PolicyRoute;
			const coveredPrefixes = route.selectProbablyCoveredPrefixes( Object.keys( this.prefixes ) );

			for ( const prefix of Object.keys( coveredPrefixes ) ) {
				const prefixList = this.prefixes[prefix];
				let found = false;

				if ( !isPolicy ) {
					// in dispatching routes only first one matching certain
					// pattern is obeyed
					// -> do not add multiple routes using exactly same pattern
					const numExisting = prefixList.length;

					for ( let i = 0; !found && i < numExisting; i++ ) {
						if ( prefixList[i].pattern.toString() === route.pattern.toString() ) {
							found = true;
						}
					}
				}

				if ( !found ) {
					prefixList.push( route );
				}
			}


			const prefix = route.prefix;

			if ( !this.prefixes.hasOwnProperty( prefix ) ) {
				if ( this.extensible ) {
					this.prefixes[prefix] = [route];
				}
			} else if ( !coveredPrefixes[prefix] ) {
				this.prefixes[prefix].push( route );
			}

			return this;
		}

		/**
		 * Iterates over all groups of prefixes sorting each group from routes
		 * matching generic routes to those matching more specific ones.
		 *
		 * @param {boolean} preferSpecific set true to prefer routes with more specific prefix over those with generic prefix in a set of routes per prefix
		 * @returns {void}
		 */
		sortPerPrefix( preferSpecific = false ) {
			const prefixes = Object.keys( this.prefixes );
			const numPrefixes = prefixes.length;

			for ( let p = 0; p < numPrefixes; p++ ) {
				const prefix = prefixes[p];
				const routes = this.prefixes[prefix];
				const numRoutes = routes.length;

				const sources = [];
				for ( let r = 0; r < numRoutes; r++ ) {
					const route = routes[r];

					if ( sources.indexOf( route.prefix ) < 0 ) {
						sources.push( route.prefix );
					}
				}

				if ( preferSpecific ) {
					sources.sort( ( l, r ) => r.length - l.length );
				} else {
					sources.sort( ( l, r ) => l.length - r.length );
				}

				const numSources = sources.length;
				const sorted = new Array( sources.length );

				for ( let s = 0; s < numSources; s++ ) {
					sorted[s] = [];
				}

				for ( let r = 0; r < numRoutes; r++ ) {
					const route = routes[r];

					sorted[sources.indexOf( route.prefix )].push( route );
				}

				this.prefixes[prefix] = [].concat( ...sorted );
			}
		}

		/**
		 * Fetches set of routes bound to longest prefix matching (by means of
		 * "covering" or "including") provided one.
		 *
		 * @param {string} prefix prefix to be covered by delivered routes
		 * @returns {Array<Route>} lists routes covering provided prefix
		 */
		onPrefix( prefix ) {
			const longestMatching = this.getLongestMatchingPrefix( prefix );
			if ( longestMatching !== null ) {
				return this.prefixes[longestMatching];
			}

			return [];
		}

		/**
		 * Retrieves description of current instance for debugging purposes.
		 *
		 * @returns {string} description of current instance
		 */
		dump() {
			const set = this.prefixes;

			return Object.keys( set )
				.sort()
				.map( name => `- ${name} includes:\n` +
				              set[name].map( route => `  - ${route.path}` ).join( "\n" )
				)
				.join( "\n" );
		}
	}

	return RoutesPerPrefix;
}

export const appendFolders = false;
