//
// NOTE:
//
//        DO NOT USE CMP HERE!
//
// This file may be imported() directly w/o support for CMP on rendering
// responses to early requests received during bootstrap, too.
//

/**
 * Exposes functions for generating polyfills for request and response
 * descriptors,
 */
export default class RoutingPolyfillFactory {
	static ExtensionToMime = {
		html: ["text/html"],
		xhtml: ["application/xhtml+xml"],
		json: [ "text/json", "application/json" ],
		text: ["text/plain"],
		xml: [ "text/xml", "application/xml" ],
		csv: ["text/csv"],
	};

	/**
	 * Maps short-cut aliases into MIME types to test a request's
	 * content-type header with.
	 *
	 * @type {Object<string, string>}
	 */
	static NormalizedIsTests = {
		text: "text/plain",
		multipart: "multipart/*",
		urlencoded: "application/x-www-form-urlencoded",
	};

	/**
	 * Extracts sorted list of MIME types accepted by client according to
	 * Accept header of provided request's header.
	 *
	 * @param {IncomingMessage} request request descriptor
	 * @returns {string[]} sorted list of MIME types
	 */
	static requestAccept( { request } ) {
		const { accept } = request.headers;
		if ( accept == null ) {
			return [];
		}

		const ptnProposal = /^\s*([^/\s]+\/[^;\s]+)(?:\s*;\s*q\s*=\s*(\d(?:\.\d+)?))?/i;
		const proposals = String( accept ).trim().split( /,/ );
		const numProposals = proposals.length;
		const collected = [];

		for ( let ri = 0; ri < numProposals; ri++ ) {
			const info = ptnProposal.exec( proposals[ri].trim() );

			if ( info ) {
				collected.push( {
					mime: info[1],
					quality: info[2] ? parseFloat( info[2] ) : 1,
					index: ri,
				} );
			}
		}

		collected.sort( ( l, r ) => ( l.quality === r.quality ? l.index - r.index : r.quality - l.quality ) );

		const numCollected = collected.length;

		for ( let ci = 0; ci < numCollected; ci++ ) {
			collected[ci] = collected[ci].mime.toLowerCase();
		}

		return collected;
	}

	/**
	 * Generates method request.body() for receiving and parsing provided
	 * request's body.
	 *
	 * @param {Hitchy.Core.API} [api] Hitchy API
	 * @param {IncomingMessage} request request descriptor
	 * @returns {(function((boolean|BodyParser)=): Promise<RawOrParsedBody>)} custom implementation of request.body() bound to provided request
	 */
	static requestBody( { api, request } ) { // eslint-disable-line consistent-this
		const { headers } = request;
		const mimePtn = /^([^,;]*)/;

		let rawBodyPromise = null;
		let gotRawBodyPromiseFromStream = false;
		const parsedBodyPromises = new Map();

		/**
		 * Extracts body data of current request.
		 *
		 * @name IncomingMessage#fetchBody
		 * @param {boolean|BodyParser} parse enables/disables body parsing, provide callback for actual parsing
		 * @returns {Promise<RawOrParsedBody>} promises extracted request body
		 */
		return ( parse = null ) => {
			if ( !rawBodyPromise ) {
				rawBodyPromise = new Promise( ( resolve, reject ) => {
					if ( request.body ) {
						// support existing output of bodyParser e.g. when injected into ExpressJS
						resolve( request.body );
					} else {
						gotRawBodyPromiseFromStream = true;

						const chunks = [];

						request.on( "data", chunk => chunks.push( chunk ) );
						request.once( "end", () => resolve( Buffer.concat( chunks ) ) );

						request.once( "error", reject );
						request.once( "aborted", reject );
					}
				} );
			}

			if ( request.body ) {
				// request.body might have been assigned since preparing raw promise
				// -> switch to promise whatever is provided in request.body now
				if ( gotRawBodyPromiseFromStream ) {
					rawBodyPromise = Promise.resolve( request.body );
				}

				return rawBodyPromise;
			}

			const configuredParser = api?.config?.bodyParser;
			const _parse = parse == null ? configuredParser == null ? true : configuredParser : parse;

			if ( _parse === false ) {
				return rawBodyPromise;
			}

			if ( !parsedBodyPromises.has( _parse ) ) {
				parsedBodyPromises.set( _parse, rawBodyPromise
					.then( body => {
						if ( !Buffer.isBuffer( body ) ) {
							return body;
						}

						return processBody( body, _parse );
					} ) );
			}

			return parsedBodyPromises.get( _parse );
		};

		/**
		 * Optionally parses provided body content according to selected parser
		 * configuration value.
		 *
		 * @param {Buffer} body body to be parsed
		 * @param {boolean|BodyParser} parse enables/disables parsing or provides actual parser as callback
		 * @return {RawOrParsedBody} raw or parsed content of provided body
		 */
		function processBody( body, parse ) {
			if ( parse ) {
				if ( typeof parse === "function" ) {
					return parse( body );
				}

				const mime = mimePtn.exec( headers["content-type"] ) || [];

				switch ( mime[1].trim().toLowerCase() ) {
					case "application/json" :
					case "text/json" :
						return JSON.parse( body.toString( "utf8" ) );

					case "application/x-www-form-urlencoded" : {
						const parser = api?.utility?.parser?.query;

						if ( typeof parser === "function" ) {
							return parser( body.toString( "utf8" ), { isEncodedData: true } );
						}

						return Object.fromEntries( new URLSearchParams( body.toString( "utf8" ) ).entries() );
					}

					default :
						return body;
				}
			}

			return body;
		}
	}

	/**
	 * Generates polyfill for request.ignore() method.
	 *
	 * @param {Hitchy.Core.RequestContext} requestContext context of dispatched request and its response
	 * @returns {(function(): void)} custom implementation of request.ignore() bound to provided context's request
	 */
	static requestIgnore( requestContext ) {
		return () => {
			requestContext.ignored = true;
			requestContext.consumed.byController = true;
		};
	}

	/**
	 * Generates polyfill for request.is() method.
	 *
	 * @param {IncomingMessage} request request descriptor
	 * @returns {function(string, ...string): (null|string|boolean)} custom implementation of request.is() bound to provided request
	 */
	static requestIs( { request } ) {
		const { headers } = request;

		/**
		 * Extracts properly sorted list of accepted types of response content from
		 * current request header.
		 *
		 * @param {string|RegExp|Array<(string|RegExp)>} test describes content type to test for
		 * @param {string|RegExp} additional further tests to obey
		 * @returns {boolean|string|RegExp} matching test out of provided ones, false on mismatch
		 */
		return ( test, ...additional ) => {
			if ( headers["transfer-encoding"] == null && !( headers["content-length"] > 0 ) ) {
				return null;
			}

			// always fail unless request comes with claimed type of request body
			const type = headers["content-type"];
			if ( type == null ) {
				return false;
			}

			// extract parts of client-provided MIME information
			const actual = /^\s*([^;/\s]+)\/([^;/\s]+)\s*/.exec( type );
			if ( !actual ) {
				throw new Error( "invalid content-type header: " + type );
			}

			// iterate over provided tests
			const [ , actualMajor, actualMinor ] = actual;
			const tests = ( Array.isArray( test ) ? test : [test] ).concat( additional );
			const numTests = tests.length;
			const patternTest = /([?.+(){}[\]^$])|([*])/g;
			const patternFixer = ( _, literal, quantifier ) => ( literal ? "\\" + literal : "." + quantifier );

			for ( let i = 0; i < numTests; i++ ) {
				const _test = tests[i];

				if ( _test instanceof RegExp ) {
					if ( _test.test( type ) ) {
						return `${actualMajor}/${actualMinor}`;
					}
				} else if ( typeof _test === "string" ) {
					const normalized = this.NormalizedIsTests[_test.toLowerCase()] || ( _test[0] === "+" ? "*/*" + _test : _test );

					const wanted = /^\s*([^;/\s]+)(\/([^;/\s]+))?\s*/.exec( normalized );
					if ( wanted ) {
						const [ , wantedMajor, full, wantedMinor ] = wanted;

						const majorTest = new RegExp( `^${wantedMajor.replace( patternTest, patternFixer )}$`, "i" );
						const minorTest = full ? new RegExp( `^${wantedMinor.replace( patternTest, patternFixer )}$`, "i" ) : null;

						if ( full ) {
							if ( majorTest.test( actualMajor ) && minorTest.test( actualMinor ) ) {
								return _test;
							}
						} else if ( majorTest.test( actualMajor ) || majorTest.test( actualMinor ) ) {
							return _test;
						}
					}
				}
			}

			return false;
		};
	}

	/**
	 * Extracts path name of URL in provided request.
	 *
	 * @param {IncomingMessage} request request descriptor
	 * @returns {string} extracted path name of provided request's folder
	 */
	static requestPath( { request } ) {
		const path = ( /^([^#?]*)(?:[#?]|$)/.exec( request.url ) || [] )[1].replace( /\/+$/, "" );

		return path == null || path === "" ? "/" : path;
	}

	/**
	 * Exposes parsed query parameters included with provided request's URL.
	 *
	 * @param {Hitchy.Core.API} api API of current Hitchy runtime
	 * @param {IncomingMessage} request request descriptor
	 * @returns {Object<string,string>} named query parameters of provided request's URL
	 */
	static requestQuery( { api, request } ) { // eslint-disable-line consistent-this
		const parser = api?.utility?.parser?.query;

		if ( typeof parser === "function" ) {
			return parser( new URL( request.url, "http://foo.com" ).search, { isEncodedData: true } );
		}

		return Object.fromEntries( new URL( request.url, "http://foo.com" ).searchParams );
	}

	/**
	 * Delivers manager for response in provided request context.
	 *
	 * @param {Hitchy.Core.RequestContext} requestContext context of a dispatched request and its response
	 * @returns {Hitchy.Core.ServerResponse} response manager
	 */
	static requestRes( requestContext ) {
		return requestContext.response;
	}

	// ------------------------------------------------------------------------

	/**
	 * Generates polyfill for response.format() method.
	 *
	 * @param {IncomingMessage} request request descriptor
	 * @param {ServerResponse} response response manager
	 * @returns {(function(Object<string,function>): ServerResponse)} custom implementation of response.format() bound to provided response
	 */
	static responseFormat( { request, response } ) {
		/**
		 * Implements simple version of `res.format()` as provided by expressjs.
		 *
		 * @name ServerResponse#format
		 * @param {Object<string,function>} formatHandlers maps type names into callbacks generating either kind of response
		 * @returns {ServerResponse} fluent interface
		 */
		return formatHandlers => {
			if ( !formatHandlers || typeof formatHandlers !== "object" ) {
				throw new TypeError( "invalid set of format handlers" );
			}

			const accept = request.accept || this.requestAccept( { request } ) || [];
			const numAccepted = accept.length;
			const handlerNames = Object.keys( formatHandlers );
			const numNames = handlerNames.length;


			// qualify handler names (for they might use extension names instead of MIME)
			const qualified = {};

			for ( let i = 0; i < numNames; i++ ) {
				const handlerName = handlerNames[i];
				const mapping = this.ExtensionToMime[handlerName] || [handlerName];

				for ( let j = 0, l = mapping.length; j < l; j++ ) {
					qualified[mapping[j]] = handlerName;
				}
			}

			const qualifiedNames = Object.keys( qualified );


			// iterate over accepted MIME types looking for the first one supported by current set of handlers
			let found = null;

			for ( let i = 0; found == null && i < numAccepted; i++ ) {
				const accepted = accept[i];

				if ( accepted === "*/*" || accepted === "*" ) {
					const candidates = [ "text/html", "text/json" ].concat( qualifiedNames );
					const numCandidates = candidates.length;

					for ( let j = 0; j < numCandidates; j++ ) {
						const name = candidates[j];

						if ( name !== "default" && qualified[name] ) {
							found = name;
							break;
						}
					}

					break;
				}

				if ( accepted.startsWith( "*/" ) || accepted.endsWith( "/*" ) ) {
					const [ aMajor, aMinor ] = accepted.split( "/" );

					for ( let j = 0; j < numNames; j++ ) {
						const qName = qualifiedNames[j];
						const [ nMajor, nMinor ] = qName.split( "/" );

						if ( ( aMajor === "*" || aMajor === nMajor ) && ( aMinor === "*" || aMinor === nMinor ) ) {
							found = qName;
							break;
						}
					}
				} else if ( accepted !== "default" && qualified[accepted] ) {
					found = accepted;
				}
			}

			// create response using found or default handler if available
			if ( found != null ) {
				response.setHeader( "Content-Type", found + "; charset=UTF-8" );
				formatHandlers[qualified[found]].call( this, request, response );
				return response;
			}

			if ( typeof formatHandlers.default === "function" ) {
				formatHandlers.default.call( this, request, response );
				return response;
			}

			response.writeHead( 406 );
			response.end();

			return response;
		};
	}

	/**
	 * Generates polyfill for response.json() method.
	 *
	 * @param {ServerResponse} response response manager
	 * @returns {function((Object|Array|string)): ServerResponse} custom implementation of response.json() bound to provided response
	 */
	static responseJson( { response } ) {
		/**
		 * Implements simple version of `res.json()` as provided by expressjs.
		 *
		 * @name ServerResponse#json
		 * @param {*} data data to be encoded and sent as JSON
		 * @returns {ServerResponse} fluent interface
		 */
		return data => {
			response.setHeader( "Content-Type", "application/json; charset=UTF-8" );
			response.end( JSON.stringify( data ) );

			return response;
		};
	}

	/**
	 * Generates polyfill for response.redirect() method.
	 *
	 * @param {ServerResponse} response response manager
	 * @returns {function((boolean|number|string)=, string=): ServerResponse} custom implementation of response.redirect() bound to provided response
	 */
	static responseRedirect( { response } ) {
		/**
		 * Implements simple version of `res.redirect()` as provided by expressjs.
		 *
		 * @name ServerResponse#redirect
		 * @param {boolean|int} code HTTP status code to be sent, true to redirect permanently
		 * @param {string} url URL to redirect to
		 * @returns {ServerResponse} fluent interface
		 */
		return ( code = 302, url = null ) => {
			let _url = url;
			let _code = code;

			if ( _url == null && typeof _code === "string" ) {
				_url = _code;
				_code = 302;
			}

			if ( _url == null || typeof _url !== "string" ) {
				throw new TypeError( "invalid URL for redirecting to" );
			}

			let status = parseInt( _code );
			if ( isNaN( status ) ) {
				status = _code ? 301 : 302;
			}

			response.writeHead( status, {
				Location: _url,
			} );
			response.end();

			return response;
		};
	}

	/**
	 * Generates polyfill for response.send() method.
	 *
	 * @param {ServerResponse} response response manager
	 * @returns {function(any): ServerResponse} custom implementation of response.send() bound to provided response
	 */
	static responseSend( { response } ) {
		/**
		 * Implements simple version of `res.send()` as provided by expressjs.
		 *
		 * @name ServerResponse#send
		 * @param {string|object} output data to be sent
		 * @returns {ServerResponse} fluent interface
		 */
		return ( output = "" ) => {
			if ( typeof output === "string" ) {
				if ( !response.getHeader( "content-type" ) ) {
					response.setHeader( "Content-Type", "text/plain; charset=UTF-8" );
				}

				response.end( output );
			} else if ( output instanceof Buffer ) {
				if ( !response.getHeader( "content-type" ) ) {
					response.setHeader( "Content-Type", "application/octet-stream" );
				}

				response.end( output );
			} else if ( output && typeof output === "object" ) {
				if ( !response.getHeader( "content-type" ) ) {
					response.setHeader( "Content-Type", "application/json; charset=UTF-8" );
				}

				response.end( JSON.stringify( output ) );
			} else {
				if ( !response.getHeader( "content-type" ) ) {
					response.setHeader( "Content-Type", "application/octet-stream" );
				}

				response.end();
			}

			return response;
		};
	}

	/**
	 * Generates polyfill for response.set() method.
	 *
	 * @param {ServerResponse} response response manager
	 * @returns {function((string|object), string=): ServerResponse} custom implementation of response.set() bound to provided response
	 */
	static responseSet( { response } ) {
		/**
		 * Implements simple version of `res.set()` as provided by expressjs.
		 *
		 * @see http://expressjs.com/en/4x/api.html#res.set
		 *
		 * @name ServerResponse#set
		 * @param {string|object} field name of single response header field to set or object containing names and values of multiple header fields to set
		 * @param {?string} value value of single response header field to set, ignored when providing multiple fields in first argument
		 * @returns {ServerResponse} fluent interface
		 */
		return ( field, value = null ) => {
			if ( typeof field === "string" ) {
				response.setHeader( field, value );
			} else if ( field && typeof field === "object" ) {
				for ( const key of Object.keys( field ) ) {
					response.setHeader( key, field[key] );
				}
			}

			return response;
		};
	}

	/**
	 * Generates polyfill for response.status() method.
	 *
	 * @param {ServerResponse} response response manager
	 * @returns {function(number): ServerResponse} custom implementation of response.status() bound to provided response
	 */
	static responseStatus( { response } ) {
		/**
		 * Implements simple version of `res.status()` as provided by expressjs.
		 *
		 * @see http://expressjs.com/en/4x/api.html#res.status
		 *
		 * @name ServerResponse#status
		 * @param {number} code HTTP status code to set
		 * @returns {ServerResponse} fluent interface
		 */
		return code => {
			response.statusCode = code;

			return response;
		};
	}

	/**
	 * Generates polyfill for response.type() method.
	 *
	 * @param {ServerResponse} response response manager
	 * @returns {function(string): ServerResponse} custom implementation of response.type() bound to provided response
	 */
	static responseType( { response } ) {
		/**
		 * Implements simple version of `res.type()` as provided by expressjs.
		 *
		 * @name ServerResponse#type
		 * @param {string} mime MIME type of content to be advertised in response header
		 * @returns {ServerResponse} fluent interface
		 */
		return mime => {
			switch ( mime ) {
				case "json" :
				case ".json" :
				case "text/json" :
				case "application/json" :
					response.setHeader( "Content-Type", "application/json; charset=UTF-8" );
					break;

				case "html" :
				case ".html" :
				case "text/html" :
					response.setHeader( "Content-Type", "text/html; charset=UTF-8" );
					break;

				case "xml" :
				case ".xml" :
				case "text/xml" :
				case "application/xml" :
					response.setHeader( "Content-Type", "application/xml; charset=UTF-8" );
					break;

				case "text" :
				case "txt" :
				case ".txt" :
				case "text/plain" :
					response.setHeader( "Content-Type", "text/plain; charset=UTF-8" );
					break;

				case "png" :
				case ".png" :
				case "image/png" :
					response.setHeader( "Content-Type", "image/png" );
					break;

				case "jpg" :
				case "jpeg" :
				case ".jpg" :
				case ".jpeg" :
				case "image/jpeg" :
					response.setHeader( "Content-Type", "image/jpeg" );
					break;

				case "gif" :
				case ".gif" :
				case "image/gif" :
					response.setHeader( "Content-Type", "image/gif" );
					break;
			}

			return response;
		};
	}
}

export const useCMP = false;
