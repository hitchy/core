/** */
export default function() {
	/**
	 * Implements generic service for managing a sequence of processes with
	 * every other process enqueued being invoked once the previously invoked
	 * process has finished.
	 */
	return class Sequence {
		#list = [];

		/**
		 * Schedules another callback to be invoked once all currently existing
		 * callbacks in the sequence have finished.
		 *
		 * @param {() => (any|Promise<any>)} fn callback invoked once all previously schedules callbacks have finished
		 * @param {any} id unique identifier of provided callback, defaults to the callback itself
		 * @returns {Promise<any>} promise for given callback's result
		 */
		append( fn, id = undefined ) {
			return this.#enqueue( false, fn, id );
		}

		/**
		 * Schedules another callback to be invoked after some currently running
		 * callback has finished. If sequence is currently empty, the callback
		 * will be invoked right away.
		 *
		 * @param {() => (any|Promise<any>)} fn callback invoked once all previously schedules callbacks have finished
		 * @param {any} id unique identifier of provided callback, defaults to the callback itself
		 * @returns {Promise<any>} promise for given callback's result
		 */
		prepend( fn, id = undefined ) {
			return this.#enqueue( true, fn, id );
		}

		/**
		 * Indicates if uniquely selected callback is part of sequence or not.
		 *
		 * @param {any} id some callback's unique identifier
		 * @returns {boolean} true if identifier is associated with existing callback of sequence, false otherwise
		 */
		has( id ) {
			const list = this.#list;
			const count = list.length;

			for ( let i = 0; i < count; i++ ) {
				if ( list[i].id === id ) {
					return true;
				}
			}

			return false;
		}

		/**
		 * Detects if provided identifier is associated with currently running
		 * callback of sequence or not.
		 *
		 * @param {any} id some callback's unique identifier
		 * @returns {boolean} true if sequence isn't empty and currently running callback is associated with provided identifier, false otherwise
		 */
		isCurrent( id ) {
			const list = this.#list;
			const count = list.length;

			return count > 0 && list[0].id === id;
		}

		/**
		 * Indicates if sequence is currently empty or not.
		 *
		 * @returns {boolean} true if sequence is empty, false otherwise
		 */
		isEmpty() {
			return this.#list.length === 0;
		}

		/**
		 * Returns promise settled once the identified callback has finished.
		 *
		 * @param {any} id some callback's unique identifier
		 * @returns {Promise<any>|undefined} promise settled by identified callback, undefined if no callback is associated with provided identifier
		 */
		waitFor( id ) {
			const list = this.#list;
			const count = list.length;

			for ( let i = 0; i < count; i++ ) {
				const { id: _id, promise } = list[i];

				if ( _id === id ) {
					return promise;
				}
			}

			return undefined;
		}

		/**
		 * Creates promise settled once the sequence of callbacks has finished.
		 *
		 * @returns {Promise<void>} promise resolved if no callback is left in sequence
		 */
		whenIdle() {
			return new Promise( resolve => {
				const check = () => {
					const { length } = this.#list;

					if ( length === 0 ) {
						resolve();
					} else {
						this.#list[length - 1].promise.finally( check );
					}
				};

				check();
			} );
		}

		/**
		 * Schedules another callback to be invoked either after some currently
		 * running callback has finished or once all currently existing
		 * callbacks in the sequence have finished.
		 *
		 * @param {boolean} asap if true, the callback will be invoked after currently running callback
		 * @param {() => (any|Promise<any>)} fn callback invoked once all previously schedules callbacks have finished
		 * @param {any} id unique identifier of provided callback, defaults to the callback itself
		 * @returns {Promise<any>} promise for given callback's result
		 */
		#enqueue( asap, fn, id = undefined ) {
			let done;
			let fail;

			if ( typeof fn !== "function" ) {
				throw new TypeError( "Sequence: callback must be a function" );
			}

			const promise = new Promise( ( resolve, reject ) => {
				done = resolve;
				fail = reject;
			} );

			const handle = {
				id: id ?? fn,
				handler: post => Promise.resolve( fn() ).finally( post ).then( done ).catch( fail ),
				promise,
			};

			if ( asap && this.#list.length > 0 ) {
				this.#list.splice( 1, 0, handle );
			} else {
				this.#list.push( handle );
			}

			if ( this.#list.length === 1 ) {
				this.#next();
			}

			return promise;
		}

		/**
		 * Resumes sequence of callbacks by invoking first scheduled callback.
		 * When callback is done, this method is automatically invoked to switch
		 * to next callback in sequence.
		 */
		#next() {
			if ( this.#list.length > 0 ) {
				this.#list[0].handler( () => this.#list.shift() );

				this.#list[0].promise.finally( () => this.#next() );
			}
		}
	};
}
