import { randomBytes } from "node:crypto";

const PtnUuidParser = /^[0-9a-f]{8}(?:-[0-9a-f]{4}){3}-[0-9a-f]{12}$/i;
const PtnUuidFormatter = /^(.{8})(.{4})(.{4})(.{4})(.{12})$/;

/** */
export default function() {
	/**
	 * Implements general-purpose support for dealing with v4 UUIDs.
	 */
	class Uuid {
		/**
		 * Creates buffer containing binary UUID.
		 *
		 * @returns {Promise<Buffer>} promises buffer with binary UUID
		 */
		static create() {
			return new Promise( ( resolve, reject ) => {
				randomBytes( 16, ( error, buffer ) => {
					if ( error ) {
						reject( new Error( "fetching random data failed: " + error ) );
						return;
					}

					// mark buffer to contain UUIDv4
					buffer[6] = ( buffer[6] & 0x0f ) | 0x40;
					buffer[8] = ( buffer[8] & 0x3f ) | 0x80;

					resolve( buffer );
				} );
			} );
		}

		/**
		 * Renders provided UUID as string.
		 *
		 * @param {Buffer} uuid buffer containing normalized UUID
		 * @returns {string} representation of provided UUID as string
		 */
		static format( uuid ) {
			if ( !Buffer.isBuffer( uuid ) || uuid.length < 16 ) {
				throw new TypeError( "UUID must be provided as buffer of sufficient size" );
			}

			return uuid.toString( "hex" ).toLowerCase().slice( 0 , 32 )
				.replace( PtnUuidFormatter, "$1-$2-$3-$4-$5" );
		}

		/**
		 * Detects if provided value is a UUID.
		 *
		 * @param {*} value possible binary UUID or representation of UUID as string
		 * @returns {boolean} true if value is considered valid UUID
		 */
		static isUUID( value ) {
			if ( !value ) {
				return false;
			}

			if ( Buffer.isBuffer( value ) && value.length >= 16 ) {
				return true;
			}

			return typeof value === "string" && PtnUuidParser.test( value );
		}

		/**
		 * Normalizes provided value to be binary UUID.
		 *
		 * @param {*} value value to be normalized
		 * @returns {Buffer} found UUID as binary buffer of 16 octets
		 * @throws TypeError on providing value that can not be converted to UUID
		 */
		static normalize( value ) {
			if ( value != null ) {
				if ( Buffer.isBuffer( value ) ) {
					if ( value.length === 16 ) {
						return value;
					}

					if ( value.length >= 16 ) {
						return value.slice( 0, 16 );
					}
				} else if ( typeof value === "string" ) {
					const trimmed = value.trim();

					if ( PtnUuidParser.test( trimmed ) ) {
						return Buffer.from( trimmed.replace( /-/g, "" ), "hex" );
					}
				}

				throw new TypeError( "provided value is not a UUID" );
			}

			return null;
		}

		/**
		 * Implements extended toString() method for use with binary UUIDs to
		 * support their convenient conversion to UUIDs formatted as string.
		 *
		 * This function is expected to be invoked in context of a Buffer
		 * instance with 16 octets representing the UUID to render. The method
		 * should be assigned to instances of Buffer assumed to contain a UUID.
		 *
		 *     @example
		 *     const binaryUuid = Buffer.from( [ ... 16 octet values ... ] );
		 *     binaryUuid.toString = api.service.Uuid.toString;
		 *
		 * When invoked without any argument or when invoked with single
		 * argument `"uuid"`, a properly formatted UUID is returned as string.
		 * Otherwise, the original toString() method of `Buffer` is invoked.
		 *
		 * @param {any[]} args lists arguments provided to toString(), if provided, the original toString() of Buffer is invoked
		 * @returns {string} UUID rendered as string if invoked without any arguments, otherwise result of invoking Buffer's original `toString()` on `this`
		 */
		static toString( ...args ) {
			if ( args.length > 0 && ( args.length !== 1 || args[0] !== "uuid" ) ) {
				return this.constructor.prototype.toString.call( this, ...args );
			}

			return Uuid.format( this );
		}
	}

	Object.defineProperties( Uuid, {
		/**
		 * Exposes pattern detecting whether some string is properly representing
		 * UUID or not.
		 *
		 * @name Uuid.ptnUuid
		 * @property {RegExp}
		 * @readonly
		 */
		ptnUuid: { value: PtnUuidParser },
	} );

	return Uuid;
};
