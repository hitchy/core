export const useCMP = false;

/**
 * Implements methods for validating data to represent value(s) of a certain
 * type.
 */
export default class ValueParser {
	/**
	 * Matches a UUID.
	 *
	 * @type {RegExp}
	 */
	static PtnUuid = /^[0-9a-f]{8}(?:-[0-9a-f]{4}){3}-[0-9a-f]{12}$/i;

	/**
	 * Matches an integer.
	 *
	 * @type {RegExp}
	 */
	static PtnInteger = /^[+-]?\d{1,16}$/;

	/**
	 * Matches a number.
	 *
	 * @type {RegExp}
	 */
	static PtnNumber = /^[+-]?\d{1,16}(?:\.\d{1,16})?$/;

	/**
	 * Matches a keyword.
	 *
	 * @type {RegExp}
	 */
	static PtnKeyword = /^[_\p{ID_Start}][_\p{ID_Continue}]{0,63}$/u;

	/**
	 * Checks if provided value(s) is/are valid UUID(s).
	 *
	 * @param {string|string[]} value value(s) to be checked
	 * @returns {boolean} true if value(s) is/are valid UUID(s)
	 */
	static isUuid( value ) {
		if ( Array.isArray( value ) ) {
			for ( let i = 0; i < value.length; i++ ) {
				if ( !this.PtnUuid.test( value[i] ) ) {
					return false;
				}
			}

			return true;
		}

		return this.PtnUuid.test( value );
	}

	/**
	 * Checks if provided value(s) is/are valid integer(s).
	 *
	 * @param {string|string[]} value value(s) to be checked
	 * @returns {boolean} true if value(s) is/are valid integer(s)
	 */
	static isInteger( value ) {
		if ( Array.isArray( value ) ) {
			for ( let i = 0; i < value.length; i++ ) {
				if ( !this.PtnInteger.test( value[i] ) ) {
					return false;
				}
			}

			return true;
		}

		return this.PtnInteger.test( value );
	}

	/**
	 * Checks if provided value(s) is/are valid number(s).
	 *
	 * @param {string|string[]} value value(s) to be checked
	 * @returns {boolean} true if value(s) is/are valid number(s)
	 */
	static isNumber( value ) {
		if ( Array.isArray( value ) ) {
			for ( let i = 0; i < value.length; i++ ) {
				if ( !this.PtnNumber.test( value[i] ) ) {
					return false;
				}
			}

			return true;
		}

		return this.PtnNumber.test( value );
	}

	/**
	 * Checks if provided value(s) is/are valid keyword(s).
	 *
	 * @param {string|string[]} value value(s) to be checked
	 * @returns {boolean} true if value(s) is/are valid keyword(s)
	 */
	static isKeyword( value ) {
		if ( Array.isArray( value ) ) {
			for ( let i = 0; i < value.length; i++ ) {
				if ( !this.PtnKeyword.test( value[i] ) ) {
					return false;
				}
			}

			return true;
		}

		return this.PtnKeyword.test( value );
	}
}
