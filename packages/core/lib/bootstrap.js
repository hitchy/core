import { env } from "node:process";
import { deepSeal } from "./misc/object.js";
import { createAPI } from "./misc/api.js";

/**
 * Loads Hitchy framework incl. all discoverable plugins and initializes core
 * and all found plugins prior to delivering API for using framework finally.
 *
 * @param {Hitchy.Core.Options=} options options for customizing hitchy
 * @returns {Promise<Hitchy.Core.API>} promises API of initialized hitchy web framework
 */
export default async function( options = {} ) {
	let api; // eslint-disable-line consistent-this

	try {
		/** @type {Hitchy.Core.API} */
		api = await createAPI( options ); // eslint-disable-line consistent-this

		// load library of core and expose in API
		const { bootstrap, service } = api;

		// detect context framework is running in
		const qualifiedOptions = await bootstrap.triangulate();

		// add environment to options
		qualifiedOptions.environment ??= { ...env };

		// discover all further core components as well as plugins
		const plugins = await bootstrap.discover( qualifiedOptions );

		// injecting shutdown code as early as possible
		bootstrap.shutdown = bootstrap.prepareShutdown( plugins );

		// gather application API to be exposed
		await bootstrap.expose( plugins );

		// following exposure of all components and prior to configuration, the
		// project's optionally available .env file is processed to augment
		// environment variables included with the options passed along so far
		Object.assign( qualifiedOptions.environment, await api.service.Dotenv.read() );

		// configure all plugins
		await service.Configuration.gather( plugins );

		// initialize plugins
		await bootstrap.initialize( plugins );

		// collect all finally desired routes
		await service.Router.configure( plugins );

		// eventually seal compiled Hitchy API
		return deepSeal( api, path => path[0] !== "data" );
	} catch ( cause ) {
		Object.defineProperties( cause, {
			hitchy: { value: api },
		} );

		throw cause;
	}
}
