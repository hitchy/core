import Path from "node:path";

import PromiseUtils from "promise-essentials";

import { cmp } from "../misc/cmp.js";

/**
 * Provides implementation for first stage of bootstrapping hitchy instance by
 * discovering available plugins.
 *
 * @this {Hitchy.Core.API}
 * @param {Hitchy.Core.Options} options global options customizing Hitchy
 * @returns {function():Promise.<Hitchy.Core.PluginHandle[]>} function for discovering plugins
 */
export default function( options ) {
	const api = this;
	let logInfo;

	return _bootstrapDiscover;


	/**
	 * Discovers plugins available with core distribution of Hitchy as well as
	 * in locally installed npm packages.
	 *
	 * @returns {Promise.<Hitchy.Core.PluginHandle[]>} lists descriptions of discovered plugins
	 * @private
	 */
	function _bootstrapDiscover() {
		if ( !options.projectFolder ) {
			return Promise.reject( new Error( "missing information on application root folder" ) );
		}

		logInfo = api.log( "hitchy:bootstrap:info" );

		options.projectFolder = Path.resolve( options.projectFolder );

		return readApplicationMeta( options.projectFolder )
			.then( () => import( "./discover/plugins.js" ) )
			.then( ( { default: fn } ) => fn.call( api, options )() )
			.then( plugins => readMetaData( plugins ) )
			.then( dropPluginsWithoutMeta )
			.then( loadPluginAPIs )
			.then( dropPluginsWithoutRole )
			.then( sortByWeight )
			.then( promoteAPIs );
	}

	/**
	 * Compiles meta information on application or one of its plugins from
	 * content of either one's package.json and hitchy.json files.
	 *
	 * @param {object} packageJson content of application's or some plugin's package.json file
	 * @param {object} hitchyJson content of application's or some plugin's hitchy.json file
	 * @returns {object} meta information on application or some plugin
	 */
	function compileMeta( packageJson, hitchyJson ) {
		return api.utility.object.merge( {}, [ packageJson.hitchy, {
			version: packageJson.version,
			license: packageJson.license,
			// inject type of project for properly importing files with extension .js
			type: packageJson.type,
		}, hitchyJson ] );
	}

	/**
	 * Compiles application's meta information by merging content of its
	 * package.json and hitchy.json files and injects the resulting record as
	 * `meta` into Hitchy API of current runtime.
	 *
	 * @returns {Promise<void>} promises meta information read from files package.json and hitchy.json of application and exposed in Hitchy's API
	 */
	async function readApplicationMeta() {
		const { file } = api.utility;
		let hitchyJson;

		try {
			const text = await file.read( [ options.projectFolder, "hitchy.json" ], { encoding: "utf-8" } );
			hitchyJson = text ? JSON.parse( text ) : {};
		} catch ( cause ) {
			if ( cause.code === "ENOENT" ) {
				hitchyJson = {};
			} else {
				throw cause;
			}
		}

		let packageJson;

		try {
			const text = await file.read( [ options.projectFolder, "package.json" ], { encoding: "utf-8" } );
			packageJson = text ? JSON.parse( text ) : {};
		} catch {
			packageJson = {};
		}

		api.meta = compileMeta( packageJson, hitchyJson );
	}

	/**
	 * Reads hitchy.json files of all detected plugin candidates.
	 *
	 * @param {string[]} pluginPathNames path names of plugin candidates
	 * @returns {Promise.<Hitchy.Core.PluginHandle[]>} promises package data of all candidates
	 */
	function readMetaData( pluginPathNames ) {
		const { file } = api.utility;

		return Promise.all( pluginPathNames.map( pluginPathName => {
			const name = Path.basename( pluginPathName );
			const handle = {
				name: name,
				staticRole: name,
				folder: pluginPathName,
				meta: {}
			};

			return readBeaconFile( [ handle.folder, "hitchy.json" ], { failIfMissing: true } )
				.catch( function( error ) {
					if ( error.code !== "ENOENT" ) {
						logInfo( "failed reading hitchy.json in folder %s", handle.folder );
					}
				} )
				.then( async meta => {
					let _meta = meta;

					if ( _meta && typeof _meta === "object" ) {
						_meta.$valid = true;
					} else {
						_meta = {};
					}

					if ( _meta.role ) {
						handle.staticRole = _meta.role = String( _meta.role ).trim();
					}

					let packageJson;

					try {
						packageJson = JSON.parse( await file.read( [ options.projectFolder, "package.json" ], { encoding: "utf-8" } ) ) || {};
					} catch {
						packageJson = {};
					}

					handle.meta = compileMeta( packageJson, _meta );

					return handle;
				} );
		} ) );
	}

	/**
	 * Drops all plugin candidates that do not comply with properly describing
	 * a hitchy plugin.
	 *
	 * @param {Hitchy.Core.PluginHandle[]} pluginHandles raw list of descriptions of discovered plugins
	 * @returns {Hitchy.Core.PluginHandle[]} list of complying plugins' descriptions
	 */
	function dropPluginsWithoutMeta( pluginHandles ) {
		const length = pluginHandles.length;
		const filtered = new Array( length );
		let write = 0;

		for ( let read = 0; read < length; read++ ) {
			const handle = pluginHandles[read];

			if ( handle.meta.$valid ) {
				filtered[write++] = handle;
			}
		}

		filtered.splice( write, length - write );

		return filtered;
	}

	/**
	 * Loads all plugins in order of provided plugin handles.
	 *
	 * Achievements:
	 *  * Loads all discovered plugins.
	 *  * Fetches API of either plugin.
	 *  * Qualifies meta data and requested role per plugin.
	 *  * Detects plugins claiming to fill the same role.
	 *
	 * @param {Hitchy.Core.PluginHandle[]} handles description of discovered plugins
	 * @return {Promise<Hitchy.Core.PluginHandle[]>} same description as provided, extended by either plugins basically integrating API
	 */
	function loadPluginAPIs( handles ) {
		const allLoadedPluginsByName = {};

		handles.forEach( handle => {
			const name = handle.name;
			if ( allLoadedPluginsByName.hasOwnProperty( name ) ) {
				logInfo( `double discovery of plugin ${name}` );
			}

			allLoadedPluginsByName[name] = handle;
		} );

		return new Promise( ( resolve, reject ) => {
			loadPlugin( 0, handles.length );

			/**
			 * Loads another plugin from list of discovered plugins.
			 *
			 * @param {int} nextIndex index into list of plugin descriptions
			 * @param {int} count cached number of available plugin descriptions
			 * @returns {void}
			 * @private
			 */
			function loadPlugin( nextIndex, count ) {
				if ( nextIndex >= count ) {
					resolve( handles );
					return;
				}

				// load next plugin in list
				const handle = handles[nextIndex];


				cmp( api, options, handle.meta, undefined, handle.folder, allLoadedPluginsByName, handle )
					.then( compilePluginIntegrationApi )
					.catch( _fail );


				/**
				 * Collects important elements from integrating API of loaded
				 * plugin.
				 *
				 * @param {object} _api API exposed by loaded plugin's entry file
				 * @returns {void}
				 * @private
				 */
				function compilePluginIntegrationApi( _api ) {
					let __api = _api;
					if ( !__api ) {
						logInfo( `plugin ${handle.name} does not export any API` );
						__api = {};
					}

					const dynamicMeta = __api.$meta || {};
					const dynamicRole = String( dynamicMeta.role ?? "" ).trim();

					// inject meta information qualified by obeying extra info
					// provided by plugin's code providing API itself
					__api.$meta = api.utility.object.merge( handle.meta, dynamicMeta );

					// inject name of plugin
					__api.$name = handle.name;

					// manage plugin's role
					if ( dynamicRole ) {
						// plugin dynamically declares to fill some role
						// -> revoke this role from any other plugin
						//    declaring it statically, only
						// -> ensure this plugin is only one declaring
						//    dynamically to fill this role
						for ( let i = 0, length = handles.length; i < length; i++ ) {
							const iter = handles[i];

							if ( iter.staticRole === dynamicRole ) {
								logInfo( `dropping static claim for role ${dynamicRole} of ${iter.name} in favor of dynamic claim of ${handle.name}` );
								iter.staticRole = null;
							}

							if ( dynamicRole === iter.api?.$role ) {
								reject( new Error( `multiple plugins claim role ${dynamicRole}: found ${iter.name} and ${handle.name}` ) );
								return;
							}
						}

						// expose role claimed to fill by plugin to approve it
						__api.$role = dynamicRole;
					}


					// track reference on plugin's API in handle as well
					handle.api = __api;

					// start loading next plugin in list (w/o wasting stack frames)
					process.nextTick( loadPlugin, nextIndex + 1, count );
				}

				/**
				 * Normalizes error message on failed loading a discovered
				 * plugin's integration API.
				 *
				 * @param {Error} error actually encountered error
				 * @returns {void}
				 * @private
				 */
				function _fail( error ) {
					// TODO add support for optionally keeping hitchy bootstrapping
					//      any other plugin (by invoking setTimeout() above instead of reject)
					logInfo( "Loading plugin %s failed: %s", handle.name, error );

					reject( new Error( `loading plugin ${handle.name} failed: ${String( error.message || error || "unknown error" )}` ) );
				}
			}
		} )
			.then( pluginHandles => {
				// make sure any plugin declaring undisputed role statically
				// is considered filling that role eventually
				pluginHandles.forEach( handle => {
					if ( !handle.api.$role && handle.staticRole ) {
						handle.api.$role = handle.staticRole;
					}
				} );

				// permit every plugin to handle final discovery of all
				// available plugins (now being able to access either plugin's
				// API e.g. to overload/extend it on replacing plugin).
				return PromiseUtils.each( pluginHandles, handle => {
					if ( handle.api.$role && typeof handle.api.onDiscovered === "function" ) {
						return handle.api.onDiscovered.call( api, options, allLoadedPluginsByName, handle );
					}

					return undefined;
				} );
			} );
	}

	/**
	 * Removes all discovered plugins fail to claim a role in Hitchy application.
	 *
	 * @note Every plugin is assumed to claim a role. Different plugins might
	 *       claim the same role, e.g. "odm" to indicate they compete with each
	 *       other in providing a particular API for an application.
	 *
	 * @param {Hitchy.Core.PluginHandle[]} handles descriptions of discovered plugins
	 * @returns {Hitchy.Core.PluginHandle[]} descriptions of plugins properly claiming a role
	 */
	function dropPluginsWithoutRole( handles ) {
		return handles.filter( handle => handle && handle.api.$role );
	}

	/**
	 * Sorts plugins according to dependencies provided by either plugin.
	 *
	 * @param {Hitchy.Core.PluginHandle[]} discoveredPlugins descriptions of discovered plugins
	 * @return {Promise<Hitchy.Core.PluginHandle[]>} descriptions of discovered plugins sorted in order of depending on each other
	 */
	async function sortByWeight( discoveredPlugins ) {
		// create index for accessing either discovered plugin by the role it's serving
		const pluginByRole = {};
		const weights = {};

		for ( let i = 0; i < discoveredPlugins.length; i++ ) {
			const plugin = discoveredPlugins[i];
			const role = plugin.api.$role;

			if ( pluginByRole.hasOwnProperty( role ) ) {
				const error = new Error( "multiple plugins are claiming to take same role: " + role );

				logInfo( error.message );

				throw error;
			}

			pluginByRole[role] = plugin;
		}

		// read hitchy.json file of application for custom definition of roles it depends on
		const appMeta = await readBeaconFile( [ options.projectFolder, "hitchy.json" ], { keepMissingDependencies: true } );

		// create collection of enabled plugins based on this sorted set of sources:
		//  1. prefer plugins serving roles declared as dependencies on CLI
		//  2. prefer plugins serving roles declared as dependencies in application's hitchy.json file
		//  3. use all discovered plugins
		let enabledPlugins;

		if ( options.dependencies ) {
			if ( typeof options.dependencies === "string" ) {
				options.dependencies = options.dependencies.trim().split( /[,\s]+/ );
			}

			if ( Array.isArray( options.dependencies ) ) {
				appMeta.dependencies = options.dependencies;
			}
		}

		if ( Array.isArray( appMeta.dependencies ) ) {
			for ( let i = 0; i < appMeta.dependencies.length; i++ ) {
				const trimmed = String( appMeta.dependencies[i] ?? "" ).trim();

				if ( trimmed ) {
					appMeta.dependencies[i] = trimmed;
				} else {
					appMeta.dependencies.splice( i--, 1 );
				}
			}

			enabledPlugins = _getHandlesOfRoles( appMeta.dependencies, "hitchy.json file of project" );
		} else {
			enabledPlugins = discoveredPlugins;
		}

		// discovered plugins may claim that other plugins depend on them
		// -> inject proper declaration of dependency for any enabled
		//    plugin addressed as dependant of a discovered plugin
		let added;

		do {
			added = 0;

			for ( let i = 0; i < discoveredPlugins.length; i++ ) {
				const plugin = discoveredPlugins[i];
				const role = plugin.api.$role;
				const dependingRoles = plugin.api.$meta.dependants || [];

				for ( let j = 0; j < dependingRoles.length; j++ ) {
					const dependingRole = String( dependingRoles[j] ?? "" ).trim();

					if ( !dependingRole ) {
						continue;
					}

					const dependingPlugin = pluginByRole[dependingRole];

					if ( dependingPlugin && enabledPlugins.some( p => p === dependingPlugin ) ) {
						const meta = dependingPlugin.api.$meta;

						if ( Array.isArray( meta.dependencies ) ) {
							if ( !meta.dependencies.some( r => r === role ) ) {
								meta.dependencies.push( role );
								added++;
							}
						} else {
							meta.dependencies = [role];
							added++;
						}
					}
				}
			}
		} while ( added > 0 );

		// recursively enable dependencies of either enabled plugin
		do {
			added = 0;

			for ( let i = 0; i < enabledPlugins.length; i++ ) {
				const plugin = enabledPlugins[i];
				const dependenciesOfPlugin = plugin.api.$meta.dependencies || [];

				for ( let j = 0; j < dependenciesOfPlugin.length; j++ ) {
					let role = String( dependenciesOfPlugin[j] ?? "" ).trim();

					const optional = role.startsWith( "?" );

					if ( optional ) {
						role = role.substring( 1 ).trim();
					}

					if ( !role ) {
						continue;
					}

					const dependency = pluginByRole[role];

					if ( !dependency ) {
						if ( !optional ) {
							throw new Error( `missing plugin for role ${role} as required by ${plugin.api.$name}` );
						}
					} else if ( !enabledPlugins.some( p => p === dependency ) ) {
						enabledPlugins.push( dependency );
						added++;
					}
				}
			}
		} while ( added > 0 );

		// calculate dependency weights per enabled plugin
		enabledPlugins.forEach( handle => countRequests( handle.api ) );

		if ( options.debug ) {
			discoveredPlugins.forEach( function( handle ) {
				logInfo( `dependency weight of role ${handle.api.$role} filled by plugin ${handle.name} is ${weights[handle.api.$role]}` );
			} );
		}

		const sorted = discoveredPlugins
			// drop plugins w/o weight (not required by any enabled one)
			.filter( handle => weights[handle.api.$role] > 0 )
			// sort left handles from high weight to low
			.sort( ( left, right ) => ( weights[right.api.$role] || 0 ) - ( weights[left.api.$role] || 0 ) );

		// inject index per handle enabling tests assessing sorting
		sorted.forEach( ( handle, i ) => ( handle.api.$index = i ) );

		if ( options.debug ) {
			logInfo( `resulting sorting order of plugins is:` );
			sorted.forEach( ( handle, i ) => {
				logInfo( `${i + 1}. ${handle.name} (${handle.api.$role})` );
			} );
		}

		return sorted;


		/**
		 * Counts immediate and mediate dependencies of a given plugin.
		 *
		 * @param {Hitchy.Core.PluginAPI} plugin description of single plugin
		 * @param {string=} initial role of dependency counted before (used to
		 *        detect circular dependencies)
		 * @returns {void}
		 */
		function countRequests( plugin, initial ) {
			const role = plugin.$role;

			let dependencies = plugin.$meta.dependencies || [];

			if ( !Array.isArray( dependencies ) ) {
				dependencies = dependencies ? [dependencies] : [];
			}

			weights[role] = ( weights[role] || 0 ) + 1;

			for ( let i = 0; i < dependencies.length; i++ ) {
				let dependency = dependencies[i];
				const isOptional = dependency.startsWith( "?" );

				if ( isOptional ) {
					dependency = dependency.substring( 1 ).trim();
				}

				if ( !pluginByRole.hasOwnProperty( dependency ) ) {
					if ( isOptional ) {
						continue;
					}

					logInfo( `ERROR: ${dependency} is missing, but required by ${plugin.$name}` );

					throw new Error( "unmet plugin dependency: " + plugin.$name + " depends on missing " + dependency );
				}

				if ( initial && dependency === initial ) {
					logInfo( `ERROR: circular dependencies between plugins ${plugin.$name} and ${dependency}` );

					throw new Error( "circular dependency on plugin " + plugin.$name + " depending on " + dependency + " which is also depending on the former" );
				}

				// put more weight on this dependency's dependencies
				countRequests( pluginByRole[dependency].api, initial || plugin.$role );
			}
		}

		/**
		 * Maps provided list of role names into handles selecting plugin for
		 * filling either role.
		 *
		 * @throws TypeError on list containing invalid name of role
		 * @throws Error on missing plugin filling some selected role
		 * @param {string[]} roles lists roles claimed in current setup
		 * @param {string} context name of plugin or role claiming dependency on a role
		 * @returns {Hitchy.Core.PluginHandle[]} descriptions of plugins
		 * @private
		 */
		function _getHandlesOfRoles( roles, context ) {
			return roles.map( role => {
				if ( typeof role !== "string" || !role ) {
					throw new TypeError( `invalid dependency defined in ${context}: ${role}` );
				}

				const plugin = pluginByRole[role];
				if ( !plugin ) {
					throw new Error( `missing dependency defined in ${context}: ${role}` );
				}

				return plugin;
			} );
		}
	}

	/**
	 * Injects all finally available plugins in runtime section of Hitchy API.
	 *
	 * @param {Hitchy.Core.PluginHandle[]} plugins descriptions of plugins
	 * @returns {Hitchy.Core.PluginHandle[]} forwarded descriptions of plugins
	 */
	function promoteAPIs( plugins ) {
		plugins.forEach( plugin => {
			api.plugins[plugin.api.$role] = plugin.api;
		} );

		return plugins;
	}

	/**
	 * Reads hitchy meta data from file selected by file name.
	 *
	 * @note Just like HitchyUtilityFileAPI#readFile() this methods supports
	 *       filename given as array of strings to be resolved first.
	 *
	 * @param {string|string[]} filename name of file or fragments of pathname to compile using Path.resolve
	 * @param {HitchyFileReadMetaOptions} readOptions options customizing read operation
	 * @returns {Promise.<HitchyPluginMeta>} promises normalized meta data read from file
	 */
	async function readBeaconFile( filename, readOptions = {} ) {
		const name = Array.isArray( filename ) ? Path.resolve( ...filename ) : filename;
		const content = await api.utility.file.read( name, { ...readOptions, encoding: "utf-8" } );

		if ( content.trim().length === 0 ) {
			// be flexible if user was providing empty file instead of
			// file with empty object
			return {};
		}

		let data;

		try {
			data = JSON.parse( content );
		} catch {
			throw new Error( "invalid JSON in file " + name );
		}

		if ( !data.hasOwnProperty( "name" ) ) {
			data.name = Path.basename( Path.dirname( name ) );
		}

		if ( !data.hasOwnProperty( "role" ) ) {
			data.role = data.name;
		}

		if ( !data.hasOwnProperty( "dependencies" ) ) {
			if ( !readOptions.keepMissingDependencies ) {
				data.dependencies = [];
			}
		} else if ( !Array.isArray( data.dependencies ) ) {
			data.dependencies = data.dependencies ? [data.dependencies] : [];
		}

		if ( !Array.isArray( data.dependants ) ) {
			data.dependants = data.dependants ? [data.dependants] : [];
		}

		return data;
	}
}
