import Path from "node:path";
import File from "node:fs/promises";

/**
 * Filters provided list of pathnames keeping only those addressing folder
 * containing file named **hitchy.json**.
 *
 * @param {string[]} folders list of pathnames considered addressing folders
 * @returns {Promise<string[]>} promises filtered list of pathnames
 */
export async function validateFolders( folders ) {
	const numFolders = folders.length;
	const checks = new Array( numFolders );

	// check every provided folder for containing a hitchy.json file
	for ( let i = 0; i < numFolders; i++ ) {
		let folder = folders[i];

		if ( folder != null ) {
			folder = Path.resolve( String( folder ).trim() );
		}

		if ( folder ) {
			checks[i] = File.stat( Path.join( folder, "hitchy.json" ) )
				.catch( cause => {
					switch ( cause.code ) {
						case "ENOENT" :
						case "ENOTDIR" :
							return null;

						default :
							throw cause;
					}
				} );
		} else {
			checks[i] = null;
		}
	}

	const stats = await Promise.all( checks );

	// create list of folders actually containing hitchy.json file
	const kept = new Array( numFolders );
	let write = 0;

	for ( let read = 0; read < numFolders; read++ ) {
		if ( stats[read]?.isFile() ) {
			kept[write++] = folders[read];
		}
	}

	kept.splice( write );

	return kept;
}
