import { cmp } from "../misc/cmp.js";

/**
 * Provides implementation for third stage of bootstrapping hitchy instance.
 *
 * @this {Hitchy.Core.API}
 * @param {Hitchy.Core.Options} options global options customizing Hitchy
 * @returns {function(Hitchy.Core.PluginHandle[]):Promise<Hitchy.Core.PluginHandle[]>} function initializing every discovered plugin's API
 */
export default function( options ) {
	const api = this;

	return _bootstrapInitialize;

	/**
	 * Invokes either discovered plugin's optionally provided function for
	 * initializing either plugin's API.
	 *
	 * @param {Hitchy.Core.PluginHandle[]} plugins descriptions of discovered plugins
	 * @returns {Promise<void>} promise for having initialized every discovered plugin's API and the application
	 */
	async function _bootstrapInitialize( plugins ) {
		// invoke optional per-plugin hook for initializing either one
		for ( let i = 0; i < plugins.length; i++ ) {
			const plugin = plugins[i];
			const fn = plugin.api.initialize;

			if ( typeof fn === "function" ) {
				await fn.call( api, options, plugin ); // eslint-disable-line no-await-in-loop
			}
		}

		const file = await api.utility.file.select( "initialize", options.projectFolder );

		// invoke application's initialization eventually
		if ( file ) {
			await cmp( api, options, api.meta, undefined, file );
		}
	}
}
