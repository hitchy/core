import { cmp } from "../misc/cmp.js";

/**
 * Provides implementation for shutting down hitchy instance.
 *
 * @this {Hitchy.Core.API}
 * @param {Hitchy.Core.Options} options global options customizing Hitchy
 * @returns {function(Hitchy.Core.PluginHandle[]):Promise<Hitchy.Core.PluginHandle[]>} factory for shutdown function
 */
export default function( options ) {
	const api = this;

	const logDebug = api.log( "hitchy:bootstrap:debug" );
	const logError = api.log( "hitchy:bootstrap:error" );

	return _bootstrapPrepareShutdown;

	/**
	 * Creates function suitable for gracefully shutting all discovered plugins
	 * by calling either one's shutdown function.
	 *
	 * @param {Hitchy.Core.PluginHandle[]} plugins descriptions of discovered plugins
	 * @returns {function():Promise} function shutting down all discovered plugins
	 * @private
	 */
	function _bootstrapPrepareShutdown( plugins ) {
		let shuttingDown = null;

		return _bootstrapShutdown;

		/**
		 * Triggers shutdown of current Hitchy-based application.
		 *
		 * @return {Promise<*>} promises Hitchy-based application shut down
		 * @private
		 */
		function _bootstrapShutdown() {
			if ( !shuttingDown ) {
				logDebug( "entering shutdown stage" );

				shuttingDown = api.utility.file.select( "shutdown", options.projectFolder )
					.then( async file => {
						if ( file ) {
							logDebug( "shutting down application code" );

							await cmp( api, options, api.meta, undefined, file );
						}

						const reverse = plugins.reverse();

						for ( let i = 0; i < reverse.length; i++ ) {
							const plugin = reverse[i];
							const fn = plugin.api.shutdown;

							if ( typeof fn === "function" ) {
								logDebug( "shutting down plugin %s", plugin.name );

								await fn.call( api, options, plugin ); // eslint-disable-line no-await-in-loop
							}
						}

						logDebug( "shutdown complete" );

						api.emit( "close" );
					} )
					.catch( error => {
						logError( "shutdown failed: %s", error.stack );

						api.emit( "error", error );

						throw error;
					} );
			}

			return shuttingDown;
		}
	}
}
