import Response from "../../api/services/response.js";

/**
 * Provides API for injecting Hitchy into express/connect-based application
 * as middleware.
 *
 * @param {Hitchy.Core.Options=} options global options customizing Hitchy
 * @returns {HitchyConnectInstance} middleware suitable for integrating with Express.js
 */
export default function( options ) {
	/** @type {Hitchy.Core.API} */
	let hitchy = null;

	/** @type {Error} */
	let startupError = null;

	let logDebug = console.log; // eslint-disable-line no-console
	let logError = console.error;

	const starter = import( "../bootstrap.js" )
		.then( ( { default: fn } ) => fn( options ) )
		.then( api => {
			hitchy = api;

			Object.defineProperties( hitchyRequestHandler, {
				hitchy: { value: api, enumerable: true },
				api: { value: api, enumerable: true },
			} );

			logDebug = api.log( "hitchy:injector:node:debug" );
			logError = api.log( "hitchy:injector:node:error" );

			return api;
		}, cause => {
			startupError = cause;
			hitchy = cause.hitchy;

			// keep rejecting promise
			throw cause;
		} );


	Object.defineProperties( hitchyRequestHandler, {
		/**
		 * Promises Hitchy application having started.
		 *
		 * @name HitchyConnectInstance#onStarted
		 * @property {Promise}
		 * @readonly
		 */
		onStarted: { value: starter },

		/**
		 * Exposes event emitter of created hitchy instance for use by server
		 * controller.
		 *
		 * @param {string} name name of event/notification to emit
		 * @param {any[]} args list of arguments dispatched with event/notification
		 * @returns {void}
		 */
		emit: {
			value: ( name, ...args ) => {
				if ( hitchy ) {
					return hitchy.emit( name, ...args );
				}

				return false;
			}
		},

		/**
		 * Gracefully shuts down Hitchy application.
		 *
		 * @name HitchyConnectInstance#stop
		 * @property {function():Promise}
		 * @readonly
		 */
		stop: {
			value: () => {
				logDebug( "stopping injector" );

				return Promise.race( [
					starter,
					new Promise( ( _, reject ) => {
						const timeout = ( parseInt( process.env.STARTUP_TIMEOUT ) || 10 ) * 1000;

						setTimeout( reject, timeout, Object.assign( new Error( "FATAL: cancelling start-up blocking shutdown" ), {
							startBlocked: true,
						} ) );
					} ),
				] )
					.catch( error => {
						if ( error.startBlocked ) {
							logError( error.message );
						}

						// do not re-expose any issue encountered during start-up
					} )
					.then( () => ( hitchy ? hitchy.bootstrap.shutdown() : undefined ) );
			},
		},

		injector: { value: "connect" },
	} );

	return hitchyRequestHandler;


	/**
	 * Handles request.
	 *
	 * @param {IncomingMessage} req request descriptor
	 * @param {ServerResponse} res response manager
	 * @param {function} next callback to invoke for passing request to next available handler
	 * @returns {void}
	 */
	async function hitchyRequestHandler( req, res, next ) {
		/** @type {Hitchy.Core.RequestContext} */
		const context = {
			context: "express",
			request: req,
			response: res,
			done: next,
			local: {},
			consumed: {
				byPolicy: false,
				byController: false,
			},
		};

		if ( hitchy ) {
			hitchy.utility.introduce( context );

			hitchy.service.Router.normalize( context );
			hitchy.service.Responder.normalize( context );

			try {
				await hitchy.service.Router.dispatch( context );

				const { byController, byPolicy } = context.consumed;

				if ( !res.finished && !byController ) {
					if ( byPolicy ) {
						hitchy.service.Response.renderFallback( context );
					} else {
						next();
					}
				}
			} catch ( cause ) {
				hitchy.service.Response.renderFallback( context, cause );
			}
		} else if ( startupError ) {
			logDebug( "got request on node which failed during start-up" );
			Response().renderFallback( context, startupError );
		} else {
			logDebug( "got request during startup, sending splash" );
			Response().renderFallback( context );
		}
	}
}
