import connectInjector from "./connect.js";
import nodeInjector from "./node.js";

export const connect = connectInjector;
export const express = connectInjector;

export const node = nodeInjector;
