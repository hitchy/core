import Response from "../../api/services/response.js";

/**
 * Provides API for injecting Hitchy into native HTTP service of Node.js.
 *
 * @param {Hitchy.Core.Options=} options global options customizing hitchy
 * @returns {Hitchy.Core.NodeInstance} instance of Hitchy
 */
export default function( options ) {
	/** @type {Hitchy.Core.API} */
	let hitchy = null;

	/** @type {Error} */
	let startupError = null;

	let logDebug = console.log; // eslint-disable-line no-console
	let logError = console.error;

	const starter = import( "../bootstrap.js" )
		.then( ( { default: fn } ) => fn( options ) )
		.then( api => {
			hitchy = api;

			Object.defineProperties( hitchyRequestHandler, {
				hitchy: { value: api, enumerable: true },
				api: { value: api, enumerable: true },
			} );

			logDebug = api.log( "hitchy:injector:node:debug" );
			logError = api.log( "hitchy:injector:node:error" );

			return api;
		}, cause => {
			startupError = cause;
			hitchy = cause.hitchy;

			// keep rejecting promise
			throw cause;
		} );


	Object.defineProperties( hitchyRequestHandler, {
		/**
		 * Promises hitchy node has been started successfully.
		 *
		 * @name Hitchy.Core.NodeInstance#onStarted
		 * @property {Promise<Hitchy.Core.API>}
		 * @readonly
		 */
		onStarted: { value: starter },

		/**
		 * Exposes event emitter of created hitchy instance for use by server
		 * controller.
		 *
		 * @param {string} name name of event/notification to emit
		 * @param {any[]} args list of arguments dispatched with event/notification
		 * @returns {boolean} true if the event had listeners, false otherse
		 */
		emit: {
			value: ( name, ...args ) => {
				if ( hitchy ) {
					return hitchy.emit( name, ...args );
				}

				return false;
			}
		},

		/**
		 * Shuts down hitchy node.
		 *
		 * @note Shutting down hitchy node does not actually shut down any socket
		 *       this node was bound to before. Thus shutting down Hitchy node
		 *       w/o first shutting down all sockets it was listening on should
		 *       be prevented.
		 *
		 * @name Hitchy.Core.NodeInstance#stop
		 * @property {function():Promise}
		 */
		stop: {
			value: () => {
				logDebug( "stopping injector" );

				return Promise.race( [
					starter,
					new Promise( ( _, reject ) => {
						const timeout = ( parseInt( process.env.STARTUP_TIMEOUT ) || 10 ) * 1000;

						setTimeout( reject, timeout, Object.assign( new Error( "FATAL: cancelling start-up blocking shutdown" ), {
							startBlocked: true,
						} ) );
					} ),
				] )
					.catch( error => {
						if ( error.startBlocked ) {
							logError( error.message );
						}

						// do not re-expose any issue encountered during start-up
					} )
					.then( () => {
						if ( hitchy ) {
							return hitchy.bootstrap.shutdown();
						}

						return undefined;
					} );
			},
		},

		injector: { value: "node" },
	} );

	return hitchyRequestHandler;


	/**
	 * Implements request handler for integrating hitchy with a regular Node.js
	 * HTTP server.
	 *
	 * @param {IncomingMessage} req description of request to be handled
	 * @param {ServerResponse} res response manager
	 * @returns {void}
	 */
	async function hitchyRequestHandler( req, res ) {
		/** @type {Hitchy.Core.RequestContext} */
		const context = {
			context: "standalone",
			request: req,
			response: res,
			done: _error => {
				if ( _error ) {
					logError( `got error on dispatching ${req.method} ${req.url}: ${_error.statusCode ? _error.message : _error.stack}` );
				}
			},
			local: {},
			consumed: {
				byPolicy: false,
				byController: false,
			},
		};

		if ( hitchy ) {
			try {
				hitchy.utility.introduce( context );

				hitchy.service.Router.normalize( context );
				hitchy.service.Responder.normalize( context );

				await hitchy.service.Router.dispatch( context );

				if ( !context.consumed.byController && !res.finished && options.handleErrors !== false ) {
					throw Object.assign( new Error( "Page not found!" ), { status: 404 } );
				}
			} catch ( cause ) {
				if ( options.handleErrors === false ) {
					context.done( cause );
				} else {
					options.handleErrors = true;

					hitchy.service.Response.renderFallback( context, cause );
				}
			}
		} else if ( startupError ) {
			logDebug( "got request on node which failed during start-up" );
			Response().renderFallback( context, process.env.NODE_ENV === "production" ? true : startupError );
		} else {
			logDebug( "got request during startup, sending splash" );
			Response().renderFallback( context );
		}
	}
}
