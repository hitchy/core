import Path from "node:path";
import EventEmitter from "node:events";

import PromiseUtils from "promise-essentials";

import DiscoverAPI from "../bootstrap/discover.js";
import ExposeAPI from "../bootstrap/expose.js";
import InitializeAPI from "../bootstrap/initialize.js";
import PrepareShutdownAPI from "../bootstrap/prepareShutdown.js";
import Triangulate from "./triangulate.js";

import IntroduceAPI from "./introduce.js";
import Logger from "./logger.cjs";
import * as FileAPI from "./file.js";
import * as ParserAPI from "./parser.js";
import * as CaseAPI from "./case.js";
import * as ValueAPI from "./value.js";
import { deepSeal as seal, deepFreeze as freeze, deepMerge as merge } from "./object.js";
import { cmp } from "./cmp.js";

/**
 * Creates initial skeleton API instance.
 *
 * @param {Hitchy.Core.Options} options global options customizing Hitchy
 * @returns {Promise<Partial<Hitchy.Core.LibraryAPI>>} incomplete version of Hitchy's API
 * @private
 */
export async function createAPI( options = {} ) {
	/**
	 * Implements Hitchy's API based on EventEmitter.
	 */
	class HitchyAPI extends EventEmitter {}

	const api = new HitchyAPI(); // eslint-disable-line consistent-this

	api.config = {
		hitchy: {},
	};

	api.log = namespace => Logger.get( namespace );

	// create collections of components per supported type
	api.controllers = {};
	api.models = {};
	api.policies = {};
	api.services = {};

	// create read-only aliases for simplified fault-tolerant access on those
	// four collections of components in API
	Object.defineProperties( api, {
		model: { value: api.models },
		service: { value: api.services },
		policy: { value: api.policies },
		controller: { value: api.controllers },
	} );

	// silently keep providing now deprecated `runtime` alias for accessing those collections
	Object.defineProperties( api, {
		runtime: { value: {
			models: api.models,
			services: api.services,
			policies: api.policies,
			controllers: api.controllers,

			model: api.models,
			service: api.services,
			policy: api.policies,
			controller: api.controllers,
		} },
	} );

	api.plugins = {};
	api.data = {};

	api.folder = name => Path.resolve( options.projectFolder, String( name || "" )
		.replace( /^@(hitchy|project)(?=$|[/\\])/i, ( _, key ) => options[`${key}Folder`] ) );

	api.cmp = ( ...args ) => cmp( api, options, api.meta, undefined, ...args );
	api.cmfp = ( fn, ...args ) => ( typeof fn === "function" && fn.useCMP !== false && fn.useCMFP !== false ? fn.call( api, options, ...args ) : fn );

	api.__onShutdown = null;

	let alwaysRejectWith = null;

	api.crash = cause => {
		alwaysRejectWith = cause instanceof Error ? cause : new Error( String( cause ) );
		alwaysRejectWith.intentionalCrash = true;

		return api.shutdown()
			.catch( () => {} ); // eslint-disable-line no-empty-function
	};

	api.shutdown = () => {
		if ( api.__onShutdown == null ) {
			// prepare promise to be resolved after server and Hitchy have been shut down
			api.__onShutdown = new Promise( ( resolve, reject ) => {
				api.once( "close", () => {
					if ( alwaysRejectWith instanceof Error ) {
						reject( alwaysRejectWith );
					} else {
						resolve();
					}
				} );

				api.once( "error", error => {
					if ( alwaysRejectWith instanceof Error ) {
						console.error( "error while crashing Hitchy: %s", error.stack );

						reject( alwaysRejectWith );
					} else {
						reject( error );
					}
				} );
			} );

			// emit event to notify injecting server to shut down or crash
			if ( alwaysRejectWith ) {
				api.emit( "crash", alwaysRejectWith );
			} else {
				api.emit( "shutdown" );
			}
		}

		return api.__onShutdown;
	};

	api.utility = {
		introduce: await IntroduceAPI.call( api, options ),
		file: { ...FileAPI },
		parser: { ...ParserAPI },
		promise: PromiseUtils,
		logger: Logger,
		object: { seal, freeze, merge },
		case: { ...CaseAPI },
		value: { ...ValueAPI },
	};

	api.bootstrap = {
		discover: await DiscoverAPI.call( api, options ),
		expose: await ExposeAPI.call( api, options ),
		initialize: await InitializeAPI.call( api, options ),
		prepareShutdown: await PrepareShutdownAPI.call( api, options ),
		shutdown: () => Promise.resolve(), // to be replaced by prepareShutdown() during bootstrap
		triangulate: Triangulate.bind( api, options ),
	};

	// alias some services to keep API backwards-compatible, but use getters for
	// target services are not available yet
	Object.defineProperties( api, {
		Client: {
			get: () => api.service.LocalClientRequest,
			enumerable: true,
		},
		router: {
			get: () => api.service.Router,
			enumerable: true,
		},
		responder: {
			get: () => api.service.Responder,
			enumerable: true,
		},
	} );

	return api;
}
