/**
 * Converts provided string from kebab-case to PascalCase.
 *
 * @name Hitchy.Core.UtilityAPI.case.kebabToPascal
 * @param {string} input string assumed to be in kebab-case
 * @returns {string} provided string converted to PascalCase
 */
export const kebabToPascal = input => String( input )
	.toLowerCase()
	.replace( /(?:^|-)([a-z])/g, ( _, letter ) => letter.toUpperCase() );

/**
 * Converts provided string from kebab-case to camelCase.
 *
 * @name Hitchy.Core.UtilityAPI.case.kebabToCamel
 * @param {string} input string assumed to be in kebab-case
 * @returns {string} provided string converted to camelCase
 */
export const kebabToCamel = input => String( input )
	.toLowerCase()
	.replace( /-([a-z])/g, ( _, letter ) => letter.toUpperCase() );

/**
 * Converts provided string from PascalCase to kebab-case.
 *
 * @name Hitchy.Core.UtilityAPI.case.pascalToKebab
 * @param {string} input string assumed to be in PascalCase
 * @returns {string} provided string converted to kebab-case
 */
export const pascalToKebab = input => String( input )
	.replace( /(A-Z)([A-Z]+)/g, ( _, first, trailing ) => first + trailing.toLowerCase() )
	.replace( /(?<!^)([A-Z])/g, ( _, letter ) => "-" + letter.toLowerCase() )
	.toLowerCase();

/**
 * Converts provided string from camelCase to kebab-case.
 *
 * @name Hitchy.Core.UtilityAPI.case.camelToKebab
 * @param {string} input string assumed to be in camelCase
 * @returns {string} provided string converted to kebab-case
 */
export const camelToKebab = input => String( input )
	.replace( /(A-Z)([A-Z]+)/g, ( _, first, trailing ) => first + trailing.toLowerCase() )
	.replace( /[A-Z]/g, letter => "-" + letter.toLowerCase() )
	.toLowerCase();

/**
 * Converts provided string from camelCase to PascalCase.
 *
 * @name Hitchy.Core.UtilityAPI.case.camelToPascal
 * @param {string} input string assumed to be in camelCase
 * @returns {string} provided string converted to PascalCase
 */
export const camelToPascal = input => String( input )
	.replace( /^[a-z]/g, letter => letter.toUpperCase() );
