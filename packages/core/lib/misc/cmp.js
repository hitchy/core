import Path from "node:path";
import { pathToFileURL } from "node:url";

/**
 * Loads a code file optionally complying with common module pattern.
 *
 * @param {Hitchy.Core.API} api Hitchy API to expose on applying common module pattern
 * @param {Hitchy.Core.Options} options options to pass in first argument on applying common module pattern
 * @param {object} meta meta information on plugin or application the loaded file is related to
 * @param {function|undefined} fn callback invoked with loaded module's API prior to optionally applying CMP
 * @param {string} pathname path name of file or folder to load
 * @param {any[]} moduleArguments list of additional arguments to pass in succeeding arguments on applying common module pattern
 * @returns {Promise<object>} promise for loaded file's exported API
 */
export async function cmp( api, options, meta, fn, pathname, ...moduleArguments ) { // eslint-disable-line consistent-this
	const { file } = api.utility;
	let filename = Path.resolve( options.projectFolder, pathname );

	const stat = await file.stat( filename );
	if ( stat?.isDirectory() ) {
		filename = await pickEntryFileOfFolder( api, options, filename );
	}

	const esm = filename && ( filename.endsWith( ".mjs" ) || ( filename.endsWith( ".js" ) && meta.type === "module" ) );
	const module = filename ? await import( pathToFileURL( filename ) ) : {};
	let moduleApi;

	if ( module.default && Object.keys( module ).length === 1 ) {
		// module provides a default export, only --> use it
		moduleApi = module.default;
	} else if ( module.default && module["module.exports"] && Object.keys( module ).length === 2 ) {
		// Node v23+ provides `default` export for CommonJS modules pointing to their default at `module.exports` --> use that "default"
		moduleApi = module.default;
	} else if ( typeof module.default === "function" ) {
		// module's default export is a function --> assume CMP compliance, so prefer it
		moduleApi = module.default;
	} else if ( Object.keys( module.default || {} ).length > 0 ) {
		// module has a non-empty default export --> prefer it
		moduleApi = module.default;
	} else {
		moduleApi = module;
	}

	const config = esm ? module : moduleApi;
	const useCMP = config.useCMP ?? moduleApi.useCMP;

	if ( typeof fn === "function" ) {
		moduleApi = fn( module, moduleApi, config, moduleArguments ) || moduleApi;
	}

	if ( useCMP !== false && typeof moduleApi === "function" && !String( moduleApi ).startsWith( "class " ) ) {
		return await moduleApi.call( api, options, ...moduleArguments );
	}

	if ( moduleApi instanceof Promise ) {
		return await moduleApi;
	}

	// Node v23+ automatically freezes any imported module's API, but Hitchy is
	// going to attach additional top-level properties for internal management
	// --> always provide a shallow copy in case module's API is an object
	return typeof moduleApi === "object" && moduleApi ? { ...moduleApi } : moduleApi;
}

/**
 * Polyfills import's lack of supporting import of folders by looking up an
 * existing package.json file for a custom entry file to load using index.[cm]js
 * as fallback.
 *
 * @param {Hitchy.Core.API} api Hitchy API to expose on applying common module pattern
 * @param {Hitchy.Core.Options} options options to pass in first argument on applying common module pattern
 * @param {string} folder path name of a folder to be imported
 * @returns {Promise<string>} promise for name of file to import instead of given folder, empty string if no file has been found
 */
async function pickEntryFileOfFolder( api, options, folder ) { // eslint-disable-line consistent-this
	const { file } = api.utility;

	try {
		const meta = JSON.parse( await file.read( Path.resolve( folder, "package.json" ), { encoding: "utf-8" } ) ) || {};

		if ( meta.main ) {
			return Path.resolve( folder, meta.main );
		}
	} catch {}

	return await file.select( "index", folder ) || "";
}
