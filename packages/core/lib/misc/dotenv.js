const reset = ctx => {
	ctx.name = ctx.value = undefined;
	ctx.trimValue = true;
};

const nop = () => {}; // eslint-disable-line no-empty-function
const name = ( ctx, token ) => { ctx.name = String( ctx.name ?? "" ) + token; };
const value = ( ctx, token ) => { ctx.value = String( ctx.value ?? "" ) + token; };
const fail = msg => ( _, __, line ) => { throw new Error( msg.replace( /%line/g, () => line ) ); };
const checkName = ( ctx, __, line ) => {
	if ( ctx.name == null || !ctx.name.trim() ) {
		throw new Error( `missing name in line ${line()}` );
	}

	return "ASSIGNMENT";
};

const finish = fn => ( ctx, __, line ) => {
	if ( ctx.name == null || !ctx.name.trim() ) {
		throw new Error( `missing name in assignment in line ${line()}` );
	}

	let _value = ctx.value == null ? undefined : ctx.trimValue ? ctx.value.trim() : ctx.value;

	if ( typeof ctx.qualifyDataFn === "function" ) {
		_value = ctx.qualifyDataFn( ctx.result, ctx.name, _value );
	}

	ctx.result[ctx.name] = _value;

	reset( ctx );

	return fn ? fn( ctx, __, line ) : "END";
};

/* eslint-disable consistent-return */
const Transitions = {
	START: {
		"#": () => "COMMENT",
		"'": () => "SINGLE_QUOTED_NAME",
		'"': () => "DOUBLE_QUOTED_NAME",
		"=": fail( "missing name in line %line" ),
		"\n": nop,
		"": ( ctx, token ) => {
			token = token.trim();

			if ( token ) {
				ctx.name = token;
				return "ASSIGNMENT";
			}
		},
	},
	SINGLE_QUOTED_NAME: {
		"#": name,
		'"': name,
		"'": checkName,
		"=": name,
		"\n": name,
		"": name,
	},
	DOUBLE_QUOTED_NAME: {
		"#": name,
		'"': checkName,
		"'": name,
		"=": name,
		"\n": name,
		"": name,
	},
	ASSIGNMENT: {
		"#": fail( "expecting = in line %line" ),
		'"': fail( "expecting = in line %line" ),
		"'": fail( "expecting = in line %line" ),
		"=": () => "VALUE",
		"\n": fail( "expecting = in line %line" ),
		"": ( ctx, token, line ) => {
			if ( token.trim() ) {
				throw new Error( `expecting = in line ${line()}` );
			}
		}
	},
	VALUE: {
		"#": finish( () => "COMMENT" ),
		"'": ctx => {
			if ( ctx.value == null ) {
				ctx.trimValue = false;
				return "SINGLE_QUOTED_VALUE";
			}

			ctx.value += "'";
		},
		'"': ctx => {
			if ( ctx.value == null ) {
				ctx.trimValue = false;
				return "DOUBLE_QUOTED_VALUE";
			}

			ctx.value += '"';
		},
		"=": value,
		"\n": finish( () => "START" ),
		"": ( ctx, token ) => {
			if ( ctx.value == null ) {
				token = token.trimStart();

				if ( token ) {
					ctx.value = token;
				}
			} else {
				ctx.value += token;
			}
		},
	},
	SINGLE_QUOTED_VALUE: {
		"#": value,
		'"': value,
		"'": finish(),
		"=": value,
		"\n": value,
		"": value,
	},
	DOUBLE_QUOTED_VALUE: {
		"#": value,
		'"': finish(),
		"'": value,
		"=": value,
		"\n": value,
		"": value,
	},
	COMMENT: {
		"\n": () => "START",
	},
	END: {
		"#": () => "COMMENT",
		"'": fail( "unexpected content in line %line" ),
		'"': fail( "unexpected content in line %line" ),
		"=": fail( "unexpected content in line %line" ),
		"\n": () => "START",
		"": ( _, token, line ) => {
			if ( token.trim() ) {
				throw new Error( `unexpected content in line ${line()}` );
			}
		}
	}
};
/*  eslint-enable consistent-return */

/**
 * Parses content of a .env file.
 *
 * @param content raw content of a .env file
 * @param {function(Object<string,string>, string, string):string} [qualifyDataFn] callback invoked to qualify value prior to assigning it to the result
 * @returns {Object<string,string>} key-value pairs found in parsed .env file
 */
export function parseDotEnv( content, qualifyDataFn = undefined ) {
	const tokens = String( content ?? "" ).replace( /\r\n/g, "\n" ).split( /([#"'=\n])/ );
	const numTokens = tokens.length;
	const context = { result: {}, qualifyDataFn };
	let state = "START";

	const line = index => tokens.slice( 0, index ).join( "" ).split( "\n" ).length;
	reset( context );

	for ( let i = 0; i < numTokens; i++ ) {
		const token = tokens[i];
		const transition = Transitions[state][token] || Transitions[state][""];

		if ( transition ) {
			state = transition( context, token, () => line( i ) ) || state;
		}
	}

	switch ( state ) {
		case "START" :
		case "COMMENT" :
		case "END" :
			return context.result;

		case "VALUE" :
			finish()( context, "", () => line( i ) );
			return context.result;

		default :
			throw new Error( "unexpected end of file" );
	}
}

/**
 * Replaces markers in provided code with values from one of the given sources
 * matching name used in either marker and optionally applies some adjustments
 * based on found marker extensions.
 *
 * Supported markers are like :
 *
 *     ${MARKER_NAME}
 *
 * Supported extensions are:
 *
 *     ${NAME:-default}   replacing marker with value of NAME or "default" if value of NAME is empty or undefined
 *     ${NAME:+alt}       replacing marker with "alt" if value of NAME is not empty or undefined, otherwise replacing it empty string or undefined
 *
 * @param {string} [code] code to process
 * @param {Object<string,string>} sources list of sources with named values to look up on processing markers
 * @returns {string|undefined|null} provided code with found markers replaced with optionally adjusted values from one of the given sources
 */
export function resolveVariables( code, ...sources ) {
	if ( code == null ) {
		return code;
	}

	return code.replace( /\$\{([^}:]+)(?::([^}]*))?}/g, ( _, key, extension ) => {
		key = key.trim();

		let _value;

		for ( const source of sources ) {
			_value = source[key];

			if ( _value != null ) {
				break;
			}
		}

		if ( extension != null ) {
			if ( extension.startsWith( "-" ) ) {
				return String( _value ?? "" ) === "" ? extension.slice( 1 ) : "";
			}

			if ( extension.startsWith( "+" ) ) {
				return String( _value ?? "" ) === "" ? "" : extension.slice( 1 );
			}
		}

		return _value ?? "";
	} );
}
