import File from "node:fs/promises";
import Path from "node:path";

/**
 * Lists shallow entries in a selected folder.
 *
 * @param {string} pathname pathname of folder to read entries from.
 * @returns {Promise<string[]>} found entries of folder prefixed with pathname
 */
export async function list( pathname ) {
	try {
		return await File.readdir( pathname );
	} catch ( cause ) {
		if ( cause.code === "ENOENT" ) {
			return [];
		}

		throw cause;
	}
}

/**
 * Reads content of selected file.
 *
 * @note This method provides empty content if selected file does not exist.
 *       This special behaviour might be disabled in options by setting
 *       addition option `failIfMissing`.
 *
 * @note As a special case filename might be an array of elements to pass
 *       through node's `path.resolve()` first before trying to read file.
 *
 * @param {string|string[]} filename name of file or fragments of pathname to compile using Path.resolve()
 * @param {{encoding:?string, flag:?string, failIfMissing:?boolean}} readOptions options customizing read operation
 * @returns {Promise<Buffer|string>} promises content of file as buffer or string
 */
export async function read( filename, readOptions = {} ) {
	const name = Array.isArray( filename ) ? Path.resolve( ...filename ) : filename;

	try {
		return await File.readFile( name, readOptions );
	} catch ( cause ) {
		if ( cause.code === "ENOENT" && !readOptions.failIfMissing ) {
			return readOptions.encoding ? "" : Buffer.alloc( 0 );
		}

		throw cause;
	}
}

/**
 * Inspects a selected file or folder
 *
 * @note As a special case filename might be an array of elements to pass
 *       through node's `path.resolve()` first before trying to read file.
 *
 * @param {string|string[]} filename name of file or fragments of pathname to compile using Path.resolve()
 * @returns {Promise<Stats>} promises stats on named file
 */
export async function stat( filename ) {
	return await File.stat( Array.isArray( filename ) ? Path.resolve( ...filename ) : filename );
}

/**
 * Selects existing code file out of several options.
 *
 * @param {string} name name of file to load without extension
 * @param {string} folder path name of folder file is expected in
 * @returns {Promise<string>} path name of found file with its proper extension
 */
export async function select( name, folder = undefined ) {
	const ignoreMissing = cause => {
		if ( cause.code === "ENOENT" ) {
			return undefined;
		}

		throw cause;
	};

	const pathname = folder == null ? name : Path.resolve( folder, name );

	const jsFile = pathname + ".js";
	const cjsFile = pathname + ".cjs";
	const esmFile = pathname + ".mjs";

	const [ js, cjs, mjs ] = await Promise.all( [
		File.stat( jsFile ).then( info => info && info.isFile() && jsFile ).catch( ignoreMissing ),
		File.stat( cjsFile ).then( info => info && info.isFile() && cjsFile ).catch( ignoreMissing ),
		File.stat( esmFile ).then( info => info && info.isFile() && esmFile ).catch( ignoreMissing ),
	] );

	const [ file, altFile ] = [ js, cjs, mjs ].filter( i => i );

	if ( altFile ) {
		throw new Error( `several candidates for ${name}.[mc]js exist -> can not decide which one to use` );
	}

	return file;
}

/**
 * @typedef {object} HitchyPluginMeta
 * @property {boolean} $valid set true on discovering plugin
 * @property {?string} name name of plugin
 * @property {?string} role role filled by plugin
 * @property {?string[]} dependencies roles of plugins this one depends on
 * @property {?string[]} dependants roles of plugins becoming dependants when
 *           discovering current plugin to be enabled on bootstrap
 */

/**
 * @typedef {object} HitchyFileReadOptions
 * @property {string} [encoding]
 * @property {string} [flag]
 * @property {boolean} [failIfMissing] set true to reject promise if file is missing
 */

/**
 * @typedef {HitchyFileReadOptions} HitchyFileReadMetaOptions
 * @property {boolean} [keepMissingDependencies] set true to disable injection
 *           of empty list of dependencies e.g. to detect if user has provided
 *           any list explicitly
 */
