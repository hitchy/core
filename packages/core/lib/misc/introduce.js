export default function() { // eslint-disable-line jsdoc/require-jsdoc
	const api = this;

	return _utilityIntroduce;


	/**
	 * Injects runtime information into current request's context.
	 *
	 * @param {HitchyRequestContext} requestContext request context to extend
	 * @returns {HitchyRequestContext} provided request context with API injected
	 */
	function _utilityIntroduce( requestContext ) {
		const { controllers, policies, services, models, config } = api;
		const { NetworkUtility } = services;

		const { request } = requestContext;
		const forwarded = NetworkUtility.parseKeyValueHeader( ( ( request || {} ).headers || {} ).forwarded );

		Object.defineProperties( requestContext, {
			startTime: { value: Date.now(), enumerable: true },
			api: { value: api, enumerable: true },
			config: { value: config, enumerable: true },
			runtime: { value: api.runtime },
			controllers: { value: controllers, enumerable: true },
			controller: { value: controllers },
			policies: { value: policies, enumerable: true },
			policy: { value: policies },
			services: { value: services, enumerable: true },
			service: { value: services },
			models: { value: models, enumerable: true },
			model: { value: models },
			url: { value: NetworkUtility.compileRequestUrl( request, forwarded ), enumerable: true },
			clientIp: { value: NetworkUtility.extractClientIp( request, forwarded ), enumerable: true },
		} );

		return requestContext;
	}
}
