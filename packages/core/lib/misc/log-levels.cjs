const SortedLogLevelNames = [ "debug2", "debug", "info", "notice", "warning", "error", "alert", "critical" ];
const LogLevels = {};

for ( let i = 0; i < SortedLogLevelNames.length; i++ ) {
	const name = SortedLogLevelNames[i];

	LogLevels[name.toUpperCase()] = SortedLogLevelNames.slice( i ).map( n => `*:${n}` ).join( "," );
}

module.exports = LogLevels;
module.exports.SortedLogLevelNames = SortedLogLevelNames;
