/*
 * NOTE! This file is not complying with CMP by intention as it is used early on
 *       booting up Hitchy's framework. It is integrated with its API, though.
 */

const AbstractLogger = require( "./logger/abstract.cjs" );
const DebugLogger = require( "./logger/debug.cjs" );

/**
 * Controls logger manager of running application.
 */
module.exports = class CurrentLogger {
	/**
	 * Refers to current logger manager.
	 *
	 * @type {AbstractLogger}
	 */
	static #current = new DebugLogger();

	/**
	 * Tracks latest application of switches controlling what namespaces should
	 * be logged or not.
	 *
	 * @type {string} comma-separated list of namespace patterns to match against namespaces to detect enabled/disabled namespaces
	 */
	static #latestUpdate = process.env.DEBUG || "*,*:debug";

	/** @internal */
	static useCMP = false;

	/**
	 * Fetches logging method for provided namespace.
	 *
	 * @param {string} namespace namespace of messages to be generated by returned logger
	 * @returns {function(message: string, ...args: any):void} function logging message interpolated with optional arguments
	 */
	static get( namespace ) {
		const logger = this.#current.get( namespace );

		if ( typeof logger.update !== "function" ) {
			logger.update = switches => this.#current.update( switches );
		}

		return logger;
	}

	/**
	 * Enables and disables existing and future loggers based on provided
	 * description of namespaces to enable/disable.
	 *
	 * @param {string} switches comma-separated list of namespace patterns to match against namespaces for enabling/disabling them.
	 * @returns {void}
	 */
	static update( switches ) {
		if ( typeof switches === "string" && switches.trim() !== "" ) {
			this.#current.update( switches );
			this.#latestUpdate = switches;
		}
	}

	/**
	 * Switches to another instance of default logger.
	 *
	 * @param {string} switches optional set of switches to apply in addition
	 * @returns {void}
	 */
	static reset( switches = undefined ) {
		if ( switches != null ) {
			this.#latestUpdate = switches;
		}

		this.replace( new DebugLogger() );
	}

	/**
	 * Replaces current logger manager with provided one.
	 *
	 * @param {AbstractLogger} manager logger manager to replace current one with
	 * @returns {void}
	 */
	static replace( manager ) {
		if ( !( manager instanceof AbstractLogger ) ) {
			throw new TypeError( "invalid logger manager" );
		}

		this.#current = manager;

		manager.update( this.#latestUpdate );
	}
};
