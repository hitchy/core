/**
 * Manages methods for logging messages per different namespaces.
 *
 * @abstract
 */
module.exports = class Logger {
	/**
	 * Fetches logging method for selected namespace. Multiple requests for the
	 * same namespace should return identical logging method.
	 *
	 * @param {string} namespace namespace of requested logging method
	 * @returns {function(message:string, ...args:any):void} logging method for selected namespace
	 * @abstract
	 */
	get( namespace ) {
		throw new Error( "invalid use of abstract logger" );
	}

	/**
	 * Adjusts all logging methods - previously fetched ones as well as future
	 * ones - actually generating log output based on provided description of
	 * namespace switches.
	 *
	 * @param {string} switches comma-separated list of patterns enabling/disabling matching namespaces
	 * @returns {void}
	 * @abstract
	 */
	update( switches ) {
		throw new Error( "invalid use of abstract logger" );
	}
};
