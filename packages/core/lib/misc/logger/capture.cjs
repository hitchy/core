const Utility = require( "node:util" );

const Debug = require( "debug" );

const AbstractLogger = require( "./abstract.cjs" );

/**
 * Implements log manager capturing all generated messages internally for later
 * inspection. This is intended for use with unit-testing log generation.
 */
module.exports = class CapturingLogger extends AbstractLogger {
	/**
	 * Maps previously requested namespaces into logging methods returned in
	 * either case.
	 *
	 * @type {Object<string,function(message: string, ...args:any):void>}
	 */
	#cache = {};

	/**
	 * Lists captured log messages.
	 *
	 * @type {string[]}
	 */
	logged = [];

	/**
	 * Collects message for provided namespace. Can be replaced with custom
	 * callback on construction.
	 *
	 * @param {string} namespace namespace of message to collect
	 * @param {string} message message to collect
	 * @returns {void}
	 */
	#collectorFn = ( namespace, message ) => {
		this.logged.push( namespace + " " + message );
	};

	/**
	 * @param {function(namespace: string, message: string):void} collectorFn custom callback to invoke for collecting messages instead of using local list
	 */
	constructor( collectorFn = undefined ) {
		super();

		if ( typeof collectorFn === "function" ) {
			this.#collectorFn = collectorFn;
		}
	}

	/** @inheritDoc */
	get( namespace ) {
		if ( typeof namespace !== "string" ) {
			throw new TypeError( "invalid type of logging namespace" );
		}

		const trimmed = namespace.trim();

		if ( !this.#cache.hasOwnProperty( trimmed ) ) {
			this.#cache[trimmed] = ( message, ...args ) => {
				if ( Debug.enabled( namespace ) ) {
					this.#collectorFn( namespace, Utility.format( message, ...args ) );
				}
			};
		}

		return this.#cache[trimmed];
	}

	/** @inheritDoc */
	update( switches ) {
		Debug.enable( switches );
	}
};

