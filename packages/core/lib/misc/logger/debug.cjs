const Debug = require( "debug" );

const AbstractLogger = require( "./abstract.cjs" );

/**
 * Implements log manager based on 3rd-party package "debug" for use with Hitchy.
 */
module.exports = class DebugLogger extends AbstractLogger {
	/**
	 * Maps previously requested namespaces into logging methods returned in
	 * either case.
	 *
	 * @type {Object<string,function(message: string, ...args:any):void>}
	 */
	#cache = {};

	/** @inheritDoc */
	get( namespace ) {
		if ( typeof namespace !== "string" ) {
			throw new TypeError( "invalid type of logging namespace" );
		}

		const trimmed = namespace.trim();

		if ( !this.#cache.hasOwnProperty( trimmed ) ) {
			this.#cache[trimmed] = Debug( namespace );
		}

		return this.#cache[trimmed];
	}

	/** @inheritDoc */
	update( switches ) {
		Debug.enable( switches );
	}
};
