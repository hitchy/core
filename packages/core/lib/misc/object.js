import EventEmitter from "node:events";

/**
 * Deeply seals a given object w/o copying it.
 *
 * @param {object} object object to be sealed deeply
 * @param {function(string[]):boolean} testFn callback invoked to decide whether some property should be sealed or not
 * @param {string[]} path breadcrumb of segments, used internally
 * @returns {object} reference on provided object, now deeply sealed
 */
export function deepSeal( object, testFn = null, path = [] ) {
	if ( object && typeof object === "object" && !object.$$doNotSeal$$ && !Object.isSealed( object ) ) {
		const isEventEmitter = object instanceof EventEmitter;

		if ( isEventEmitter ) {
			// prevent properties managed for event handling from being sealed implicitly
			// see [_addEventListener() of events.js](https://github.com/nodejs/node/blob/master/lib/events.js#L349)
			if ( !Object.prototype.hasOwnProperty.call( object, "_events" ) ) {
				object._events = Object.create( null );
			}

			let counter = object._eventsCount || 0;

			Object.defineProperty( object, "_eventsCount", {
				get: () => counter,
				set: value => { counter = value; },
			} );
		}

		const fn = testFn && typeof testFn === "function" ? testFn : null;

		for ( const name of Object.keys( object ) ) {
			const prop = object[name];

			if ( prop && typeof prop === "object" && !prop.$$doNotSeal$$ && !( isEventEmitter && name === "_events" ) ) {
				path.push( name );
				deepSeal( prop, fn, path );
				path.pop();
			}
		}

		if ( !fn || fn( path ) ) {
			Object.seal( object );
		}
	}

	return object;
}

/**
 * Deeply freezes a given object w/o copying it.
 *
 * @param {object} object object to be frozen deeply
 * @param {function(string[]):boolean} testFn callback invoked to decide whether some property should be frozen or not
 * @param {string[]} path breadcrumb of segments, used internally
 * @returns {object} reference on provided object, now deeply frozen
 */
export function deepFreeze( object, testFn = null, path = [] ) {
	if ( object && typeof object === "object" && !object.$$doNotFreeze$$ && !Object.isFrozen( object ) ) {
		const isEventEmitter = object instanceof EventEmitter;

		if ( isEventEmitter ) {
			// prevent properties managed for event handling from being sealed implicitly
			// see [_addEventListener() of events.js](https://github.com/nodejs/node/blob/master/lib/events.js#L349)
			if ( !Object.prototype.hasOwnProperty.call( object, "_events" ) ) {
				object._events = Object.create( null );
			}

			let counter = object._eventsCount || 0;

			Object.defineProperty( object, "_eventsCount", {
				get: () => counter,
				set: value => { counter = value; },
			} );
		}

		const fn = testFn && typeof testFn === "function" ? testFn : null;

		for ( const name of Object.keys( object ) ) {
			const prop = object[name];

			if ( prop && typeof prop === "object" && !prop.$$doNotFreeze$$ && !( isEventEmitter && name === "_events" ) ) {
				path.push( name );
				deepFreeze( prop, fn, path );
				path.pop();
			}
		}

		if ( !fn || fn( path ) ) {
			Object.freeze( object );
		}
	}

	return object;
}

/**
 * Deeply merges properties of one or more objects into a given target object.
 *
 * @param {object} target object properties of provided sources are merged into
 * @param {object|object[]} sources single source object or list of source objects properties are read from
 * @param {function(string):string} strategyFn callback invoked per property of object to be merged for picking merging strategy
 * @returns {object} reference on provided target object with properties of sources merged
 */
export function deepMerge( target, sources, strategyFn = null ) {
	const _target = target && typeof target === "object" ? target : {};
	const _sources = Array.isArray( sources ) ? sources : sources == null ? [] : [sources];
	const numSources = _sources.length;

	for ( let s = 0; s < numSources; s++ ) {
		merge( _target, _sources[s], strategyFn, [] );
	}

	return _target;

	/**
	 * Recursive merges given source information with some destination using
	 * strategy optionally selected by some callback.
	 *
	 * @param {object} to target properties of `from` are transferred to
	 * @param {*} from value to be merged,
	 * @param {?function(string, string, *, object):string} fn optional callback invoked to select strategy for transferring either property
	 * @param {string[]} segments lists names of properties superordinated to the current one to be merged (breadcrumb into object hierarchy)
	 * @return {object|*} object provided in `to` with properties of object in `from` or non-object value provided in `from`
	 */
	function merge( to, from, fn, segments ) {
		if ( from && typeof from === "object" ) {
			const names = Array.isArray( from ) ? Array.from( Array( from.length ).keys() ) : from instanceof Map ? Array.from( from.keys() ) : Object.keys( from ); // eslint-disable-line max-len
			const numNames = names.length;

			for ( let i = 0; i < numNames; i++ ) {
				const name = names[i];

				if ( name === "__proto__" || name === "constructor" || name === "prototype" ) {
					continue;
				}

				const subSegments = segments.concat( name );
				let sValue = from instanceof Map ? from.get( name ) : from[name];
				let dValue = to[name];
				let strategy;

				if ( sValue === undefined ) {
					strategy = "keep";
				} else if ( dValue && typeof dValue === "object" ) {
					strategy = Array.isArray( dValue ) ? "concat" : "blend";
				} else {
					strategy = "replace";
				}

				if ( fn ) {
					strategy = fn( subSegments, strategy, sValue, dValue );
				}

				switch ( strategy ) {
					case "keep" :
						break;

					default :
					case "replace" :
						to[name] = dValue = null;

						// falls through
					case "concat" :
						if ( strategy === "concat" && !Array.isArray( sValue ) ) {
							sValue = sValue == null ? [] : [sValue];
						}

						// falls through
					case "merge" : // deprecated, use "blend" instead
					case "blend" :
						if ( sValue && typeof sValue === "object" ) {
							if ( Array.isArray( sValue ) ) {
								const concat = strategy === "concat";
								const _numSources = sValue.length;

								if ( dValue == null ) {
									dValue = [];
								} else if ( concat && !Array.isArray( dValue ) ) {
									dValue = [dValue];
								}

								if ( to instanceof Map ) {
									to.set( name, dValue );
								} else {
									to[name] = dValue;
								}

								for ( let j = 0; j < _numSources; j++ ) {
									let item = sValue[j];

									if ( concat || ( Array.isArray( dValue ) && j >= dValue.length ) ) {
										dValue.push( item );
									} else {
										if ( item && typeof item === "object" ) {
											item = merge( {}, item, fn, subSegments.concat( "[]" ) );
										}

										dValue[j] = item;
									}
								}
							} else if ( sValue.constructor === Object || sValue instanceof Map ) {
								// got some native object or Map -> merge recursively
								if ( to instanceof Map ) {
									to.set( name, merge( dValue || {}, sValue, fn, subSegments ) );
								} else {
									to[name] = merge( dValue || {}, sValue, fn, subSegments );
								}
							} else {
								// got instance of some custom class -> replace
								to[name] = sValue;
							}
						} else {
							to[name] = sValue;
						}
				}
			}

			return to;
		}

		return from;
	}
}
