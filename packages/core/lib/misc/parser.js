const pairPtn = /([^#&=]+)(=([^#&]+)?)?(?:$|&)/g;
const bracketPtn = /^([^[]+)(?:\[(.*)])?/;

/**
 * Parses provided string for containing URL-encoded query consisting of
 * key-value-pairs.
 *
 * @param {string} serialized query as extracted from URL or form-encoded request body
 * @param {boolean} isEncodedData set true on providing URI-encoded (request body) data (in opposition to some URL's query)
 * @returns {Object<string,string>} object parsed query data
 */
export function query( serialized, { isEncodedData = false } = {} ) {
	const parsed = {};

	if ( typeof serialized === "string" ) {
		const trimmed = serialized.replace( /^\?/, "" );
		let match;

		while ( ( match = pairPtn.exec( trimmed ) ) ) {
			let name = decodeURIComponent( isEncodedData ? match[1].replace( /\+/g, "%20" ) : match[1] );
			const value = typeof match[3] === "undefined" ? match[2] ? null : true :
				decodeURIComponent( isEncodedData ? match[3].replace( /\+/g, "%20" ) : match[3] );

			let key = bracketPtn.exec( name );
			if ( key ) {
				name = key[1];
				key = key[2];

				if ( key == null ) {
					parsed[name] = value;
				} else if ( key === "" ) {
					let store = parsed[name];

					if ( !store || typeof store !== "object" ) {
						store = parsed[name] = [];
					}

					if ( key === "" ) {
						if ( Array.isArray( store ) ) {
							store.push( value );
						} else {
							store[Object.keys( store ).length] = value;
						}
					}
				} else {
					let store = parsed[name];

					if ( !store || typeof store !== "object" ) {
						store = parsed[name] = {};
					}

					const index = parseInt( key );
					if ( index > -1 ) {
						store[index] = value;
					} else {
						store[key] = value;
					}
				}
			}
		}
	}

	return parsed;
}
