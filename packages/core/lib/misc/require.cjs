/**
 * Provides information included with require statement in CommonJS mode of
 * Node.js for use in ES modules.
 */

/**
 * Exposes information on current runtime's main module which is the module that
 * has been loaded initially for running current process.
 *
 * @type {NodeJS.Module}
 */
exports.main = require.main;
