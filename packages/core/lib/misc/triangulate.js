import File from "node:fs";
import Path from "node:path";
import { fileURLToPath } from "node:url";
import { createRequire } from "node:module";

const require = createRequire( import.meta.url );

const __dirname = Path.dirname( fileURLToPath( import.meta.url ) );

/**
 * Qualifies options to properly select packages involved in running current
 * web application.
 *
 * Included options are:
 *
 * * `projectFolder` selecting folder containing web application to be served
 *   by hitchy
 * * `hitchyFolder` selecting folder containing instance of hitchy framework
 *   to be used
 *
 * Regarding `projectFolder` this method is choosing folder according to this
 * list:
 *
 * 1. existing option named `projectFolder`
 * 2. folder closest to current main script and containing sub "node_modules"
 * 3. folder farthest to current instance of hitchy containing sub "node_modules"
 *
 * @param {Hitchy.Core.Options} options global options provided on starting hitchy
 * @param {string=} currentWorkingDirectory pathname of current working directory
 * @returns {Promise<Hitchy.Core.Options>} promises proper discovery of basic options' fallback values
 */
export default async function triangulate( options, currentWorkingDirectory ) {
	const Logger = await import( "./logger.cjs" );
	const Log = Logger.default.get( "hitchy:bootstrap:triangulate" );

	// choose current hitchy framework instance to do the job
	options.hitchyFolder = Path.resolve( __dirname, "../.." );


	// prefer explicitly provided project folder the most
	if ( options.hasOwnProperty( "projectFolder" ) ) {
		// but make sure it's addressing an actual folder
		const stat = await File.promises.stat( options.projectFolder );

		if ( stat.isDirectory() ) {
			return options;
		}

		throw new Error( "selected project folder does not exist" );
	}


	// if missing explicit selection of project folder:
	let projectFolder;

	try {
		// 1. prefer context of current main script
		projectFolder = await _findDirectory( currentWorkingDirectory || Path.dirname( require.main.filename ), "node_modules", "..", true );
	} catch {
		// 2. check context current instance of hitchy is running in as fallback
		// (assuming hitchy is in node_modules/@hitchy/core folder of that project)
		projectFolder = await _findDirectory( Path.resolve( __dirname, "../../../.." ), "node_modules", "../.." );
	}

	if ( !projectFolder ) {
		Log( "can not detect root folder of current project" );
		throw new Error( "detecting project root folder failed" );
	}

	options.projectFolder = projectFolder;

	return options;


	/**
	 * Tests if given pathname contains selected sub directory or not.
	 *
	 * If test fails on provided pathname but there is a pathname in `step` then
	 * test is repeated once in folder selected by combining current pathname
	 * and step pathname.
	 *
	 * @param {string} pathname pathname of folder probably containing subfolder
	 * @param {string} subDirectory relative pathname of subfolder to test
	 * @param {string} step relative pathname
	 * @param {boolean=} keepIteratingIfFailing set true to keep iterating
	 *        until test is successful (instead of stopping on test failing)
	 * @returns {Promise<string>} promises pathname of preferred match
	 */
	function _findDirectory( pathname, subDirectory, step, keepIteratingIfFailing ) {
		return new Promise( function( resolve, reject ) {
			let latestMatch = null;

			const testPath = path => {
				const modulesPath = Path.resolve( path, subDirectory );

				File.stat( modulesPath, ( err, stat ) => {
					if ( err ) {
						switch ( err.code ) {
							case "EACCES" :
							case "ENOENT" :
								if ( latestMatch ) {
									resolve( latestMatch );
									return;
								}
						}

						reject( err );
						return;
					}

					const isMatch = stat.isDirectory();
					const stopTraversal = keepIteratingIfFailing ? isMatch : !isMatch;

					if ( isMatch ) {
						latestMatch = path;
					}

					if ( stopTraversal ) {
						resolve( latestMatch );
					} else if ( step ) {
						// check if current package is in use by another one
						testPath( Path.resolve( path, step ) );
					} else {
						resolve( latestMatch );
					}
				} );
			};

			testPath( pathname );
		} );
	}
}
