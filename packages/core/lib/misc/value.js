/**
 * Detects boolean keyword in provided input string returning related
 * boolean value.
 *
 * If provided string does not contain any supported keyword or if input
 * can not be handled as a string at all, input is returned as-is.
 *
 * @name Hitchy.Core.UtilityAPI.value.asBoolean
 * @param {string} input string assumed to contain a boolean keyword like "yes"
 * @returns {boolean<*>} detected boolean value, provided value otherwise
 */
export const asBoolean = input => {
	if ( /^\s*(?:y(?:es)?|true|on|set|enabled?|1)\s*$/i.test( input ) ) {
		return true;
	}

	if ( /^\s*(?:no?|false|off|unset|disabled?|0)\s*$/i.test( input ) ) {
		return false;
	}

	return input;
};
