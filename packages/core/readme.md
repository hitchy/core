# Hitchy [![pipeline status](https://gitlab.com/hitchy/core/badges/master/pipeline.svg)](https://gitlab.com/hitchy/core/-/commits/master)

_server-side web application framework_

## License

[MIT](LICENSE)

## Documentation

The official documentation is available at https://core.hitchy.org/.
