import { env } from "node:process";
import File from "node:fs/promises";
import Path from "node:path";
import { fork } from "node:child_process";

import BasicServer from "../../lib/server/basic.js";
import Logger from "../../lib/misc/logger.cjs";

/**
 * Implements CLI action for starting Hitchy application.
 *
 * @param {Hitchy.Core.Options} options global options customizing Hitchy
 * @param {HitchyCLIArguments} args arguments passed for processing in context of start action
 * @returns {Promise} promises action finished processing
 */
export default async function( options, args ) {
	const logInfo = Logger.get( "hitchy:cli:info" );

	if ( args.plugins || args.extensions ) {
		options.pluginsFolder = args.plugins || args.extensions;
	}

	if ( args.plugin ) {
		options.explicitPlugins = Array.isArray( args.plugin ) ? args.plugin : [args.plugin];
	}

	if ( args["explicit-only"] ) {
		options.explicitPluginsOnly = true;
	}

	if ( args["depend-on"] ) {
		options.dependencies = Array.isArray( args["depend-on"] ) ? args["depend-on"] : [args["depend-on"]];
	}

	options.arguments = args;

	if ( args.injector ) {
		return start();
	}

	/*
	 * check if current project contains some script called server.js or
	 * app.js or main.js and invoke this instead of trying to run some own
	 * simple server internally.
	 */
	const files = [
		"server.mjs", "server.cjs", "server.js",
		"app.mjs", "app.cjs", "app.js",
		"main.mjs", "main.cjs", "main.js"
	];

	try {
		for ( let i = 0; i < files.length; i++ ) {
			try {
				const pathname = Path.resolve( options.projectFolder, files[i] );
				const stat = await File.stat( pathname ); // eslint-disable-line no-await-in-loop

				if ( stat.isFile() ) {
					return start( pathname );
				}
			} catch ( cause ) {
				if ( cause.code === "ENOENT" ) {
					continue;
				}

				throw cause;
			}
		}

		return start();
	} catch ( cause ) {
		console.error( "error while looking for start script: " + ( cause.message || cause || "unknown error" ) );
		process.exitCode = 1;
	}

	return undefined;


	/**
	 * Starts server preferring some found script over internal basic server.
	 *
	 * @param {string=} scriptName filename of script to run instead of internal server
	 * @returns {Promise} promises result of having run server (this blocks if server stays in foreground!)
	 */
	function start( scriptName = undefined ) {
		if ( scriptName ) {
			return new Promise( ( resolve, reject ) => {
				logInfo( `invoking custom start script ${scriptName} ...` );

				const child = fork( scriptName, {
					cwd: options.projectFolder,
					env: { ...env, ...options.environment },
				} );

				child.on( "exit", ( status, signal ) => {
					if ( status === 0 ) {
						resolve();
					} else {
						reject( new Error( "application script exited on " + ( status || signal ) ) );
					}
				} );
			} );
		}

		return BasicServer( options, args, () => {
			logInfo( "exiting with %d", process.exitCode );

			process.exit();
		} );
	}
}
