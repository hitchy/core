#!/usr/bin/env node
import Minimist from "minimist";

import LogLevels from "../lib/misc/log-levels.cjs";
import Logger from "../lib/misc/logger.cjs";

const Args = Minimist( process.argv.slice( 2 ) );
const command = Args._.shift() || "start";

if ( Args.help || Args.h ) {
	usage();
} else {
	if ( Args.debug ) {
		Args["log-level"] = "*";
	} else if ( Args.quiet && !Args["log-level"] ) {
		Args["log-level"] = "-*";
	} else if ( Args["log-level"] ) {
		Args["log-level"] = LogLevels[Args["log-level"] || "INFO"] || Args["log-level"];
	} else {
		Args["log-level"] = LogLevels["INFO"];
	}

	Logger.update( Args["log-level"] || process.env.DEBUG );

	process.on( "unhandledRejection", _unhandledRejection );
	process.on( "uncaughtException", _unhandledException );

	/**
	 * @type {Hitchy.Core.Options}
	 */
	const options = {
		debug: Boolean( Args.debug ) || /(?:^|,)\*:debug\d*(?:,|$)/.test( Args["log-level"] ),
		environment: {},
	};

	// process arguments requesting to pass additional environment variables
	const extraEnvironment = Array.isArray( Args.env ) ? Args.env : Args.env ? [Args.env] : [];
	for ( const arg of extraEnvironment ) {
		const split = arg.indexOf( "=" );
		if ( split < 0 ) {
			options.environment[arg] = "1";
		} else {
			options.environment[arg.substring( 0, split )] = arg.substring( split + 1 );
		}
	}

	// use explicitly provided project folder
	if ( Args.project ) {
		options.projectFolder = Args.project;
	}

	import( "../lib/misc/triangulate.js" )
		.then( ( { default: fn } ) => fn( options, process.cwd() ) )
		.then( _options => {
			switch ( command ) {
				case "start" :
					return import( "./cli-commands/start.js" )
						.then( ( { default: fn } ) => fn( _options, Args ) );

				default :
					usage();
			}

			return undefined;
		} )
		.catch( error => {
			console.error( `discovering runtime context failed: ${error.message}` );
		} );
}

/**
 * Dumps help on stderr.
 *
 * @returns {void}
 */
function usage() {
	console.error( `
Usage: hitchy [ <action> ] [ options ]

Supported actions are:

 start                Starts Hitchy serving application in current folder.
                      [default on omitting action]

Common options are:

 --project=path       Selects directory containing hitchy-based project to 
                      control. Defaults to current working directory.
 --debug              Enables noisy logging for debugging purposes.
 --log-level=names    Controls logging facilities (see npm package "debug" or 
                      use DEBUG2, DEBUG, INFO, NOTICE, WARNING, ERROR, ALERT or
                      CRITICAL).
 --env=NAME=VALUE     adds NAME as environment variable with VALUE 
 
Action "start" supports additional options:
 
 --plugins=path       Selects directory containing node_modules folder with 
                      Hitchy plugins to discover. Defaults to project's folder.
 --plugin=path        Requests to discover plugin in provided folder explicitly.
                      This option can be used multiple times.
 --explicit-only      Limits discovered plugins to those provided via --plugin.
 --depend-on=role     Selects plugin role to depend on explicitly. This option
                      can be used multiple times.
 --injector=name      Picks injector to use (default: "node", may be "express").
 --port=number        Chooses port to listen on for incoming requests.
                      Use "auto" for picking some random port number.
 --ip=address         Chooses IP address to listen on for incoming requests.
 --quiet              Suppresses output regarding successful start of service.
 --sslKey=file        Selects file with SSL key for serving over HTTPS.
 --sslCert=file       Selects file with SSL certificate for serving over HTTPS.
 --sslCaCert=file     Selects file with SSL chain certificates.

` );
}

/**
 * Dumps information on unhandled promise rejection on stderr.
 *
 * @param {Error} reason reason for rejecting promise
 * @param {Promise} promise rejected but unhandled promise instance
 * @returns {void}
 * @private
 */
function _unhandledRejection( reason, promise ) {
	console.error( "unhandled rejection of promise", reason, promise );
}

/**
 * Dumps information on unhandled exception on stderr.
 *
 * @param {Error} error thrown error
 * @returns {void}
 * @private
 */
function _unhandledException( error ) {
	console.error( "unhandled exception", error );
}



/**
 * @typedef {Object<string,(string|boolean)>} HitchyCLIOptionArguments
 */

/**
 * @typedef {object} HitchyCLIArguments
 * @extends {HitchyCLIOptionArguments}
 * @property {string[]} _ non-option arguments
 */

