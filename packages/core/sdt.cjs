const Logger = require( "./lib/misc/logger.cjs" );
const AbstractLogger = require( "./lib/misc/logger/abstract.cjs" );
const CapturingLogger = require( "./lib/misc/logger/capture.cjs" );
const LogLevels = require( "./lib/misc/log-levels.cjs" );

const { version } = require( "./package.json" );

/**
 * Exposes API containing minimum set of code necessary for running an instance
 * of Hitchy for testing code in context of @hitchy/server-dev-tools.
 *
 * @type {Hitchy.ServerDevTools.CoreAPI}
 */
module.exports = {
	version,
	Logger,
	AbstractLogger,
	CapturingLogger,
	LogLevels,

	/**
	 * Implements basic version of internal server for exposing hitchy-based
	 * application.
	 *
	 * @param {Hitchy.Core.Options} options global options customizing Hitchy
	 * @param {HitchyCLIArguments} args arguments passed for processing in context of start action
	 * @param {function} onShutdownComplete callback invoked **after** having shut down server
	 * @returns {Promise<{server:Server, hitchy:Hitchy.Core.Instance}>} promises server started
	 */
	BasicServer( options, args, onShutdownComplete ) {
		return import( "./lib/server/basic.js" )
			.then( ( { default: fn } ) => fn( options, args, onShutdownComplete ) );
	},
};
