import { createRequire } from "node:module";

import Logger from "./lib/misc/logger.cjs";
import AbstractLogger from "./lib/misc/logger/abstract.cjs";
import CapturingLogger from "./lib/misc/logger/capture.cjs";
import LogLevels from "./lib/misc/log-levels.cjs";
import BasicServer from "./lib/server/basic.js";

const require = createRequire( import.meta.url );
const { version } = require( "./package.json" );

/**
 * Exposes API containing minimum set of code necessary for running an instance
 * of Hitchy for testing code in context of @hitchy/server-dev-tools.
 *
 * @type {Hitchy.ServerDevTools.CoreAPI}
 */
export default {
	version,
	Logger,
	AbstractLogger,
	CapturingLogger,
	LogLevels,
	BasicServer,
};
