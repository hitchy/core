module.exports = {
	staticProperty: "original static controller property",
	staticMethod: function( req, res ) { // eslint-disable-line no-unused-vars
		return "original static controller method";
	}
};
