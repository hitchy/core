"use strict";

module.exports = {
	staticProperty: "original static policy property",
	staticMethod: function( req, res ) { // eslint-disable-line no-unused-vars
		return "original static policy method";
	}
};
