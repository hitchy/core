exports.earlyScalar = () => "early";
exports.beforeScalar = () => "before";
exports.afterScalar = () => "after";
exports.lateScalar = () => "late";
exports.blueprint = () => "inner";
