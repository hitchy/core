exports.earlyScalar = exports.earlyArray = () => "early";
exports.beforeScalar = exports.beforeArray = () => "before";
exports.afterScalar = exports.afterArray = () => "after";
exports.lateScalar = exports.lateArray = () => "late";
