exports.policies = {
	early: {
		"/scalar": "app.earlyScalar",
		"/array": ["app.earlyArray"],
	},
	before: {
		"/scalar": "app.beforeScalar",
		"/array": ["app.beforeArray"],
	},
	after: {
		"/scalar": "app.afterScalar",
		"/array": ["app.afterArray"],
	},
	late: {
		"/scalar": "app.lateScalar",
		"/array": ["app.lateArray"],
	},
};
