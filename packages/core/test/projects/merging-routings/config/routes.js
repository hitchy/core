exports.routes = {
	early: {
		"/scalar": "app.earlyScalar",
	},
	before: {
		"/scalar": "app.beforeScalar",
	},
	after: {
		"/scalar": "app.afterScalar",
	},
	late: {
		"/scalar": "app.lateScalar",
	},
};
