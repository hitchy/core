module.exports = {
	json( req, res ) {
		res.json( { success: true } );
	},
	send( req, res ) {
		res.send( "success" );
	},
	write( req, res ) {
		res.write( "success" );
		res.end();
	},
	end( req, res ) {
		res.end( "success" );
	},
	setHeader( req, res ) {
		res.setHeader( "x-a", 1 );
		res.setHeader( "x-content-type", 2 );
		res.setHeader( "content-type", "text/plain" );
		res.setHeader( "x-b", "3" );
		res.end( "success" );
	},
	writeHead( req, res ) {
		res.writeHead( 200, {
			"x-a": 1,
			"x-content-type": 2,
			"content-type": "text/plain",
			"x-b": "3",
		} );
		res.end( "success" );
	},
	singleSet( req, res ) {
		res
			.set( "x-a", 1 )
			.set( "x-content-type", 2 )
			.set( "content-type", "text/plain" )
			.set( "x-b", "3" )
			.end( "success" );
	},
	multiSet( req, res ) {
		res.set( {
			"x-a": 1,
			"x-content-type": 2,
			"content-type": "text/plain",
			"x-b": "3",
		} )
			.end( "success" );
	},
	format( req, res ) {
		res.format( {
			html( _req, _res ) {
				_res.end( "<p>success</p>" );
			},
			json( _req, _res ) {
				_res.json( { success: true } );
			},
			text( _req, _res ) {
				_res.end( "success" );
			},
			default( _req, _res ) {
				_res.end( Buffer.from( "\x01success\x02" ) );
			},
		} );
	},
};
