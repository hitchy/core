module.exports = {
	/**
	 * @param {IncomingMessage} req request descriptor
	 * @param {ServerResponse} res response manager
	 * @param {Array} args additional arguments tracked in session
	 * @returns {void}
	 */
	mirror: function( req, res, ...args ) {
		res.json( {
			method: req.method,
			params: req.params,
			args: args,
			query: req.query,
			session: req.session,
			type: "instant",
		} );
	},

	/**
	 * @param {IncomingMessage} req request descriptor
	 * @param {ServerResponse} res response manager
	 * @param {Array} args additional arguments tracked in session
	 * @returns {void}
	 */
	deferredMirror: function( req, res, ...args ) {
		const data = {
			method: req.method,
			params: req.params,
			args: args,
			query: req.query,
			session: req.session,
			type: "deferred",
		};

		setTimeout( () => {
			res.json( data );
		}, 50 );
	},

	/**
	 * @param {IncomingMessage} req request descriptor
	 * @param {ServerResponse} res response manager
	 * @param {Array} args additional arguments tracked in session
	 * @returns {void}
	 */
	fullyDeferredMirror: function( req, res, ...args ) {
		setTimeout( () => {
			res.json( {
				method: req.method,
				params: req.params,
				args: args,
				query: req.query,
				session: req.session,
				type: "deferred",
			} );
		}, 50 );
	},
};
