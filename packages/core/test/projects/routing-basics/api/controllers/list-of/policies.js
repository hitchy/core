exports.check = ( req, res ) => {
	if ( req.custom[0] === 1 && req.custom[1] === 2 &&
	     req.custom[2] === 3 && req.custom[3] === 4 ) {
		res.status( 200 ).json( { success: true } );
	} else {
		res.status( 500 ).json( { failed: true } );
	}
};
