const session = {
	method: {
		early: null,
		late: null,
	},
	params: {},
	query: {},
	args: {
		early: [],
		late: [],
	},
};

/**
 * @param {RegExp} pattern controls current use case
 * @param {IncomingMessage} req request descriptor
 * @param {ServerResponse} res response manager
 * @param {function([error]):void} next callback invoked for triggering next handler
 * @param {Array} args additional arguments tracked in session
 * @returns {void}
 */
function filter( pattern, req, res, next, ...args ) {
	session.method.late = req.method;

	[ "params", "query" ]
		.forEach( set => {
			for ( const name of req[set] || {} ) {
				const match = pattern.exec( name );

				if ( match ) {
					session[set][match[1]] = req[set][name];
				}
			}
		} );

	if ( args.length ) {
		session.args.late = args.slice();
	}

	next();
}


module.exports = {
	/**
	 * @param {IncomingMessage} req request descriptor
	 * @param {ServerResponse} res response manager
	 * @param {function([error]):void} next callback invoked for triggering next handler
	 * @returns {void}
	 */
	inject: function( req, res, next ) {
		req.session = session;

		next();
	},

	myImplementation: () => {},

	/**
	 * @param {IncomingMessage} req request descriptor
	 * @param {ServerResponse} res response manager
	 * @param {function([error]):void} next callback invoked for triggering next handler
	 * @returns {void}
	 */
	early: filter.bind( {}, /^early(\w+)$/ ),

	/**
	 * @param {IncomingMessage} req request descriptor
	 * @param {ServerResponse} res response manager
	 * @param {function([error]):void} next callback invoked for triggering next handler
	 * @returns {void}
	 */
	late: filter.bind( {}, /^late(\w+)$/ ),
};
