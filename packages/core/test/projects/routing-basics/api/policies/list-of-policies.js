exports.one = ( req, res, next ) => {
	req.custom = [1];

	next();
};

exports.two = ( req, res, next ) => {
	req.custom.push( 2 );

	next();
};

exports.three = ( req, res, next ) => {
	req.custom.push( 3 );

	next();
};

exports.four = ( req, res, next ) => {
	req.custom.push( 4 );

	next();
};

exports.plusOne = ( req, res, next ) => {
	global.mySpecialTestCalculationVariable += 1;

	next();
};

exports.plusTwo = ( req, res, next ) => {
	global.mySpecialTestCalculationVariable += 2;

	next();
};

exports.plusThree = ( req, res, next ) => {
	global.mySpecialTestCalculationVariable += 3;

	next();
};

exports.double = ( req, res, next ) => {
	global.mySpecialTestCalculationVariable *= 2;

	next();
};

exports.triple = ( req, res, next ) => {
	global.mySpecialTestCalculationVariable *= 3;

	next();
};

exports.quadruple = ( req, res, next ) => {
	global.mySpecialTestCalculationVariable *= 4;

	next();
};
