module.exports = function( options ) {
	switch ( options.scenario ) {
		case "empty" :
			return {};

		case "simple-controller" :
			return {
				routes: {
					"/instant": "controller.mirror",
					"/partial/deferred": "controller.deferredMirror",
					"/full/deferred": "controller.fullyDeferredMirror",
				},
			};

		case "list-of-policies" :
			return {
				policies: {
					"/": [
						"listOfPolicies.one",
						"listOfPolicies.two",
						"listOfPolicies.three",
						"listOfPolicies.four",
					]
				},
				routes: {
					"/listOfPolicies": "listOfPolicies.check",
				},
			};

		case "early-policies-sorting-generic-first" :
			return {
				policies: {
					"/": [
						"listOfPolicies.one",
						"listOfPolicies.two",
					],
					"/prefix": [
						"listOfPolicies.three",
						"listOfPolicies.four",
					],
					"/prefix/check": [
						"listOfPolicies.three",
						"listOfPolicies.four",
					],
				},
				routes: {
					"/prefix/check": "sortingOfPolicies.check",
				},
			};

		case "early-policies-sorting-specific-first" :
			return {
				policies: {
					"/prefix/check": [
						"listOfPolicies.three",
						"listOfPolicies.four",
					],
					"/prefix": [
						"listOfPolicies.three",
						"listOfPolicies.four",
					],
					"/": [
						"listOfPolicies.one",
						"listOfPolicies.two",
					],
				},
				routes: {
					"/prefix/check": "sortingOfPolicies.check",
				},
			};

		case "late-policies-sorting-generic-first" :
			return {
				policies: {
					after: {
						"/": [
							"listOfPolicies.plusOne",
							"listOfPolicies.double",
						],
						"/prefix": [
							"listOfPolicies.plusTwo",
							"listOfPolicies.triple",
						],
						"/prefix/check": [
							"listOfPolicies.plusThree",
							"listOfPolicies.quadruple",
						],
					},
				},
				routes: {
					"/prefix/check": "sortingOfPolicies.checkLate",
				},
			};

		case "late-policies-sorting-specific-first" :
			return {
				policies: {
					after: {
						"/prefix/check": [
							"listOfPolicies.plusThree",
							"listOfPolicies.quadruple",
						],
						"/prefix": [
							"listOfPolicies.plusTwo",
							"listOfPolicies.triple",
						],
						"/": [
							"listOfPolicies.plusOne",
							"listOfPolicies.double",
						],
					},
				},
				routes: {
					"/prefix/check": "sortingOfPolicies.checkLate",
				},
			};

		case "blueprint" :
			return {
				routes: {
					before: {
						"GET /blueprint/catched": "controller.mirror",
					},
					after: {
						"/blueprint/missed": "controller.mirror",
					}
				},
			};

		case "auto-limit-response" :
			return {
				routes: {
					"ALL /limit/json": "AutoLimitResponse::json",
					"ALL /limit/send": "AutoLimitResponse::send",
					"ALL /limit/write": "AutoLimitResponse::write",
					"ALL /limit/end": "AutoLimitResponse::end",
					"ALL /limit/setHeader": "AutoLimitResponse::setHeader",
					"ALL /limit/writeHead": "AutoLimitResponse::writeHead",
					"ALL /limit/singleSet": "AutoLimitResponse::singleSet",
					"ALL /limit/multiSet": "AutoLimitResponse::multiSet",
					"ALL /limit/format": "AutoLimitResponse::format",
				},
			};

		default :
			return {
				policies: {
					"ALL /": "filter.inject",
					"GET /": "filter.early",
				},
				routes: {
					"/": "controller.mirror",
				},
			};
	}
};
