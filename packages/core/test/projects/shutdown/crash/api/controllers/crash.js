module.exports = function() {
	const api = this;
	const { models, services } = api;

	return {
		viaClosure( req ) {
			api.crash( new Error( "testing crash with " + req.headers["x-cause"] ) );
		},
		viaContext( req ) {
			this.api.crash( new Error( "testing crash with " + req.headers["x-cause"] ) );
		},
		viaHelper( req ) {
			req.hitchy.crash( new Error( "testing crash with " + req.headers["x-cause"] ) );
		},
		viaModel( req ) {
			return models.Crash.handle( req.headers["x-cause"] );
		},
		viaService( req ) {
			return services.Crash.handle( req.headers["x-cause"] );
		},
	};
};
