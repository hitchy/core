import { describe, before, after, it } from "mocha";
import "should";
import SDT from "@hitchy/server-dev-tools";

import Core from "../../../sdt.js";

const Test = await SDT( Core );

describe( "Hitchy's API data property", () => {
	const ctx = {};

	after( Test.after( ctx ) );
	before( Test.before( ctx, {
		tools: Test,
		projectFolder: false,
		files: {
			"config/routes.cjs": `module.exports = function() {
				const api = this;

				return { routes: {
					"/set": ( _, res ) => {
						api.data.value = "has been set";
						res.json( { success: true } );
					},
					"/get": ( _, res ) => {
						res.json( { value: api.data.value } );
					},
				} };
			 };`,
		},
		options: {},
	} ) );

	it( "survives multiple requests", async() => {
		const res = await ctx.get( "/set" );

		res.statusCode.should.equal( 200 );
		res.data.success.should.be.true();

		const res2 = await ctx.get( "/get" );

		res2.statusCode.should.equal( 200 );
		res2.data.value.should.equal( "has been set" );
	} );
} );
