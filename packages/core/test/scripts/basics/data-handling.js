import { describe, before, after, it } from "mocha";
import "should";
import SDT from "@hitchy/server-dev-tools";

import Core from "../../../sdt.js";

const Test = await SDT( Core );

describe( "Data of incoming requests", () => {
	const ctx = {};

	after( Test.after( ctx ) );
	before( Test.before( ctx, {
		tools: Test,
		projectFolder: false,
		files: {
			"config/routes.cjs": `exports.routes = { 
				"ALL /get-query": ( req, res ) => res.json( { query: req.query } ), 
				"ALL /get-params": ( req, res ) => res.json( { params: req.params } ), 
				"ALL /get-header": ( req, res ) => res.json( { headers: req.headers } ), 
				"ALL /get-body": ( req, res ) => req.fetchBody().then( body => res.json( { body } ) ), 
			};`,
		},
		options: {},
	} ) );

	it( "decodes URI-encoded records passed in request body", async() => {
		const { data } = await ctx.post( "/get-body", "a=foo&b=bar+baz+boom", { "content-type": "application/x-www-form-urlencoded" } );

		data.should.be.Object();
		data.body.should.be.Object();
		data.body.a.should.be.equal( "foo" );
		data.body.b.should.be.equal( "bar baz boom" );
	} );
} );
