import { describe, it, before, after } from "mocha";
import "should";
import "should-http";
import SDT from "@hitchy/server-dev-tools";

import Core from "../../../sdt.js";

const Test = await SDT( Core );

const config = {
	projectFolder: "test/projects/empty",
	options: {
		// debug: true,
	},
};

describe( "Hitchy standalone", function() {
	const ctx = {};

	before( Test.before( ctx, config ) );
	after( Test.after( ctx ) );

	it( "can be started", () => ctx.hitchy.onStarted.should.be.Promise().which.is.resolved() );
	it( "can be stopped", () => ctx.hitchy.api.shutdown().should.be.Promise().which.is.resolved() );
} );

describe( "Serving empty project a request accepting HTML", function() {
	const ctx = {};

	before( Test.before( ctx, config ) );
	after( Test.after( ctx ) );

	it( "misses GETting /", function() {
		return ctx.get( "/" )
			.then( response => {
				response.should.have.value( "statusCode", 404 );
				response.should.be.html();
				response.text.should.be.String().and.match( /\bnot\s+found\b/i ).and.match( /<html\b/i );
			} );
	} );

	it( "misses POSTing /", function() {
		return ctx.post( "/" )
			.then( response => {
				response.should.have.value( "statusCode", 404 );
				response.should.be.html();
				response.text.should.be.String().and.match( /\bnot\s+found\b/i ).and.match( /<html\b/i );
			} );
	} );

	it( "misses GETting /view", function() {
		return ctx.get( "/view" )
			.then( response => {
				response.should.have.value( "statusCode", 404 );
				response.should.be.html();
				response.text.should.be.String().and.match( /\bnot\s+found\b/i ).and.match( /<html\b/i );
			} );
	} );

	it( "misses POSTing /view", function() {
		return ctx.post( "/view" )
			.then( response => {
				response.should.have.value( "statusCode", 404 );
				response.should.be.html();
				response.text.should.be.String().and.match( /\bnot\s+found\b/i ).and.match( /<html\b/i );
			} );
	} );

	it( "misses GETting /view/read", function() {
		return ctx.get( "/view/read" )
			.then( response => {
				response.should.have.value( "statusCode", 404 );
				response.should.be.html();
				response.text.should.be.String().and.match( /\bnot\s+found\b/i ).and.match( /<html\b/i );
			} );
	} );

	it( "misses POSTing /view/read", function() {
		return ctx.post( "/view/read" )
			.then( response => {
				response.should.have.value( "statusCode", 404 );
				response.should.be.html();
				response.text.should.be.String().and.match( /\bnot\s+found\b/i ).and.match( /<html\b/i );
			} );
	} );
} );

describe( "Serving empty project a request accepting text", function() {
	const ctx = {};

	before( Test.before( ctx, config ) );
	after( Test.after( ctx ) );

	it( "misses GETting /", function() {
		return ctx.get( "/", { accept: "text/plain" } )
			.then( response => {
				response.should.have.value( "statusCode", 404 );
				response.should.have.contentType( "text/plain" );
				response.text.should.be.String().and.match( /\bnot\s+found\b/i ).and.not.match( /<html\b/i );
			} );
	} );

	it( "misses POSTing /", function() {
		return ctx.post( "/", undefined, { accept: "text/plain" } )
			.then( response => {
				response.should.have.value( "statusCode", 404 );
				response.should.have.contentType( "text/plain" );
				response.text.should.be.String().and.match( /\bnot\s+found\b/i ).and.not.match( /<html\b/i );
			} );
	} );

	it( "misses GETting /view", function() {
		return ctx.get( "/view", { accept: "text/plain" } )
			.then( response => {
				response.should.have.value( "statusCode", 404 );
				response.should.have.contentType( "text/plain" );
				response.text.should.be.String().and.match( /\bnot\s+found\b/i ).and.not.match( /<html\b/i );
			} );
	} );

	it( "misses POSTing /view", function() {
		return ctx.post( "/view", undefined, { accept: "text/plain" } )
			.then( response => {
				response.should.have.value( "statusCode", 404 );
				response.should.have.contentType( "text/plain" );
				response.text.should.be.String().and.match( /\bnot\s+found\b/i ).and.not.match( /<html\b/i );
			} );
	} );

	it( "misses GETting /view/read", function() {
		return ctx.get( "/view/read", { accept: "text/plain" } )
			.then( response => {
				response.should.have.value( "statusCode", 404 );
				response.should.have.contentType( "text/plain" );
				response.text.should.be.String().and.match( /\bnot\s+found\b/i ).and.not.match( /<html\b/i );
			} );
	} );

	it( "misses POSTing /view/read", function() {
		return ctx.post( "/view/read", undefined, { accept: "text/plain" } )
			.then( response => {
				response.should.have.value( "statusCode", 404 );
				response.should.have.contentType( "text/plain" );
				response.text.should.be.String().and.match( /\bnot\s+found\b/i ).and.not.match( /<html\b/i );
			} );
	} );
} );

describe( "Serving empty project a request accepting JSON", function() {
	const ctx = {};

	before( Test.before( ctx, config ) );
	after( Test.after( ctx ) );

	it( "misses GETting /", function() {
		return ctx.get( "/", { accept: "application/json" } )
			.then( response => {
				response.should.have.value( "statusCode", 404 );
				response.should.be.json();
				response.should.not.have.property( "text" );
				response.data.should.have.property( "error" );
				response.data.error.should.be.String().and.match( /\bnot\s+found\b/i ).and.not.match( /<html\b/i );
				response.data.should.have.property( "code" );
			} );
	} );

	it( "misses POSTing /", function() {
		return ctx.post( "/", undefined, { accept: "application/json" } )
			.then( response => {
				response.should.have.value( "statusCode", 404 );
				response.should.be.json();
				response.should.not.have.property( "text" );
				response.data.should.have.property( "error" );
				response.data.error.should.be.String().and.match( /\bnot\s+found\b/i ).and.not.match( /<html\b/i );
				response.data.should.have.property( "code" );
			} );
	} );

	it( "misses GETting /view", function() {
		return ctx.get( "/view", { accept: "application/json" } )
			.then( response => {
				response.should.have.value( "statusCode", 404 );
				response.should.be.json();
				response.should.not.have.property( "text" );
				response.data.should.have.property( "error" );
				response.data.error.should.be.String().and.match( /\bnot\s+found\b/i ).and.not.match( /<html\b/i );
				response.data.should.have.property( "code" );
			} );
	} );

	it( "misses POSTing /view", function() {
		return ctx.post( "/view", undefined, { accept: "application/json" } )
			.then( response => {
				response.should.have.value( "statusCode", 404 );
				response.should.be.json();
				response.should.not.have.property( "text" );
				response.data.should.have.property( "error" );
				response.data.error.should.be.String().and.match( /\bnot\s+found\b/i ).and.not.match( /<html\b/i );
				response.data.should.have.property( "code" );
			} );
	} );

	it( "misses GETting /view/read", function() {
		return ctx.get( "/view/read", { accept: "application/json" } )
			.then( response => {
				response.should.have.value( "statusCode", 404 );
				response.should.be.json();
				response.should.not.have.property( "text" );
				response.data.should.have.property( "error" );
				response.data.error.should.be.String().and.match( /\bnot\s+found\b/i ).and.not.match( /<html\b/i );
				response.data.should.have.property( "code" );
			} );
	} );

	it( "misses POSTing /view/read", function() {
		return ctx.post( "/view/read", undefined, { accept: "application/json" } )
			.then( response => {
				response.should.have.value( "statusCode", 404 );
				response.should.be.json();
				response.should.not.have.property( "text" );
				response.data.should.have.property( "error" );
				response.data.error.should.be.String().and.match( /\bnot\s+found\b/i ).and.not.match( /<html\b/i );
				response.data.should.have.property( "code" );
			} );
	} );
} );
