import { describe, it, after, before } from "mocha";
import "should";
import "should-http";
import SDT from "@hitchy/server-dev-tools";

import Core from "../../../sdt.js";

const Test = await SDT( Core );

const config = {
	projectFolder: "test/projects/core-only",
	options: {
		// debug: true,
	},
};

describe( "Hitchy node running project with routed controllers for dispatching requests", () => {
	const ctx = {};

	before( Test.before( ctx, { ...config } ) );
	after( Test.after( ctx ) );

	it( "handles GET-request for /internal/dispatch", () => {
		return ctx.get( "/internal/dispatch" )
			.then( res => {
				res.should.have.status( 500 );
				res.data.should.be.Object().which.has.property( "error" )
					.which.is.equal( "This result has NOT been fetched using internal dispatching." );
			} );
	} );

	it( "forwards request internally to same URL using `req.hitchy.Client`", () => {
		return ctx.get( "/forward" )
			.then( res => {
				res.should.have.status( 200 );
				res.data.should.be.Object().which.has.property( "info" )
					.which.is.equal( "This result has been fetched using internal dispatching." );
			} );
	} );
} );
