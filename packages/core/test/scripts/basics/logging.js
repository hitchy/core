import { describe, it, afterEach } from "mocha";
import SDT from "@hitchy/server-dev-tools";
import "should";

import Core from "../../../sdt.js";
import { SortedLogLevelNames } from "../../../lib/misc/log-levels.cjs";

const Test = await SDT( Core );

const config = {
	projectFolder: false,
	files: {
		"config/routes.mjs": `export const routes = { "/foo{/:level}": ( req, res ) => { 
			req.api.log( "foo:bar:bam:debug" )( "hello!" );
			req.api.log( "foo:debug" )( "hey!" );
			req.api.log( "bam:foo:bar:debug" )( "hi!" );
			req.api.log( \`foo:\${req.params.level || "info"}\` )( "hallo!" );
			req.api.log( "foo:bar:bam" )( "ciao!" );

			res.send( "done" );
		} };`
	},
	options: {
		// debug: true,
	},
};

const configWithArgs = ( args = undefined ) => ( {
	...config,
	options: {
		logger: new Core.CapturingLogger(),
		...config.options,
		...args ? { arguments: args } : {},
	}
} );

describe( "Log level control", () => {
	const ctx = {};

	afterEach( Test.after( ctx ) );

	[ "debug2", "debug" ]
		.forEach( level => {
			it( `disables messages on levels ending in \`:${level}\` by default`, async() => {
				await Test.before( ctx, configWithArgs( { quiet: false } ) )();

				( await ctx.get( "/foo/" + level ) ).text.should.equal( "done" );

				ctx.hitchyOptions.logger.logged.should.not.containEql( `foo:${level} hallo!` );
			} );
		} );

	[ "info", "notice", "warning", "error", "alert", "critical" ]
		.forEach( level => {
			it( `enables messages on levels ending in \`:${level}\` by default`, async() => {
				await Test.before( ctx, configWithArgs( { quiet: false } ) )();

				( await ctx.get( "/foo/" + level ) ).text.should.equal( "done" );

				ctx.hitchyOptions.logger.logged.should.containEql( `foo:${level} hallo!` );
			} );
		} );

	it( "enables messages on all levels when using --debug argument", async() => {
		await Test.before( ctx, configWithArgs( { debug: true } ) )();

		( await ctx.get( "/foo" ) ).text.should.equal( "done" );

		ctx.hitchyOptions.logger.logged.should.containEql( `foo:bar:bam:debug hello!` );

		for ( let i = 0; i < SortedLogLevelNames.length; i++ ) {
			const testLevel = SortedLogLevelNames[i];

			( await ctx.get( "/foo/" + testLevel ) ).text.should.equal( "done" ); // eslint-disable-line no-await-in-loop

			ctx.hitchyOptions.logger.logged.should.containEql( `foo:${testLevel} hallo!` );
		}
	} );

	it( "enables messages on all levels ending in `:debug` when using --log-level=*:debug", async() => {
		await Test.before( ctx, configWithArgs( { "log-level": "*:debug" } ) )();

		( await ctx.get( "/foo" ) ).text.should.equal( "done" );

		ctx.hitchyOptions.logger.logged.should.containEql( "foo:bar:bam:debug hello!" );
	} );

	it( "enables messages on all levels in scope of `foo:` ending in `:debug` when using --log-level=foo:*:debug", async() => {
		await Test.before( ctx, configWithArgs( { "log-level": "foo:*:debug" } ) )();

		( await ctx.get( "/foo" ) ).text.should.equal( "done" );

		ctx.hitchyOptions.logger.logged.should.containEql( "foo:bar:bam:debug hello!" );
	} );

	it( "enables messages on all levels in scope of `foo:bar:` ending in `:debug` when using --log-level=foo:bar:*:debug", async() => {
		await Test.before( ctx, configWithArgs( { "log-level": "foo:bar:*:debug" } ) )();

		( await ctx.get( "/foo" ) ).text.should.equal( "done" );

		ctx.hitchyOptions.logger.logged.should.containEql( "foo:bar:bam:debug hello!" );
	} );

	it( "disables messages in particular namespace `foo:bar:bam:debug` when using --log-level=foo:bar:bam:*:debug", async() => {
		await Test.before( ctx, configWithArgs( { "log-level": "foo:bar:bam:*:debug" } ) )();

		( await ctx.get( "/foo" ) ).text.should.equal( "done" );

		ctx.hitchyOptions.logger.logged.should.not.containEql( "foo:bar:bam:debug hello!" );
	} );

	it( "enables messages in particular namespace `foo:bar:bam:debug` when using --log-level=foo:bar:bam:debug", async() => {
		await Test.before( ctx, configWithArgs( { "log-level": "foo:bar:bam:debug" } ) )();

		( await ctx.get( "/foo" ) ).text.should.equal( "done" );

		ctx.hitchyOptions.logger.logged.should.containEql( "foo:bar:bam:debug hello!" );
	} );

	SortedLogLevelNames.forEach( ( level, refIndex ) => {
		const LEVEL = level.toUpperCase();

		it( `supports special log level name ${LEVEL} when using --log-level argument`, async() => {
			await Test.before( ctx, configWithArgs( { "log-level": LEVEL } ) )();

			for ( let i = 0; i < SortedLogLevelNames.length; i++ ) {
				const testLevel = SortedLogLevelNames[i];

				( await ctx.get( "/foo/" + testLevel ) ).text.should.equal( "done" ); // eslint-disable-line no-await-in-loop

				if ( i < refIndex ) {
					ctx.hitchyOptions.logger.logged.should.not.containEql( `foo:${testLevel} hallo!` );
				} else {
					ctx.hitchyOptions.logger.logged.should.containEql( `foo:${testLevel} hallo!` );
				}
			}
		} );
	} );
} );
