import { createRequire } from "node:module";
import { Server } from "node:http";

import { describe, it } from "mocha";
import "should";

const require = createRequire( import.meta.url );

describe( "API for integrating server dev tools with Hitchy's core", () => {
	it( "is available as ES module", async() => {
		await assertApi( { ...( await import( "../../../sdt.js" ) ).default } );
	} );

	it( "is available as CommonJs module imported synchronously using require()", async() => {
		await assertApi( require( "../../../sdt.cjs" ) );
	} );

	it( "is available as CommonJs module imported asynchronously using import()", async() => {
		await assertApi( { ...( await import( "../../../sdt.cjs" ) ).default } );
	} );
} );

/**
 * Asserts provided API to meet elements necessary for integrating SDT with
 * Hitchy's core.
 *
 * @param {object} api API to test
 * @returns {Promise<void>} promise resolved if API is valid
 */
async function assertApi( api ) { // eslint-disable-line consistent-this
	api.should.be.an.Object();

	api.CapturingLogger.should.be.a.Function();
	api.AbstractLogger.should.be.a.Function();
	api.Logger.should.be.a.Function();

	api.LogLevels.should.be.an.Object();
	api.LogLevels.DEBUG2.should.be.a.String();
	api.LogLevels.DEBUG.should.be.a.String();
	api.LogLevels.INFO.should.be.a.String();
	api.LogLevels.NOTICE.should.be.a.String();
	api.LogLevels.WARNING.should.be.a.String();
	api.LogLevels.ERROR.should.be.a.String();
	api.LogLevels.ALERT.should.be.a.String();
	api.LogLevels.CRITICAL.should.be.a.String();

	api.LogLevels.SortedLogLevelNames.should.be.an.Array();
	api.LogLevels.SortedLogLevelNames.every( name => name && typeof name === "string" ).should.be.true();

	api.BasicServer.should.be.a.Function().which.has.length( 3 );

	const ctx = await api.BasicServer( {}, {}, () => {} );

	ctx.server.should.be.instanceof( Server );
	ctx.hitchy.api.should.be.Object();

	ctx.hitchy.api.service.ControllerRoute.should.be.a.Function();

	await ctx.hitchy.api.shutdown();
}
