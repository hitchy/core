import { describe, it, before, after } from "mocha";
import Should from "should";
import SDT from "@hitchy/server-dev-tools";

import Core from "../../../sdt.js";

const Test = await SDT( Core );

const config = {
	projectFolder: "test/projects/setup-teardown",
	options: {
		// debug: true,
	},
};

describe( "A Hitchy application", function() {
	const ctx = {};

	before( Test.before( ctx, config ) );
	after( Test.after( ctx ) );

	it( "may provide initialize.js in project root to be invoked at end of bootstrap", () => {
		global.startedHitchyProjectNamedSetupTeardown.should.be.true();
		global.startedHitchyProjectNamedSetupTeardownWith.should.be.Object().which.has.size( 2 ).and.has.properties( "options", "api" );
		global.startedHitchyProjectNamedSetupTeardownWith.options.projectFolder.replace( /\\/g, "/" ).should.endWith( "test/projects/setup-teardown" );

		Should( global.stoppedHitchyProjectNamedSetupTeardown ).not.be.ok();
		Should( global.stoppedHitchyProjectNamedSetupTeardownWith ).not.be.ok();
	} );

	it( "may provide shutdown.js in project root to be invoked right before shutting down all plugins", () => {
		return ctx.hitchy.api.shutdown()
			.then( () => {
				global.stoppedHitchyProjectNamedSetupTeardown.should.be.true();
				global.stoppedHitchyProjectNamedSetupTeardownWith.should.be.Object().which.has.size( 2 ).and.has.properties( "options", "api" );
				global.stoppedHitchyProjectNamedSetupTeardownWith.options.projectFolder.replace( /\\/g, "/" ).should.endWith( "test/projects/setup-teardown" );
			} );
	} );
} );
