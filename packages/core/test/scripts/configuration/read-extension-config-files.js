import { describe, it, after, before } from "mocha";
import "should";
import "should-http";
import SDT from "@hitchy/server-dev-tools";

import Core from "../../../sdt.js";

const Test = await SDT( Core );

const config = {
	projectFolder: "test/projects/configuration/plugin-file",
	options: {
		// debug: true,
	},
};

describe( "Hitchy", function() {
	const ctx = {};

	before( Test.before( ctx, config ) );
	after( Test.after( ctx ) );

	it( "is reading configuration files provided by discovered plugin", function() {
		ctx.hitchy.api.config.should.be.Object();
		ctx.hitchy.api.config.should.have.property( "custom" ).which.is.an.Object().which.has.property( "visible" ).which.is.true();
		ctx.hitchy.api.config.should.have.property( "customConfig" ).which.is.an.Object().which.has.property( "additional" ).which.is.true();
		ctx.hitchy.api.config.should.have.property( "customConfig" ).which.is.an.Object().which.has.property( "overloaded" ).which.is.equal( "yes" );
	} );
} );
