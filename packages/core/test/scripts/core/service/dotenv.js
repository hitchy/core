import { writeFile } from "node:fs/promises";
import { describe, it, beforeEach, afterEach } from "mocha";
import "should";
import SDT from "@hitchy/server-dev-tools";

import Core from "../../../../sdt.js";

const Test = await SDT( Core );

const config = {
	projectFolder: "test/projects/empty",
	files: {}, // necessary to have tests work on a temporary copy of selected project
	options: {
		// debug: true,
	},
};

describe( "Service Dotenv", () => {
	const ctx = {};

	beforeEach( Test.before( ctx, { ...config } ) );
	afterEach( Test.after( ctx ) );

	it( "is available", () => {
		ctx.hitchy.api.service.should.have.property( "Dotenv" );
	} );

	it( "reads and parses content of a selected .env file", async() => {
		const file = ctx.hitchy.api.folder( ".env.extra" );
		await writeFile( file, "dotenv-foo-test=bar", "utf-8" );

		const env = await ctx.hitchy.api.service.Dotenv.readFile( file );
		env.should.have.property( "dotenv-foo-test" ).which.is.equal( "bar" );
	} );
} );
