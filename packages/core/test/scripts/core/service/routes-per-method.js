import { describe, beforeEach, afterEach, it } from "mocha";
import "should";
import "should-http";
import SDT from "@hitchy/server-dev-tools";

import Core from "../../../../sdt.js";

const Test = await SDT( Core );

describe( "Service RoutesPerMethod", function() {
	const ctx = {};

	afterEach( Test.after( ctx ) );
	beforeEach( Test.before( ctx, {
		files: {},
		projectFolder: "test/projects/empty",
		options: {
			// debug: true,
		}
	} ) );

	it( "exists", function() {
		const { RoutesPerMethod } = ctx.hitchy.api.service;

		return RoutesPerMethod.should.be.ok().and.should.be.Object();
	} );

	it( "can be instantiated", () => {
		const { RoutesPerMethod } = ctx.hitchy.api.service;

		( () => new RoutesPerMethod() ).should.not.throw();
	} );

	it( "exposes collection of routes per method", () => {
		const { RoutesPerMethod } = ctx.hitchy.api.service;

		const collector = new RoutesPerMethod();
		collector.should.have.property( "methods" );
		collector.methods.should.be.Object().and.be.ok();
	} );

	it( "exposes flag indicating whether instance is adjustable or not", () => {
		const { RoutesPerMethod } = ctx.hitchy.api.service;

		const collector = new RoutesPerMethod();
		collector.should.have.property( "isAdjustable" );
		collector.isAdjustable.should.be.Boolean();
	} );

	it( "provides method for adding routes", function() {
		const { RoutesPerMethod, ControllerRoute } = ctx.hitchy.api.service;

		new RoutesPerMethod().append.should.be.Function().and.have.length( 1 );

		( () => new RoutesPerMethod().append() ).should.throw();
		( () => new RoutesPerMethod().append( undefined ) ).should.throw();
		( () => new RoutesPerMethod().append( null ) ).should.throw();
		( () => new RoutesPerMethod().append( true ) ).should.throw();
		( () => new RoutesPerMethod().append( false ) ).should.throw();
		( () => new RoutesPerMethod().append( 0 ) ).should.throw();
		( () => new RoutesPerMethod().append( "0" ) ).should.throw();
		( () => new RoutesPerMethod().append( "" ) ).should.throw();
		( () => new RoutesPerMethod().append( {} ) ).should.throw();
		( () => new RoutesPerMethod().append( { prefix: "/" } ) ).should.throw();
		( () => new RoutesPerMethod().append( [] ) ).should.throw();
		( () => new RoutesPerMethod().append( ["/"] ) ).should.throw();

		new RoutesPerMethod().append( new ControllerRoute( "/", () => {} ) ).should.not.throw();

		const collector = new RoutesPerMethod();
		collector.concat( new RoutesPerMethod() ).should.equal( collector );
	} );

	it( "provides method for concatenating two sets of lists", function() {
		const { RoutesPerMethod } = ctx.hitchy.api.service;

		new RoutesPerMethod().concat.should.be.Function().and.have.length( 1 );

		( () => new RoutesPerMethod().concat() ).should.throw();
		( () => new RoutesPerMethod().concat( undefined ) ).should.throw();
		( () => new RoutesPerMethod().concat( null ) ).should.throw();
		( () => new RoutesPerMethod().concat( true ) ).should.throw();
		( () => new RoutesPerMethod().concat( false ) ).should.throw();
		( () => new RoutesPerMethod().concat( 0 ) ).should.throw();
		( () => new RoutesPerMethod().concat( "0" ) ).should.throw();
		( () => new RoutesPerMethod().concat( "" ) ).should.throw();
		( () => new RoutesPerMethod().concat( {} ) ).should.throw();
		( () => new RoutesPerMethod().concat( { prefix: "/" } ) ).should.throw();
		( () => new RoutesPerMethod().concat( [] ) ).should.throw();
		( () => new RoutesPerMethod().concat( ["/"] ) ).should.throw();

		( () => new RoutesPerMethod().concat( new RoutesPerMethod() ) ).should.not.throw();

		const collector = new RoutesPerMethod();
		collector.concat( new RoutesPerMethod() ).should.equal( collector );
	} );

	it( "provides method for fetching routes associated with an HTTP method", function() {
		const { RoutesPerMethod, RoutesPerPrefix, ControllerRoute } = ctx.hitchy.api.service;

		new RoutesPerMethod().onMethod.should.be.Function().and.have.length( 1 );

		( () => new RoutesPerMethod().onMethod() ).should.throw();
		( () => new RoutesPerMethod().onMethod( undefined ) ).should.throw();
		( () => new RoutesPerMethod().onMethod( null ) ).should.throw();
		( () => new RoutesPerMethod().onMethod( true ) ).should.throw();
		( () => new RoutesPerMethod().onMethod( false ) ).should.throw();
		( () => new RoutesPerMethod().onMethod( 0 ) ).should.throw();
		( () => new RoutesPerMethod().onMethod( "" ) ).should.throw();
		( () => new RoutesPerMethod().onMethod( {} ) ).should.throw();
		( () => new RoutesPerMethod().onMethod( { prefix: "/" } ) ).should.throw();
		( () => new RoutesPerMethod().onMethod( [] ) ).should.throw();
		( () => new RoutesPerMethod().onMethod( ["/"] ) ).should.throw();

		( () => new RoutesPerMethod().onMethod( "0" ) ).should.not.throw();
		( () => new RoutesPerMethod().onMethod( "GET" ) ).should.not.throw();

		( new RoutesPerMethod().onMethod( "GET" ) == null ).should.be.true();

		const collector = new RoutesPerMethod();
		collector.append( new ControllerRoute( "/", () => {} ) );

		collector.onMethod( "GET" ).should.be.instanceof( RoutesPerPrefix );
	} );

	it( "provides method for optimizing sets of routes", function() {
		const { RoutesPerMethod } = ctx.hitchy.api.service;

		new RoutesPerMethod().optimizeByPrefix.should.be.Function().have.length( 0 );

		( () => new RoutesPerMethod().optimizeByPrefix() ).should.not.throw();
		( () => new RoutesPerMethod().optimizeByPrefix( undefined ) ).should.not.throw();
		( () => new RoutesPerMethod().optimizeByPrefix( null ) ).should.not.throw();
		( () => new RoutesPerMethod().optimizeByPrefix( true ) ).should.not.throw();
		( () => new RoutesPerMethod().optimizeByPrefix( false ) ).should.not.throw();
		( () => new RoutesPerMethod().optimizeByPrefix( 0 ) ).should.not.throw();
		( () => new RoutesPerMethod().optimizeByPrefix( "" ) ).should.not.throw();
		( () => new RoutesPerMethod().optimizeByPrefix( {} ) ).should.not.throw();
		( () => new RoutesPerMethod().optimizeByPrefix( { prefix: "/" } ) ).should.not.throw();
		( () => new RoutesPerMethod().optimizeByPrefix( [] ) ).should.not.throw();
		( () => new RoutesPerMethod().optimizeByPrefix( ["/"] ) ).should.not.throw();

		( () => new RoutesPerMethod().optimizeByPrefix( "0" ) ).should.not.throw();
		( () => new RoutesPerMethod().optimizeByPrefix( "GET" ) ).should.not.throw();

		const collector = new RoutesPerMethod();
		collector.optimizeByPrefix().should.equal( collector );
	} );

	it( "becomes inadjustable on optimizing sets of routes", function() {
		const { RoutesPerMethod, ControllerRoute } = ctx.hitchy.api.service;

		const collector = new RoutesPerMethod();

		collector.isAdjustable.should.be.ok();
		( () => collector.append( new ControllerRoute( "/", () => {} ) ) ).should.not.throw();

		collector.optimizeByPrefix();

		collector.isAdjustable.should.be.false();
		( () => collector.append( new ControllerRoute( "/", () => {} ) ) ).should.throw();
	} );

	it( "exposes initially empty map of method names into sorted lists of routes bound it", function() {
		const { RoutesPerMethod, ControllerRoute } = ctx.hitchy.api.service;

		const collector = new RoutesPerMethod();
		const route = new ControllerRoute( "GET /", () => {} );

		collector.methods.should.be.empty();

		collector.append( route );
		collector.methods.should.not.be.empty();
	} );

	it( "collects sets of non-unique routes per method each route is bound to", function() {
		const { RoutesPerMethod, ControllerRoute } = ctx.hitchy.api.service;

		const collector = new RoutesPerMethod();

		collector.methods.should.be.empty();

		collector.append( new ControllerRoute( "GET /", () => {} ) );
		collector.methods.should.have.properties( "GET" ).and.have.size( 1 );
		collector.methods.GET.should.be.Array().and.have.length( 1 );

		collector.append( new ControllerRoute( "GET /", () => {} ) );
		collector.methods.GET.should.be.Array().and.have.length( 2 );

		collector.append( new ControllerRoute( "GET /", () => {} ) );
		collector.methods.GET.should.be.Array().and.have.length( 3 );
	} );

	it( "collects appended routes in context of method each route is bound to", function() {
		const { RoutesPerMethod, ControllerRoute } = ctx.hitchy.api.service;

		const collector = new RoutesPerMethod();

		collector.methods.should.be.empty();

		collector.append( new ControllerRoute( "GET /", () => {} ) );
		collector.methods.should.have.properties( "GET" ).and.have.size( 1 );
		collector.methods.GET.should.be.Array().and.have.length( 1 );

		collector.append( new ControllerRoute( "GET /test", () => {} ) );
		collector.methods.GET.should.be.Array().and.have.length( 2 );

		collector.append( new ControllerRoute( "POST /", () => {} ) );
		collector.methods.should.have.properties( "GET", "POST" ).and.have.size( 2 );
		collector.methods.GET.should.be.Array().and.have.length( 2 );
		collector.methods.POST.should.be.Array().and.have.length( 1 );

		collector.append( new ControllerRoute( "POST /test", () => {} ) );
		collector.methods.should.have.properties( "GET", "POST" ).and.have.size( 2 );
		collector.methods.GET.should.be.Array().and.have.length( 2 );
		collector.methods.POST.should.be.Array().and.have.length( 2 );

		collector.append( new ControllerRoute( "PUT /test", () => {} ) );
		collector.methods.should.have.properties( "GET", "POST", "PUT" ).and.have.size( 3 );
		collector.methods.GET.should.be.Array().and.have.length( 2 );
		collector.methods.POST.should.be.Array().and.have.length( 2 );
		collector.methods.PUT.should.be.Array().and.have.length( 1 );

		collector.append( new ControllerRoute( "PUT /", () => {} ) );
		collector.methods.should.have.properties( "GET", "POST", "PUT" ).and.have.size( 3 );
		collector.methods.GET.should.be.Array().and.have.length( 2 );
		collector.methods.POST.should.be.Array().and.have.length( 2 );
		collector.methods.PUT.should.be.Array().and.have.length( 2 );

		collector.methods.GET[0].path.should.equal( "/" );
		collector.methods.GET[1].path.should.equal( "/test" );
		collector.methods.POST[0].path.should.equal( "/" );
		collector.methods.POST[1].path.should.equal( "/test" );
		collector.methods.PUT[0].path.should.equal( "/test" );
		collector.methods.PUT[1].path.should.equal( "/" );
	} );

	it( "collects appended routes not bound to any method in context of any previously collected method, too", function() {
		const { RoutesPerMethod, ControllerRoute } = ctx.hitchy.api.service;

		const collector = new RoutesPerMethod();

		collector.methods.should.be.empty();

		collector.append( new ControllerRoute( "GET /", () => {} ) );
		collector.methods.should.have.properties( "GET" ).and.have.size( 1 );
		collector.methods.GET.should.be.Array().and.have.length( 1 );

		collector.append( new ControllerRoute( "GET /test", () => {} ) );
		collector.methods.GET.should.be.Array().and.have.length( 2 );

		collector.append( new ControllerRoute( "POST /", () => {} ) );
		collector.methods.should.have.properties( "GET", "POST" ).and.have.size( 2 );
		collector.methods.GET.should.be.Array().and.have.length( 2 );
		collector.methods.POST.should.be.Array().and.have.length( 1 );

		collector.append( new ControllerRoute( "POST /test", () => {} ) );
		collector.methods.should.have.properties( "GET", "POST" ).and.have.size( 2 );
		collector.methods.GET.should.be.Array().and.have.length( 2 );
		collector.methods.POST.should.be.Array().and.have.length( 2 );

		collector.append( new ControllerRoute( "ALL /test", () => {} ) );
		collector.methods.should.have.properties( "GET", "POST", "ALL" ).and.have.size( 3 );
		collector.methods.GET.should.be.Array().and.have.length( 3 );
		collector.methods.POST.should.be.Array().and.have.length( 3 );
		collector.methods.ALL.should.be.Array().and.have.length( 1 );

		collector.append( new ControllerRoute( "ALL /", () => {} ) );
		collector.methods.should.have.properties( "GET", "POST", "ALL" ).and.have.size( 3 );
		collector.methods.GET.should.be.Array().and.have.length( 4 );
		collector.methods.POST.should.be.Array().and.have.length( 4 );
		collector.methods.ALL.should.be.Array().and.have.length( 2 );

		collector.methods.GET[0].path.should.equal( "/" );
		collector.methods.GET[1].path.should.equal( "/test" );
		collector.methods.GET[2].path.should.equal( "/test" );
		collector.methods.GET[3].path.should.equal( "/" );
		collector.methods.POST[0].path.should.equal( "/" );
		collector.methods.POST[1].path.should.equal( "/test" );
		collector.methods.POST[2].path.should.equal( "/test" );
		collector.methods.POST[3].path.should.equal( "/" );
		collector.methods.ALL[0].path.should.equal( "/test" );
		collector.methods.ALL[1].path.should.equal( "/" );
	} );

	it( "starts sets for any new method with previously collected routes not bound to any method", function() {
		const { RoutesPerMethod, ControllerRoute } = ctx.hitchy.api.service;

		const collector = new RoutesPerMethod();

		collector.methods.should.be.empty();

		collector.append( new ControllerRoute( "GET /get", () => {} ) );
		collector.methods.should.have.properties( "GET" ).and.have.size( 1 );
		collector.methods.GET.should.be.Array().and.have.length( 1 );

		collector.append( new ControllerRoute( "ALL /all", () => {} ) );
		collector.methods.should.have.properties( "GET", "ALL" ).and.have.size( 2 );
		collector.methods.GET.should.be.Array().and.have.length( 2 );
		collector.methods.ALL.should.be.Array().and.have.length( 1 );

		collector.append( new ControllerRoute( "POST /post", () => {} ) );
		collector.methods.should.have.properties( "GET", "ALL", "POST" ).and.have.size( 3 );
		collector.methods.GET.should.be.Array().and.have.length( 2 );
		collector.methods.ALL.should.be.Array().and.have.length( 1 );
		collector.methods.POST.should.be.Array().and.have.length( 2 );

		collector.methods.GET[0].path.should.equal( "/get" );
		collector.methods.GET[1].path.should.equal( "/all" );
		collector.methods.ALL[0].path.should.equal( "/all" );
		collector.methods.POST[0].path.should.equal( "/all" );
		collector.methods.POST[1].path.should.equal( "/post" );
	} );

	it( "converts simple lists of routes per method into instances of RoutesPerPrefix per method on optimizing", function() {
		const { RoutesPerMethod, RoutesPerPrefix, ControllerRoute } = ctx.hitchy.api.service;

		const collector = new RoutesPerMethod();

		collector.methods.should.be.empty();

		collector.append( new ControllerRoute( "GET /get", () => {} ) );
		collector.append( new ControllerRoute( "GET /get2", () => {} ) );
		collector.append( new ControllerRoute( "POST /post", () => {} ) );
		collector.append( new ControllerRoute( "POST /post2", () => {} ) );
		collector.append( new ControllerRoute( "PUT /put", () => {} ) );
		collector.append( new ControllerRoute( "PUT /put2", () => {} ) );

		collector.methods.GET.should.be.Array();
		collector.methods.POST.should.be.Array();
		collector.methods.PUT.should.be.Array();

		collector.optimizeByPrefix();

		collector.methods.GET.should.be.instanceof( RoutesPerPrefix );
		collector.methods.POST.should.be.instanceof( RoutesPerPrefix );
		collector.methods.PUT.should.be.instanceof( RoutesPerPrefix );
	} );

	it( "concatenates all routes per method of two instances by appended a given one's routes to current one's", function() {
		const { RoutesPerMethod, ControllerRoute } = ctx.hitchy.api.service;

		const collector = new RoutesPerMethod();
		const provider = new RoutesPerMethod();

		collector.append( new ControllerRoute( "GET /get", () => {} ) );
		collector.append( new ControllerRoute( "ALL /all", () => {} ) );

		collector.methods.should.have.properties( "GET", "ALL" ).and.have.size( 2 );
		collector.methods.GET.should.be.Array().and.have.length( 2 );
		collector.methods.ALL.should.be.Array().and.have.length( 1 );

		collector.methods.GET.map( r => r.path ).should.eql( [ "/get", "/all" ] );
		collector.methods.ALL.map( r => r.path ).should.eql( ["/all"] );


		provider.append( new ControllerRoute( "GET /get2", () => {} ) );
		provider.append( new ControllerRoute( "POST /post", () => {} ) );
		provider.append( new ControllerRoute( "POST /post2", () => {} ) );
		provider.append( new ControllerRoute( "PUT /put", () => {} ) );

		provider.methods.should.have.properties( "GET", "POST", "PUT" ).and.have.size( 3 );
		provider.methods.GET.should.be.Array().and.have.length( 1 );
		provider.methods.POST.should.be.Array().and.have.length( 2 );
		provider.methods.PUT.should.be.Array().and.have.length( 1 );

		provider.methods.GET.map( r => r.path ).should.eql( ["/get2"] );
		provider.methods.POST.map( r => r.path ).should.eql( [ "/post", "/post2" ] );
		provider.methods.PUT.map( r => r.path ).should.eql( ["/put"] );


		collector.concat( provider );

		collector.methods.should.have.properties( "GET", "ALL", "POST", "PUT" );
		collector.methods.GET.should.be.Array().and.have.length( 3 );
		collector.methods.ALL.should.be.Array().and.have.length( 1 );
		collector.methods.POST.should.be.Array().and.have.length( 3 );
		collector.methods.PUT.should.be.Array().and.have.length( 2 );

		collector.methods.GET.map( r => r.path ).should.eql( [ "/get", "/all", "/get2" ] );
		collector.methods.ALL.map( r => r.path ).should.eql( ["/all"] );
		collector.methods.POST.map( r => r.path ).should.eql( [ "/all", "/post", "/post2" ] );
		collector.methods.PUT.map( r => r.path ).should.eql( [ "/all", "/put" ] );
	} );
} );
