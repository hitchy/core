import { describe, beforeEach, afterEach, it } from "mocha";
import "should";
import "should-http";
import SDT from "@hitchy/server-dev-tools";

import Core from "../../../../sdt.js";

const Test = await SDT( Core );

const config = {
	projectFolder: "test/projects/empty",
	options: {
		// debug: true,
	},
};

describe( "Library.Router.Types.List.RoutesPerPrefix", function() {
	const ctx = {};

	beforeEach( Test.before( ctx, { ...config } ) );
	afterEach( Test.after( ctx ) );

	it( "exists", function() {
		const { RoutesPerPrefix } = ctx.hitchy.api.service;

		RoutesPerPrefix.should.be.ok().and.should.be.Object();
	} );

	it( "can be instantiated", function() {
		const { RoutesPerPrefix } = ctx.hitchy.api.service;

		( () => { new RoutesPerPrefix(); } ).should.not.throw();
	} );

	it( "exposes collection of routes per prefix", function() {
		const { RoutesPerPrefix } = ctx.hitchy.api.service;

		const collector = new RoutesPerPrefix();
		collector.should.have.property( "prefixes" );
		collector.prefixes.should.be.Object().and.be.ok();
	} );

	it( "exposes flag indicating whether list of supported prefixes can be extended or not", function() {
		const { RoutesPerPrefix } = ctx.hitchy.api.service;

		const collector = new RoutesPerPrefix();
		collector.should.have.property( "extensible" );
		collector.extensible.should.be.Boolean();
	} );

	it( "provides method for adding routes", function() {
		const { RoutesPerPrefix, ControllerRoute } = ctx.hitchy.api.service;

		new RoutesPerPrefix().append.should.be.Function().and.have.length( 1 );

		( () => new RoutesPerPrefix().append() ).should.throw();
		( () => new RoutesPerPrefix().append( undefined ) ).should.throw();
		( () => new RoutesPerPrefix().append( null ) ).should.throw();
		( () => new RoutesPerPrefix().append( true ) ).should.throw();
		( () => new RoutesPerPrefix().append( false ) ).should.throw();
		( () => new RoutesPerPrefix().append( 0 ) ).should.throw();
		( () => new RoutesPerPrefix().append( "0" ) ).should.throw();
		( () => new RoutesPerPrefix().append( "" ) ).should.throw();
		( () => new RoutesPerPrefix().append( {} ) ).should.throw();
		( () => new RoutesPerPrefix().append( { prefix: "/" } ) ).should.throw();
		( () => new RoutesPerPrefix().append( [] ) ).should.throw();
		( () => new RoutesPerPrefix().append( ["/"] ) ).should.throw();

		( () => new RoutesPerPrefix().append( new ControllerRoute( "/", () => {} ) ) ).should.not.throw();
	} );

	it( "provides method for getting prefix addressing list of routes matching best some given prefix", function() {
		const { RoutesPerPrefix, ControllerRoute } = ctx.hitchy.api.service;

		let collector = new RoutesPerPrefix();
		collector.getLongestMatchingPrefix.should.be.Function().and.have.length( 1 );

		collector.getLongestMatchingPrefix.bind( collector ).should.throw();
		collector.getLongestMatchingPrefix.bind( collector, "" ).should.throw();

		collector.getLongestMatchingPrefix.bind( collector, "/" ).should.not.throw();

		let prefix = collector.getLongestMatchingPrefix( "/" );
		( prefix == null ).should.be.true();

		collector = new RoutesPerPrefix();
		collector.append( new ControllerRoute( "/", () => {} ) );

		prefix = collector.getLongestMatchingPrefix( "/" );
		prefix.should.be.String();
		collector.prefixes.should.have.property( prefix );
	} );

	it( "provides method for getting sorted list of routes bound to prefix matching best some given one", function() {
		const { RoutesPerPrefix, ControllerRoute } = ctx.hitchy.api.service;

		let collector = new RoutesPerPrefix();
		collector.onPrefix.should.be.Function().and.have.length( 1 );

		collector.onPrefix.bind( collector ).should.throw();
		collector.onPrefix.bind( collector, "" ).should.throw();

		collector.onPrefix.bind( collector, "/" ).should.not.throw();

		let sortedList = collector.onPrefix( "/" );
		sortedList.should.be.Array().which.is.empty();

		collector = new RoutesPerPrefix();
		collector.append( new ControllerRoute( "/", () => {} ) );

		sortedList = collector.onPrefix( "/" );
		sortedList.should.be.Array();
	} );

	it( "is marked extensible unless providing explicit set of supported prefixes on constructing", function() {
		const { RoutesPerPrefix } = ctx.hitchy.api.service;

		let collector = new RoutesPerPrefix();
		collector.extensible.should.be.true();

		collector = new RoutesPerPrefix( [] );
		collector.extensible.should.be.false();

		collector = new RoutesPerPrefix( [ "/", "/prefix" ] );
		collector.extensible.should.be.false();
	} );

	it( "exposes initially empty map of prefixes into routes unless fixing set of supported prefixes to be provided initially", function() {
		const { RoutesPerPrefix } = ctx.hitchy.api.service;

		let collector = new RoutesPerPrefix();
		collector.prefixes.should.be.empty();

		collector = new RoutesPerPrefix( [] );
		collector.prefixes.should.be.empty();   // still empty due to empty list of prefixes to support

		collector = new RoutesPerPrefix( [ "/", "/prefix" ] );
		collector.prefixes.should.not.be.false();
	} );

	it( "collects routes by group on appending each keeping sorting order prefix", function() {
		const { RoutesPerPrefix, ControllerRoute } = ctx.hitchy.api.service;

		const collector = new RoutesPerPrefix();
		const route1 = new ControllerRoute( "GET /", () => {} );
		const route2 = new ControllerRoute( "GET /:a/sub", () => {} );
		const route3 = new ControllerRoute( "GET /:a", () => {} );

		collector.prefixes.should.be.empty();

		collector.append( route1 );
		collector.prefixes.should.have.properties( "/" ).and.have.size( 1 );
		collector.prefixes["/"].should.be.Array().and.eql( [route1] ).and.have.length( 1 );
		collector.append( route2 );
		collector.prefixes.should.have.properties( "/" ).and.have.size( 1 );
		collector.prefixes["/"].should.be.Array().and.eql( [ route1, route2 ] ).and.have.length( 2 );
		collector.append( route3 );
		collector.prefixes.should.have.properties( "/" ).and.have.size( 1 );
		collector.prefixes["/"].should.be.Array().and.eql( [ route1, route2, route3 ] ).and.have.length( 3 );
	} );

	it( "does not collect routes using same pattern like a route collected before", function() {
		const { RoutesPerPrefix, ControllerRoute } = ctx.hitchy.api.service;

		const collector = new RoutesPerPrefix();
		const route1 = new ControllerRoute( "GET /", () => {} );
		const route2 = new ControllerRoute( "GET /", () => {} );
		const route3 = new ControllerRoute( "PUT /", () => {} );

		collector.prefixes.should.be.empty();

		collector.append( route1 );
		collector.prefixes.should.have.properties( "/" ).and.have.size( 1 );
		collector.prefixes["/"].should.be.Array().and.eql( [route1] ).and.have.length( 1 );
		collector.append( route2 );
		collector.prefixes.should.have.properties( "/" ).and.have.size( 1 );
		collector.prefixes["/"].should.be.Array().and.eql( [route1] ).and.have.length( 1 );
		collector.append( route3 );
		collector.prefixes.should.have.properties( "/" ).and.have.size( 1 );
		collector.prefixes["/"].should.be.Array().and.eql( [route1] ).and.have.length( 1 );
	} );

	it( "creates new group on appending route not matching any existing prefix", function() {
		const { RoutesPerPrefix, ControllerRoute } = ctx.hitchy.api.service;

		const collector = new RoutesPerPrefix();

		collector.prefixes.should.be.empty();
		collector.append( new ControllerRoute( "GET /", () => {} ) );
		collector.prefixes.should.have.properties( "/" ).and.have.size( 1 );
		collector.prefixes["/"].should.be.Array().and.have.length( 1 );

		collector.append( new ControllerRoute( "GET /:a", () => {} ) );
		collector.prefixes.should.have.properties( "/" ).and.have.size( 1 );
		collector.prefixes["/"].should.be.Array().and.have.length( 2 );

		collector.append( new ControllerRoute( "POST /:b", () => {} ) );
		collector.prefixes.should.have.properties( "/" ).and.have.size( 1 );
		collector.prefixes["/"].should.be.Array().and.have.length( 3 );

		collector.append( new ControllerRoute( "GET /test", () => {} ) );
		collector.prefixes.should.have.properties( "/", "/test" ).and.have.size( 2 );
		collector.prefixes["/"].should.be.Array().and.have.length( 3 );
		collector.prefixes["/test"].should.be.Array().and.have.length( 1 );
	} );

	it( "does not create new group on appending route not matching any of the pre-defined prefixes", function() {
		const { RoutesPerPrefix, ControllerRoute } = ctx.hitchy.api.service;

		let collector = new RoutesPerPrefix( [] );

		collector.prefixes.should.be.empty();
		collector.append( new ControllerRoute( "GET /", () => {} ) );
		collector.prefixes.should.be.empty();

		collector = new RoutesPerPrefix( ["/"] );

		collector.prefixes.should.have.size( 1 );
		collector.prefixes.should.have.property( "/" ).which.has.length( 0 );
		collector.prefixes.should.not.have.property( "/test" );
		collector.append( new ControllerRoute( "GET /", () => {} ) );
		collector.prefixes.should.have.property( "/" ).which.has.length( 1 );
		collector.append( new ControllerRoute( "GET /test", () => {} ) );
		collector.prefixes.should.not.have.property( "/test" );

		collector = new RoutesPerPrefix( [ "/", "/test" ] );

		collector.prefixes.should.have.size( 2 );
		collector.prefixes.should.have.property( "/" ).and.have.length( 0 );
		collector.prefixes.should.have.property( "/test" ).and.have.size( 0 );

		collector.append( new ControllerRoute( "GET /", () => {} ) );
		collector.prefixes.should.have.property( "/" ).and.have.length( 1 );
		collector.prefixes.should.have.property( "/test" ).and.have.size( 1 );

		collector.append( new ControllerRoute( "GET /test", () => {} ) );
		collector.prefixes.should.have.property( "/" ).and.have.length( 1 );
		collector.prefixes.should.have.property( "/test" ).and.have.size( 2 );
	} );

	it( "collects routes with divergent prefixes in separate groups, only", function() {
		const { RoutesPerPrefix, ControllerRoute } = ctx.hitchy.api.service;

		const collector = new RoutesPerPrefix();

		collector.append( new ControllerRoute( "/test1", () => {} ) );
		collector.prefixes.should.have.properties( "/test1" ).and.have.size( 1 );
		collector.prefixes["/test1"].should.be.Array().and.have.length( 1 );

		collector.append( new ControllerRoute( "/test2", () => {} ) );
		collector.prefixes.should.have.properties( "/test1", "/test2" ).and.have.size( 2 );
		collector.prefixes["/test1"].should.be.Array().and.have.length( 1 );
		collector.prefixes["/test2"].should.be.Array().and.have.length( 1 );
	} );

	it( "collects policy routes with more generic prefixes in all groups of more specific matching prefixes, too", function() {
		const { RoutesPerPrefix, PolicyRoute } = ctx.hitchy.api.service;

		const collector = new RoutesPerPrefix();

		collector.append( new PolicyRoute( "/", () => {} ) );
		collector.append( new PolicyRoute( "/test1/sub1", () => {} ) );
		collector.append( new PolicyRoute( "/test2/sub1", () => {} ) );
		collector.prefixes.should.have.properties( "/", "/test1/sub1", "/test2/sub1" ).and.have.size( 3 );
		collector.prefixes["/"].should.be.Array().and.have.length( 1 );
		collector.prefixes["/test1/sub1"].should.be.Array().and.have.length( 1 );
		collector.prefixes["/test2/sub1"].should.be.Array().and.have.length( 1 );

		collector.append( new PolicyRoute( "/test1", () => {} ) );
		collector.prefixes.should.have.properties( "/", "/test1", "/test1/sub1", "/test2/sub1" ).and.have.size( 4 );
		collector.prefixes["/"].should.be.Array().and.have.length( 1 );
		collector.prefixes["/test1"].should.be.Array().and.have.length( 1 );
		collector.prefixes["/test1/sub1"].should.be.Array().and.have.length( 2 );
		collector.prefixes["/test2/sub1"].should.be.Array().and.have.length( 1 );

		collector.append( new PolicyRoute( "/", () => {} ) );
		collector.prefixes.should.have.properties( "/", "/test1", "/test1/sub1", "/test2/sub1" ).and.have.size( 4 );
		collector.prefixes["/"].should.be.Array().and.have.length( 2 );
		collector.prefixes["/test1"].should.be.Array().and.have.length( 2 );
		collector.prefixes["/test1/sub1"].should.be.Array().and.have.length( 3 );
		collector.prefixes["/test2/sub1"].should.be.Array().and.have.length( 2 );
	} );

	it( "also collects controller routes with more generic prefixes in all groups of more specific matching prefixes", function() {
		const { RoutesPerPrefix, ControllerRoute } = ctx.hitchy.api.service;

		const collector = new RoutesPerPrefix();

		collector.append( new ControllerRoute( "/", () => {} ) );
		collector.append( new ControllerRoute( "/test1/sub1", () => {} ) );
		collector.append( new ControllerRoute( "/test2/sub1", () => {} ) );
		collector.prefixes.should.have.properties( "/", "/test1/sub1", "/test2/sub1" ).and.have.size( 3 );
		collector.prefixes["/"].should.be.Array().and.have.length( 1 );
		collector.prefixes["/test1/sub1"].should.be.Array().and.have.length( 1 );
		collector.prefixes["/test2/sub1"].should.be.Array().and.have.length( 1 );

		collector.append( new ControllerRoute( "/test1", () => {} ) );
		collector.prefixes.should.have.properties( "/", "/test1", "/test1/sub1", "/test2/sub1" ).and.have.size( 4 );
		collector.prefixes["/"].should.be.Array().and.have.length( 1 );
		collector.prefixes["/test1"].should.be.Array().and.have.length( 1 );
		collector.prefixes["/test1/sub1"].should.be.Array().and.have.length( 2 );
		collector.prefixes["/test2/sub1"].should.be.Array().and.have.length( 1 );

		collector.append( new ControllerRoute( "/:alt", () => {} ) );
		collector.prefixes.should.have.properties( "/", "/test1", "/test1/sub1", "/test2/sub1" ).and.have.size( 4 );
		collector.prefixes["/"].should.be.Array().and.have.length( 2 );
		collector.prefixes["/test1"].should.be.Array().and.have.length( 2 );
		collector.prefixes["/test1/sub1"].should.be.Array().and.have.length( 3 );
		collector.prefixes["/test2/sub1"].should.be.Array().and.have.length( 2 );
	} );

	it( "collects generic path w/ optional and repeated parameters on existing prefix, too", function() {
		const { RoutesPerPrefix, PolicyRoute } = ctx.hitchy.api.service;

		const collector = new RoutesPerPrefix();

		collector.append( new PolicyRoute( "/test/sub", () => {} ) );
		collector.prefixes.should.have.property( "/test/sub" ).and.be.Array().and.have.length( 1 );

		collector.append( new PolicyRoute( "/:major/sub", () => {} ) );
		collector.prefixes.should.have.property( "/test/sub" ).and.be.Array().and.have.length( 2 );

		// FIXME using optional first segment
		//        - must be either rejected OR
		//        - fixed so that {/:major}/test becomes /{:major/}test OR
		//        - fixed so that the prefix is /
		collector.append( new PolicyRoute( "{/:major}/test", () => {} ) );
		collector.prefixes.should.have.property( "/test/sub" ).and.be.Array().and.have.length( 3 );


		collector.append( new PolicyRoute( "/test/su/b", () => {} ) );
		collector.prefixes.should.have.property( "/test/sub" ).and.be.Array().and.have.length( 3 );

		collector.append( new PolicyRoute( "/te/st/sub", () => {} ) );
		collector.prefixes.should.have.property( "/test/sub" ).and.be.Array().and.have.length( 3 );

		collector.append( new PolicyRoute( "/tast/sub", () => {} ) );
		collector.prefixes.should.have.property( "/test/sub" ).and.be.Array().and.have.length( 3 );

		collector.append( new PolicyRoute( "/*major/sup", () => {} ) );
		collector.prefixes.should.have.property( "/test/sub" ).and.be.Array().and.have.length( 3 );
	} );
} );
