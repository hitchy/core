import { describe, it, beforeEach, afterEach } from "mocha";
import Core from "../../../../sdt.js";
import SDT from "@hitchy/server-dev-tools";
import "should";

const Test = await SDT( Core );

const config = {
	projectFolder: "test/projects/empty",
	options: {
		// debug: true,
	},
};

const delay = ( ms, result = undefined ) => new Promise( resolve => setTimeout( resolve, ms, result ) );

describe.only( "Sequence", () => {
	const ctx = {};

	afterEach( Test.after( ctx ) );
	beforeEach( Test.before( ctx, { ...config } ) );

	it( "is exposed as core service", () => {
		ctx.hitchy.api.service.Sequence.should.be.ok();
	} );

	it( "can be instantiated", () => {
		const sequence = new ctx.hitchy.api.service.Sequence();

		sequence.should.be.ok();
	} );

	it( "provides methods for managing a sequence of callbacks", () => {
		const sequence = new ctx.hitchy.api.service.Sequence();

		sequence.append.should.be.a.Function();
		sequence.prepend.should.be.a.Function();
		sequence.has.should.be.a.Function();
		sequence.isCurrent.should.be.a.Function();
		sequence.isEmpty.should.be.a.Function();
		sequence.waitFor.should.be.a.Function();
		sequence.whenIdle.should.be.a.Function();
	} );

	it( "requires callback function to append to sequence", () => {
		const sequence = new ctx.hitchy.api.service.Sequence();

		( () => sequence.append() ).should.throw( /must be a function/ );
		( () => sequence.append( undefined ) ).should.throw( /must be a function/ );
		( () => sequence.append( null ) ).should.throw( /must be a function/ );
		( () => sequence.append( false ) ).should.throw( /must be a function/ );
		( () => sequence.append( true ) ).should.throw( /must be a function/ );
		( () => sequence.append( 0 ) ).should.throw( /must be a function/ );
		( () => sequence.append( NaN ) ).should.throw( /must be a function/ );
		( () => sequence.append( -123.456 ) ).should.throw( /must be a function/ );
		( () => sequence.append( 7890123456 ) ).should.throw( /must be a function/ );
		( () => sequence.append( "" ) ).should.throw( /must be a function/ );
		( () => sequence.append( "foo" ) ).should.throw( /must be a function/ );
		( () => sequence.append( [] ) ).should.throw( /must be a function/ );
		( () => sequence.append( ["foo"] ) ).should.throw( /must be a function/ );
		( () => sequence.append( [() => {}] ) ).should.throw( /must be a function/ );
		( () => sequence.append( {} ) ).should.throw( /must be a function/ );
		( () => sequence.append( { method: "foo" } ) ).should.throw( /must be a function/ );
		( () => sequence.append( { method: () => {} } ) ).should.throw( /must be a function/ );

		( () => sequence.append( () => {} ) ).should.not.throw();
	} );

	it( "accepts custom ID when appending a callback to the sequence", () => {
		const sequence = new ctx.hitchy.api.service.Sequence();

		sequence.append( () => {}, "123" );
	} );

	it( "requires callback function to prepend to sequence", () => {
		const sequence = new ctx.hitchy.api.service.Sequence();

		( () => sequence.prepend() ).should.throw( /must be a function/ );
		( () => sequence.prepend( undefined ) ).should.throw( /must be a function/ );
		( () => sequence.prepend( null ) ).should.throw( /must be a function/ );
		( () => sequence.prepend( false ) ).should.throw( /must be a function/ );
		( () => sequence.prepend( true ) ).should.throw( /must be a function/ );
		( () => sequence.prepend( 0 ) ).should.throw( /must be a function/ );
		( () => sequence.prepend( NaN ) ).should.throw( /must be a function/ );
		( () => sequence.prepend( -123.456 ) ).should.throw( /must be a function/ );
		( () => sequence.prepend( 7890123456 ) ).should.throw( /must be a function/ );
		( () => sequence.prepend( "" ) ).should.throw( /must be a function/ );
		( () => sequence.prepend( "foo" ) ).should.throw( /must be a function/ );
		( () => sequence.prepend( [] ) ).should.throw( /must be a function/ );
		( () => sequence.prepend( ["foo"] ) ).should.throw( /must be a function/ );
		( () => sequence.prepend( [() => {}] ) ).should.throw( /must be a function/ );
		( () => sequence.prepend( {} ) ).should.throw( /must be a function/ );
		( () => sequence.prepend( { method: "foo" } ) ).should.throw( /must be a function/ );
		( () => sequence.prepend( { method: () => {} } ) ).should.throw( /must be a function/ );

		( () => sequence.prepend( () => {} ) ).should.not.throw();
	} );

	it( "accepts custom ID when prepending a callback to the sequence", () => {
		const sequence = new ctx.hitchy.api.service.Sequence();

		sequence.prepend( () => {}, "123" );
	} );

	it( "reports sequence to be empty initially", () => {
		const sequence = new ctx.hitchy.api.service.Sequence();

		sequence.isEmpty().should.be.true();
	} );

	it( "reports sequence to be non-empty after appending a callback", () => {
		const sequence = new ctx.hitchy.api.service.Sequence();

		sequence.append( () => {} );
		sequence.isEmpty().should.be.false();
	} );

	it( "reports sequence to be non-empty after prepending a callback", () => {
		const sequence = new ctx.hitchy.api.service.Sequence();

		sequence.prepend( () => {} );
		sequence.isEmpty().should.be.false();
	} );

	it( "does not find a callback unless it has been appended to a sequence", () => {
		const fn = () => {};
		const sequence = new ctx.hitchy.api.service.Sequence();

		sequence.has( fn ).should.be.false();
		sequence.has( "123" ).should.be.false();

		sequence.append( fn );

		sequence.has( fn ).should.be.true();
		sequence.has( "123" ).should.be.false();

		sequence.append( () => {}, "123" );

		sequence.has( fn ).should.be.true();
		sequence.has( "123" ).should.be.true();
	} );

	it( "does not find a callback unless it has been prepended to a sequence", () => {
		const fn = () => {};
		const sequence = new ctx.hitchy.api.service.Sequence();

		sequence.has( fn ).should.be.false();
		sequence.has( "123" ).should.be.false();

		sequence.prepend( fn );

		sequence.has( fn ).should.be.true();
		sequence.has( "123" ).should.be.false();

		sequence.prepend( () => {}, "123" );

		sequence.has( fn ).should.be.true();
		sequence.has( "123" ).should.be.true();
	} );

	it( "detects if some identified callback is currently running or not", async() => {
		const fn = () => {};
		const sequence = new ctx.hitchy.api.service.Sequence();

		sequence.isCurrent( fn ).should.be.false();
		sequence.isCurrent( "123" ).should.be.false();

		const done = sequence.append( fn );

		sequence.isCurrent( fn ).should.be.true();
		sequence.isCurrent( "123" ).should.be.false();

		sequence.append( () => {}, "123" );

		sequence.isCurrent( fn ).should.be.true();
		sequence.isCurrent( "123" ).should.be.false();

		await done;

		sequence.isCurrent( fn ).should.be.false();
		sequence.isCurrent( "123" ).should.be.true();

		await sequence.whenIdle();

		sequence.isCurrent( fn ).should.be.false();
		sequence.isCurrent( "123" ).should.be.false();
	} );

	it( "returns promise settled with return value of appended callback", async() => {
		const item = {};
		const fn = () => item;
		const sequence = new ctx.hitchy.api.service.Sequence();

		await sequence.append( fn ).should.resolvedWith( item );
	} );

	it( "returns promise settled with return value of prepended callback", async() => {
		const item = {};
		const fn = () => item;
		const sequence = new ctx.hitchy.api.service.Sequence();

		await sequence.prepend( fn ).should.resolvedWith( item );
	} );

	it( "successively invokes all callbacks in same order as appended", async() => {
		const sequence = new ctx.hitchy.api.service.Sequence();
		const collector = [];
		const collect = value => collector.push( value );

		sequence.append( () => delay( 50, 1 ).then( collect ) );
		sequence.append( () => delay( 30, 2 ).then( collect ) );
		sequence.append( () => delay( 10, 3 ).then( collect ) );

		await sequence.whenIdle();

		collector.should.deepEqual( [ 1, 2, 3 ] );
	} );

	it( "invokes prepended callback after first appended one, but before all additionally appended ones", async() => {
		const sequence = new ctx.hitchy.api.service.Sequence();
		const collector = [];
		const collect = value => collector.push( value );

		sequence.append( () => delay( 50, 1 ).then( collect ) );
		sequence.append( () => delay( 30, 2 ).then( collect ) );
		sequence.append( () => delay( 10, 3 ).then( collect ) );

		sequence.prepend( () => delay( 30, 4 ).then( collect ) );

		await sequence.whenIdle();

		collector.should.deepEqual( [ 1, 4, 2, 3 ] );
	} );

	it( "provides promise for either enqueued callback's result", async() => {
		const sequence = new ctx.hitchy.api.service.Sequence();

		sequence.append( () => delay( 50, 1 ), "a" );
		sequence.append( () => delay( 30, 2 ), "b" );
		sequence.append( () => delay( 10, 3 ), "c" );

		sequence.prepend( () => delay( 30, 4 ), "d" );

		await sequence.waitFor( "b" ).should.be.resolvedWith( 2 );

		sequence.append( () => delay( 50, 5 ), "e" );
		sequence.append( () => delay( 30, 6 ), "f" );

		sequence.prepend( () => delay( 30, 7 ), "g" );

		await sequence.waitFor( "g" ).should.be.resolvedWith( 7 );
	} );

	it( "fails to provide promise for an enqueued callback's result once it has passed", async() => {
		const sequence = new ctx.hitchy.api.service.Sequence();

		sequence.append( () => delay( 50, 1 ), "a" );
		sequence.append( () => delay( 30, 2 ), "b" );
		sequence.append( () => delay( 10, 3 ), "c" );

		sequence.prepend( () => delay( 30, 4 ), "d" );

		await sequence.waitFor( "b" );

		( sequence.waitFor( "a" ) == null ).should.be.true();
		( sequence.waitFor( "b" ) == null ).should.be.true();
		( sequence.waitFor( "c" ) == null ).should.be.false();
		( sequence.waitFor( "d" ) == null ).should.be.true();
	} );

	it( "keeps processing callbacks even if one of them is crashing", async() => {
		const sequence = new ctx.hitchy.api.service.Sequence();
		const collector = [];
		const collect = value => collector.push( value );
		const crash = value => { throw new Error( "value is " + value ); };

		sequence.append( () => delay( 50, 1 ).then( crash ) );
		sequence.append( () => delay( 30, 2 ).then( collect ) );
		sequence.append( () => delay( 10, 3 ).then( collect ) );

		sequence.prepend( () => delay( 30, 4 ).then( crash ) );

		await sequence.whenIdle();

		collector.should.deepEqual( [ 2, 3 ] );
	} );
} );
