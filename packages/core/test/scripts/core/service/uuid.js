import { describe, it, beforeEach, afterEach } from "mocha";
import "should";
import SDT from "@hitchy/server-dev-tools";

import Core from "../../../../sdt.js";

const Test = await SDT( Core );

const config = {
	projectFolder: "test/projects/empty",
	options: {
		// debug: true,
	},
};

describe( "Service UUID", () => {
	const ctx = {};

	beforeEach( Test.before( ctx, { ...config } ) );
	afterEach( Test.after( ctx ) );

	it( "is available", () => {
		ctx.hitchy.api.service.should.have.property( "Uuid" );
	} );

	describe( "has static method UUID.normalize() that", () => {
		it( "does not throw when used without arguments", () => {
			( () => ctx.hitchy.api.service.Uuid.normalize() ).should.not.throw( TypeError );
		} );

		it( "throws when used with argument not suitable for representing UUID", () => {
			( () => ctx.hitchy.api.service.Uuid.normalize( false ) ).should.throw( TypeError );
			( () => ctx.hitchy.api.service.Uuid.normalize( true ) ).should.throw( TypeError );
			( () => ctx.hitchy.api.service.Uuid.normalize( 0 ) ).should.throw( TypeError );
			( () => ctx.hitchy.api.service.Uuid.normalize( 1.0 ) ).should.throw( TypeError );
			( () => ctx.hitchy.api.service.Uuid.normalize( -1000 ) ).should.throw( TypeError );
			( () => ctx.hitchy.api.service.Uuid.normalize( [] ) ).should.throw( TypeError );
			( () => ctx.hitchy.api.service.Uuid.normalize( ["12345678-1234-1234-1234-1234567890ab"] ) ).should.throw( TypeError );
			( () => ctx.hitchy.api.service.Uuid.normalize( {} ) ).should.throw( TypeError );
			( () => ctx.hitchy.api.service.Uuid.normalize( { uuid: "12345678-1234-1234-1234-1234567890ab" } ) ).should.throw( TypeError );
			( () => ctx.hitchy.api.service.Uuid.normalize( () => {} ) ).should.throw( TypeError );
			( () => ctx.hitchy.api.service.Uuid.normalize( () => "12345678-1234-1234-1234-1234567890ab" ) ).should.throw( TypeError );
		} );


		it( "accepts null or undefined as argument", () => {
			( () => ctx.hitchy.api.service.Uuid.normalize( null ) ).should.not.throw( TypeError );
			( () => ctx.hitchy.api.service.Uuid.normalize( undefined ) ).should.not.throw( TypeError );
		} );

		it( "rejects array consisting of 16 integers in range 0-0xFF as argument", () => {
			( () => ctx.hitchy.api.service.Uuid.normalize( [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 ] ) ).should.throw( TypeError );
		} );

		it( "rejects array consisting of 4 integers in range 0-0xFFFFFFFF as argument", () => {
			( () => ctx.hitchy.api.service.Uuid.normalize( [ 1, 2, 3, 4 ] ) ).should.throw( TypeError );
		} );

		it( "rejects string not containing valid UUID as argument", () => {
			( () => ctx.hitchy.api.service.Uuid.normalize( "" ) ).should.throw( TypeError );
			( () => ctx.hitchy.api.service.Uuid.normalize( "1-1234-1234-1234-1234567890ab" ) ).should.throw( TypeError );
			( () => ctx.hitchy.api.service.Uuid.normalize( "12345678_1234-1234-1234-1234567890ab" ) ).should.throw( TypeError );
			( () => ctx.hitchy.api.service.Uuid.normalize( "g2345678-1234-1234-1234-1234567890ab" ) ).should.throw( TypeError );
		} );

		it( "accepts string containing valid UUID as argument", () => {
			( () => ctx.hitchy.api.service.Uuid.normalize( "12345678-1234-1234-1234-1234567890ab" ) ).should.not.throw();
			( () => ctx.hitchy.api.service.Uuid.normalize( "12345678-1234-1234-1234-1234567890AB" ) ).should.not.throw();
			( () => ctx.hitchy.api.service.Uuid.normalize( "  12345678-1234-1234-1234-1234567890ab  " ) ).should.not.throw();
		} );

		it( "accepts Buffer consisting of 16 bytes", () => {
			( () => ctx.hitchy.api.service.Uuid.normalize( Buffer.alloc( 16 ) ) ).should.not.throw();
		} );

		it( "accepts Buffer consisting of more than 16 bytes", () => {
			( () => ctx.hitchy.api.service.Uuid.normalize( Buffer.alloc( 17 ) ) ).should.not.throw();
			( () => ctx.hitchy.api.service.Uuid.normalize( Buffer.alloc( 32 ) ) ).should.not.throw();
			( () => ctx.hitchy.api.service.Uuid.normalize( Buffer.alloc( 8192 ) ) ).should.not.throw();
		} );

		it( "rejects Buffer consisting of less than 16 bytes", () => {
			( () => ctx.hitchy.api.service.Uuid.normalize( Buffer.alloc( 15 ) ) ).should.throw( TypeError );
			( () => ctx.hitchy.api.service.Uuid.normalize( Buffer.alloc( 1 ) ) ).should.throw( TypeError );
		} );
	} );

	describe( "provides static method UUIDv4.create() that", () => {
		it( "does not throw", () => {
			let result;

			( () => ( result = ctx.hitchy.api.service.Uuid.create() ) ).should.not.throw();

			return result;
		} );

		it( "returns a promise", () => {
			return ctx.hitchy.api.service.Uuid.create().should.be.Promise().which.is.resolved();
		} );

		it( "promises Buffer with 16 bytes", () => {
			return ctx.hitchy.api.service.Uuid.create()
				.then( result => {
					result.should.be.instanceOf( Buffer );
					result.should.have.length( 16 );
				} );
		} );
	} );

	describe( "provides static method UUIDv4.isUUID() that", () => {
		it( "requires provision of one argument", () => {
			ctx.hitchy.api.service.Uuid.isUUID.should.be.Function().which.has.length( 1 );
		} );

		it( "does not throw when calling without arguments", () => {
			( () => ctx.hitchy.api.service.Uuid.isUUID() ).should.not.throw();
		} );

		it( "rejects most regular types of input not suitable for purely describing single UUID", () => {
			ctx.hitchy.api.service.Uuid.isUUID().should.be.false();
			ctx.hitchy.api.service.Uuid.isUUID( null ).should.be.false();
			ctx.hitchy.api.service.Uuid.isUUID( undefined ).should.be.false();
			ctx.hitchy.api.service.Uuid.isUUID( false ).should.be.false();
			ctx.hitchy.api.service.Uuid.isUUID( true ).should.be.false();
			ctx.hitchy.api.service.Uuid.isUUID( 0 ).should.be.false();
			ctx.hitchy.api.service.Uuid.isUUID( 0.0 ).should.be.false();
			ctx.hitchy.api.service.Uuid.isUUID( -1 ).should.be.false();
			ctx.hitchy.api.service.Uuid.isUUID( 1.0 ).should.be.false();
			ctx.hitchy.api.service.Uuid.isUUID( [] ).should.be.false();
			ctx.hitchy.api.service.Uuid.isUUID( {} ).should.be.false();
			ctx.hitchy.api.service.Uuid.isUUID( { uuid: "01234567-89ab-cdef-fedc-ba9876543210" } ).should.be.false();
			ctx.hitchy.api.service.Uuid.isUUID( () => {} ).should.be.false();
			ctx.hitchy.api.service.Uuid.isUUID( () => "01234567-89ab-cdef-fedc-ba9876543210" ).should.be.is.false();
			ctx.hitchy.api.service.Uuid.isUUID( new Set() ).should.be.is.false();
			ctx.hitchy.api.service.Uuid.isUUID( ["01234567-89ab-cdef-fedc-ba9876543210"] ).should.be.false();
		} );

		it( "rejects strings not representing UUID", () => {
			ctx.hitchy.api.service.Uuid.isUUID( "" ).should.be.false();

			// using invalid separator
			ctx.hitchy.api.service.Uuid.isUUID( "01234567_89ab-cdef-fedc-ba9876543210" ).should.be.false();
			ctx.hitchy.api.service.Uuid.isUUID( "01234567-89ab_cdef-fedc-ba9876543210" ).should.be.false();
			ctx.hitchy.api.service.Uuid.isUUID( "01234567-89ab-cdef_fedc-ba9876543210" ).should.be.false();
			ctx.hitchy.api.service.Uuid.isUUID( "01234567-89ab-cdef-fedc_ba9876543210" ).should.be.false();
			ctx.hitchy.api.service.Uuid.isUUID( "01234567 89ab-cdef-fedc-ba9876543210" ).should.be.false();
			ctx.hitchy.api.service.Uuid.isUUID( "01234567-89ab cdef-fedc-ba9876543210" ).should.be.false();
			ctx.hitchy.api.service.Uuid.isUUID( "01234567-89ab-cdef fedc-ba9876543210" ).should.be.false();
			ctx.hitchy.api.service.Uuid.isUUID( "01234567-89ab-cdef-fedc ba9876543210" ).should.be.false();

			// lacking one character
			ctx.hitchy.api.service.Uuid.isUUID( "1234567-89ab-cdef-fedc-ba9876543210" ).should.be.false();
			ctx.hitchy.api.service.Uuid.isUUID( "01234567-9ab-cdef-fedc-ba9876543210" ).should.be.false();
			ctx.hitchy.api.service.Uuid.isUUID( "01234567-89ab-def-fedc-ba9876543210" ).should.be.false();
			ctx.hitchy.api.service.Uuid.isUUID( "01234567-89ab-cdef-edc-ba9876543210" ).should.be.false();
			ctx.hitchy.api.service.Uuid.isUUID( "01234567-89ab-cdef-fedc-a9876543210" ).should.be.false();

			// containing extra character
			ctx.hitchy.api.service.Uuid.isUUID( "012345678-89ab-cdef-fedc-ba9876543210" ).should.be.false();
			ctx.hitchy.api.service.Uuid.isUUID( "01234567-89abb-cdef-fedc-ba9876543210" ).should.be.false();
			ctx.hitchy.api.service.Uuid.isUUID( "01234567-89ab-cdeff-fedc-ba9876543210" ).should.be.false();
			ctx.hitchy.api.service.Uuid.isUUID( "01234567-89ab-cdef-fedcc-ba9876543210" ).should.be.false();
			ctx.hitchy.api.service.Uuid.isUUID( "01234567-89ab-cdef-fedc-ba98765432100" ).should.be.false();

			// using invalid digit
			ctx.hitchy.api.service.Uuid.isUUID( "0123456g-89ab-cdef-fedc-ba9876543210" ).should.be.false();
			ctx.hitchy.api.service.Uuid.isUUID( "01234567-89ag-cdef-fedc-ba9876543210" ).should.be.false();
			ctx.hitchy.api.service.Uuid.isUUID( "01234567-89ab-cgef-fedc-ba9876543210" ).should.be.false();
			ctx.hitchy.api.service.Uuid.isUUID( "01234567-89ab-cdef-gedc-ba9876543210" ).should.be.false();
			ctx.hitchy.api.service.Uuid.isUUID( "01234567-89ab-cdef-fedc-ga9876543210" ).should.be.false();
		} );

		it( "accepts strings representing UUID using lowercase letters", () => {
			ctx.hitchy.api.service.Uuid.isUUID( "01234567-89ab-cdef-fedc-ba9876543210" ).should.be.true();
		} );

		it( "accepts strings representing UUID using uppercase letters", () => {
			ctx.hitchy.api.service.Uuid.isUUID( "01234567-89AB-CDEF-FEDC-BA9876543210" ).should.be.true();
		} );

		it( "accepts Buffer consisting of 16 bytes", () => {
			ctx.hitchy.api.service.Uuid.isUUID( Buffer.alloc( 16 ) ).should.be.true();
		} );

		it( "accepts Buffer consisting of more than 16 bytes", () => {
			ctx.hitchy.api.service.Uuid.isUUID( Buffer.alloc( 17 ) ).should.be.true();
			ctx.hitchy.api.service.Uuid.isUUID( Buffer.alloc( 32 ) ).should.be.true();
			ctx.hitchy.api.service.Uuid.isUUID( Buffer.alloc( 8192 ) ).should.be.true();
		} );

		it( "rejects Buffer consisting of less than 16 bytes", () => {
			ctx.hitchy.api.service.Uuid.isUUID( Buffer.alloc( 15 ) ).should.be.false();
			ctx.hitchy.api.service.Uuid.isUUID( Buffer.alloc( 1 ) ).should.be.false();
		} );
	} );

	describe( "provides static method UUIDv4.normalize() that", () => {
		it( "requires provision of one argument", () => {
			ctx.hitchy.api.service.Uuid.normalize.should.be.Function().which.has.length( 1 );
		} );

		it( "does not throw when calling without arguments", () => {
			( () => ctx.hitchy.api.service.Uuid.normalize() ).should.not.throw( TypeError );
		} );

		it( "rejects most regular types of input not suitable for purely describing single UUID", () => {
			( () => ctx.hitchy.api.service.Uuid.normalize( false ) ).should.throw( TypeError );
			( () => ctx.hitchy.api.service.Uuid.normalize( true ) ).should.throw( TypeError );
			( () => ctx.hitchy.api.service.Uuid.normalize( 0 ) ).should.throw( TypeError );
			( () => ctx.hitchy.api.service.Uuid.normalize( 0.0 ) ).should.throw( TypeError );
			( () => ctx.hitchy.api.service.Uuid.normalize( -1 ) ).should.throw( TypeError );
			( () => ctx.hitchy.api.service.Uuid.normalize( 1.0 ) ).should.throw( TypeError );
			( () => ctx.hitchy.api.service.Uuid.normalize( [] ) ).should.throw( TypeError );
			( () => ctx.hitchy.api.service.Uuid.normalize( {} ) ).should.throw( TypeError );
			( () => ctx.hitchy.api.service.Uuid.normalize( { uuid: "01234567-89ab-cdef-fedc-ba9876543210" } ) ).should.throw( TypeError );
			( () => ctx.hitchy.api.service.Uuid.normalize( () => {} ) ).should.be.throw( TypeError );
			( () => ctx.hitchy.api.service.Uuid.normalize( () => "01234567-89ab-cdef-fedc-ba9876543210" ) ).should.throw( TypeError );
			( () => ctx.hitchy.api.service.Uuid.normalize( new Set() ) ).should.throw( TypeError );
			( () => ctx.hitchy.api.service.Uuid.normalize( [ 0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef, 0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef ] ) ).should.throw( TypeError );
			( () => ctx.hitchy.api.service.Uuid.normalize( new Uint8Array( [ 0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef, 0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef ] ) ) ).should.throw( TypeError );
			( () => ctx.hitchy.api.service.Uuid.normalize( new Uint32Array( [ 0x01234567, 0x89abcdef, 0x01234567, 0x89abcdef ] ) ) ).should.throw( TypeError );
			( () => ctx.hitchy.api.service.Uuid.normalize( ["01234567-89ab-cdef-fedc-ba9876543210"] ) ).should.throw( TypeError );
		} );

		it( "rejects strings not representing UUID", () => {
			ctx.hitchy.api.service.Uuid.isUUID( "" ).should.be.false();

			// using invalid separator
			ctx.hitchy.api.service.Uuid.isUUID( "01234567_89ab-cdef-fedc-ba9876543210" ).should.be.false();
			ctx.hitchy.api.service.Uuid.isUUID( "01234567-89ab_cdef-fedc-ba9876543210" ).should.be.false();
			ctx.hitchy.api.service.Uuid.isUUID( "01234567-89ab-cdef_fedc-ba9876543210" ).should.be.false();
			ctx.hitchy.api.service.Uuid.isUUID( "01234567-89ab-cdef-fedc_ba9876543210" ).should.be.false();
			ctx.hitchy.api.service.Uuid.isUUID( "01234567 89ab-cdef-fedc-ba9876543210" ).should.be.false();
			ctx.hitchy.api.service.Uuid.isUUID( "01234567-89ab cdef-fedc-ba9876543210" ).should.be.false();
			ctx.hitchy.api.service.Uuid.isUUID( "01234567-89ab-cdef fedc-ba9876543210" ).should.be.false();
			ctx.hitchy.api.service.Uuid.isUUID( "01234567-89ab-cdef-fedc ba9876543210" ).should.be.false();

			// lacking one character
			ctx.hitchy.api.service.Uuid.isUUID( "1234567-89ab-cdef-fedc-ba9876543210" ).should.be.false();
			ctx.hitchy.api.service.Uuid.isUUID( "01234567-9ab-cdef-fedc-ba9876543210" ).should.be.false();
			ctx.hitchy.api.service.Uuid.isUUID( "01234567-89ab-def-fedc-ba9876543210" ).should.be.false();
			ctx.hitchy.api.service.Uuid.isUUID( "01234567-89ab-cdef-edc-ba9876543210" ).should.be.false();
			ctx.hitchy.api.service.Uuid.isUUID( "01234567-89ab-cdef-fedc-a9876543210" ).should.be.false();

			// containing extra character
			ctx.hitchy.api.service.Uuid.isUUID( "012345678-89ab-cdef-fedc-ba9876543210" ).should.be.false();
			ctx.hitchy.api.service.Uuid.isUUID( "01234567-89abb-cdef-fedc-ba9876543210" ).should.be.false();
			ctx.hitchy.api.service.Uuid.isUUID( "01234567-89ab-cdeff-fedc-ba9876543210" ).should.be.false();
			ctx.hitchy.api.service.Uuid.isUUID( "01234567-89ab-cdef-fedcc-ba9876543210" ).should.be.false();
			ctx.hitchy.api.service.Uuid.isUUID( "01234567-89ab-cdef-fedc-ba98765432100" ).should.be.false();

			// using invalid digit
			ctx.hitchy.api.service.Uuid.isUUID( "0123456g-89ab-cdef-fedc-ba9876543210" ).should.be.false();
			ctx.hitchy.api.service.Uuid.isUUID( "01234567-89ag-cdef-fedc-ba9876543210" ).should.be.false();
			ctx.hitchy.api.service.Uuid.isUUID( "01234567-89ab-cgef-fedc-ba9876543210" ).should.be.false();
			ctx.hitchy.api.service.Uuid.isUUID( "01234567-89ab-cdef-gedc-ba9876543210" ).should.be.false();
			ctx.hitchy.api.service.Uuid.isUUID( "01234567-89ab-cdef-fedc-ga9876543210" ).should.be.false();
		} );

		it( "accepts null or undefined", () => {
			( () => ctx.hitchy.api.service.Uuid.normalize( null ) ).should.not.throw( TypeError );
			( () => ctx.hitchy.api.service.Uuid.normalize( undefined ) ).should.not.throw( TypeError );
		} );

		it( "accepts strings representing UUID using lowercase letters", () => {
			ctx.hitchy.api.service.Uuid.isUUID( "01234567-89ab-cdef-fedc-ba9876543210" ).should.be.true();
		} );

		it( "accepts strings representing UUID using uppercase letters", () => {
			ctx.hitchy.api.service.Uuid.isUUID( "01234567-89AB-CDEF-FEDC-BA9876543210" ).should.be.true();
		} );

		it( "accepts Buffer consisting of 16 bytes", () => {
			ctx.hitchy.api.service.Uuid.isUUID( Buffer.alloc( 16 ) ).should.be.true();
		} );

		it( "accepts Buffer consisting of more than 16 bytes", () => {
			ctx.hitchy.api.service.Uuid.isUUID( Buffer.alloc( 17 ) ).should.be.true();
			ctx.hitchy.api.service.Uuid.isUUID( Buffer.alloc( 32 ) ).should.be.true();
			ctx.hitchy.api.service.Uuid.isUUID( Buffer.alloc( 8192 ) ).should.be.true();
		} );

		it( "rejects Buffer consisting of less than 16 bytes", () => {
			ctx.hitchy.api.service.Uuid.isUUID( Buffer.alloc( 15 ) ).should.be.false();
			ctx.hitchy.api.service.Uuid.isUUID( Buffer.alloc( 1 ) ).should.be.false();
		} );
	} );

	describe( "provides static method UUIDv4#format() that", () => {
		it( "is function requiring one argument", () => {
			ctx.hitchy.api.service.Uuid.format.length.should.be.equal( 1 );
		} );

		it( "throws on calling without any argument", () => {
			( () => ctx.hitchy.api.service.Uuid.format() ).should.throw();
		} );

		it( "throws on providing something that's not a Buffer", () => {
			( () => ctx.hitchy.api.service.Uuid.format( undefined ) ).should.throw();
			( () => ctx.hitchy.api.service.Uuid.format( null ) ).should.throw();
			( () => ctx.hitchy.api.service.Uuid.format( false ) ).should.throw();
			( () => ctx.hitchy.api.service.Uuid.format( true ) ).should.throw();
			( () => ctx.hitchy.api.service.Uuid.format( 0 ) ).should.throw();
			( () => ctx.hitchy.api.service.Uuid.format( "" ) ).should.throw();
			( () => ctx.hitchy.api.service.Uuid.format( "some-value" ) ).should.throw();
			( () => ctx.hitchy.api.service.Uuid.format( "01234567-0123-0123-0123-0123456789ab" ) ).should.throw();
			( () => ctx.hitchy.api.service.Uuid.format( {} ) ).should.throw();
			( () => ctx.hitchy.api.service.Uuid.format( { uuid: "01234567-0123-0123-0123-0123456789ab" } ) ).should.throw();
			( () => ctx.hitchy.api.service.Uuid.format( [] ) ).should.throw();
			( () => ctx.hitchy.api.service.Uuid.format( ["01234567-0123-0123-0123-0123456789ab"] ) ).should.throw();
			( () => ctx.hitchy.api.service.Uuid.format( () => {} ) ).should.throw();
			( () => ctx.hitchy.api.service.Uuid.format( () => "01234567-0123-0123-0123-0123456789ab" ) ).should.throw();
		} );

		it( "renders string representing UUID provided as binary buffer", () => {
			const uuid = ctx.hitchy.api.service.Uuid.format( Buffer.from( [
				0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef, 0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef
			] ) );

			uuid.toString().should.be.String().which.match( /^[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}$/i );
		} );

		it( "keeps byte order UUID", () => {
			const uuid = ctx.hitchy.api.service.Uuid.format( Buffer.from( [
				0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef, 0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef
			] ) );

			uuid.toString().should.be.equal( "01234567-89ab-cdef-0123-456789abcdef" );
		} );
	} );
} );
