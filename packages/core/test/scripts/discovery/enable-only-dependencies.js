import { describe, it, after, before } from "mocha";
import "should";
import "should-http";
import SDT from "@hitchy/server-dev-tools";

import Core from "../../../sdt.js";

const Test = await SDT( Core );

const config = {
	projectFolder: "test/projects/empty-plugins",
	options: {
		dependencies: "important",
		// debug: true,
	},
};

describe( "Serving project w/ empty plugins", function() {
	const ctx = {};

	before( Test.before( ctx, config ) );
	after( Test.after( ctx ) );

	it( "enables only requested plugins", function() {
		return ctx.get( "/", { accept: "text/json" } )
			.then( response => {
				response.should.have.status( 200 );
				response.should.be.json();

				response.data.should.not.have.property( "empty-a" );    // due to project lacking dependency list thus loading EVERY available component
				response.data.should.not.have.property( "aliased-b" );  // due to filling differently named role
				response.data.should.not.have.property( "b" );
				response.data.should.not.have.property( "final-c" );
				response.data.should.not.have.property( "strong-role" );// due to filling differently named role
				response.data.should.not.have.property( "weak-role" );  // due to filling differently named role
				response.data.should.have.property( "important" );      // role filled by either of the two
				response.data.should.not.have.property( "non-plugin" );
			} );
	} );
} );
