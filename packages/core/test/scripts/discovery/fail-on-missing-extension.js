import { describe, it } from "mocha";
import "should";
import "should-http";
import SDT from "@hitchy/server-dev-tools";

import Core from "../../../sdt.js";

const Test = await SDT( Core );

const config = {
	projectFolder: "test/projects/empty-plugins",
	options: {
		dependencies: [ "important", "supporting" ],
		logger: true,
		// debug: true,
	},
};

describe( "Serving project w/ empty plugins", function() {
	it( "fails on missing plugin (default injector)", function() {
		const ctx = {};

		return Test.before( ctx, config )()
			.then( () => {
				throw new Error( "starting Hitchy did not fail" );
			}, error => {
				if ( !error.message.match( /missing\b.+\bsupporting/i ) ) {
					throw new Error( "startup failed with unexpected error: " + error );
				}
			} );
	} );
} );
