import { describe, it, after, before } from "mocha";
import "should";
import "should-http";
import SDT from "@hitchy/server-dev-tools";

import Core from "../../../sdt.js";

const Test = await SDT( Core );

describe( "Serving project with plugins optionally depending on others", function() {
	const ctx = {};

	after( Test.after( ctx ) );
	before( Test.before( ctx, {
		projectFolder: "test/projects/optional-dependencies",
		options: {
			dependencies: "important",
			// debug: true,
		},
	} ) );

	it( "does not fail on missing optional plugins", function() {
		return ctx.get( "/", { accept: "text/json" } )
			.then( response => {
				response.should.have.status( 200 );
				response.should.be.json();

				response.data.should.not.have.property( "empty-a" );    // due to project lacking dependency list thus loading EVERY available component
				response.data.should.not.have.property( "aliased-b" );  // due to filling differently named role
				response.data.should.not.have.property( "b" );
				response.data.should.not.have.property( "final-c" );
				response.data.should.not.have.property( "strong-role" );// due to filling differently named role
				response.data.should.not.have.property( "weak-role" );  // due to filling differently named role
				response.data.should.have.property( "important" );      // role filled by either of the two
				response.data.should.have.property( "useful" );
				response.data.should.not.have.property( "optional" );
				response.data.should.not.have.property( "non-plugin" );
			} );
	} );
} );
