import { describe, it, afterEach, beforeEach } from "mocha";
import "should";
import "should-http";
import SDT from "@hitchy/server-dev-tools";

import Core from "../../../sdt.js";

const Test = await SDT( Core );

const config = {
	projectFolder: "test/projects/empty",
	options: {
		// debug: true,
	},
};

describe( "Serving project from empty folder", () => {
	const ctx = {};

	beforeEach( Test.before( ctx, { ...config } ) );
	afterEach( Test.after( ctx ) );

	it( "provides core service component HttpClient", async() => {
		const { HttpClient } = ctx.hitchy.api.services;

		HttpClient.should.be.ok()
			.and.have.property( "fetch" )
			.which.is.a.Function();

		const response = await HttpClient.fetch( "GET", "https://duckduckgo.com/" );

		response.should.have.property( "statusCode" ).which.is.a.Number().and.is.equal( 200 );
		response.should.have.property( "body" ).which.is.a.Function();
		response.should.have.property( "json" ).which.is.a.Function();

		const body = await response.body();

		body.should.be.instanceOf( Buffer );
		body.toString( "utf8" ).should.be.String().which.match( /<html\W/i );
	} );

	it( "provides core service component HttpException", () => {
		ctx.hitchy.api.services.should.have.property( "HttpException" );

		const exception = new ctx.hitchy.api.services.HttpException( 404, "requested content not found" );

		exception.should.have.property( "statusCode" ).which.is.a.Number().and.is.equal( 404 );
	} );
} );
