import { describe, afterEach, it } from "mocha";
import "should";
import SDT from "@hitchy/server-dev-tools";

import Core from "../../../sdt.js";

const Test = await SDT( Core );

const config = {
	tools: Test,
	projectFolder: false,
	files: {
		"config/routes.mjs": `export const routes = { "/foo": "view.foo" };`,
	},
	options: {
		// debug: true,
	},
};

const delay = "new Promise( r => setTimeout( r, 30 ) )";

const handler = `( _, res ) => res.send( "Hello!" )`;
const handlerFn = `foo = ${handler}`;
const handlerMethod = `foo: ${handler}`;

const Controllers = {
	commonIndividual: `exports.${handlerFn};`,
	commonDefault: `module.exports = { ${handlerMethod} };`,
	commonDeferredDefault: `module.exports = ${delay}.then( () => ( { ${handlerMethod} } ) );`,
	commonCmp: `module.exports = function() { return { ${handlerMethod} }; };`,
	commonDeferredCmp: `module.exports = function() { return ${delay}.then( () => ( { ${handlerMethod} } ) ); };`,

	esmIndividual: `export const ${handlerFn};`,
	esmDefault: `export default { ${handlerMethod} };`,
	esmDeferredDefault: `export default ${delay}.then( () => ( { ${handlerMethod} } ) );`,
	esmCmp: `export default function() { return { ${handlerMethod} }; }`,
	esmDeferredCmp: `export default function() { return ${delay}.then( () => ( { ${handlerMethod} } ) ); }`,
};

const Policies = {
	commonIndividual: `exports.${handlerFn};`,
	commonDefault: `module.exports = { ${handlerMethod} };`,
	commonDeferredDefault: `module.exports = ${delay}.then( () => ( { ${handlerMethod} } ) );`,
	commonCmp: `module.exports = function() { return { ${handlerMethod} }; };`,
	commonDeferredCmp: `module.exports = function() { return ${delay}.then( () => ( { ${handlerMethod} } ) ); };`,

	esmIndividual: `export const ${handlerFn};`,
	esmDefault: `export default { ${handlerMethod} };`,
	esmDeferredDefault: `export default ${delay}.then( () => ( { ${handlerMethod} } ) );`,
	esmCmp: `export default function() { return { ${handlerMethod} }; }`,
	esmDeferredCmp: `export default function() { return ${delay}.then( () => ( { ${handlerMethod} } ) ); }`,
};

const Services = {
	commonIndividual: `exports.foo = () => "Hello!";`,
	commonDefault: `module.exports = { foo: () => "Hello!" };`,
	commonDeferredDefault: `module.exports = ${delay}.then( () => ( { foo: () => "Hello!" } ) );`,
	commonCmp: `module.exports = function() { return { foo: () => "Hello!" }; };`,
	commonDeferredCmp: `module.exports = function() { return ${delay}.then( () => ( { foo: () => "Hello!" } ) ); };`,

	esmIndividual: `export const foo = () => "Hello!";`,
	esmDefault: `export default { foo: () => "Hello!" };`,
	esmDeferredDefault: `export default ${delay}.then( () => ( { foo: () => "Hello!" } ) );`,
	esmCmp: `export default function() { return { foo: () => "Hello!" }; }`,
	esmDeferredCmp: `export default function() { return ${delay}.then( () => ( { foo: () => "Hello!" } ) ); }`,
};

const Models = {
	commonIndividual: `exports.foo = () => "Hello!";`,
	commonDefault: `module.exports = { foo: () => "Hello!" };`,
	commonDeferredDefault: `module.exports = ${delay}.then( () => ( { foo: () => "Hello!" } ) );`,
	commonCmp: `module.exports = function() { return { foo: () => "Hello!" }; };`,
	commonDeferredCmp: `module.exports = function() { return ${delay}.then( () => ( { foo: () => "Hello!" } ) ); };`,

	esmIndividual: `export const foo = () => "Hello!";`,
	esmDefault: `export default { foo: () => "Hello!" };`,
	esmDeferredDefault: `export default ${delay}.then( () => ( { foo: () => "Hello!" } ) );`,
	esmCmp: `export default function() { return { foo: () => "Hello!" }; }`,
	esmDeferredCmp: `export default function() { return ${delay}.then( () => ( { foo: () => "Hello!" } ) ); }`,
};

const Configurations = {
	commonIndividual: `exports.foo = "Hello!";`,
	commonDefault: `module.exports = { foo: "Hello!" };`,
	commonDeferredDefault: `module.exports = ${delay}.then( () => ( { foo: "Hello!" } ) );`,
	commonCmp: `module.exports = function() { return { foo: "Hello!" }; };`,
	commonDeferredCmp: `module.exports = function() { return ${delay}.then( () => ( { foo: "Hello!" } ) ); };`,

	esmIndividual: `export const foo = "Hello!";`,
	esmDefault: `export default { foo: "Hello!" };`,
	esmDeferredDefault: `export default ${delay}.then( () => ( { foo: "Hello!" } ) );`,
	esmCmp: `export default function() { return { foo: "Hello!" }; }`,
	esmDeferredCmp: `export default function() { return ${delay}.then( () => ( { foo: "Hello!" } ) ); }`,
};

describe( "Support for CommonJS and ES modules", () => {
	const Contexts = [
		{
			label: "controllers",
			componentType: "controller",
			code: Controllers,
			extraFiles: {},
			prefix: "api/",
		},
		{
			label: "policies",
			componentType: "policy",
			code: Policies,
			extraFiles: {
				"config/policies.cjs": `module.exports = { policies: { "/": "view.foo" } };`
			},
			prefix: "api/",
		},
		{
			label: "services",
			componentType: "service",
			code: Services,
			extraFiles: {
				"config/policies.cjs": `exports.policies = { "/": ( req, res ) => res.send( req.api.service.View.foo() ) };`
			},
			prefix: "api/",
		},
		{
			label: "models",
			componentType: "model",
			code: Models,
			extraFiles: {
				"config/policies.cjs": `exports.policies = { "/": ( req, res ) => res.send( req.api.model.View.foo() ) };`
			},
			prefix: "api/",
		},
		{
			label: "configurations",
			componentType: "config",
			code: Configurations,
			extraFiles: {
				"config/policies.cjs": `exports.policies = { "/": ( req, res ) => res.send( req.api.config.foo ) };`
			},
			prefix: "",
		},
	];

	const commonPackageJson = {
		"package.json": `{"type": "commonjs"}`,
	};

	const esmPackageJson = {
		"package.json": `{"type": "module"}`,
	};

	for ( const { label, componentType, code, extraFiles, prefix } of Contexts ) {
		describe( `in ${label}`, () => {
			const ctx = {};

			const useFiles = files => Test.before( ctx, {
				...config,
				files: {
					...config.files,
					...extraFiles,
					...files,
				}
			} );

			afterEach( Test.after( ctx ) );

			describe( "in a default project", () => {
				it( "is recognizing non-CMP-compliant code marked as CommonJS individually exporting its methods", async() => {
					await useFiles( {
						[`${prefix}${componentType}/view.cjs`]: code.commonIndividual
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant code marked as CommonJS using single default export", async() => {
					await useFiles( {
						[`${prefix}${componentType}/view.cjs`]: code.commonDefault
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant deferred code marked as CommonJS using single default export", async() => {
					await useFiles( {
						[`${prefix}${componentType}/view.cjs`]: code.commonDeferredDefault
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant code marked as CommonJS", async() => {
					await useFiles( {
						[`${prefix}${componentType}/view.cjs`]: code.commonCmp
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant deferred code marked as CommonJS", async() => {
					await useFiles( {
						[`${prefix}${componentType}/view.cjs`]: code.commonDeferredCmp
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );


				it( "is recognizing non-CMP-compliant code implicitly considered to be CommonJS individually exporting its methods", async() => {
					await useFiles( {
						...commonPackageJson,
						[`${prefix}${componentType}/view.js`]: code.commonIndividual
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant code implicitly considered to be CommonJS using single default export", async() => {
					await useFiles( {
						...commonPackageJson,
						[`${prefix}${componentType}/view.js`]: code.commonDefault
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant deferred code implicitly considered to be CommonJS using single default export", async() => {
					await useFiles( {
						...commonPackageJson,
						[`${prefix}${componentType}/view.js`]: code.commonDeferredDefault
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant code implicitly considered to be CommonJS using single default export", async() => {
					await useFiles( {
						...commonPackageJson,
						[`${prefix}${componentType}/view.js`]: code.commonCmp
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant deferred code implicitly considered to be CommonJS using single default export", async() => {
					await useFiles( {
						...commonPackageJson,
						[`${prefix}${componentType}/view.js`]: code.commonDeferredCmp
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );


				it( "is recognizing non-CMP-compliant code marked as ES module individually exporting its methods", async() => {
					await useFiles( {
						[`${prefix}${componentType}/view.mjs`]: code.esmIndividual
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant code marked as ES module using single default export", async() => {
					await useFiles( {
						[`${prefix}${componentType}/view.mjs`]: code.esmDefault
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant deferred code marked as ES module using single default export", async() => {
					await useFiles( {
						[`${prefix}${componentType}/view.mjs`]: code.esmDeferredDefault
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant code marked as ES module using single default export", async() => {
					await useFiles( {
						[`${prefix}${componentType}/view.mjs`]: code.esmCmp
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant deferred code marked as ES module using single default export", async() => {
					await useFiles( {
						[`${prefix}${componentType}/view.mjs`]: code.esmDeferredCmp
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );
			} );

			describe( "in a CommonJS project", () => {
				it( "is recognizing non-CMP-compliant code marked as CommonJS individually exporting its methods", async() => {
					await useFiles( {
						[`${prefix}${componentType}/view.cjs`]: code.commonIndividual,
						"package.json": `{"type": "commonjs"}`
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant code marked as CommonJS using single default export", async() => {
					await useFiles( {
						[`${prefix}${componentType}/view.cjs`]: code.commonDefault,
						"package.json": `{"type": "commonjs"}`
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant deferred code marked as CommonJS using single default export", async() => {
					await useFiles( {
						[`${prefix}${componentType}/view.cjs`]: code.commonDeferredDefault,
						"package.json": `{"type": "commonjs"}`
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant code marked as CommonJS", async() => {
					await useFiles( {
						[`${prefix}${componentType}/view.cjs`]: code.commonCmp,
						"package.json": `{"type": "commonjs"}`
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant deferred code marked as CommonJS", async() => {
					await useFiles( {
						[`${prefix}${componentType}/view.cjs`]: code.commonDeferredCmp,
						"package.json": `{"type": "commonjs"}`
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );


				it( "is recognizing non-CMP-compliant code implicitly considered to be CommonJS individually exporting its methods", async() => {
					await useFiles( {
						[`${prefix}${componentType}/view.js`]: code.commonIndividual,
						"package.json": `{"type": "commonjs"}`
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant code implicitly considered to be CommonJS using single default export", async() => {
					await useFiles( {
						[`${prefix}${componentType}/view.js`]: code.commonDefault,
						"package.json": `{"type": "commonjs"}`
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant deferred code implicitly considered to be CommonJS using single default export", async() => {
					await useFiles( {
						[`${prefix}${componentType}/view.js`]: code.commonDeferredDefault,
						"package.json": `{"type": "commonjs"}`
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant code implicitly considered to be CommonJS using single default export", async() => {
					await useFiles( {
						[`${prefix}${componentType}/view.js`]: code.commonCmp,
						"package.json": `{"type": "commonjs"}`
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant deferred code implicitly considered to be CommonJS using single default export", async() => {
					await useFiles( {
						[`${prefix}${componentType}/view.js`]: code.commonDeferredCmp,
						"package.json": `{"type": "commonjs"}`
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );


				it( "is recognizing non-CMP-compliant code marked as ES module individually exporting its methods", async() => {
					await useFiles( {
						[`${prefix}${componentType}/view.mjs`]: code.esmIndividual,
						"package.json": `{"type": "commonjs"}`
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant code marked as ES module using single default export", async() => {
					await useFiles( {
						[`${prefix}${componentType}/view.mjs`]: code.esmDefault,
						"package.json": `{"type": "commonjs"}`
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant deferred code marked as ES module using single default export", async() => {
					await useFiles( {
						[`${prefix}${componentType}/view.mjs`]: code.esmDeferredDefault,
						"package.json": `{"type": "commonjs"}`
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant code marked as ES module using single default export", async() => {
					await useFiles( {
						[`${prefix}${componentType}/view.mjs`]: code.esmCmp,
						"package.json": `{"type": "commonjs"}`
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant deferred code marked as ES module using single default export", async() => {
					await useFiles( {
						[`${prefix}${componentType}/view.mjs`]: code.esmDeferredCmp,
						"package.json": `{"type": "commonjs"}`
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );
			} );

			describe( "in a ES module project", () => {
				it( "is recognizing non-CMP-compliant code marked as CommonJS individually exporting its methods", async() => {
					await useFiles( {
						[`${prefix}${componentType}/view.cjs`]: code.commonIndividual,
						"package.json": `{"type": "module"}`
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant code marked as CommonJS using single default export", async() => {
					await useFiles( {
						[`${prefix}${componentType}/view.cjs`]: code.commonDefault,
						"package.json": `{"type": "module"}`
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant deferred code marked as CommonJS using single default export", async() => {
					await useFiles( {
						[`${prefix}${componentType}/view.cjs`]: code.commonDeferredDefault,
						"package.json": `{"type": "module"}`
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant code marked as CommonJS", async() => {
					await useFiles( {
						[`${prefix}${componentType}/view.cjs`]: code.commonCmp,
						"package.json": `{"type": "module"}`
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant deferred code marked as CommonJS", async() => {
					await useFiles( {
						[`${prefix}${componentType}/view.cjs`]: code.commonDeferredCmp,
						"package.json": `{"type": "module"}`
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );


				it( "is recognizing non-CMP-compliant code implicitly considered to be ES module individually exporting its methods", async() => {
					await useFiles( {
						[`${prefix}${componentType}/view.js`]: code.esmIndividual,
						"package.json": `{"type": "module"}`
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant code implicitly considered to be ES module using single default export", async() => {
					await useFiles( {
						[`${prefix}${componentType}/view.js`]: code.esmDefault,
						"package.json": `{"type": "module"}`
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant deferred code implicitly considered to be ES module using single default export", async() => {
					await useFiles( {
						[`${prefix}${componentType}/view.js`]: code.esmDeferredDefault,
						"package.json": `{"type": "module"}`
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant code implicitly considered to be ES module using single default export", async() => {
					await useFiles( {
						[`${prefix}${componentType}/view.js`]: code.esmCmp,
						"package.json": `{"type": "module"}`
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant deferred code implicitly considered to be ES module using single default export", async() => {
					await useFiles( {
						[`${prefix}${componentType}/view.js`]: code.esmDeferredCmp,
						"package.json": `{"type": "module"}`
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );


				it( "is recognizing non-CMP-compliant code marked as ES module individually exporting its methods", async() => {
					await useFiles( {
						[`${prefix}${componentType}/view.mjs`]: code.esmIndividual,
						"package.json": `{"type": "module"}`
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant code marked as ES module using single default export", async() => {
					await useFiles( {
						[`${prefix}${componentType}/view.mjs`]: code.esmDefault,
						"package.json": `{"type": "module"}`
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant deferred code marked as ES module using single default export", async() => {
					await useFiles( {
						[`${prefix}${componentType}/view.mjs`]: code.esmDeferredDefault,
						"package.json": `{"type": "module"}`
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant code marked as ES module using single default export", async() => {
					await useFiles( {
						[`${prefix}${componentType}/view.mjs`]: code.esmCmp,
						"package.json": `{"type": "module"}`
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant deferred code marked as ES module using single default export", async() => {
					await useFiles( {
						[`${prefix}${componentType}/view.mjs`]: code.esmDeferredCmp,
						"package.json": `{"type": "module"}`
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );
			} );

			describe( "in a default project with a default plugin", () => {
				it( "is recognizing non-CMP-compliant code in the plugin marked as CommonJS individually exporting its methods", async() => {
					await useFiles( {
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.cjs`]: code.commonIndividual
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant code in the plugin marked as CommonJS using single default export", async() => {
					await useFiles( {
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.cjs`]: code.commonDefault
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant deferred code in the plugin marked as CommonJS using single default export", async() => {
					await useFiles( {
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.cjs`]: code.commonDeferredDefault
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant code in the plugin marked as CommonJS", async() => {
					await useFiles( {
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.cjs`]: code.commonCmp
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant deferred code in the plugin marked as CommonJS", async() => {
					await useFiles( {
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.cjs`]: code.commonDeferredCmp
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );


				it( "is recognizing non-CMP-compliant code in the plugin implicitly considered to be CommonJS individually exporting its methods", async() => {
					await useFiles( {
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.js`]: code.commonIndividual
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant code in the plugin implicitly considered to be CommonJS using single default export", async() => {
					await useFiles( {
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.js`]: code.commonDefault
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant deferred code in the plugin implicitly considered to be CommonJS using single default export", async() => {
					await useFiles( {
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.js`]: code.commonDeferredDefault
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant code in the plugin implicitly considered to be CommonJS using single default export", async() => {
					await useFiles( {
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.js`]: code.commonCmp
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant deferred code in the plugin implicitly considered to be CommonJS using single default export", async() => {
					await useFiles( {
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.js`]: code.commonDeferredCmp
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );


				it( "is recognizing non-CMP-compliant code in the plugin marked as ES module individually exporting its methods", async() => {
					await useFiles( {
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.mjs`]: code.esmIndividual
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant code in the plugin marked as ES module using single default export", async() => {
					await useFiles( {
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.mjs`]: code.esmDefault
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant deferred code in the plugin marked as ES module using single default export", async() => {
					await useFiles( {
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.mjs`]: code.esmDeferredDefault
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant code in the plugin marked as ES module using single default export", async() => {
					await useFiles( {
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.mjs`]: code.esmCmp
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant deferred code in the plugin marked as ES module using single default export", async() => {
					await useFiles( {
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.mjs`]: code.esmDeferredCmp
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );
			} );

			describe( "in a default project with a CommonJS plugin", () => {
				it( "is recognizing non-CMP-compliant code in the plugin marked as CommonJS individually exporting its methods", async() => {
					await useFiles( {
						"node_modules/a/package.json": `{"type": "commonjs"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.cjs`]: code.commonIndividual
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant code in the plugin marked as CommonJS using single default export", async() => {
					await useFiles( {
						"node_modules/a/package.json": `{"type": "commonjs"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.cjs`]: code.commonDefault
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant deferred code in the plugin marked as CommonJS using single default export", async() => {
					await useFiles( {
						"node_modules/a/package.json": `{"type": "commonjs"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.cjs`]: code.commonDeferredDefault
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant code in the plugin marked as CommonJS", async() => {
					await useFiles( {
						"node_modules/a/package.json": `{"type": "commonjs"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.cjs`]: code.commonCmp
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant deferred code in the plugin marked as CommonJS", async() => {
					await useFiles( {
						"node_modules/a/package.json": `{"type": "commonjs"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.cjs`]: code.commonDeferredCmp
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );


				it( "is recognizing non-CMP-compliant code in the plugin implicitly considered to be CommonJS individually exporting its methods", async() => {
					await useFiles( {
						"node_modules/a/package.json": `{"type": "commonjs"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.js`]: code.commonIndividual
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant code in the plugin implicitly considered to be CommonJS using single default export", async() => {
					await useFiles( {
						"node_modules/a/package.json": `{"type": "commonjs"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.js`]: code.commonDefault
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant deferred code in the plugin implicitly considered to be CommonJS using single default export", async() => {
					await useFiles( {
						"node_modules/a/package.json": `{"type": "commonjs"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.js`]: code.commonDeferredDefault
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant code in the plugin implicitly considered to be CommonJS using single default export", async() => {
					await useFiles( {
						"node_modules/a/package.json": `{"type": "commonjs"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.js`]: code.commonCmp
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant deferred code in the plugin implicitly considered to be CommonJS using single default export", async() => {
					await useFiles( {
						"node_modules/a/package.json": `{"type": "commonjs"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.js`]: code.commonDeferredCmp
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );


				it( "is recognizing non-CMP-compliant code in the plugin marked as ES module individually exporting its methods", async() => {
					await useFiles( {
						"node_modules/a/package.json": `{"type": "commonjs"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.mjs`]: code.esmIndividual
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant code in the plugin marked as ES module using single default export", async() => {
					await useFiles( {
						"node_modules/a/package.json": `{"type": "commonjs"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.mjs`]: code.esmDefault
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant deferred code in the plugin marked as ES module using single default export", async() => {
					await useFiles( {
						"node_modules/a/package.json": `{"type": "commonjs"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.mjs`]: code.esmDeferredDefault
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant code in the plugin marked as ES module using single default export", async() => {
					await useFiles( {
						"node_modules/a/package.json": `{"type": "commonjs"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.mjs`]: code.esmCmp
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant deferred code in the plugin marked as ES module using single default export", async() => {
					await useFiles( {
						"node_modules/a/package.json": `{"type": "commonjs"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.mjs`]: code.esmDeferredCmp
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );
			} );

			describe( "in a default project with a ES module plugin", () => {
				it( "is recognizing non-CMP-compliant code in the plugin marked as CommonJS individually exporting its methods", async() => {
					await useFiles( {
						"node_modules/a/package.json": `{"type": "module"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.cjs`]: code.commonIndividual
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant code in the plugin marked as CommonJS using single default export", async() => {
					await useFiles( {
						"node_modules/a/package.json": `{"type": "module"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.cjs`]: code.commonDefault
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant deferred code in the plugin marked as CommonJS using single default export", async() => {
					await useFiles( {
						"node_modules/a/package.json": `{"type": "module"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.cjs`]: code.commonDeferredDefault
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant code in the plugin marked as CommonJS", async() => {
					await useFiles( {
						"node_modules/a/package.json": `{"type": "module"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.cjs`]: code.commonCmp
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant deferred code in the plugin marked as CommonJS", async() => {
					await useFiles( {
						"node_modules/a/package.json": `{"type": "module"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.cjs`]: code.commonDeferredCmp
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );


				it( "is recognizing non-CMP-compliant code in the plugin implicitly considered to be CommonJS individually exporting its methods", async() => {
					await useFiles( {
						"node_modules/a/package.json": `{"type": "module"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.js`]: code.esmIndividual
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant code in the plugin implicitly considered to be CommonJS using single default export", async() => {
					await useFiles( {
						"node_modules/a/package.json": `{"type": "module"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.js`]: code.esmDefault
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant deferred code in the plugin implicitly considered to be CommonJS using single default export", async() => {
					await useFiles( {
						"node_modules/a/package.json": `{"type": "module"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.js`]: code.esmDeferredDefault
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant code in the plugin implicitly considered to be CommonJS using single default export", async() => {
					await useFiles( {
						"node_modules/a/package.json": `{"type": "module"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.js`]: code.esmCmp
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant deferred code in the plugin implicitly considered to be CommonJS using single default export", async() => {
					await useFiles( {
						"node_modules/a/package.json": `{"type": "module"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.js`]: code.esmDeferredCmp
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );


				it( "is recognizing non-CMP-compliant code in the plugin marked as ES module individually exporting its methods", async() => {
					await useFiles( {
						"node_modules/a/package.json": `{"type": "module"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.mjs`]: code.esmIndividual
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant code in the plugin marked as ES module using single default export", async() => {
					await useFiles( {
						"node_modules/a/package.json": `{"type": "module"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.mjs`]: code.esmDefault
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant deferred code in the plugin marked as ES module using single default export", async() => {
					await useFiles( {
						"node_modules/a/package.json": `{"type": "module"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.mjs`]: code.esmDeferredDefault
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant code in the plugin marked as ES module using single default export", async() => {
					await useFiles( {
						"node_modules/a/package.json": `{"type": "module"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.mjs`]: code.esmCmp
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant deferred code in the plugin marked as ES module using single default export", async() => {
					await useFiles( {
						"node_modules/a/package.json": `{"type": "module"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.mjs`]: code.esmDeferredCmp
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );
			} );

			describe( "in a CommonJS project with a default plugin", () => {
				it( "is recognizing non-CMP-compliant code in the plugin marked as CommonJS individually exporting its methods", async() => {
					await useFiles( {
						"package.json": `{"type": "commonjs"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.cjs`]: code.commonIndividual
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant code in the plugin marked as CommonJS using single default export", async() => {
					await useFiles( {
						"package.json": `{"type": "commonjs"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.cjs`]: code.commonDefault
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant deferred code in the plugin marked as CommonJS using single default export", async() => {
					await useFiles( {
						"package.json": `{"type": "commonjs"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.cjs`]: code.commonDeferredDefault
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant code in the plugin marked as CommonJS", async() => {
					await useFiles( {
						"package.json": `{"type": "commonjs"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.cjs`]: code.commonCmp
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant deferred code in the plugin marked as CommonJS", async() => {
					await useFiles( {
						"package.json": `{"type": "commonjs"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.cjs`]: code.commonDeferredCmp
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );


				it( "is recognizing non-CMP-compliant code in the plugin implicitly considered to be CommonJS individually exporting its methods", async() => {
					await useFiles( {
						"package.json": `{"type": "commonjs"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.js`]: code.commonIndividual
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant code in the plugin implicitly considered to be CommonJS using single default export", async() => {
					await useFiles( {
						"package.json": `{"type": "commonjs"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.js`]: code.commonDefault
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant deferred code in the plugin implicitly considered to be CommonJS using single default export", async() => {
					await useFiles( {
						"package.json": `{"type": "commonjs"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.js`]: code.commonDeferredDefault
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant code in the plugin implicitly considered to be CommonJS using single default export", async() => {
					await useFiles( {
						"package.json": `{"type": "commonjs"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.js`]: code.commonCmp
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant deferred code in the plugin implicitly considered to be CommonJS using single default export", async() => {
					await useFiles( {
						"package.json": `{"type": "commonjs"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.js`]: code.commonDeferredCmp
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );


				it( "is recognizing non-CMP-compliant code in the plugin marked as ES module individually exporting its methods", async() => {
					await useFiles( {
						"package.json": `{"type": "commonjs"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.mjs`]: code.esmIndividual
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant code in the plugin marked as ES module using single default export", async() => {
					await useFiles( {
						"package.json": `{"type": "commonjs"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.mjs`]: code.esmDefault
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant deferred code in the plugin marked as ES module using single default export", async() => {
					await useFiles( {
						"package.json": `{"type": "commonjs"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.mjs`]: code.esmDeferredDefault
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant code in the plugin marked as ES module using single default export", async() => {
					await useFiles( {
						"package.json": `{"type": "commonjs"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.mjs`]: code.esmCmp
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant deferred code in the plugin marked as ES module using single default export", async() => {
					await useFiles( {
						"package.json": `{"type": "commonjs"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.mjs`]: code.esmDeferredCmp
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );
			} );

			describe( "in a CommonJS project with a CommonJS plugin", () => {
				it( "is recognizing non-CMP-compliant code in the plugin marked as CommonJS individually exporting its methods", async() => {
					await useFiles( {
						"package.json": `{"type": "commonjs"}`,
						"node_modules/a/package.json": `{"type": "commonjs"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.cjs`]: code.commonIndividual
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant code in the plugin marked as CommonJS using single default export", async() => {
					await useFiles( {
						"package.json": `{"type": "commonjs"}`,
						"node_modules/a/package.json": `{"type": "commonjs"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.cjs`]: code.commonDefault
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant deferred code in the plugin marked as CommonJS using single default export", async() => {
					await useFiles( {
						"package.json": `{"type": "commonjs"}`,
						"node_modules/a/package.json": `{"type": "commonjs"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.cjs`]: code.commonDeferredDefault
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant code in the plugin marked as CommonJS", async() => {
					await useFiles( {
						"package.json": `{"type": "commonjs"}`,
						"node_modules/a/package.json": `{"type": "commonjs"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.cjs`]: code.commonCmp
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant deferred code in the plugin marked as CommonJS", async() => {
					await useFiles( {
						"package.json": `{"type": "commonjs"}`,
						"node_modules/a/package.json": `{"type": "commonjs"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.cjs`]: code.commonDeferredCmp
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );


				it( "is recognizing non-CMP-compliant code in the plugin implicitly considered to be CommonJS individually exporting its methods", async() => {
					await useFiles( {
						"package.json": `{"type": "commonjs"}`,
						"node_modules/a/package.json": `{"type": "commonjs"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.js`]: code.commonIndividual
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant code in the plugin implicitly considered to be CommonJS using single default export", async() => {
					await useFiles( {
						"package.json": `{"type": "commonjs"}`,
						"node_modules/a/package.json": `{"type": "commonjs"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.js`]: code.commonDefault
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant deferred code in the plugin implicitly considered to be CommonJS using single default export", async() => {
					await useFiles( {
						"package.json": `{"type": "commonjs"}`,
						"node_modules/a/package.json": `{"type": "commonjs"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.js`]: code.commonDeferredDefault
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant code in the plugin implicitly considered to be CommonJS using single default export", async() => {
					await useFiles( {
						"package.json": `{"type": "commonjs"}`,
						"node_modules/a/package.json": `{"type": "commonjs"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.js`]: code.commonCmp
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant deferred code in the plugin implicitly considered to be CommonJS using single default export", async() => {
					await useFiles( {
						"package.json": `{"type": "commonjs"}`,
						"node_modules/a/package.json": `{"type": "commonjs"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.js`]: code.commonDeferredCmp
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );


				it( "is recognizing non-CMP-compliant code in the plugin marked as ES module individually exporting its methods", async() => {
					await useFiles( {
						"package.json": `{"type": "commonjs"}`,
						"node_modules/a/package.json": `{"type": "commonjs"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.mjs`]: code.esmIndividual
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant code in the plugin marked as ES module using single default export", async() => {
					await useFiles( {
						"package.json": `{"type": "commonjs"}`,
						"node_modules/a/package.json": `{"type": "commonjs"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.mjs`]: code.esmDefault
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant deferred code in the plugin marked as ES module using single default export", async() => {
					await useFiles( {
						"package.json": `{"type": "commonjs"}`,
						"node_modules/a/package.json": `{"type": "commonjs"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.mjs`]: code.esmDeferredDefault
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant code in the plugin marked as ES module using single default export", async() => {
					await useFiles( {
						"package.json": `{"type": "commonjs"}`,
						"node_modules/a/package.json": `{"type": "commonjs"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.mjs`]: code.esmCmp
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant deferred code in the plugin marked as ES module using single default export", async() => {
					await useFiles( {
						"package.json": `{"type": "commonjs"}`,
						"node_modules/a/package.json": `{"type": "commonjs"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.mjs`]: code.esmDeferredCmp
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );
			} );

			describe( "in a CommonJS project with a ES module plugin", () => {
				it( "is recognizing non-CMP-compliant code in the plugin marked as CommonJS individually exporting its methods", async() => {
					await useFiles( {
						"package.json": `{"type": "commonjs"}`,
						"node_modules/a/package.json": `{"type": "module"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.cjs`]: code.commonIndividual
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant code in the plugin marked as CommonJS using single default export", async() => {
					await useFiles( {
						"package.json": `{"type": "commonjs"}`,
						"node_modules/a/package.json": `{"type": "module"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.cjs`]: code.commonDefault
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant deferred code in the plugin marked as CommonJS using single default export", async() => {
					await useFiles( {
						"package.json": `{"type": "commonjs"}`,
						"node_modules/a/package.json": `{"type": "module"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.cjs`]: code.commonDeferredDefault
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant code in the plugin marked as CommonJS", async() => {
					await useFiles( {
						"package.json": `{"type": "commonjs"}`,
						"node_modules/a/package.json": `{"type": "module"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.cjs`]: code.commonCmp
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant deferred code in the plugin marked as CommonJS", async() => {
					await useFiles( {
						"package.json": `{"type": "commonjs"}`,
						"node_modules/a/package.json": `{"type": "module"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.cjs`]: code.commonDeferredCmp
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );


				it( "is recognizing non-CMP-compliant code in the plugin implicitly considered to be CommonJS individually exporting its methods", async() => {
					await useFiles( {
						"package.json": `{"type": "commonjs"}`,
						"node_modules/a/package.json": `{"type": "module"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.js`]: code.esmIndividual
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant code in the plugin implicitly considered to be CommonJS using single default export", async() => {
					await useFiles( {
						"package.json": `{"type": "commonjs"}`,
						"node_modules/a/package.json": `{"type": "module"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.js`]: code.esmDefault
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant deferred code in the plugin implicitly considered to be CommonJS using single default export", async() => {
					await useFiles( {
						"package.json": `{"type": "commonjs"}`,
						"node_modules/a/package.json": `{"type": "module"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.js`]: code.esmDeferredDefault
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant code in the plugin implicitly considered to be CommonJS using single default export", async() => {
					await useFiles( {
						"package.json": `{"type": "commonjs"}`,
						"node_modules/a/package.json": `{"type": "module"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.js`]: code.esmCmp
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant deferred code in the plugin implicitly considered to be CommonJS using single default export", async() => {
					await useFiles( {
						"package.json": `{"type": "commonjs"}`,
						"node_modules/a/package.json": `{"type": "module"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.js`]: code.esmDeferredCmp
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );


				it( "is recognizing non-CMP-compliant code in the plugin marked as ES module individually exporting its methods", async() => {
					await useFiles( {
						"package.json": `{"type": "commonjs"}`,
						"node_modules/a/package.json": `{"type": "module"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.mjs`]: code.esmIndividual
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant code in the plugin marked as ES module using single default export", async() => {
					await useFiles( {
						"package.json": `{"type": "commonjs"}`,
						"node_modules/a/package.json": `{"type": "module"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.mjs`]: code.esmDefault
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant deferred code in the plugin marked as ES module using single default export", async() => {
					await useFiles( {
						"package.json": `{"type": "commonjs"}`,
						"node_modules/a/package.json": `{"type": "module"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.mjs`]: code.esmDeferredDefault
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant code in the plugin marked as ES module using single default export", async() => {
					await useFiles( {
						"package.json": `{"type": "commonjs"}`,
						"node_modules/a/package.json": `{"type": "module"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.mjs`]: code.esmCmp
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant deferred code in the plugin marked as ES module using single default export", async() => {
					await useFiles( {
						"package.json": `{"type": "commonjs"}`,
						"node_modules/a/package.json": `{"type": "module"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.mjs`]: code.esmDeferredCmp
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );
			} );

			describe( "in a ES module project with a default plugin", () => {
				it( "is recognizing non-CMP-compliant code in the plugin marked as CommonJS individually exporting its methods", async() => {
					await useFiles( {
						"package.json": `{"type": "module"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.cjs`]: code.commonIndividual
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant code in the plugin marked as CommonJS using single default export", async() => {
					await useFiles( {
						"package.json": `{"type": "module"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.cjs`]: code.commonDefault
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant deferred code in the plugin marked as CommonJS using single default export", async() => {
					await useFiles( {
						"package.json": `{"type": "module"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.cjs`]: code.commonDeferredDefault
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant code in the plugin marked as CommonJS", async() => {
					await useFiles( {
						"package.json": `{"type": "module"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.cjs`]: code.commonCmp
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant deferred code in the plugin marked as CommonJS", async() => {
					await useFiles( {
						"package.json": `{"type": "module"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.cjs`]: code.commonDeferredCmp
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );


				it( "is recognizing non-CMP-compliant code in the plugin implicitly considered to be CommonJS individually exporting its methods", async() => {
					await useFiles( {
						"package.json": `{"type": "module"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.js`]: code.commonIndividual
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant code in the plugin implicitly considered to be CommonJS using single default export", async() => {
					await useFiles( {
						"package.json": `{"type": "module"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.js`]: code.commonDefault
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant deferred code in the plugin implicitly considered to be CommonJS using single default export", async() => {
					await useFiles( {
						"package.json": `{"type": "module"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.js`]: code.commonDeferredDefault
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant code in the plugin implicitly considered to be CommonJS using single default export", async() => {
					await useFiles( {
						"package.json": `{"type": "module"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.js`]: code.commonCmp
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant deferred code in the plugin implicitly considered to be CommonJS using single default export", async() => {
					await useFiles( {
						"package.json": `{"type": "module"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.js`]: code.commonDeferredCmp
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );


				it( "is recognizing non-CMP-compliant code in the plugin marked as ES module individually exporting its methods", async() => {
					await useFiles( {
						"package.json": `{"type": "module"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.mjs`]: code.esmIndividual
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant code in the plugin marked as ES module using single default export", async() => {
					await useFiles( {
						"package.json": `{"type": "module"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.mjs`]: code.esmDefault
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant deferred code in the plugin marked as ES module using single default export", async() => {
					await useFiles( {
						"package.json": `{"type": "module"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.mjs`]: code.esmDeferredDefault
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant code in the plugin marked as ES module using single default export", async() => {
					await useFiles( {
						"package.json": `{"type": "module"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.mjs`]: code.esmCmp
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant deferred code in the plugin marked as ES module using single default export", async() => {
					await useFiles( {
						"package.json": `{"type": "module"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.mjs`]: code.esmDeferredCmp
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );
			} );

			describe( "in a ES module project with a CommonJS plugin", () => {
				it( "is recognizing non-CMP-compliant code in the plugin marked as CommonJS individually exporting its methods", async() => {
					await useFiles( {
						"package.json": `{"type": "module"}`,
						"node_modules/a/package.json": `{"type": "commonjs"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.cjs`]: code.commonIndividual
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant code in the plugin marked as CommonJS using single default export", async() => {
					await useFiles( {
						"package.json": `{"type": "module"}`,
						"node_modules/a/package.json": `{"type": "commonjs"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.cjs`]: code.commonDefault
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant deferred code in the plugin marked as CommonJS using single default export", async() => {
					await useFiles( {
						"package.json": `{"type": "module"}`,
						"node_modules/a/package.json": `{"type": "commonjs"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.cjs`]: code.commonDeferredDefault
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant code in the plugin marked as CommonJS", async() => {
					await useFiles( {
						"package.json": `{"type": "module"}`,
						"node_modules/a/package.json": `{"type": "commonjs"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.cjs`]: code.commonCmp
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant deferred code in the plugin marked as CommonJS", async() => {
					await useFiles( {
						"package.json": `{"type": "module"}`,
						"node_modules/a/package.json": `{"type": "commonjs"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.cjs`]: code.commonDeferredCmp
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );


				it( "is recognizing non-CMP-compliant code in the plugin implicitly considered to be CommonJS individually exporting its methods", async() => {
					await useFiles( {
						"package.json": `{"type": "module"}`,
						"node_modules/a/package.json": `{"type": "commonjs"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.js`]: code.commonIndividual
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant code in the plugin implicitly considered to be CommonJS using single default export", async() => {
					await useFiles( {
						"package.json": `{"type": "module"}`,
						"node_modules/a/package.json": `{"type": "commonjs"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.js`]: code.commonDefault
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant deferred code in the plugin implicitly considered to be CommonJS using single default export", async() => {
					await useFiles( {
						"package.json": `{"type": "module"}`,
						"node_modules/a/package.json": `{"type": "commonjs"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.js`]: code.commonDeferredDefault
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant code in the plugin implicitly considered to be CommonJS using single default export", async() => {
					await useFiles( {
						"package.json": `{"type": "module"}`,
						"node_modules/a/package.json": `{"type": "commonjs"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.js`]: code.commonCmp
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant deferred code in the plugin implicitly considered to be CommonJS using single default export", async() => {
					await useFiles( {
						"package.json": `{"type": "module"}`,
						"node_modules/a/package.json": `{"type": "commonjs"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.js`]: code.commonDeferredCmp
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );


				it( "is recognizing non-CMP-compliant code in the plugin marked as ES module individually exporting its methods", async() => {
					await useFiles( {
						"package.json": `{"type": "module"}`,
						"node_modules/a/package.json": `{"type": "commonjs"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.mjs`]: code.esmIndividual
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant code in the plugin marked as ES module using single default export", async() => {
					await useFiles( {
						"package.json": `{"type": "module"}`,
						"node_modules/a/package.json": `{"type": "commonjs"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.mjs`]: code.esmDefault
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant deferred code in the plugin marked as ES module using single default export", async() => {
					await useFiles( {
						"package.json": `{"type": "module"}`,
						"node_modules/a/package.json": `{"type": "commonjs"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.mjs`]: code.esmDeferredDefault
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant code in the plugin marked as ES module using single default export", async() => {
					await useFiles( {
						"package.json": `{"type": "module"}`,
						"node_modules/a/package.json": `{"type": "commonjs"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.mjs`]: code.esmCmp
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant deferred code in the plugin marked as ES module using single default export", async() => {
					await useFiles( {
						"package.json": `{"type": "module"}`,
						"node_modules/a/package.json": `{"type": "commonjs"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.mjs`]: code.esmDeferredCmp
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );
			} );

			describe( "in a ES module project with a ES module plugin", () => {
				it( "is recognizing non-CMP-compliant code in the plugin marked as CommonJS individually exporting its methods", async() => {
					await useFiles( {
						"package.json": `{"type": "module"}`,
						"node_modules/a/package.json": `{"type": "module"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.cjs`]: code.commonIndividual
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant code in the plugin marked as CommonJS using single default export", async() => {
					await useFiles( {
						"package.json": `{"type": "module"}`,
						"node_modules/a/package.json": `{"type": "module"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.cjs`]: code.commonDefault
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant deferred code in the plugin marked as CommonJS using single default export", async() => {
					await useFiles( {
						"package.json": `{"type": "module"}`,
						"node_modules/a/package.json": `{"type": "module"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.cjs`]: code.commonDeferredDefault
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant code in the plugin marked as CommonJS", async() => {
					await useFiles( {
						"package.json": `{"type": "module"}`,
						"node_modules/a/package.json": `{"type": "module"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.cjs`]: code.commonCmp
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant deferred code in the plugin marked as CommonJS", async() => {
					await useFiles( {
						"package.json": `{"type": "module"}`,
						"node_modules/a/package.json": `{"type": "module"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.cjs`]: code.commonDeferredCmp
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );


				it( "is recognizing non-CMP-compliant code in the plugin implicitly considered to be CommonJS individually exporting its methods", async() => {
					await useFiles( {
						"package.json": `{"type": "module"}`,
						"node_modules/a/package.json": `{"type": "module"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.js`]: code.esmIndividual
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant code in the plugin implicitly considered to be CommonJS using single default export", async() => {
					await useFiles( {
						"package.json": `{"type": "module"}`,
						"node_modules/a/package.json": `{"type": "module"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.js`]: code.esmDefault
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant deferred code in the plugin implicitly considered to be CommonJS using single default export", async() => {
					await useFiles( {
						"package.json": `{"type": "module"}`,
						"node_modules/a/package.json": `{"type": "module"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.js`]: code.esmDeferredDefault
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant code in the plugin implicitly considered to be CommonJS using single default export", async() => {
					await useFiles( {
						"package.json": `{"type": "module"}`,
						"node_modules/a/package.json": `{"type": "module"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.js`]: code.esmCmp
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant deferred code in the plugin implicitly considered to be CommonJS using single default export", async() => {
					await useFiles( {
						"package.json": `{"type": "module"}`,
						"node_modules/a/package.json": `{"type": "module"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.js`]: code.esmDeferredCmp
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );


				it( "is recognizing non-CMP-compliant code in the plugin marked as ES module individually exporting its methods", async() => {
					await useFiles( {
						"package.json": `{"type": "module"}`,
						"node_modules/a/package.json": `{"type": "module"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.mjs`]: code.esmIndividual
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant code in the plugin marked as ES module using single default export", async() => {
					await useFiles( {
						"package.json": `{"type": "module"}`,
						"node_modules/a/package.json": `{"type": "module"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.mjs`]: code.esmDefault
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing non-CMP-compliant deferred code in the plugin marked as ES module using single default export", async() => {
					await useFiles( {
						"package.json": `{"type": "module"}`,
						"node_modules/a/package.json": `{"type": "module"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.mjs`]: code.esmDeferredDefault
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant code in the plugin marked as ES module using single default export", async() => {
					await useFiles( {
						"package.json": `{"type": "module"}`,
						"node_modules/a/package.json": `{"type": "module"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.mjs`]: code.esmCmp
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );

				it( "is recognizing CMP-compliant deferred code in the plugin marked as ES module using single default export", async() => {
					await useFiles( {
						"package.json": `{"type": "module"}`,
						"node_modules/a/package.json": `{"type": "module"}`,
						"node_modules/a/hitchy.json": `{}`,
						[`node_modules/a/${prefix}${componentType}/view.mjs`]: code.esmDeferredCmp
					} )();

					( await ctx.get( "/foo" ) ).body.toString( "utf8" ).should.be.equal( "Hello!" );
				} );
			} );
		} );
	}
} );
