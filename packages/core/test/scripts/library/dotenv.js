import { describe, it, beforeEach, afterEach } from "mocha";
import "should";
import { parseDotEnv, resolveVariables } from "../../../lib/misc/dotenv.js";

import SDT from "@hitchy/server-dev-tools";

import Core from "../../../sdt.js";

const Test = await SDT( Core );

const config = {
	tools: Test,
	projectFolder: false,
	files: {
		"api/service/my-env.mjs": `export default function( options ) { return { values: { ...options.environment }, getter: () => options.environment }; }`,
		"config/env.mjs": `export default function( options ) { return { env: options.environment }; }`,
		".env": `foo=dotenv-test
BAM=\${PATH:+is set}
Bam=\${Path:+is set}
bar=\${foo}`,
	},
	options: {
		// debug: true,
	},
};

describe( ".env parser", () => {
	describe( "delivers empty object", () => {
		it( "when invoked without any parameters", () => {
			parseDotEnv().should.be.deepEqual( {} );
		} );

		it( "when invoked with a nullish parameter", () => {
			parseDotEnv( undefined ).should.be.deepEqual( {} );
			parseDotEnv( null ).should.be.deepEqual( {} );
		} );

		it( "when invoked with an empty string", () => {
			parseDotEnv( "" ).should.be.deepEqual( {} );
		} );

		it( "when invoked with a string solely consisting of whitespace", () => {
			parseDotEnv( `  
    ` ).should.be.deepEqual( {} );
		} );

		it( "when invoked with a string consisting of comments and whitespace, only", () => {
			parseDotEnv( `# line 1
 # line 2
	# line 3
` ).should.be.deepEqual( {} );
		} );
	} );

	it( "supports most simple assignments", () => {
		parseDotEnv( `a=` ).should.be.deepEqual( { a: undefined } );
	} );

	it( "supports simple assignments", () => {
		parseDotEnv( `a=b` ).should.be.deepEqual( { a: "b" } );
	} );

	it( "supports multiple characters in name", () => {
		parseDotEnv( `alpha=b` ).should.be.deepEqual( { alpha: "b" } );
	} );

	it( "supports multiple characters in value", () => {
		parseDotEnv( `a=beta` ).should.be.deepEqual( { a: "beta" } );
	} );

	it( "supports extra whitespace before name", () => {
		parseDotEnv( `  alpha=beta` ).should.be.deepEqual( { alpha: "beta" } );
	} );

	it( "supports extra whitespace after name", () => {
		parseDotEnv( `  alpha   =beta` ).should.be.deepEqual( { alpha: "beta" } );
	} );

	it( "supports extra whitespace before value", () => {
		parseDotEnv( `  alpha   =   beta` ).should.be.deepEqual( { alpha: "beta" } );
	} );

	it( "supports extra whitespace after value", () => {
		parseDotEnv( `  alpha   =   beta     ` ).should.be.deepEqual( { alpha: "beta" } );
	} );

	it( "supports extra whitespace after assignment", () => {
		parseDotEnv( `  alpha   =    ` ).should.be.deepEqual( { alpha: undefined } );
	} );

	it( "supports multiple assignments in separate lines", () => {
		parseDotEnv( `a=b
c=d` ).should.be.deepEqual( { a: "b", c: "d" } );
	} );

	it( "replaces previously assigned value", () => {
		parseDotEnv( `  alpha   =  beta  
alpha=gamma` ).should.be.deepEqual( { alpha: "gamma" } );
	} );

	it( "assumes names to be case-sensitive", () => {
		parseDotEnv( `  alpha   =  beta  
aLpha=gamma` ).should.be.deepEqual( { alpha: "beta", aLpha: "gamma" } );
	} );

	it( "ignores empty lines", () => {
		parseDotEnv( `a=b

c=d

` ).should.be.deepEqual( { a: "b", c: "d" } );
	} );

	it( "ignores lines with comments", () => {
		parseDotEnv( `#a=b
c=d` ).should.be.deepEqual( { c: "d" } );
	} );

	it( "accepts indented comments", () => {
		parseDotEnv( `  #a=b
c=d` ).should.be.deepEqual( { c: "d" } );
	} );

	it( "supports inline comments", () => {
		parseDotEnv( `  a=#b=a
c=d` ).should.be.deepEqual( { a: undefined, c: "d" } );
	} );

	it( "supports names enclosed in single quotes", () => {
		parseDotEnv( `'a'=b` ).should.be.deepEqual( { a: "b" } );
	} );

	it( "supports functional characters in names enclosed in single quotes", () => {
		parseDotEnv( `'a =
#"'=b` ).should.be.deepEqual( { 'a =\n#"': "b" } );
	} );

	it( "supports leading/trailing whitespace in names enclosed in single quotes", () => {
		parseDotEnv( `' a =
#" '=b` ).should.be.deepEqual( { ' a =\n#" ': "b" } );
	} );

	it( "supports names enclosed in double quotes", () => {
		parseDotEnv( `"a"=b` ).should.be.deepEqual( { a: "b" } );
	} );

	it( "supports functional characters in names enclosed in double quotes", () => {
		parseDotEnv( `"a =
#'"=b` ).should.be.deepEqual( { "a =\n#'": "b" } );
	} );

	it( "supports leading/trailing whitespace in names enclosed in double quotes", () => {
		parseDotEnv( `" a =
#' "=b` ).should.be.deepEqual( { " a =\n#' ": "b" } );
	} );

	it( "supports values enclosed in single quotes", () => {
		parseDotEnv( `a='b'` ).should.be.deepEqual( { a: "b" } );
	} );

	it( "supports functional characters in values enclosed in single quotes", () => {
		parseDotEnv( `a='b = 
#"'` ).should.be.deepEqual( { a: 'b = \n#"' } );
	} );

	it( "supports leading/trailing whitespace in values enclosed in single quotes", () => {
		parseDotEnv( `a='  b = 
#"  '` ).should.be.deepEqual( { a: '  b = \n#"  ' } );
	} );

	it( "supports values enclosed in double quotes", () => {
		parseDotEnv( `a="b"` ).should.be.deepEqual( { a: "b" } );
	} );

	it( "supports functional characters in values enclosed in double quotes", () => {
		parseDotEnv( `a="b = 
#'"` ).should.be.deepEqual( { a: "b = \n#'" } );
	} );

	it( "supports leading/trailing whitespace in values enclosed in double quotes", () => {
		parseDotEnv( `a="  b = 
#'  "` ).should.be.deepEqual( { a: "  b = \n#'  " } );
	} );

	it( "fails if name is missing", () => {
		( () => parseDotEnv( `=b` ) ).should.throwError( /missing name/ );
		( () => parseDotEnv( `   =b` ) ).should.throwError( /missing name/ );
		( () => parseDotEnv( ` ""  =b` ) ).should.throwError( /missing name/ );
		( () => parseDotEnv( ` ''  =b` ) ).should.throwError( /missing name/ );
	} );

	it( "fails if assignment is missing", () => {
		( () => parseDotEnv( `a` ) ).should.throwError( /unexpected end of file/ );
		( () => parseDotEnv( `a#` ) ).should.throwError( /expecting =/ );
		( () => parseDotEnv( `a'='` ) ).should.throwError( /expecting =/ );
		( () => parseDotEnv( `a"="` ) ).should.throwError( /expecting =/ );
		( () => parseDotEnv( `a'='` ) ).should.throwError( /expecting =/ );
		( () => parseDotEnv( `"a"b=` ) ).should.throwError( /expecting =/ );
	} );

	it( "fails if content is found after quoted value", () => {
		( () => parseDotEnv( `a=b c d ` ) ).should.not.throwError( /unexpected content/ );
		( () => parseDotEnv( `a="b c"d ` ) ).should.throwError( /unexpected content/ );
		( () => parseDotEnv( `a='b c'd ` ) ).should.throwError( /unexpected content/ );
		( () => parseDotEnv( `a="b c" d ` ) ).should.throwError( /unexpected content/ );
		( () => parseDotEnv( `a='b c' d ` ) ).should.throwError( /unexpected content/ );
		( () => parseDotEnv( `a="b c"#d ` ) ).should.not.throwError( /unexpected content/ );
		( () => parseDotEnv( `a='b c'#d ` ) ).should.not.throwError( /unexpected content/ );
		( () => parseDotEnv( `a="b c" #d ` ) ).should.not.throwError( /unexpected content/ );
		( () => parseDotEnv( `a='b c' #d ` ) ).should.not.throwError( /unexpected content/ );
	} );
} );

describe( "Method resolveVariables()", () => {
	it( "is a function", () => {
		resolveVariables.should.be.a.Function();
	} );

	it( "can be invoked without any argument", () => {
		( resolveVariables() == null ).should.be.true();
		( resolveVariables( undefined ) == null ).should.be.true();
		( resolveVariables( null ) == null ).should.be.true();
	} );

	it( "returns string without markers as given in first argument", () => {
		resolveVariables( "" ).should.equal( "" );
		resolveVariables( " \t\n\r " ).should.equal( " \t\n\r " );
		resolveVariables( " just some string " ).should.equal( " just some string " );
	} );

	it( "removes markers in provided string when invoked without any source", () => {
		resolveVariables( " just ${some} string ${foo} " ).should.equal( " just  string  " );
	} );

	it( "replaces markers in provided string when invoked with values from a single source", () => {
		resolveVariables( " just ${some} string ${foo} ", {
			some: "boo",
			foo: "bam",
		} ).should.equal( " just boo string bam " );
	} );

	it( "replaces markers in provided string when invoked with values from a single source", () => {
		resolveVariables( " just ${some} string ${foo} ", {}, {
			some: "boo",
			foo: "bam",
		} ).should.equal( " just boo string bam " );

		resolveVariables( " just ${some} string ${foo} ", {
			some: "boo",
		}, {
			foo: "bam",
		} ).should.equal( " just boo string bam " );
	} );

	it( "replaces names case-sensitively", () => {
		resolveVariables( " just ${sOme} string ${foO} ", {
			some: "BOO",
			foo: "BAM",
		}, {
			sOme: "boo",
			foO: "bam",
		} ).should.equal( " just boo string bam " );
	} );

	it( "applies default when named value does not exist in either source", () => {
		resolveVariables( " just ${sOme:-yay} string ${foO:-yikes} ", {
			some: "BOO",
		}, {
			foo: "bam",
		} ).should.equal( " just yay string yikes " );
	} );

	it( "applies alternative value when named value exists in either source", () => {
		resolveVariables( " just ${some:+yay} string ${foo:+yikes} ", {
			some: "BOO",
		}, {
			foo: "bam",
		} ).should.equal( " just yay string yikes " );
	} );
} );

describe( ".env file in project folder", () => {
	const ctx = {};

	afterEach( Test.after( ctx ) );
	beforeEach( Test.before( ctx, config ) );

	it( "is read automatically", () => {
		const { env } = ctx.hitchy.api.config;

		env.foo.should.equal( "dotenv-test" );
		env.bar.should.equal( "dotenv-test" );

		( env.BAM || env.Bam ).should.equal( "is set" );
	} );

	it( "has not been read during discovery stage of bootstrap", () => {
		const env = ctx.hitchy.api.service.MyEnv.values;

		( env.foo == null ).should.be.true();
		( env.bar == null ).should.be.true();
		( env.BAM == null ).should.be.true();
		( env.Bam == null ).should.be.true();
	} );

	it( "has been read at runtime", () => {
		const env = ctx.hitchy.api.service.MyEnv.getter();

		env.foo.should.equal( "dotenv-test" );
		env.bar.should.equal( "dotenv-test" );

		( env.BAM || env.Bam ).should.equal( "is set" );
	} );
} );
