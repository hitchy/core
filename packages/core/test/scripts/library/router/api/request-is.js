import { describe, it, beforeEach, afterEach } from "mocha";
import "should";
import SDT from "@hitchy/server-dev-tools";

import Core from "../../../../../sdt.js";

const Test = await SDT( Core );

const config = {
	projectFolder: "test/projects/empty",
	options: {
		// debug: true,
	},
};

const TestCases = [
	// actual , test , test result
	[ undefined, "text/html", false ],
	[ undefined, "text/html", false ],
	[ "text/html", "text/html", "text/html" ],
	[ "text/html", "text/*", "text/*" ],
	[ "text/html", "*/html", "*/html" ],
	[ "text/html", "html", "html" ],
	[ "text/html", "text", false ],
	[ "text/plain", "text", "text" ],
	[ "application/json", "text", false ],
	[ "application/json", "json", "json" ],
	[ "text/json", "json", "json" ],
	[ "text/json", "text", false ],
	[ "application/xml", "xml", "xml" ],
	[ "application/xml", "*/xml", "*/xml" ],
	[ "text/xml", "xml", "xml" ],
	[ "text/xml", "text", false ],
	[ "text/xml", "t*e*x*t", "t*e*x*t" ],
	[ "text/xml", [ "text", "xml", "json" ], "xml" ],
	[ "text/plain", [ "text", "xml", "json" ], "text" ],
	[ "text/json", [ "text", "xml", "json" ], "json" ],
	[ "text/json", [ "text", "xml", "*t/json", "json" ], "*t/json" ],
	[ "application/ld+json", [ "json", "+json" ], "+json" ],
	[ "application/vnd.api+json", [ "json", "+json" ], "+json" ],
	[ "application/vnd.api+json", "vnd.api+json", "vnd.api+json" ],
	[ "application/vnd-api+json", "vnd.api+json", false ],
	[ "application/x-resource+json", "x-*", "x-*" ],
	[ "aPPlication/jSOn", "json", "json" ],
	[ "aPPlication/jSOn", "*/json", "*/json" ],
	[ "aPPlication/jSOn", "application/json", "application/json" ],
	[ "aPPlication/jSOn", "JSON", "JSON" ],
	[ "aPPlication/jSOn", "application/JSON", "application/JSON" ],
	[ "aPPlication/jSOn", "appl*Ion/JSO*N", "appl*Ion/JSO*N" ],
];

describe( "RoutingPolyfillFactory service features per-request method is() which", () => {
	const ctx = {};

	beforeEach( Test.before( ctx, { ...config } ) );
	afterEach( Test.after( ctx ) );

	it( "can be created in context of a request", () => {
		ctx.hitchy.api.service.RoutingPolyfillFactory.requestIs.should.be.Function();
		( () => ctx.hitchy.api.service.RoutingPolyfillFactory.requestIs( { request: { headers: {} } } ) ).should.not.throw();
	} );

	it( "returns `null` if there is no body according to some usual suspect request headers", () => {
		TestCases.forEach( ( [ mime, test ] ) => {
			const context = {
				request: {
					headers: {
					},
				},
			};

			if ( mime ) {
				context.request.headers["content-type"] = mime;
			}

			const is = ctx.hitchy.api.service.RoutingPolyfillFactory.requestIs( context );

			( is( test ) == null ).should.be.true();
		} );
	} );

	it( "properly tests existing information on provided content type", () => {
		TestCases.forEach( ( [ mime, test, expected ] ) => {
			const context = {
				request: {
					headers: {
						"content-length": 1,
					},
				},
			};

			if ( mime ) {
				context.request.headers["content-type"] = mime;
			}

			const is = ctx.hitchy.api.service.RoutingPolyfillFactory.requestIs( context );
			const actual = is( test );

			actual.should.equal( expected, `expected ${expected} on testing ${mime} with ${typeof test} ${test.toString()}, but got ${actual}` );
		} );
	} );
} );
