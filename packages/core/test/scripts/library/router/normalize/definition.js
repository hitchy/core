import { describe, it, beforeEach, afterEach } from "mocha";
import "should";
import "should-http";
import SDT from "@hitchy/server-dev-tools";

import Core from "../../../../../sdt.js";

const Test = await SDT( Core );

const config = {
	projectFolder: "test/projects/empty",
};

describe( "Route definition normalizer", () => {
	const ctx = {};

	beforeEach( Test.before( ctx, { ...config } ) );
	afterEach( Test.after( ctx ) );

	it( "exposes function for normalizing route definitions of plugins", () => {
		ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin.should.be.Function();
	} );

	it( "exposes function for normalizing custom route definitions of any current application", () => {
		ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom.should.be.Function();
	} );
} );

describe( "Normalizer for module-related route definitions", () => {
	const ctx = {};

	beforeEach( Test.before( ctx, { ...config } ) );
	afterEach( Test.after( ctx ) );

	it( "does not throw on processing empty route definition", () => {
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin() ).should.not.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( null ) ).should.not.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( undefined ) ).should.not.throw();
	} );

	it( "throws on processing invalid route definition", () => {
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( false ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( true ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( 0 ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( 1 ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( -2 ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( "" ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( "0" ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( "/route" ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( () => {} ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( () => {} ) ).should.throw();
	} );

	it( "does not throw on processing valid definition without any element", () => {
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( {} ) ).should.not.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( [] ) ).should.not.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( new Map() ) ).should.not.throw();
	} );

	it( "provides object always covering either supported stage", () => {
		const a = ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( {} );

		a.should.be.Object();
		a.should.have.properties( "before", "after" ).and.have.size( 2 );
	} );

	it( "basically cares for wellformedness of provided route definitions", () => {
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( { null: null } ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( { false: false } ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( { true: true } ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( { undefined: undefined } ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( { emptyString: "" } ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( { string: "some value" } ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( { emptyArray: [] } ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( { array: ["some value"] } ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( { emptyObject: {} } ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( { object: { someValue: "some value" } } ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( { function: () => {} } ) ).should.throw();

		// test cases value due to least difference from cases tested above
		// @note THIS DOES NOT MEAN THESE CASES ARE VALID AT LAST, but passing
		//       simple tests included with normalization.
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( { "/null": null } ) ).should.not.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( { "/false": false } ) ).should.not.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( { "/true": true } ) ).should.not.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( { "/undefined": undefined } ) ).should.not.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( { "/emptyString": "" } ) ).should.not.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( { "/string": "some value" } ) ).should.not.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( { "/emptyArray": [] } ) ).should.not.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( { "/array": ["some value"] } ) ).should.not.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( { "/emptyObject": {} } ) ).should.not.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( { "/object": { someValue: "some value" } } ) ).should.not.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( { "/function": () => {} } ) ).should.not.throw();

		// using array for collecting route definitions
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( [null] ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( [false] ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( [true] ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( [undefined] ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( [""] ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( ["some value"] ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( [[]] ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( [["some value"]] ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( [{}] ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( [{ someValue: "some value" }] ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( [() => {}] ) ).should.throw();

		// test cases value due to least difference from cases tested above
		// @note THIS DOES NOT MEAN THESE CASES ARE VALID AT LAST, but passing
		//       simple tests included with normalization.
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( ["/"] ) ).should.not.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( ["/some value"] ) ).should.not.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( ["some /value"] ) ).should.not.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( [[ "/some", "value" ]] ) ).should.not.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( [{ "/someValue": "some value" }] ) ).should.throw();
	} );

	it( "accepts set of routes explicitly bound to before-stage", () => {
		const definition = {
			before: ["/ => anything"],
		};

		const normalized = ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( definition );

		normalized.should.not.equal( definition );

		normalized.should.be.Object().and.have.properties( "before", "after" ).and.have.size( 2 );
		normalized.before.should.be.instanceof( Map ).and.have.size( 1 );
		normalized.before.values().next().value.should.equal( "/ => anything" );
		normalized.after.should.be.instanceof( Map ).and.be.empty();
	} );

	it( "accepts set of routes explicitly bound to after-stage", () => {
		const definition = {
			after: ["/ => anything"],
		};

		const normalized = ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( definition );

		normalized.should.not.equal( definition );

		normalized.should.be.Object().and.have.properties( "before", "after" ).and.have.size( 2 );
		normalized.before.should.be.instanceof( Map ).and.be.empty();
		normalized.after.should.be.instanceof( Map ).and.have.size( 1 );
		normalized.after.values().next().value.should.equal( "/ => anything" );
	} );

	it( "accepts combined provision of sets of routes explicitly bound to before- and after-stage", () => {
		const definition = {
			before: ["/ => something"],
			after: ["/ => anything"],
		};

		const normalized = ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( definition );

		normalized.should.not.equal( definition );

		normalized.should.be.Object().and.have.properties( "before", "after" ).and.have.size( 2 );
		normalized.before.should.be.instanceof( Map ).and.have.size( 1 );
		normalized.before.values().next().value.should.equal( "/ => something" );
		normalized.after.should.be.instanceof( Map ).and.have.size( 1 );
		normalized.after.values().next().value.should.equal( "/ => anything" );
	} );

	it( "implicitly binds set of routes to before-stage", () => {
		let normalized = ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( {
			"/something": "something",
		} );

		normalized.should.be.Object().and.have.properties( "before", "after" );
		normalized.before.should.be.instanceof( Map ).and.have.size( 1 );
		normalized.before.values().next().value.should.equal( "something" );
		normalized.after.should.be.instanceof( Map ).and.be.empty();

		normalized = ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( [
			"/something => something",
		] );

		normalized.should.be.Object().and.have.properties( "before", "after" );
		normalized.before.should.be.instanceof( Map ).and.have.size( 1 );
		normalized.before.values().next().value.should.equal( "/something => something" );
		normalized.after.should.be.instanceof( Map ).and.be.empty();
	} );

	it( "rejects set of routes explicitly bound to unknown stage", () => {
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( {
			foo: [],
		} ) ).should.throw();

		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( {
			foo: ["/ => something"],
		} ) ).should.throw();
	} );

	it( "rejects definition combining sets bound to known stage with sets bound to unknown stage", () => {
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( {
			before: ["/ => something"],
			foo: [],
		} ) ).should.throw();

		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( {
			before: ["/ => something"],
			foo: ["/ => something"],
		} ) ).should.throw();

		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( {
			after: ["/ => something"],
			foo: [],
		} ) ).should.throw();

		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( {
			after: ["/ => something"],
			foo: ["/ => something"],
		} ) ).should.throw();

		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( {
			before: ["/ => something"],
			after: ["/ => something"],
			foo: [],
		} ) ).should.throw();

		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( {
			before: ["/ => something"],
			after: ["/ => something"],
			foo: ["/ => something"],
		} ) ).should.throw();
	} );

	it( "rejects explicitly bound sets of routes mixed with implicitly bound routes", () => {
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( {
			// explicit:
			before: ["/ => anything"],
			after: ["/ => nothing"],
			// implicit:
			"/something": "something",
		} ) ).should.throw();
	} );

	it( "rejects definition including stages basically known, but not supported for plugins", () => {
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( { before: ["/ => anything"] } ) ).should.not.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( { after: ["/ => anything"] } ) ).should.not.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( { early: ["/ => anything"] } ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( { late: ["/ => anything"] } ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( { early: ["/ => something"], late: ["/ => anything"] } ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( { before: ["/ => something"], early: ["/ => anything"] } ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( { before: ["/ => something"], late: ["/ => anything"] } ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( { after: ["/ => something"], early: ["/ => anything"] } ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( { after: ["/ => something"], late: ["/ => anything"] } ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( { before: ["/ => everything"], after: ["/ => something"], early: ["/ => anything"] } ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( { before: ["/ => everything"], after: ["/ => something"], late: ["/ => anything"] } ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Plugin( { before: ["/ => everything"], after: ["/ => something"], early: ["/ => anything"], late: ["/ => nothing"] } ) ).should.throw();
	} );
} );

describe( "Normalizer for application-related custom route definitions", () => {
	const ctx = {};

	beforeEach( Test.before( ctx, { ...config } ) );
	afterEach( Test.after( ctx ) );

	it( "does not throw on processing empty route definition", () => {
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom() ).should.not.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( null ) ).should.not.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( undefined ) ).should.not.throw();
	} );

	it( "throws on processing invalid route definition", () => {
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( false ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( true ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( 0 ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( 1 ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( -2 ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( "" ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( "0" ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( "/route" ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( () => {} ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( () => {} ) ).should.throw();
	} );

	it( "does not throw on processing valid definition without any element", () => {
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( {} ) ).should.not.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( [] ) ).should.not.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( new Map() ) ).should.not.throw();
	} );

	it( "provides object always covering either supported stage", () => {
		const a = ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( {} );

		a.should.be.Object();
		a.should.have.properties( "early", "before", "after", "late" ).and.have.size( 4 );
	} );

	it( "basically cares for wellformedness of provided route definitions", () => {
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( { null: null } ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( { false: false } ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( { true: true } ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( { undefined: undefined } ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( { emptyString: "" } ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( { string: "some value" } ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( { emptyArray: [] } ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( { array: ["some value"] } ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( { emptyObject: {} } ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( { object: { someValue: "some value" } } ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( { function: () => {} } ) ).should.throw();

		// test cases value due to least difference from cases tested above
		// @note THIS DOES NOT MEAN THESE CASES ARE VALID AT LAST, but passing
		//       simple tests included with normalization.
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( { "/null": null } ) ).should.not.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( { "/false": false } ) ).should.not.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( { "/true": true } ) ).should.not.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( { "/undefined": undefined } ) ).should.not.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( { "/emptyString": "" } ) ).should.not.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( { "/string": "some value" } ) ).should.not.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( { "/emptyArray": [] } ) ).should.not.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( { "/array": ["some value"] } ) ).should.not.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( { "/emptyObject": {} } ) ).should.not.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( { "/object": { someValue: "some value" } } ) ).should.not.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( { "/function": () => {} } ) ).should.not.throw();

		// using array for collecting route definitions
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( [null] ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( [false] ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( [true] ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( [undefined] ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( [""] ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( ["some value"] ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( [[]] ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( [["some value"]] ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( [{}] ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( [{ someValue: "some value" }] ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( [() => {}] ) ).should.throw();

		// test cases value due to least difference from cases tested above
		// @note THIS DOES NOT MEAN THESE CASES ARE VALID AT LAST, but passing
		//       simple tests included with normalization.
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( ["/"] ) ).should.not.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( ["/some value"] ) ).should.not.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( ["some /value"] ) ).should.not.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( [[ "/some", "value" ]] ) ).should.not.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( [{ "/someValue": "some value" }] ) ).should.throw();
	} );

	it( "accepts set of routes explicitly bound to early-stage", () => {
		const definition = {
			early: ["/ => anything"],
		};

		const normalized = ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( definition );

		normalized.should.not.equal( definition );

		normalized.should.be.Object().and.have.properties( "early", "before", "after", "late" ).and.have.size( 4 );
		normalized.early.should.be.instanceof( Map ).and.have.size( 1 );
		normalized.early.values().next().value.should.equal( "/ => anything" );
		normalized.before.should.be.instanceof( Map ).and.be.empty();
		normalized.after.should.be.instanceof( Map ).and.be.empty();
		normalized.late.should.be.instanceof( Map ).and.be.empty();
	} );

	it( "accepts set of routes explicitly bound to before-stage", () => {
		const definition = {
			before: ["/ => anything"],
		};

		const normalized = ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( definition );

		normalized.should.not.equal( definition );

		normalized.should.be.Object().and.have.properties( "early", "before", "after", "late" ).and.have.size( 4 );
		normalized.early.should.be.instanceof( Map ).and.be.empty();
		normalized.before.should.be.instanceof( Map ).and.have.size( 1 );
		normalized.before.values().next().value.should.equal( "/ => anything" );
		normalized.after.should.be.instanceof( Map ).and.be.empty();
		normalized.late.should.be.instanceof( Map ).and.be.empty();
	} );

	it( "accepts set of routes explicitly bound to after-stage", () => {
		const definition = {
			after: ["/ => anything"],
		};

		const normalized = ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( definition );

		normalized.should.not.equal( definition );

		normalized.should.be.Object().and.have.properties( "early", "before", "after", "late" ).and.have.size( 4 );
		normalized.early.should.be.instanceof( Map ).and.be.empty();
		normalized.before.should.be.instanceof( Map ).and.be.empty();
		normalized.after.should.be.instanceof( Map ).and.have.size( 1 );
		normalized.after.values().next().value.should.equal( "/ => anything" );
		normalized.late.should.be.instanceof( Map ).and.be.empty();
	} );

	it( "accepts set of routes explicitly bound to late-stage", () => {
		const definition = {
			late: ["/ => anything"],
		};

		const normalized = ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( definition );

		normalized.should.not.equal( definition );

		normalized.should.be.Object().and.have.properties( "early", "before", "after", "late" ).and.have.size( 4 );
		normalized.early.should.be.instanceof( Map ).and.be.empty();
		normalized.before.should.be.instanceof( Map ).and.be.empty();
		normalized.after.should.be.instanceof( Map ).and.be.empty();
		normalized.late.should.be.instanceof( Map ).and.have.size( 1 );
		normalized.late.values().next().value.should.equal( "/ => anything" );
	} );

	it( "accepts combined provision of sets of routes explicitly bound to early-, before-, after- and late-stage", () => {
		const definition = {
			early: ["/ => everything"],
			before: ["/ => something"],
			after: ["/ => anything"],
			late: ["/ => nothing"],
		};

		const normalized = ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( definition );

		normalized.should.not.equal( definition );

		normalized.should.be.Object().and.have.properties( "early", "before", "after", "late" ).and.have.size( 4 );
		normalized.early.should.be.instanceof( Map ).and.have.size( 1 );
		normalized.early.values().next().value.should.equal( "/ => everything" );
		normalized.before.should.be.instanceof( Map ).and.have.size( 1 );
		normalized.before.values().next().value.should.equal( "/ => something" );
		normalized.after.should.be.instanceof( Map ).and.have.size( 1 );
		normalized.after.values().next().value.should.equal( "/ => anything" );
		normalized.late.should.be.instanceof( Map ).and.have.size( 1 );
		normalized.late.values().next().value.should.equal( "/ => nothing" );
	} );

	it( "implicitly binds set of routes to before-stage", () => {
		let normalized = ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( {
			"/something": "something",
		} );

		normalized.should.be.Object().and.have.properties( "before", "after" );
		normalized.before.should.be.instanceof( Map ).and.have.size( 1 );
		normalized.before.values().next().value.should.equal( "something" );
		normalized.after.should.be.instanceof( Map ).and.be.empty();

		normalized = ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( [
			"/something => something",
		] );

		normalized.should.be.Object().and.have.properties( "before", "after" );
		normalized.before.should.be.instanceof( Map ).and.have.size( 1 );
		normalized.before.values().next().value.should.equal( "/something => something" );
		normalized.after.should.be.instanceof( Map ).and.be.empty();
	} );

	it( "rejects set of routes explicitly bound to unknown stage", () => {
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( {
			foo: [],
		} ) ).should.throw();

		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( {
			foo: ["/ => something"],
		} ) ).should.throw();
	} );

	it( "rejects definition combining sets bound to known stage with sets bound to unknown stage", () => {
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( {
			early: ["/ => something"],
			foo: [],
		} ) ).should.throw();

		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( {
			early: ["/ => something"],
			foo: ["/ => something"],
		} ) ).should.throw();

		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( {
			before: ["/ => something"],
			foo: [],
		} ) ).should.throw();

		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( {
			before: ["/ => something"],
			foo: ["/ => something"],
		} ) ).should.throw();

		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( {
			after: ["/ => something"],
			foo: [],
		} ) ).should.throw();

		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( {
			after: ["/ => something"],
			foo: ["/ => something"],
		} ) ).should.throw();

		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( {
			late: ["/ => something"],
			foo: [],
		} ) ).should.throw();

		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( {
			late: ["/ => something"],
			foo: ["/ => something"],
		} ) ).should.throw();

		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( {
			early: ["/ => something"],
			before: ["/ => something"],
			after: ["/ => something"],
			late: ["/ => something"],
			foo: [],
		} ) ).should.throw();

		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( {
			early: ["/ => something"],
			before: ["/ => something"],
			after: ["/ => something"],
			late: ["/ => something"],
			foo: ["/ => something"],
		} ) ).should.throw();
	} );

	it( "rejects explicitly bound sets of routes mixed with implicitly bound routes", () => {
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Custom( {
			// explicit:
			early: ["/ => anything"],
			before: ["/ => anything"],
			after: ["/ => nothing"],
			late: ["/ => anything"],
			// implicit:
			"/something": "something",
		} ) ).should.throw();
	} );
} );

describe( "Normalizer for blueprint route definitions", () => {
	const ctx = {};

	beforeEach( Test.before( ctx, { ...config } ) );
	afterEach( Test.after( ctx ) );

	it( "does not throw on processing empty route definition", () => {
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint() ).should.not.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( null ) ).should.not.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( undefined ) ).should.not.throw();
	} );

	it( "throws on processing invalid route definition", () => {
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( false ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( true ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( 0 ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( 1 ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( -2 ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( "" ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( "0" ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( "/route" ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( () => {} ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( () => {} ) ).should.throw();
	} );

	it( "does not throw on processing valid definition without any element", () => {
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( {} ) ).should.not.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( [] ) ).should.not.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( new Map() ) ).should.not.throw();
	} );

	it( "does not provide wrapping object always covering either supported stage", () => {
		const a = ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( {} );

		a.should.be.instanceof( Map );
		a.should.not.have.properties( "early", "before", "after", "late" ).and.be.empty();
	} );

	it( "basically cares for wellformedness of provided route definitions", () => {
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( { null: null } ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( { false: false } ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( { true: true } ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( { undefined: undefined } ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( { emptyString: "" } ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( { string: "some value" } ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( { emptyArray: [] } ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( { array: ["some value"] } ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( { emptyObject: {} } ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( { object: { someValue: "some value" } } ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( { function: () => {} } ) ).should.throw();

		// test cases value due to least difference from cases tested above
		// @note THIS DOES NOT MEAN THESE CASES ARE VALID AT LAST, but passing
		//       simple tests included with normalization.
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( { "/null": null } ) ).should.not.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( { "/false": false } ) ).should.not.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( { "/true": true } ) ).should.not.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( { "/undefined": undefined } ) ).should.not.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( { "/emptyString": "" } ) ).should.not.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( { "/string": "some value" } ) ).should.not.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( { "/emptyArray": [] } ) ).should.not.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( { "/array": ["some value"] } ) ).should.not.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( { "/emptyObject": {} } ) ).should.not.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( { "/object": { someValue: "some value" } } ) ).should.not.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( { "/function": () => {} } ) ).should.not.throw();

		// using array for collecting route definitions
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( [null] ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( [false] ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( [true] ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( [undefined] ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( [""] ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( ["some value"] ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( [[]] ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( [["some value"]] ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( [{}] ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( [{ someValue: "some value" }] ) ).should.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( [() => {}] ) ).should.throw();

		// test cases value due to least difference from cases tested above
		// @note THIS DOES NOT MEAN THESE CASES ARE VALID AT LAST, but passing
		//       simple tests included with normalization.
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( ["/"] ) ).should.not.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( ["/some value"] ) ).should.not.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( ["some /value"] ) ).should.not.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( [[ "/some", "value" ]] ) ).should.not.throw();
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( [{ "/someValue": "some value" }] ) ).should.throw();
	} );

	it( "rejects set of routes explicitly bound to early-stage due to not supporting any staging", () => {
		const definition = {
			early: ["/ => anything"],
		};

		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( definition ) ).should.throw();
	} );

	it( "rejects set of routes explicitly bound to before-stage due to not supporting any staging", () => {
		const definition = {
			before: ["/ => anything"],
		};

		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( definition ) ).should.throw();
	} );

	it( "rejects set of routes explicitly bound to after-stage due to not supporting any staging", () => {
		const definition = {
			after: ["/ => anything"],
		};

		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( definition ) ).should.throw();
	} );

	it( "rejects set of routes explicitly bound to late-stage due to not supporting any staging", () => {
		const definition = {
			late: ["/ => anything"],
		};

		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( definition ) ).should.throw();
	} );

	it( "rejects combined provision of sets of routes explicitly bound to early-, before-, after- and late-stage due to not supporting any staging", () => {
		const definition = {
			early: ["/ => everything"],
			before: ["/ => something"],
			after: ["/ => anything"],
			late: ["/ => nothing"],
		};

		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( definition ) ).should.throw();
	} );

	it( "accepts set of routes not bound to any stage due to not supporting any staging", () => {
		let normalized = ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( {
			"/something": "something",
		} );

		normalized.should.be.instanceof( Map ).and.have.size( 1 ).and.not.have.properties( "before" );
		normalized.values().next().value.should.equal( "something" );

		normalized = ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( [
			"/something => something",
		] );

		normalized.should.be.instanceof( Map ).and.have.size( 1 ).and.not.have.properties( "before" );
		normalized.values().next().value.should.equal( "/something => something" );
	} );

	it( "rejects set of routes explicitly bound to unknown stage due to not supporting any staging", () => {
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( {
			foo: [],
		} ) ).should.throw();

		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( {
			foo: ["/ => something"],
		} ) ).should.throw();
	} );

	it( "rejects definition combining sets bound to known stage with sets bound to unknown stage due to not supporting any staging", () => {
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( {
			early: ["/ => something"],
			foo: [],
		} ) ).should.throw();

		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( {
			early: ["/ => something"],
			foo: ["/ => something"],
		} ) ).should.throw();

		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( {
			before: ["/ => something"],
			foo: [],
		} ) ).should.throw();

		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( {
			before: ["/ => something"],
			foo: ["/ => something"],
		} ) ).should.throw();

		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( {
			after: ["/ => something"],
			foo: [],
		} ) ).should.throw();

		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( {
			after: ["/ => something"],
			foo: ["/ => something"],
		} ) ).should.throw();

		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( {
			late: ["/ => something"],
			foo: [],
		} ) ).should.throw();

		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( {
			late: ["/ => something"],
			foo: ["/ => something"],
		} ) ).should.throw();

		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( {
			early: ["/ => something"],
			before: ["/ => something"],
			after: ["/ => something"],
			late: ["/ => something"],
			foo: [],
		} ) ).should.throw();

		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( {
			early: ["/ => something"],
			before: ["/ => something"],
			after: ["/ => something"],
			late: ["/ => something"],
			foo: ["/ => something"],
		} ) ).should.throw();
	} );

	it( "rejects explicitly bound sets of routes mixed with implicitly bound routes due to not supporting any staging", () => {
		( () => ctx.hitchy.api.service.RouteDefinitionNormalizer.Blueprint( {
			// explicit:
			early: ["/ => anything"],
			before: ["/ => anything"],
			after: ["/ => nothing"],
			late: ["/ => anything"],
			// implicit:
			"/something": "something",
		} ) ).should.throw();
	} );
} );
