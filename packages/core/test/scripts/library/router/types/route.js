import { describe, it, beforeEach, afterEach } from "mocha";
import "should";
import "should-http";
import SDT from "@hitchy/server-dev-tools";

import Core from "../../../../../sdt.js";

const Test = await SDT( Core );

const config = {
	projectFolder: "test/projects/empty",
	options: {
		// debug: true,
	},
};

describe( "Route services", () => {
	const ctx = {};

	afterEach( Test.after( ctx ) );
	beforeEach( Test.before( ctx, { ...config } ) );

	it( "are exposed properly", () => {
		const { Route, PolicyRoute, ControllerRoute } = ctx.hitchy.api.service;

		Route.should.be.ok();
		PolicyRoute.should.be.ok();
		ControllerRoute.should.be.ok();
	} );
} );

describe( "Route service", () => {
	const ctx = {};

	afterEach( Test.after( ctx ) );
	beforeEach( Test.before( ctx, { ...config } ) );

	it( "exists", () => {
		const { Route } = ctx.hitchy.api.service;

		Route.should.be.ok().and.should.be.Object();
	} );

	it( "can be instantiated", () => {
		const { Route } = ctx.hitchy.api.service;

		( () => { new Route( "/", () => {} ); } ).should.not.throw();
	} );

	it( "provides static method for parsing routing source definitions", () => {
		const { Route } = ctx.hitchy.api.service;

		Route.parseSource.should.be.Function();
	} );

	it( "provides static method for parsing routing target definitions", () => {
		const { Route } = ctx.hitchy.api.service;

		Route.parseTarget.should.be.Function();
	} );

	it( "exposes pattern for matching and normalizing names of components containing route targets", () => {
		const { Route } = ctx.hitchy.api.service;

		Route.tailPattern.should.be.instanceof( RegExp );

		"SomeCustomController".replace( Route.tailPattern, "" ).should.equal( "SomeCustom" );
		"SomeCustomPolicy".replace( Route.tailPattern, "" ).should.equal( "SomeCustomPolicy" );
	} );

	it( "exposes singular name of collection to contain code addressable by routes", () => {
		const { Route } = ctx.hitchy.api.service;

		Route.collectionSingularName.should.be.String();
		Route.collectionSingularName.should.be.empty();
	} );

	it( "exposes plural name of collection to contain code addressable by routes", () => {
		const { Route } = ctx.hitchy.api.service;

		Route.collectionPluralName.should.be.String();
		Route.collectionPluralName.should.be.empty();
	} );
} );

describe( "PolicyRoute service", () => {
	const ctx = {};

	afterEach( Test.after( ctx ) );
	beforeEach( Test.before( ctx, { ...config } ) );

	it( "can be instantiated", () => {
		const { PolicyRoute } = ctx.hitchy.api.service;

		( () => { new PolicyRoute( "/", () => {} ); } ).should.not.throw();
	} );

	it( "is class inheriting from Route", () => {
		const { Route, PolicyRoute } = ctx.hitchy.api.service;

		const route = new PolicyRoute( "/", () => {} );
		route.should.be.instanceof( PolicyRoute );
		route.should.be.instanceof( Route );
	} );

	it( "does not use custom parser function", () => {
		const { Route, PolicyRoute } = ctx.hitchy.api.service;

		PolicyRoute.parseSource.should.be.equal( Route.parseSource );
		PolicyRoute.parseTarget.should.be.equal( Route.parseTarget );
	} );

	it( "exposes pattern for matching and normalizing names of components containing route targets", () => {
		const { PolicyRoute } = ctx.hitchy.api.service;

		PolicyRoute.tailPattern.should.be.instanceof( RegExp );

		"SomeCustomController".replace( PolicyRoute.tailPattern, "" ).should.equal( "SomeCustomController" );
		"SomeCustomPolicy".replace( PolicyRoute.tailPattern, "" ).should.equal( "SomeCustom" );
	} );

	it( "exposes singular name of collection to contain code addressable by routes", () => {
		const { PolicyRoute } = ctx.hitchy.api.service;

		PolicyRoute.collectionSingularName.should.be.String();
		PolicyRoute.collectionSingularName.should.equal( "policy" );
	} );

	it( "exposes plural name of collection to contain code addressable by routes", () => {
		const { PolicyRoute } = ctx.hitchy.api.service;

		PolicyRoute.collectionPluralName.should.be.String();
		PolicyRoute.collectionPluralName.should.equal( "policies" );
	} );
} );

describe( "ControllerRoute service", () => {
	const ctx = {};

	afterEach( Test.after( ctx ) );
	beforeEach( Test.before( ctx, { ...config } ) );

	it( "can be instantiated", () => {
		const { ControllerRoute } = ctx.hitchy.api.service;

		( () => { new ControllerRoute( "/", () => {} ); } ).should.not.throw();
	} );

	it( "is class inheriting from Route", () => {
		const { Route, ControllerRoute } = ctx.hitchy.api.service;

		const route = new ControllerRoute( "/", () => {} );
		route.should.be.instanceof( ControllerRoute );
		route.should.be.instanceof( Route );
	} );

	it( "does not use custom parser function", () => {
		const { Route, ControllerRoute } = ctx.hitchy.api.service;

		ControllerRoute.parseSource.should.be.equal( Route.parseSource );
		ControllerRoute.parseTarget.should.be.equal( Route.parseTarget );
	} );

	it( "exposes pattern for matching and normalizing names of components containing route targets", () => {
		const { ControllerRoute } = ctx.hitchy.api.service;

		ControllerRoute.tailPattern.should.be.instanceof( RegExp );

		"SomeCustomController".replace( ControllerRoute.tailPattern, "" ).should.equal( "SomeCustom" );
		"SomeCustomPolicy".replace( ControllerRoute.tailPattern, "" ).should.equal( "SomeCustomPolicy" );
	} );

	it( "exposes singular name of collection to contain code addressable by routes", () => {
		const { ControllerRoute } = ctx.hitchy.api.service;

		ControllerRoute.collectionSingularName.should.be.String();
		ControllerRoute.collectionSingularName.should.equal( "controller" );
	} );

	it( "exposes plural name of collection to contain code addressable by routes", () => {
		const { ControllerRoute } = ctx.hitchy.api.service;

		ControllerRoute.collectionPluralName.should.be.String();
		ControllerRoute.collectionPluralName.should.equal( "controllers" );
	} );
} );

describe( "Route service's parseSource() method", () => {
	const ctx = {};

	afterEach( Test.after( ctx ) );
	beforeEach( Test.before( ctx, { ...config } ) );

	it( "rejects invalid types of values for defining source of routing", () => {
		const { Route } = ctx.hitchy.api.service;

		( () => Route.parseSource() ).should.throw();
		( () => Route.parseSource( null ) ).should.throw();
		( () => Route.parseSource( undefined ) ).should.throw();
		( () => Route.parseSource( false ) ).should.throw();
		( () => Route.parseSource( true ) ).should.throw();
		( () => Route.parseSource( 1.0 ) ).should.throw();
		( () => Route.parseSource( -0.0 ) ).should.throw();
		( () => Route.parseSource( [] ) ).should.throw();
		( () => Route.parseSource( [ "GET", "/" ] ) ).should.throw();
	} );

	it( "accepts well-formed strings for defining source of routing", () => {
		const { Route } = ctx.hitchy.api.service;

		( () => Route.parseSource( "" ) ).should.throw();

		( () => Route.parseSource( "/" ) ).should.not.throw();
		( () => Route.parseSource( "GET /" ) ).should.not.throw();
	} );

	it( "accepts objects containing certaing properties for defining source of routing", () => {
		const { Route } = ctx.hitchy.api.service;

		( () => Route.parseSource( {} ) ).should.throw();
		( () => Route.parseSource( { method: "GET", path: "/" } ) ).should.throw();

		( () => Route.parseSource( { url: "/" } ) ).should.not.throw();
		( () => Route.parseSource( { type: "GET", url: "/" } ) ).should.not.throw();
		( () => Route.parseSource( { type: "GET", url: "/", module: "MyController", method: "listItems" } ) ).should.not.throw();
	} );

	it( "provides all information on parsed source of defined route", () => {
		const { Route } = ctx.hitchy.api.service;

		const source = Route.parseSource( "/" );

		source.should.be.ok();
		source.should.have.keys( "method", "definition", "prefix", "pattern", "parameters", "render", "matcher" ).and.have.size( 7 );

		source.method.should.be.String();
		source.prefix.should.be.String();
		source.pattern.should.be.instanceof( RegExp );
		source.parameters.should.be.Array();
		source.render.should.be.Function();
	} );

	it( "provides uppercase name of HTTP method route is bound to", () => {
		const { Route } = ctx.hitchy.api.service;

		const tests = {
			"/": "ALL",
			"get /": "GET",
			"GET /": "GET",
			"put /": "PUT",
			"puT /": "PUT",
			"anyThing /": "ANYTHING",
			"ANYTHING /": "ANYTHING",
			"* /": "ALL",
		};

		Object.keys( tests ).forEach( source => {
			const route = Route.parseSource( source );
			route.method.should.equal( tests[source] );
		} );
	} );

	it( "maps defined HTTP method * to ALL", () => {
		const { Route } = ctx.hitchy.api.service;

		const tests = {
			"* /": "ALL",
			"all /": "ALL",
			"ALL /": "ALL",
			"ALLe /": "ALLE",
			"any /": "ANY",
		};

		Object.keys( tests ).forEach( source => {
			const route = Route.parseSource( source );
			route.method.should.equal( tests[source] );
		} );
	} );

	it( "rejects definitions declaring invalid HTTP method", () => {
		const { Route } = ctx.hitchy.api.service;

		const tests = [ "+", "-", ".", "1", "23", "A1", "ANY_TYPE", "ANY-TYPE", "-LESS", "EXTRALONGMETHODNAME" ];

		tests.forEach( method => {
			Route.parseSource.bind( Route, method + " /" ).should.throw();
		} );
	} );

	it( "accepts non-empty paths consisting of static segments, only", () => {
		const { Route } = ctx.hitchy.api.service;

		const paths = [
			"/", "/test", "/test/", "/test/more", "/a", "/a/b", "/A/b/C", "/.foo-with/_special/char-acter-s",
		];

		paths.forEach( path => ( () => Route.parseSource( path ) ).should.not.throw() );
	} );

	it( "accepts non-empty paths consisting of optional static segments, only", () => {
		const { Route } = ctx.hitchy.api.service;

		const paths = [
			"{/}", "{/test}", "{/test}{/}", "{/test}{/more}", "{/a}", "{/a}{/b}", "{/A}{/b}{/C}", "{/.foo-with}{/_special}{/char-acter-s}",
		];

		paths.forEach( path => ( () => Route.parseSource( path ) ).should.not.throw() );
	} );

	it( "accepts paths with partially optional segments", () => {
		const { Route } = ctx.hitchy.api.service;

		const paths = [
			"/t{es}t",
		];

		paths.forEach( path => ( () => Route.parseSource( path ) ).should.not.throw() );
	} );

	it( "accepts paths solely consisting of a mandatory parameters", () => {
		const { Route } = ctx.hitchy.api.service;

		const paths = [
			"/:sole", "/:sole/:sole-too"
		];

		paths.forEach( path => ( () => Route.parseSource( path ) ).should.not.throw() );
	} );

	it( "accepts non-empty fixed-size paths combining static segments with parameters in leading, inner and tailing position", () => {
		const { Route } = ctx.hitchy.api.service;

		const paths = [
			"/prefix/:tail", "/prefix/:inner/suffix", "/:lead/suffix",
			"/prefix/:tail/:tail-too", "/prefix/:inner/:inner-too/suffix", "/:lead/:lead-too/suffix",
		];

		paths.forEach( path => ( () => Route.parseSource( path ) ).should.not.throw() );
	} );

	it( "accepts paths solely consisting of optional parameters", () => {
		const { Route } = ctx.hitchy.api.service;

		const paths = [
			"{/:sole}",
			"{/:first}{/:second}",
			"{/:first}{/:second}{/:third}",
		];

		paths.forEach( path => ( () => Route.parseSource( path ) ).should.not.throw() );
	} );

	it( "accepts non-empty paths combining static segments with optional parameters in leading, inner and tailing position", () => {
		const { Route } = ctx.hitchy.api.service;

		const paths = [
			"/prefix{/:tail}", "/prefix{/:inner}/suffix", "{/:lead}/suffix",
			"/prefix{/:tail}{/:tail-too}", "/prefix{/:inner}{/:inner-too}/suffix", "{/:lead}{/:lead-too}/suffix",
		];

		paths.forEach( path => ( () => Route.parseSource( path ) ).should.not.throw() );
	} );

	it( "accepts paths solely consisting of repeated parameters", () => {
		const { Route } = ctx.hitchy.api.service;

		const paths = [
			"/*sole",
			"/*sole/*sole-too",
		];

		paths.forEach( path => ( () => Route.parseSource( path ) ).should.not.throw() );
	} );

	it( "accepts paths combining static segments with repeated parameters in leading, inner and tailing position", () => {
		const { Route } = ctx.hitchy.api.service;

		const paths = [
			"/prefix/*tail", "/prefix/*inner/suffix", "/*lead/suffix",
			"/prefix/*tail/*tail-too", "/prefix/*inner/*inner-too/suffix", "/*lead/*lead-too/suffix",
		];

		paths.forEach( path => ( () => Route.parseSource( path ) ).should.not.throw() );
	} );

	it( "accepts paths solely consisting of optionally repeated parameters", () => {
		const { Route } = ctx.hitchy.api.service;

		const paths = [
			"{/*sole}",
			"{/*sole}{/*sole-too}",
		];

		paths.forEach( path => ( () => Route.parseSource( path ) ).should.not.throw() );
	} );

	it( "accepts paths combining static segments with optionally repeated parameters in leading, inner and tailing position", () => {
		const { Route } = ctx.hitchy.api.service;

		const paths = [
			"/prefix{/*tail}", "/prefix{/*inner}/suffix", "{/*lead}/suffix",
			"/prefix{/*tail}{/*tail-too}", "/prefix{/*inner}{/*inner-too}/suffix", "{/*lead}{/*lead-too}/suffix",
		];

		paths.forEach( path => ( () => Route.parseSource( path ) ).should.not.throw() );
	} );

	it( "accepts paths with all kinds of typed parameters", () => {
		const { Route } = ctx.hitchy.api.service;

		const paths = [
			"{/:sole.uuid}",
			"{/:first.integer}{/:second.string}",
			"{/*first.user}{/*second.ENUM}/*third.fOo",
		];

		paths.forEach( path => ( () => Route.parseSource( path ) ).should.not.throw() );
	} );

	it( "rejects empty paths", () => {
		const { Route } = ctx.hitchy.api.service;

		const paths = [ "", "    " ];

		paths.forEach( path => ( () => Route.parseSource( path ) ).should.throw() );
	} );

	it( "rejects relative paths starting with a period", () => {
		const { Route } = ctx.hitchy.api.service;

		const paths = [
			".", "..", "./", "../", "./test", ".//",
		];

		paths.forEach( path => Route.parseSource.bind( Route, path ).should.throw() );
	} );

	it( "rejects relative paths starting with anything but a slash expect for leading optional segments", () => {
		const { Route } = ctx.hitchy.api.service;

		const paths = [
			"test", ":", "("
		];

		paths.forEach( path => Route.parseSource.bind( Route, path ).should.throw() );

		[ "{/foo}", "{/:foo}" ].forEach( path => Route.parseSource.bind( Route, path ).should.not.throw() );
	} );

	it( "rejects absolute paths starting with multiple slashes", () => {
		const { Route } = ctx.hitchy.api.service;

		const paths = [
			"//", "///", "////", "//more", "///more"
		];

		paths.forEach( path => Route.parseSource.bind( Route, path ).should.throw() );
	} );

	it( "extract static prefix from route's declared path", () => {
		const { Route } = ctx.hitchy.api.service;

		Route.parseSource( "/" ).prefix.should.equal( "/" );
		Route.parseSource( "/test" ).prefix.should.equal( "/test" );
		Route.parseSource( "/test/" ).prefix.should.equal( "/test" );
		Route.parseSource( "/test/more" ).prefix.should.equal( "/test/more" );
		Route.parseSource( "/test/more/" ).prefix.should.equal( "/test/more" );

		Route.parseSource( "/test/more/:name" ).prefix.should.equal( "/test/more" );
		Route.parseSource( "/test/more/*name" ).prefix.should.equal( "/test/more" );
		Route.parseSource( "/test/more{/:name}" ).prefix.should.equal( "/test/more" );
		Route.parseSource( "/test/more{/*name}" ).prefix.should.equal( "/test/more" );

		Route.parseSource( `/test/more/:"name"` ).prefix.should.equal( "/test/more" );
		Route.parseSource( `/test/more/:"name.uuid"` ).prefix.should.equal( "/test/more" );
	} );

	it( "extracts ordered list of parameter names used in path declaration", () => {
		const { Route } = ctx.hitchy.api.service;

		Route.parseSource( "/" ).parameters.should.have.length( 0 );
		Route.parseSource( "/test" ).parameters.should.have.length( 0 );
		Route.parseSource( "/test/" ).parameters.should.have.length( 0 );
		Route.parseSource( "/test/more" ).parameters.should.have.length( 0 );

		let parameters = Route.parseSource( "/test/more/:var" ).parameters;
		parameters.should.deepEqual( [{ type: "param", name: "var" }] );

		parameters = Route.parseSource( "/test/:more/extra\\-:var" ).parameters;
		parameters.should.deepEqual( [
			{ type: "param", name: "more" },
			{ type: "param", name: "var" },
		] );
	} );

	it( "handles typed parameters declared in path", () => {
		const { Route } = ctx.hitchy.api.service;

		Route.parseSource( '/test/more/:"var.uUId"' ).parameters
			.should.deepEqual( [{
				name: "var",
				type: "param",
				source: "var.uUId",
				valueType: "uuid",
			}] );
	} );

	it( "does not recognize declaration of a parameter's type as such when omitting the wrapping quotes", () => {
		const { Route } = ctx.hitchy.api.service;

		Route.parseSource( "/test/more/:var.uUId" ).parameters
			.should.deepEqual( [{
				name: "var",
				type: "param",
			}] );
	} );
} );

describe( "Route service's parseTarget() method", () => {
	const ctx = {};

	afterEach( Test.after( ctx ) );

	it( "rejects invalid types of values for defining target of routing", async() => {
		await Test.before( ctx, { ...config } )();
		const { Route } = ctx.hitchy.api.service;

		( () => Route.parseTarget( null ) ).should.throw();
		( () => Route.parseTarget( undefined ) ).should.throw();
		( () => Route.parseTarget( false ) ).should.throw();
		( () => Route.parseTarget( true ) ).should.throw();
		( () => Route.parseTarget( 1.0 ) ).should.throw();
		( () => Route.parseTarget( -0.0 ) ).should.throw();
		( () => Route.parseTarget( {} ) ).should.throw();
		( () => Route.parseTarget( "" ) ).should.throw();
	} );

	it( "provides an implicit handler provided as target of a generic route", async() => {
		await Test.before( ctx, { ...config } )();
		const { Route } = ctx.hitchy.api.service;
		const fn = () => {};

		const target = Route.parseTarget( fn );
		target.handler.should.equal( fn );
		( target.warning == null ).should.be.true();
	} );

	it( "accepts generic route target definitions with a warning instead of a handler", async() => {
		await Test.before( ctx, { ...config } )();
		const { Route } = ctx.hitchy.api.service;

		let target = Route.parseTarget( "Filter::myImplementation" );
		( target.handler == null ).should.be.true();
		target.warning.should.be.String().which.is.not.empty();

		target = Route.parseTarget( "Filter.myImplementation" );
		( target.handler == null ).should.be.true();
		target.warning.should.be.String().which.is.not.empty();

		target = Route.parseTarget( { controller: "Filter", method: "myImplementation" } );
		( target.handler == null ).should.be.true();
		target.warning.should.be.String().which.is.not.empty();

		target = Route.parseTarget( "Controller::deferredMirror" );
		( target.handler == null ).should.be.true();
		target.warning.should.be.String().which.is.not.empty();

		target = Route.parseTarget( "Controller.deferredMirror" );
		( target.handler == null ).should.be.true();
		target.warning.should.be.String().which.is.not.empty();

		target = Route.parseTarget( { controller: "Controller", method: "deferredMirror" } );
		( target.handler == null ).should.be.true();
		target.warning.should.be.String().which.is.not.empty();
	} );

	it( "rejects multiple generic route target definitions provided as array", async() => {
		await Test.before( ctx, { ...config } )();
		const { Route } = ctx.hitchy.api.service;

		( () => Route.parseTarget( [] ) ).should.throw();
		( () => Route.parseTarget( [() => {}] ) ).should.throw();
		( () => Route.parseTarget( ["Filter::myImplementation"] ) ).should.throw();
		( () => Route.parseTarget( ["Filter.myImplementation"] ) ).should.throw();
		( () => Route.parseTarget( [ "Filter", "myImplementation" ] ) ).should.throw();
		( () => Route.parseTarget( [{ controller: "Filter", method: "myImplementation" }] ) ).should.throw();
		( () => Route.parseTarget( ["Controller::deferredMirror"] ) ).should.throw();
		( () => Route.parseTarget( ["Controller.deferredMirror"] ) ).should.throw();
		( () => Route.parseTarget( [ "Controller", "deferredMirror" ] ) ).should.throw();
		( () => Route.parseTarget( [{ controller: "Controller", method: "deferredMirror" }] ) ).should.throw();
	} );


	it( "rejects invalid types of values for defining target of policy routing", async() => {
		await Test.before( ctx, { ...config } )();
		const { PolicyRoute } = ctx.hitchy.api.service;

		( () => PolicyRoute.parseTarget( null ) ).should.throw();
		( () => PolicyRoute.parseTarget( undefined ) ).should.throw();
		( () => PolicyRoute.parseTarget( false ) ).should.throw();
		( () => PolicyRoute.parseTarget( true ) ).should.throw();
		( () => PolicyRoute.parseTarget( 1.0 ) ).should.throw();
		( () => PolicyRoute.parseTarget( -0.0 ) ).should.throw();
		( () => PolicyRoute.parseTarget( {} ) ).should.throw();
		( () => PolicyRoute.parseTarget( "" ) ).should.throw();
	} );

	it( "accepts valid policy target definitions", async() => {
		await Test.before( ctx, { ...config } )();
		const { PolicyRoute } = ctx.hitchy.api.service;

		let target = PolicyRoute.parseTarget( () => {} );
		target.should.be.ok();

		target = PolicyRoute.parseTarget( "Filter::myImplementation" );
		target.should.be.ok();

		target = PolicyRoute.parseTarget( "Filter.myImplementation" );
		target.should.be.ok();

		target = PolicyRoute.parseTarget( { controller: "Filter", method: "myImplementation" } );
		target.should.be.ok();

		target = PolicyRoute.parseTarget( "Controller::deferredMirror" );
		target.should.be.ok();

		target = PolicyRoute.parseTarget( "Controller.deferredMirror" );
		target.should.be.ok();

		target = PolicyRoute.parseTarget( { controller: "Controller", method: "deferredMirror" } );
		target.should.be.ok();
	} );

	it( "provides a warning instead of a handler on valid target definitions addressing missing policy function", async() => {
		await Test.before( ctx, { ...config } )();
		const { PolicyRoute } = ctx.hitchy.api.service;

		let target = PolicyRoute.parseTarget( "Filter::myImplementation" );
		( target.handler == null ).should.be.true();
		target.warning.should.be.String().which.is.not.empty();

		target = PolicyRoute.parseTarget( "Filter.myImplementation" );
		( target.handler == null ).should.be.true();
		target.warning.should.be.String().which.is.not.empty();

		target = PolicyRoute.parseTarget( { controller: "Filter", method: "myImplementation" } );
		( target.handler == null ).should.be.true();
		target.warning.should.be.String().which.is.not.empty();

		target = PolicyRoute.parseTarget( "Controller::deferredMirror" );
		( target.handler == null ).should.be.true();
		target.warning.should.be.String().which.is.not.empty();

		target = PolicyRoute.parseTarget( "Controller.deferredMirror" );
		( target.handler == null ).should.be.true();
		target.warning.should.be.String().which.is.not.empty();

		target = PolicyRoute.parseTarget( { controller: "Controller", method: "deferredMirror" } );
		( target.handler == null ).should.be.true();
		target.warning.should.be.String().which.is.not.empty();
	} );

	it( "provides an implicit handler provided as target of a policy route", async() => {
		await Test.before( ctx, { ...config } )();
		const { PolicyRoute } = ctx.hitchy.api.service;
		const fn = () => {};

		const target = PolicyRoute.parseTarget( fn );
		target.handler.should.equal( fn );
		( target.warning == null ).should.be.true();
	} );

	it( "provides selected controller's method as handler of a defined policy routing target", async() => {
		await Test.before( ctx, { ...config, projectFolder: "test/projects/routing-basics" } )();
		const { PolicyRoute } = ctx.hitchy.api.service;

		let target = PolicyRoute.parseTarget( "Filter::myImplementation" );
		target.handler.should.be.Function();
		( target.warning == null ).should.be.true();

		target = PolicyRoute.parseTarget( "Filter.myImplementation" );
		target.handler.should.be.Function();
		( target.warning == null ).should.be.true();

		target = PolicyRoute.parseTarget( { controller: "Filter", method: "myImplementation" } );
		target.handler.should.be.Function();
		( target.warning == null ).should.be.true();
	} );

	it( "provides a warning instead of a handler when addressing target of policy routing in wrong collection", async() => {
		await Test.before( ctx, { ...config, projectFolder: "test/projects/routing-basics" } )();
		const { PolicyRoute } = ctx.hitchy.api.service;

		let target = PolicyRoute.parseTarget( "Controller::deferredMirror" );
		( target.handler == null ).should.be.true();
		target.warning.should.be.String().which.is.not.empty();

		target = PolicyRoute.parseTarget( "Controller.deferredMirror" );
		( target.handler == null ).should.be.true();
		target.warning.should.be.String().which.is.not.empty();

		target = PolicyRoute.parseTarget( { controller: "Controller", method: "deferredMirror" } );
		( target.handler == null ).should.be.true();
		target.warning.should.be.String().which.is.not.empty();
	} );

	it( "rejects multiple policy target definitions provided as array (even though supporting lists of targets per policy route)", async() => {
		await Test.before( ctx, { ...config } )();
		const { PolicyRoute } = ctx.hitchy.api.service;

		( () => PolicyRoute.parseTarget( [] ) ).should.throw();
		( () => PolicyRoute.parseTarget( [() => {}] ) ).should.throw();
		( () => PolicyRoute.parseTarget( ["Filter::myImplementation"] ) ).should.throw();
		( () => PolicyRoute.parseTarget( ["Filter.myImplementation"] ) ).should.throw();
		( () => PolicyRoute.parseTarget( [ "Filter", "myImplementation" ] ) ).should.throw();
		( () => PolicyRoute.parseTarget( [{ controller: "Filter", method: "myImplementation" }] ) ).should.throw();
		( () => PolicyRoute.parseTarget( ["Controller::deferredMirror"] ) ).should.throw();
		( () => PolicyRoute.parseTarget( ["Controller.deferredMirror"] ) ).should.throw();
		( () => PolicyRoute.parseTarget( [ "Controller", "deferredMirror" ] ) ).should.throw();
		( () => PolicyRoute.parseTarget( [{ controller: "Controller", method: "deferredMirror" }] ) ).should.throw();
	} );


	it( "rejects invalid types of values for defining target of controller route", async() => {
		await Test.before( ctx, { ...config } )();
		const { ControllerRoute } = ctx.hitchy.api.service;

		( () => ControllerRoute.parseTarget( null ) ).should.throw();
		( () => ControllerRoute.parseTarget( undefined ) ).should.throw();
		( () => ControllerRoute.parseTarget( false ) ).should.throw();
		( () => ControllerRoute.parseTarget( true ) ).should.throw();
		( () => ControllerRoute.parseTarget( 1.0 ) ).should.throw();
		( () => ControllerRoute.parseTarget( -0.0 ) ).should.throw();
		( () => ControllerRoute.parseTarget( {} ) ).should.throw();
		( () => ControllerRoute.parseTarget( "" ) ).should.throw();
	} );

	it( "accepts valid controller target definitions", async() => {
		await Test.before( ctx, { ...config } )();
		const { ControllerRoute } = ctx.hitchy.api.service;

		let target = ControllerRoute.parseTarget( () => {} );
		target.should.be.ok();

		target = ControllerRoute.parseTarget( "Filter::myImplementation" );
		target.should.be.ok();

		target = ControllerRoute.parseTarget( "Filter.myImplementation" );
		target.should.be.ok();

		target = ControllerRoute.parseTarget( { controller: "Filter", method: "myImplementation" } );
		target.should.be.ok();

		target = ControllerRoute.parseTarget( "Controller::deferredMirror" );
		target.should.be.ok();

		target = ControllerRoute.parseTarget( "Controller.deferredMirror" );
		target.should.be.ok();

		target = ControllerRoute.parseTarget( { controller: "Controller", method: "deferredMirror" } );
		target.should.be.ok();
	} );

	it( "provides a warning instead of a handler on valid target definitions addressing missing controller function", async() => {
		await Test.before( ctx, { ...config } )();
		const { ControllerRoute } = ctx.hitchy.api.service;

		let target = ControllerRoute.parseTarget( "Filter::myImplementation" );
		( target.handler == null ).should.be.true();
		target.warning.should.be.String().which.is.not.empty();

		target = ControllerRoute.parseTarget( "Filter.myImplementation" );
		( target.handler == null ).should.be.true();
		target.warning.should.be.String().which.is.not.empty();

		target = ControllerRoute.parseTarget( { controller: "Filter", method: "myImplementation" } );
		( target.handler == null ).should.be.true();
		target.warning.should.be.String().which.is.not.empty();

		target = ControllerRoute.parseTarget( "Controller::deferredMirror" );
		( target.handler == null ).should.be.true();
		target.warning.should.be.String().which.is.not.empty();

		target = ControllerRoute.parseTarget( "Controller.deferredMirror" );
		( target.handler == null ).should.be.true();
		target.warning.should.be.String().which.is.not.empty();

		target = ControllerRoute.parseTarget( { controller: "Controller", method: "deferredMirror" } );
		( target.handler == null ).should.be.true();
		target.warning.should.be.String().which.is.not.empty();
	} );

	it( "provides an implicit handler provided as target of a controller route", async() => {
		await Test.before( ctx, { ...config } )();
		const { ControllerRoute } = ctx.hitchy.api.service;
		const fn = () => {};

		const target = ControllerRoute.parseTarget( fn );
		target.handler.should.equal( fn );
		( target.warning == null ).should.be.true();
	} );

	it( "provides selected controller's method as handler of a defined controller routing target", async() => {
		await Test.before( ctx, { ...config, projectFolder: "test/projects/routing-basics" } )();
		const { ControllerRoute } = ctx.hitchy.api.service;

		let target = ControllerRoute.parseTarget( "Controller::deferredMirror" );
		target.handler.should.be.Function();
		( target.warning == null ).should.be.true();

		target = ControllerRoute.parseTarget( "Controller.deferredMirror" );
		target.handler.should.be.Function();
		( target.warning == null ).should.be.true();

		target = ControllerRoute.parseTarget( { controller: "Controller", method: "deferredMirror" } );
		target.handler.should.be.Function();
		( target.warning == null ).should.be.true();
	} );

	it( "provides a warning instead of a handler when addressing target of controller routing in wrong collection", async() => {
		await Test.before( ctx, { ...config, projectFolder: "test/projects/routing-basics" } )();
		const { ControllerRoute } = ctx.hitchy.api.service;

		let target = ControllerRoute.parseTarget( "Filter::myImplementation" );
		( target.handler == null ).should.be.true();
		target.warning.should.be.String().which.is.not.empty();

		target = ControllerRoute.parseTarget( "Filter.myImplementation" );
		( target.handler == null ).should.be.true();
		target.warning.should.be.String().which.is.not.empty();

		target = ControllerRoute.parseTarget( { controller: "Filter", method: "myImplementation" } );
		( target.handler == null ).should.be.true();
		target.warning.should.be.String().which.is.not.empty();
	} );

	it( "rejects multiple controller target definitions provided as array", async() => {
		await Test.before( ctx, { ...config } )();
		const { ControllerRoute } = ctx.hitchy.api.service;

		( () => ControllerRoute.parseTarget( [] ) ).should.throw();
		( () => ControllerRoute.parseTarget( [() => {}] ) ).should.throw();
		( () => ControllerRoute.parseTarget( ["Filter::myImplementation"] ) ).should.throw();
		( () => ControllerRoute.parseTarget( ["Filter.myImplementation"] ) ).should.throw();
		( () => ControllerRoute.parseTarget( [ "Filter", "myImplementation" ] ) ).should.throw();
		( () => ControllerRoute.parseTarget( [{ controller: "Filter", method: "myImplementation" }] ) ).should.throw();
		( () => ControllerRoute.parseTarget( ["Controller::deferredMirror"] ) ).should.throw();
		( () => ControllerRoute.parseTarget( ["Controller.deferredMirror"] ) ).should.throw();
		( () => ControllerRoute.parseTarget( [ "Controller", "deferredMirror" ] ) ).should.throw();
		( () => ControllerRoute.parseTarget( [{ controller: "Controller", method: "deferredMirror" }] ) ).should.throw();
	} );
} );

describe( "Route service's selectProbablyCoveredPrefixes() method", () => {
	const ctx = {};

	beforeEach( Test.before( ctx, { ...config } ) );
	afterEach( Test.after( ctx ) );

	it( "requires list of prefixes to be filtered", () => {
		const { ControllerRoute, PolicyRoute } = ctx.hitchy.api.service;
		let route = new ControllerRoute( "/", () => {} );

		( () => route.selectProbablyCoveredPrefixes() ).should.throw();
		( () => route.selectProbablyCoveredPrefixes( null ) ).should.throw();
		( () => route.selectProbablyCoveredPrefixes( undefined ) ).should.throw();
		( () => route.selectProbablyCoveredPrefixes( false ) ).should.throw();
		( () => route.selectProbablyCoveredPrefixes( true ) ).should.throw();
		( () => route.selectProbablyCoveredPrefixes( 1.0 ) ).should.throw();
		( () => route.selectProbablyCoveredPrefixes( -0.0 ) ).should.throw();
		( () => route.selectProbablyCoveredPrefixes( [() => {}] ) ).should.throw();
		( () => route.selectProbablyCoveredPrefixes( [""] ) ).should.throw();
		( () => route.selectProbablyCoveredPrefixes( {} ) ).should.throw();
		( () => route.selectProbablyCoveredPrefixes( { prefix: "/test" } ) ).should.throw();
		( () => route.selectProbablyCoveredPrefixes( "" ) ).should.throw();
		( () => route.selectProbablyCoveredPrefixes( "/test" ) ).should.throw();

		( () => route.selectProbablyCoveredPrefixes( [] ) ).should.not.throw();
		( () => route.selectProbablyCoveredPrefixes( ["/test"] ) ).should.not.throw();
		( () => route.selectProbablyCoveredPrefixes( [ "/test", "/taste" ] ) ).should.not.throw();

		route = new PolicyRoute( "/", () => {} );

		( () => route.selectProbablyCoveredPrefixes() ).should.throw();
		( () => route.selectProbablyCoveredPrefixes( null ) ).should.throw();
		( () => route.selectProbablyCoveredPrefixes( undefined ) ).should.throw();
		( () => route.selectProbablyCoveredPrefixes( false ) ).should.throw();
		( () => route.selectProbablyCoveredPrefixes( true ) ).should.throw();
		( () => route.selectProbablyCoveredPrefixes( 1.0 ) ).should.throw();
		( () => route.selectProbablyCoveredPrefixes( -0.0 ) ).should.throw();
		( () => route.selectProbablyCoveredPrefixes( [() => {}] ) ).should.throw();
		( () => route.selectProbablyCoveredPrefixes( [""] ) ).should.throw();
		( () => route.selectProbablyCoveredPrefixes( {} ) ).should.throw();
		( () => route.selectProbablyCoveredPrefixes( { prefix: "/test" } ) ).should.throw();
		( () => route.selectProbablyCoveredPrefixes( "" ) ).should.throw();
		( () => route.selectProbablyCoveredPrefixes( "/test" ) ).should.throw();

		( () => route.selectProbablyCoveredPrefixes( [] ) ).should.not.throw();
		( () => route.selectProbablyCoveredPrefixes( ["/test"] ) ).should.not.throw();
		( () => route.selectProbablyCoveredPrefixes( [ "/test", "/taste" ] ) ).should.not.throw();
	} );

	it( "considers some generic route probably covering some specific prefix", () => {
		const { ControllerRoute, Route } = ctx.hitchy.api.service;

		[
			{ generic: "/", specific: "/test/name/sub" },
			{ generic: "/test/name", specific: "/test/name/sub" },
			{ generic: "/test", specific: "/test/name/sub" },
			{ generic: "/test/name/sub", specific: "/test/name/sub" },

			{ generic: "/test/:minor", specific: "/test/name/sub" },
			{ generic: "/test{/:minor}", specific: "/test/name/sub" },
			{ generic: "/test/*minor", specific: "/test/name/sub" },
			{ generic: "/test{/*minor}", specific: "/test/name/sub" },

			{ generic: "/:major", specific: "/test/name/sub" },
			{ generic: "{/:major}", specific: "/test/name/sub" },
			{ generic: "/*major", specific: "/test/name/sub" },
			{ generic: "{/*major}", specific: "/test/name/sub" },

			{ generic: "/:major/name", specific: "/test/name/sub" },
			{ generic: "{/:major}/test", specific: "/test/name/sub" },
			{ generic: "{/:major}/name", specific: "/test/name/sub" },
			{ generic: "{/*major}/test", specific: "/test/name/sub" },
			{ generic: "{/*major}/name", specific: "/test/name/sub" },
			{ generic: "{/*major}/sub", specific: "/test/name/sub" },

			{ generic: "/*major/name", specific: "/test/name/sub" },
			{ generic: "/*major/sub", specific: "/test/name/sub" },
			{ generic: "/:major/:minor", specific: "/test/name/sub" },
			{ generic: "/:major{/:minor}", specific: "/test/name/sub" },
			{ generic: "/:major/*minor", specific: "/test/name/sub" },
			{ generic: "/:major{/*minor}", specific: "/test/name/sub" },

			{ generic: "/test/:minor/sub", specific: "/test/name/sub" },
			{ generic: "/test{/:minor}/name", specific: "/test/name/sub" },
			{ generic: "/test{/*minor}/name", specific: "/test/name/sub" },
			{ generic: "/test/*minor/sub", specific: "/test/name/sub" },
		]
			.forEach( ( { generic, specific, full = false } ) => {
				const route = new ControllerRoute( generic, () => {} );
				const covered = route.selectProbablyCoveredPrefixes( [specific] );

				covered.should.be.Object()
					.and.have.property( specific )
					.and.be.greaterThanOrEqual( full ? Route.MATCH_FULL : Route.MATCH_PARTIAL, `invalid mark on ${specific} covered by ${generic}` );
			} );
	} );

	it( "considers some generic but divergent route never covering some specific prefix", () => {
		const { ControllerRoute } = ctx.hitchy.api.service;

		[
			{ generic: "/test/name/s", specific: "/test/name/sub" },
			{ generic: "/test/n", specific: "/test/name/sub" },
			{ generic: "/tes", specific: "/test/name/sub" },
			{ generic: "/te", specific: "/test/name/sub" },
			{ generic: "/t", specific: "/test/name/sub" },

			{ generic: "/test/name/sub/item", specific: "/test/name/sub" },
			{ generic: "/test/name/subs", specific: "/test/name/sub" },
			{ generic: "/test/name/su/b", specific: "/test/name/sub" },
			{ generic: "/teste", specific: "/test/name/sub" },
			{ generic: "/tee", specific: "/test/name/sub" },
			{ generic: "/s", specific: "/test/name/sub" },
			{ generic: "/tast/:minor", specific: "/test/name/sub" },
			{ generic: "/tast{/:minor}", specific: "/test/name/sub" },
			{ generic: "/tast/*minor", specific: "/test/name/sub" },
			{ generic: "/tast{/*minor}", specific: "/test/name/sub" },
			{ generic: "/:major/neme", specific: "/test/name/sub" },
			{ generic: "{/:major}/neme", specific: "/test/name/sub" },
			{ generic: "/*major/neme", specific: "/test/name/sub" },
			{ generic: "{/*major}/neme", specific: "/test/name/sub" },
		]
			.forEach( ( { generic, specific } ) => {
				const route = new ControllerRoute( generic, () => {} );
				const covered = route.selectProbablyCoveredPrefixes( [specific] );

				covered.should.deepEqual( {} );
			} );
	} );
} );
