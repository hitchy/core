import { describe, it } from "mocha";
import "should";

import * as Case from "../../../../lib/misc/case.js";

describe( "Case-conversion utility", () => {
	describe( "kebabToCamel()", () => {
		it( "is converting from kebab-case to camelCase", () => {
			Case.kebabToCamel( "some" ).should.be.equal( "some" );
			Case.kebabToCamel( "some-test-text" ).should.be.equal( "someTestText" );
			Case.kebabToCamel( "some-TEST-text" ).should.be.equal( "someTestText" );
			Case.kebabToCamel( "some-tESt-text" ).should.be.equal( "someTestText" );
		} );
	} );

	describe( "kebabToPascal()", () => {
		it( "is converting from kebab-case to camelCase", () => {
			Case.kebabToPascal( "some" ).should.be.equal( "Some" );
			Case.kebabToPascal( "some-test-text" ).should.be.equal( "SomeTestText" );
			Case.kebabToPascal( "some-TEST-text" ).should.be.equal( "SomeTestText" );
			Case.kebabToPascal( "some-tESt-text" ).should.be.equal( "SomeTestText" );
		} );
	} );

	describe( "camelToKebab()", () => {
		it( "is converting from camelCase to kebab-case", () => {
			Case.camelToKebab( "some" ).should.be.equal( "some" );
			Case.camelToKebab( "someTestText" ).should.be.equal( "some-test-text" );
		} );
	} );

	describe( "pascalToKebab()", () => {
		it( "is converting from PascalCase to kebab-case", () => {
			Case.pascalToKebab( "Some" ).should.be.equal( "some" );
			Case.pascalToKebab( "SomeTestText" ).should.be.equal( "some-test-text" );
		} );
	} );

	describe( "camelToPascal()", () => {
		it( "is converting from camelCase to PascalCase", () => {
			Case.camelToPascal( "some" ).should.be.equal( "Some" );
			Case.camelToPascal( "someTestText" ).should.be.equal( "SomeTestText" );
		} );
	} );
} );
