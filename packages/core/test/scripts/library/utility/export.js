import { describe, it, beforeEach, afterEach } from "mocha";
import "should";
import SDT from "@hitchy/server-dev-tools";

import Core from "../../../../sdt.js";

const Test = await SDT( Core );

const config = {
	projectFolder: "test/projects/empty",
	options: {
		// debug: true,
	}
};

describe( "Hitchy API has a section of utilities which", () => {
	const ctx = {};

	beforeEach( Test.before( ctx, { ...config } ) );
	afterEach( Test.after( ctx ) );

	it( "has a set of file-related tools", () => {
		ctx.hitchy.api.utility.file.should.be.ok();
	} );

	it( "has a set for qualifying a request context", () => {
		ctx.hitchy.api.utility.introduce.should.be.ok();
	} );

	it( "has a set of log-related tools", () => {
		ctx.hitchy.api.utility.logger.should.be.ok();
	} );

	it( "has a set of parser-related tools", () => {
		ctx.hitchy.api.utility.parser.should.be.ok();
	} );
} );
