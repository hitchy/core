import { describe, it, beforeEach, afterEach } from "mocha";
import "should";
import SDT from "@hitchy/server-dev-tools";

import Core from "../../../../sdt.js";

const Test = await SDT( Core );

const config = {
	projectFolder: "test/projects/empty",
	options: {
		// debug: true,
	},
};

describe( "Hitchy API's introduce() function for injecting itself into some partial request context", function() {
	const ctx = {};

	afterEach( Test.after( ctx ) );
	beforeEach( Test.before( ctx, { ...config } ) );

	it( "is exposed as part of utility namespace", () => {
		ctx.hitchy.api.utility.introduce.should.be.a.Function();
	} );

	it( "requires some object as argument for injecting a request context into it", () => {
		ctx.hitchy.api.utility.introduce.should.have.length( 1 );
	} );

	it( "returns provided object", () => {
		const object = {};

		ctx.hitchy.api.utility.introduce( object ).should.equal( object );
	} );

	it( "injects current time as seconds since Unix Epoch into provided context", function() {
		ctx.hitchy.api.utility.introduce( {} )
			.should.have.property( "startTime" )
			.which.is.a.Number()
			.within( Date.now() - 1, Date.now() + 1 );
	} );

	it( "injects runtime configuration as property `config`", function() {
		ctx.hitchy.api.utility.introduce( {} )
			.should.have.property( "config" ).which.is.equal( ctx.hitchy.api.config );
	} );

	it( "injects controllers as property `controllers`", function() {
		ctx.hitchy.api.utility.introduce( {} )
			.should.have.property( "controllers" ).which.is.equal( ctx.hitchy.api.controllers );
	} );

	it( "injects controllers as property `controller`", function() {
		ctx.hitchy.api.utility.introduce( {} )
			.should.have.property( "controller" ).which.is.equal( ctx.hitchy.api.controller );
	} );

	it( "injects controllers as property `policies`", function() {
		ctx.hitchy.api.utility.introduce( {} )
			.should.have.property( "policies" ).which.is.equal( ctx.hitchy.api.policies );
	} );

	it( "injects controllers as property `policy`", function() {
		ctx.hitchy.api.utility.introduce( {} )
			.should.have.property( "policy" ).which.is.equal( ctx.hitchy.api.policy );
	} );

	it( "injects controllers as property `models`", function() {
		ctx.hitchy.api.utility.introduce( {} )
			.should.have.property( "models" ).which.is.equal( ctx.hitchy.api.models );
	} );

	it( "injects controllers as property `model`", function() {
		ctx.hitchy.api.utility.introduce( {} )
			.should.have.property( "model" ).which.is.equal( ctx.hitchy.api.model );
	} );

	it( "injects controllers as property `services`", function() {
		ctx.hitchy.api.utility.introduce( {} )
			.should.have.property( "services" ).which.is.equal( ctx.hitchy.api.services );
	} );

	it( "injects controllers as property `service`", function() {
		ctx.hitchy.api.utility.introduce( {} )
			.should.have.property( "service" ).which.is.equal( ctx.hitchy.api.service );
	} );

	it( "injects full Hitchy API as property `api`", function() {
		ctx.hitchy.api.utility.introduce( {} )
			.should.have.property( "api" ).which.is.equal( ctx.hitchy.api );
	} );

	it( "injects absolute URL of request described by node:http.IncomingMessage provided as `request` of provided object", function() {
		const qualified = ctx.hitchy.api.utility.introduce( {
			request: {
				protocol: "http",
				host: "10.1.2.3",
				headers: {
					forwarded: `proto=https; host="foo.example.com:1234" ; for=1.2.3.4`,
				},
				url: "/major/minor/index.html?name=bar",
				socket: { remoteAddress: "8.7.6.5" },
			},
		} );

		qualified.url.should.be.instanceof( URL );
		qualified.url.toString().should.equal( "https://foo.example.com:1234/major/minor/index.html?name=bar" );
	} );

	describe( "is injecting client's IP based on node:http.IncomingMessage included as `request` in provided object which", () => {
		it( "is based on socket's IPv4 peer address by default", () => {
			const qualified = ctx.hitchy.api.utility.introduce( {
				request: {
					socket: { remoteAddress: "1.2.3.4" },
				},
			} );

			qualified.clientIp.should.equal( "1.2.3.4" );
		} );

		it( "is based on socket's IPv6 peer address by default", () => {
			const qualified = ctx.hitchy.api.utility.introduce( {
				request: {
					socket: { remoteAddress: "1::1" },
				},
			} );

			qualified.clientIp.should.equal( "1::1" );
		} );

		it( "is preferrably reading IPv4 address from provided request's `X-Real-IP` header", () => {
			const qualified = ctx.hitchy.api.utility.introduce( {
				request: {
					socket: { remoteAddress: "1.2.3.4" },
					headers: {
						"x-real-ip": "2.3.4.5",
					},
				},
			} );

			qualified.clientIp.should.equal( "2.3.4.5" );
		} );

		it( "is preferrably reading IPv6 address from provided request's `X-Real-IP` header", () => {
			const qualified = ctx.hitchy.api.utility.introduce( {
				request: {
					socket: { remoteAddress: "1::1" },
					headers: {
						"x-real-ip": "1:2::1",
					},
				},
			} );

			qualified.clientIp.should.equal( "1:2::1" );
		} );

		it( "is ignoring invalid IPv4 address in provided request's `X-Real-IP` header", () => {
			const qualified = ctx.hitchy.api.utility.introduce( {
				request: {
					socket: { remoteAddress: "1.2.3.4" },
					headers: {
						"x-real-ip": "257.1.1.1",
					},
				},
			} );

			qualified.clientIp.should.equal( "1.2.3.4" );
		} );

		it( "is ignoring invalid IPv6 address in provided request's `X-Real-IP` header", () => {
			const qualified = ctx.hitchy.api.utility.introduce( {
				request: {
					socket: { remoteAddress: "1::1" },
					headers: {
						"x-real-ip": "1:g2::1",
					},
				},
			} );

			qualified.clientIp.should.equal( "1::1" );
		} );

		it( "is preferrably reading IPv4 address from provided request's `X-Forwarded-For` header", () => {
			const qualified = ctx.hitchy.api.utility.introduce( {
				request: {
					socket: { remoteAddress: "1.2.3.4" },
					headers: {
						"x-real-ip": "2.3.4.5",
						"x-forwarded-for": " 1.2.3.5, 2.3.4.5, 6.5.4.3",
					},
				},
			} );

			qualified.clientIp.should.equal( "1.2.3.5" );
		} );

		it( "is preferrably reading IPv6 address from provided request's `X-Forwarded-For` header", () => {
			const qualified = ctx.hitchy.api.utility.introduce( {
				request: {
					socket: { remoteAddress: "1::1" },
					headers: {
						"x-real-ip": "1:2::1",
						"x-forwarded-for": " 1::2:1, 2.3.4.5, 6.5.4.3",
					},
				},
			} );

			qualified.clientIp.should.equal( "1::2:1" );
		} );

		it( "is ignoring invalid IPv4 address provided in first position of request's `X-Forwarded-For` header", () => {
			const qualified = ctx.hitchy.api.utility.introduce( {
				request: {
					socket: { remoteAddress: "1.2.3.4" },
					headers: {
						"x-real-ip": "2.3.4.5",
						"x-forwarded-for": " 1.2.3.257, 2.3.4.5, 6.5.4.3",
					},
				},
			} );

			qualified.clientIp.should.equal( "2.3.4.5" );
		} );

		it( "is ignoring invalid IPv6 address provided in first position of request's `X-Forwarded-For` header", () => {
			const qualified = ctx.hitchy.api.utility.introduce( {
				request: {
					socket: { remoteAddress: "1::1" },
					headers: {
						"x-real-ip": "1:2::1",
						"x-forwarded-for": " 1::2::1, 2.3.4.5, 6.5.4.3",
					},
				},
			} );

			qualified.clientIp.should.equal( "1:2::1" );
		} );

		it( "is not caring about invalid IPv4 addresses provided in follow-up position of request's `X-Forwarded-For` header", () => {
			const qualified = ctx.hitchy.api.utility.introduce( {
				request: {
					socket: { remoteAddress: "1.2.3.4" },
					headers: {
						"x-real-ip": "2.3.4.5",
						"x-forwarded-for": " 1.2.3.5, 257.3.4.5, 6.5.4.3",
					},
				},
			} );

			qualified.clientIp.should.equal( "1.2.3.5" );
		} );

		it( "is not caring about invalid IPv6 address provided in follow-up position of request's `X-Forwarded-For` header", () => {
			const qualified = ctx.hitchy.api.utility.introduce( {
				request: {
					socket: { remoteAddress: "1::1" },
					headers: {
						"x-real-ip": "1:2::1",
						"x-forwarded-for": " 1::2:1, ::G2, 6.5.4.3",
					},
				},
			} );

			qualified.clientIp.should.equal( "1::2:1" );
		} );

		it( "is preferrably reading IPv4 address from provided request's `Forwarded` header", () => {
			const qualified = ctx.hitchy.api.utility.introduce( {
				request: {
					socket: { remoteAddress: "1.2.3.4" },
					headers: {
						"x-real-ip": "2.3.4.5",
						"x-forwarded-for": " 1.2.3.5, 2.3.4.5, 6.5.4.3",
						forwarded: `proto=https; host="foo.example.com:1234" ; for=6.7.8.9`,
					},
				},
			} );

			qualified.clientIp.should.equal( "6.7.8.9" );
		} );

		it( "is preferrably reading IPv6 address from provided request's `Forwarded` header", () => {
			const qualified = ctx.hitchy.api.utility.introduce( {
				request: {
					socket: { remoteAddress: "1::1" },
					headers: {
						"x-real-ip": "1:2::1",
						"x-forwarded-for": " 1::2:1, 2.3.4.5, 6.5.4.3",
						forwarded: `proto=https; host="foo.example.com:1234" ; for=f::f`,
					},
				},
			} );

			qualified.clientIp.should.equal( "f::f" );
		} );

		it( "is ignoring invalid IPv4 address provided in request's `Forwarded` header", () => {
			const qualified = ctx.hitchy.api.utility.introduce( {
				request: {
					socket: { remoteAddress: "1.2.3.4" },
					headers: {
						"x-real-ip": "2.3.4.5",
						"x-forwarded-for": " 1.2.3.5, 2.3.4.5, 6.5.4.3",
						forwarded: `proto=https; host="foo.example.com:1234" ; for=6.257.8.9`,
					},
				},
			} );

			qualified.clientIp.should.equal( "1.2.3.5" );
		} );

		it( "is ignoring invalid IPv6 address provided in request's `Forwarded` header", () => {
			const qualified = ctx.hitchy.api.utility.introduce( {
				request: {
					socket: { remoteAddress: "1::1" },
					headers: {
						"x-real-ip": "1:2::1",
						"x-forwarded-for": " 1::2:1, 2.3.4.5, 6.5.4.3",
						forwarded: `proto=https; host="foo.example.com:1234" ; for=g::g`,
					},
				},
			} );

			qualified.clientIp.should.equal( "1::2:1" );
		} );
	} );
} );
