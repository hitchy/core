import { describe, it, beforeEach, afterEach } from "mocha";
import "should";
import SDT from "@hitchy/server-dev-tools";

import Core from "../../../../sdt.js";

const Test = await SDT( Core );

const config = {
	projectFolder: "test/projects/empty",
	options: {
		// debug: true,
	},
};

describe( "Hitchy API includes a parser for URLs which", () => {
	const ctx = {};

	beforeEach( Test.before( ctx, { ...config } ) );
	afterEach( Test.after( ctx ) );

	it( "is processing empty URL query", () => {
		const result = ctx.hitchy.api.utility.parser.query( "" );

		result.should.be.Object();
		result.should.have.size( 0 );
	} );

	it( "is processing single name w/o value", () => {
		const result = ctx.hitchy.api.utility.parser.query( "myvar" );

		result.should.be.Object();
		result.should.have.size( 1 );
		result.should.have.ownProperty( "myvar" );
		result.myvar.should.be.true();
	} );

	it( "is processing multiple different names each w/o value", () => {
		const result = ctx.hitchy.api.utility.parser.query( "myvar&anothervar" );

		result.should.be.Object();
		result.should.have.size( 2 );
		result.should.have.ownProperty( "myvar" );
		result.should.have.ownProperty( "anothervar" );
		result.myvar.should.be.true();
		result.anothervar.should.be.true();
	} );

	it( "is processing multiple occurrences of same names w/o value", () => {
		const result = ctx.hitchy.api.utility.parser.query( "myvar&anothervar&myvar" );

		result.should.be.Object();
		result.should.have.size( 2 );
		result.should.have.ownProperty( "myvar" );
		result.should.have.ownProperty( "anothervar" );
		result.myvar.should.be.true();
		result.anothervar.should.be.true();
	} );

	it( "supports single name w/ non-empty string value", () => {
		const result = ctx.hitchy.api.utility.parser.query( "myvar=test" );

		result.should.be.Object();
		result.should.have.size( 1 );
		result.should.have.ownProperty( "myvar" );
		result.myvar.should.be.String().and.equal( "test" );
	} );

	it( "supports multiple occurrence of same name w/ non-empty string value", () => {
		const result = ctx.hitchy.api.utility.parser.query( "myvar=foo&myvar=bar" );

		result.should.be.Object();
		result.should.have.size( 1 );
		result.should.have.ownProperty( "myvar" );
		result.myvar.should.be.String().and.equal( "bar" );
	} );

	it( "is obeying latest of multiple occurrences per name even though switching between value and no-value", () => {
		let result = ctx.hitchy.api.utility.parser.query( "myvar=test&anothervar&myvar" );

		result.should.be.Object();
		result.should.have.size( 2 );
		result.should.have.ownProperty( "myvar" );
		result.should.have.ownProperty( "anothervar" );
		result.myvar.should.be.true();
		result.anothervar.should.be.true();

		result = ctx.hitchy.api.utility.parser.query( "myvar&anothervar&myvar=test" );

		result.should.be.Object();
		result.should.have.size( 2 );
		result.should.have.ownProperty( "myvar" );
		result.should.have.ownProperty( "anothervar" );
		result.myvar.should.be.String().and.equal( "test" );
		result.anothervar.should.be.true();
	} );

	it( "supports assigning null explicitly", () => {
		const result = ctx.hitchy.api.utility.parser.query( "myvar=test&anothervar&myvar=" );

		result.should.be.Object();
		result.should.have.size( 2 );
		result.should.have.ownProperty( "myvar" );
		result.should.have.ownProperty( "anothervar" );
		( result.myvar == null ).should.be.true();
		result.anothervar.should.be.true();
	} );

	it( "supports constructing array from names using empty brackets", () => {
		const result = ctx.hitchy.api.utility.parser.query( "myvar[]=test&anothervar&myvar[]=&myvar[]" );

		result.should.be.Object();
		result.should.have.size( 2 );
		result.should.have.ownProperty( "myvar" );
		result.should.have.ownProperty( "anothervar" );
		result.myvar.should.be.Array().and.have.length( 3 );
		result.myvar.should.be.eql( [ "test", null, true ] );
		result.anothervar.should.be.true();
	} );

	it( "supports constructing object on using non-empty element names in brackets", () => {
		const result = ctx.hitchy.api.utility.parser.query( "myvar[foo]=oof&myvar[bar]=&myvar[baz]" );

		result.should.be.Object();
		result.should.have.size( 1 );
		result.should.have.ownProperty( "myvar" );
		result.myvar.should.be.Object().and.have.size( 3 );
		result.myvar.should.have.value( "foo", "oof" );
		result.myvar.should.have.value( "bar", null );
		result.myvar.should.have.value( "baz", true );
	} );

	it( "feeds array like object when oddly mixing empty brackets with non-empty ones", () => {
		const result = ctx.hitchy.api.utility.parser.query( "myvar[]=foo&myvar[bar]=&myvar[baz]" );

		result.should.be.Object();
		result.should.have.size( 1 );
		result.should.have.ownProperty( "myvar" );
		result.myvar.should.be.Array().and.have.length( 1 );
		result.myvar.should.be.Array().and.have.size( 3 );
		result.myvar.should.have.value( 0, "foo" );
		result.myvar.should.have.value( "bar", null );
		result.myvar.should.have.value( "baz", true );
	} );

	it( "chooses numeric property names when oddly mixing non-empty brackets with empty ones", () => {
		const result = ctx.hitchy.api.utility.parser.query( "myvar[baz]=foo&myvar[bar]=&myvar[]" );

		result.should.be.Object();
		result.should.have.size( 1 );
		result.should.have.ownProperty( "myvar" );
		result.myvar.should.be.Object().and.have.size( 3 );
		result.myvar.should.have.value( "baz", "foo" );
		result.myvar.should.have.value( "bar", null );
		result.myvar.should.have.value( 2, true );
	} );

	it( "obeys last of multiple occurrences addressing same element", () => {
		const result = ctx.hitchy.api.utility.parser.query( "myvar[baz]=foo&myvar[bar]=&myvar[]&myvar[bar]=3" );

		result.should.be.Object();
		result.should.have.size( 1 );
		result.should.have.ownProperty( "myvar" );
		result.myvar.should.be.Object().and.have.size( 3 );
		result.myvar.should.have.value( "baz", "foo" );
		result.myvar.should.have.value( "bar", "3" );
		result.myvar.should.have.value( 2, true );
	} );

	it( "supports URL-encoded names", () => {
		const result = ctx.hitchy.api.utility.parser.query( "myvar%5Bbaz%5D=foo&m%79%76ar[bar]=&myvar[]&myvar[%66%3D%6F%6F]=3" );

		result.should.be.Object();
		result.should.have.size( 1 );
		result.should.have.ownProperty( "myvar" );
		result.myvar.should.be.Object().and.have.size( 4 );
		result.myvar.should.have.value( "baz", "foo" );
		result.myvar.should.have.value( "bar", null );
		result.myvar.should.have.value( 2, true );
		result.myvar.should.have.value( "f=oo", "3" );
	} );

	it( "supports URL-encoded values", () => {
		const result = ctx.hitchy.api.utility.parser.query( "myvar[baz]=%66%3D%26%6F%6F&myvar[bar]=&myvar[]&myvar[foo]=%33" );

		result.should.be.Object();
		result.should.have.size( 1 );
		result.should.have.ownProperty( "myvar" );
		result.myvar.should.be.Object().and.have.size( 4 );
		result.myvar.should.have.value( "baz", "f=&oo" );
		result.myvar.should.have.value( "bar", null );
		result.myvar.should.have.value( 2, true );
		result.myvar.should.have.value( "foo", "3" );
	} );

	it( "supports URL- and UTF8-encoded names and values", () => {
		const result = ctx.hitchy.api.utility.parser.query( "myvar[%F0%9F%91%BA]=%c3%a4%c3%a9%e2%90%a0%e5%bc%88" );

		result.should.be.Object();
		result.should.have.size( 1 );
		result.should.have.ownProperty( "myvar" );
		result.myvar.should.be.Object().and.have.size( 1 );
		result.myvar.should.have.value( "👺", "äé␠弈" );
	} );

	it( "keeps + in name by default", () => {
		const result = ctx.hitchy.api.utility.parser.query( "my+var=foo&anothervar+=bar&+=baz" );

		result.should.be.Object();
		result.should.have.size( 3 );
		result.should.have.ownProperty( "my+var" );
		result.should.have.ownProperty( "anothervar+" );
		result.should.have.ownProperty( "+" );
		result["my+var"].should.be.String().and.equal( "foo" );
		result["anothervar+"].should.be.String().and.equal( "bar" );
		result["+"].should.be.String().and.equal( "baz" );
	} );

	it( "keeps + in value by default", () => {
		const result = ctx.hitchy.api.utility.parser.query( "myvar=fo+o&anothervar=bar+&baz=+" );

		result.should.be.Object();
		result.should.have.size( 3 );
		result.should.have.ownProperty( "myvar" );
		result.should.have.ownProperty( "anothervar" );
		result.should.have.ownProperty( "baz" );
		result["myvar"].should.be.String().and.equal( "fo+o" );
		result["anothervar"].should.be.String().and.equal( "bar+" );
		result.baz.should.be.String().and.equal( "+" );
	} );

	it( "replaces + with SPC in name on demand", () => {
		const result = ctx.hitchy.api.utility.parser.query( "my+var=foo&anothervar+=bar&+=baz", { isEncodedData: true } );

		result.should.be.Object();
		result.should.have.size( 3 );
		result.should.have.ownProperty( "my var" );
		result.should.have.ownProperty( "anothervar " );
		result.should.have.ownProperty( " " );
		result["my var"].should.be.String().and.equal( "foo" );
		result["anothervar "].should.be.String().and.equal( "bar" );
		result[" "].should.be.String().and.equal( "baz" );
	} );

	it( "replaces + with SPC in value on demand", () => {
		const result = ctx.hitchy.api.utility.parser.query( "myvar=fo+o&anothervar=bar+&baz=+", { isEncodedData: true } );

		result.should.be.Object();
		result.should.have.size( 3 );
		result.should.have.ownProperty( "myvar" );
		result.should.have.ownProperty( "anothervar" );
		result.should.have.ownProperty( "baz" );
		result["myvar"].should.be.String().and.equal( "fo o" );
		result["anothervar"].should.be.String().and.equal( "bar " );
		result.baz.should.be.String().and.equal( " " );
	} );
} );
