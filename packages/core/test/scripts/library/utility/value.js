import { describe, it } from "mocha";
import "should";

import * as Value from "../../../../lib/misc/value.js";

describe( "Value inspection utility", () => {
	describe( "asBoolean()", () => {
		it( "is detecting keywords representing boolean value `true`", () => {
			[
				1,
				"1",
				"y",
				"yes",
				"true",
				"set",
				"on",
				"enable",
				"enabled",
				"Y",
				"YES",
				"TRUE",
				"SET",
				"ON",
				"ENABLE",
				"ENABLED",
			].forEach( string => {
				Value.asBoolean( string ).should.be.true();
				Value.asBoolean( ` \t ${string}\n ` ).should.be.true();
				Value.asBoolean( ` \n${string}\t` ).should.be.true();
				Value.asBoolean( [string] ).should.be.true();
				Value.asBoolean( [` \n${string}\t`] ).should.be.true();
			} );
		} );

		it( "is detecting keywords representing boolean value `false`", () => {
			[
				0,
				"0",
				"n",
				"no",
				"false",
				"unset",
				"off",
				"disable",
				"disabled",
				"N",
				"NO",
				"FALSE",
				"UNSET",
				"OFF",
				"DISABLE",
				"DISABLED",
			].forEach( string => {
				Value.asBoolean( string ).should.be.false();
				Value.asBoolean( ` \t ${string}\n ` ).should.be.false();
				Value.asBoolean( ` \n${string}\t` ).should.be.false();
				Value.asBoolean( [string] ).should.be.false();
				Value.asBoolean( [` \n${string}\t`] ).should.be.false();
			} );
		} );

		it( "is passing any other input as-is", () => {
			[
				2,
				"2",
				"hey",
				"of",
				"passt",
				() => {},
				{},
				{ y: "yes" },
				[],
				["hey"],
			].forEach( string => {
				Value.asBoolean( string ).should.be.equal( string );
				Value.asBoolean( ` \t ${string}\n ` ).should.be.equal( ` \t ${string}\n ` );
				Value.asBoolean( ` \n${string}\t` ).should.be.equal( ` \n${string}\t` );
			} );
		} );
	} );
} );
