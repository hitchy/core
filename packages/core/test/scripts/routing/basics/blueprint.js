import { describe, it, after, before } from "mocha";
import "should";
import "should-http";
import SDT from "@hitchy/server-dev-tools";

import Core from "../../../../sdt.js";

const Test = await SDT( Core );

const config = {
	projectFolder: "test/projects/routing-basics",
	options: {
		scenario: "blueprint",
		// debug: true,
	},
};

describe( "Serving project in basic-routing-core w/ route accompanying blueprints", function() {
	const ctx = {};

	before( Test.before( ctx, config ) );
	after( Test.after( ctx ) );

	it( "hits blueprint route on GETting /blueprint/1234", function() {
		return ctx.get( "/blueprint/1234" )
			.then( response => {
				response.should.have.status( 200 );
				response.should.be.json();
				response.data.should.have.properties( "session", "gotBlueprint" ).and.have.size( 2 );
			} );
	} );

	it( "misses blueprint route on GETting /blueprint/catched due to preceding custom route", function() {
		return ctx.get( "/blueprint/catched" )
			.then( response => {
				response.should.have.status( 200 );
				response.should.be.json();
				response.data.should.not.have.properties( "gotBlueprint" );
				response.data.method.should.equal( "GET" );
			} );
	} );

	it( "hits blueprint route on GETting /blueprint/missed due to custom route succeeding blueprint", function() {
		return ctx.get( "/blueprint/missed" )
			.then( response => {
				response.should.have.status( 200 );
				response.should.be.json();
				response.data.should.have.properties( "session", "gotBlueprint" ).and.have.size( 2 );
			} );
	} );
} );

// NOTE Can not add another suite this time for using scenarios for dynamically
//      switching configuration to suit different testing goals in a single project.
