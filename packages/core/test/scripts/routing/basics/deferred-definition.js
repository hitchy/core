import { describe, afterEach, it } from "mocha";
import "should";
import SDT from "@hitchy/server-dev-tools";

import Core from "../../../../sdt.js";

const Test = await SDT( Core );

const config = {
	tools: Test,
	projectFolder: false,
	options: {
		// debug: true
	},
};

describe( "Deferring routing definitions", () => {
	const ctx = {};

	afterEach( Test.after( ctx ) );

	it( "works with function returning routing map", async() => {
		await Test.before( ctx, { ...config, files: {
			"config/routes.cjs": `exports.routes = () => ( { "/foo": ( _, res ) => res.send( "Hello!" ) } );`,
		} } )();

		const response = await ctx.get( "/foo" );

		response.body.toString( "utf8" ).should.be.equal( "Hello!" );
	} );

	it( "works with Promise for routing map", async() => {
		await Test.before( ctx, { ...config, files: {
			"config/routes.cjs": `exports.routes = new Promise( r => setTimeout( r, 50 ) ).then( () => ( { "/foo": ( _, res ) => res.send( "Hello!" ) } ) );`,
		} } )();

		const response = await ctx.get( "/foo" );

		response.body.toString( "utf8" ).should.be.equal( "Hello!" );
	} );

	it( "works with function returning Promise for routing map", async() => {
		await Test.before( ctx, { ...config, files: {
			"config/routes.cjs": `exports.routes = () => new Promise( r => setTimeout( r, 50 ) ).then( () => ( { "/foo": ( _, res ) => res.send( "Hello!" ) } ) );`,
		} } )();

		const response = await ctx.get( "/foo" );

		response.body.toString( "utf8" ).should.be.equal( "Hello!" );
	} );
} );
