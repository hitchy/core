import { describe, it, after, before } from "mocha";
import "should";
import "should-http";
import SDT from "@hitchy/server-dev-tools";

import Core from "../../../../sdt.js";

const Test = await SDT( Core );

const config = {
	projectFolder: "test/projects/routing-basics",
	options: {
		scenario: "early-policies-sorting-specific-first",
		// debug: true,
	},
};

describe( "Serving project in basic-routing-core w/ declaring lists of policies with decreasing specificity", function() {
	const ctx = {};

	before( Test.before( ctx, config ) );
	after( Test.after( ctx ) );

	it( "passes early policies in proper order from generic policies to specific ones", function() {
		return ctx.get( "/prefix/check" )
			.then( response => {
				response.should.have.status( 200 );
				response.should.be.json();
			} );
	} );
} );

// NOTE Can not add another suite this time for using scenarios for dynamically
//      switching configuration to suit different testing goals in a single project.
