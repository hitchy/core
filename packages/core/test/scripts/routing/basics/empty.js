import { describe, it, after, before } from "mocha";
import "should";
import "should-http";
import SDT from "@hitchy/server-dev-tools";

import Core from "../../../../sdt.js";

const Test = await SDT( Core );

const config = {
	projectFolder: "test/projects/routing-basics",
	options: {
		scenario: "empty",
		// debug: true,
	},
};

describe( "Serving project in basic-routing-core w/o any application-specific routes", function() {
	const ctx = {};

	before( Test.before( ctx, config ) );
	after( Test.after( ctx ) );

	it( "misses GETting /", function() {
		return ctx.get( "/" )
			.then( response => {
				response.should.have.status( 404 );
			} );
	} );

	it( "GETs /blueprint/6789", function() {
		return ctx.get( "/blueprint/6789" )
			.then( response => {
				response.should.have.status( 200 );
				response.data.gotBlueprint.should.be.ok();
			} );
	} );
} );

// NOTE Can not add another suite this time for using scenarios for dynamically
//      switching configuration to suit different testing goals in a single project.
