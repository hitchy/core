import { describe, it, after, before } from "mocha";
import "should";
import "should-http";
import SDT from "@hitchy/server-dev-tools";

import Core from "../../../../sdt.js";

const Test = await SDT( Core );

const config = {
	projectFolder: "test/projects/routing-basics",
	options: {
		scenario: "late-policies-sorting-generic-first",
		// debug: true,
	},
};

describe( "Serving project in basic-routing-core w/ declaring lists of policies with increasing specificity", function() {
	const ctx = {};

	before( Test.before( ctx, config ) );
	after( Test.after( ctx ) );

	it( "passes late policies in proper order from specific policies to generic ones", function() {
		// request once to trigger some processing in late policies
		return ctx.get( "/prefix/check", {
			"x-start": 10,
		} )
			.then( response => {
				response.should.have.status( 200 );
				response.should.be.json();
				response.data.success.should.be.true();

				// request again to get the result of late policies processing previous request
				return ctx.get( "/prefix/check", {
					"x-start": 10,
				} );
			} )
			.then( response => {
				response.should.have.status( 200 );
				response.should.be.json();
				response.data.success.should.be.true();
				response.data.previousResult.should.be.Number().which.is.equal( 326 );
			} );
	} );
} );

// NOTE Can not add another suite this time for using scenarios for dynamically
//      switching configuration to suit different testing goals in a single project.
