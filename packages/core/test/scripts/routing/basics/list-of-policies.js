import { describe, it, after, before } from "mocha";
import "should";
import "should-http";
import SDT from "@hitchy/server-dev-tools";

import Core from "../../../../sdt.js";

const Test = await SDT( Core );

const config = {
	projectFolder: "test/projects/routing-basics",
	options: {
		scenario: "list-of-policies",
		// debug: true,
	},
};

describe( "Serving project in basic-routing-core w/ list of policies", () => {
	const ctx = {};

	before( Test.before( ctx, { ...config } ) );
	after( Test.after( ctx ) );

	it( "succeeds test in controller after passing all listed policies", async() => {
		const response = await ctx.get( "/listOfPolicies" );

		response.should.have.status( 200 );
		response.should.be.json();
	} );
} );

// NOTE Can not add another suite this time for using scenarios for dynamically
//      switching configuration to suit different testing goals in a single project.
