import { describe, it, after, before } from "mocha";
import "should";
import "should-http";
import SDT from "@hitchy/server-dev-tools";

import Core from "../../../../sdt.js";

const Test = await SDT( Core );

const config = {
	projectFolder: "test/projects/routing-query",
	options: {
		// debug: true,
	},
};

describe( "A query parameter in request URL", function() {
	const ctx = {};

	before( Test.before( ctx, config ) );
	after( Test.after( ctx ) );

	[ "get", "post", "put" ]
		.forEach( method => {
			it( `is exposed in policies (${method.toUpperCase()})`, async() => {
				const res = await ctx[method]( "/query?foo=bar" );

				res.headers["x-query-foo"].should.be.equal( "bar" );
			} );

			it( `is exposed in controllers (${method.toUpperCase()})`, async() => {
				const res = await ctx[method]( "/query?foo=bar" );

				res.data.foo.should.be.equal( "bar" );
			} );

			it( `supports percent encoding (${method.toUpperCase()})`, async() => {
				const res = await ctx[method]( "/query?f%6f%6F=bar%26baz" );

				res.headers["x-query-foo"].should.be.equal( "bar&baz" );
				res.data.foo.should.be.equal( "bar&baz" );
			} );
		} );
} );
