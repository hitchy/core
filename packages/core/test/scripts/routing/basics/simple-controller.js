import { describe, it, afterEach, beforeEach } from "mocha";
import "should";
import "should-http";
import SDT from "@hitchy/server-dev-tools";

import Core from "../../../../sdt.js";

const Test = await SDT( Core );

const config = {
	projectFolder: "test/projects/routing-basics",
	options: {
		scenario: "simple-controller",
		// debug: true,
	},
};

describe( "Serving project in basic-routing-core w/ most simple controller route", () => {
	const ctx = {};

	beforeEach( Test.before( ctx, { ...config } ) );
	afterEach( Test.after( ctx ) );

	it( "GETs /instant", async() => {
		const response = await ctx.get( "/instant" );

		response.should.have.status( 200 );
		response.should.be.json();
		response.data.method.should.equal( "GET" );
		response.data.params.should.be.empty();
		response.data.query.should.be.empty();
		response.data.args.should.be.empty();
		response.data.type.should.be.equal( "instant" );
	} );

	it( "GETs /partial/deferred", async() => {
		const response = await ctx.get( "/partial/deferred" );

		response.should.have.status( 200 );
		response.should.be.json();
		response.data.method.should.equal( "GET" );
		response.data.params.should.be.empty();
		response.data.query.should.be.empty();
		response.data.args.should.be.empty();
		response.data.type.should.be.equal( "deferred" );
	} );

	it( "GETs /full/deferred", async() => {
		const response = await ctx.get( "/full/deferred" );

		response.should.have.status( 200 );
		response.should.be.json();
		response.data.method.should.equal( "GET" );
		response.data.params.should.be.empty();
		response.data.query.should.be.empty();
		response.data.args.should.be.empty();
		response.data.type.should.be.equal( "deferred" );
	} );
} );

// NOTE Can not add another suite this time for using scenarios for dynamically
//      switching configuration to suit different testing goals in a single project.
