import { describe, it, after, before } from "mocha";
import Should from "should";
import "should-http";
import SDT from "@hitchy/server-dev-tools";

import Core from "../../../sdt.js";

const Test = await SDT( Core );

const config = {
	projectFolder: "test/projects/core-only",
	options: {
		// debug: true,
	},
};

const configWithArgs = args => ( {
	...config,
	options: {
		...config.options,
		arguments: args,
	}
} );

describe( "Serving core-only project via expressjs w/ simple controllers and policies", function() {
	const ctx = {};

	before( Test.before( ctx, configWithArgs( { injector: "express" } ) ) );
	after( Test.after( ctx ) );

	it( "GETs /", function() {
		return ctx.get( "/" )
			.then( response => {
				response.should.have.status( 200 );
				response.should.be.html();
				response.text.should.be.String().and.match( /\bwelcome\b/i ).and.match( /<p>/i );
			} );
	} );

	it( "misses POSTing /", function() {
		return ctx.post( "/" )
			.then( response => {
				response.should.have.value( "statusCode", 404 );
				response.should.be.html();
				response.text.should.be.String().and.match( /\bCannot\s+POST\b/i ).and.match( /<html\b/i );
			} );
	} );

	it( "misses GETting /view", function() {
		return ctx.get( "/view" )
			.then( response => {
				response.should.have.value( "statusCode", 404 );
				response.should.be.html();
				response.text.should.be.String().and.match( /\bCannot\s+GET\b/i ).and.match( /<html\b/i );
			} );
	} );

	it( "misses POSTing /view", function() {
		return ctx.post( "/view" )
			.then( response => {
				response.should.have.value( "statusCode", 404 );
				response.should.be.html();
				response.text.should.be.String().and.match( /\bCannot\s+POST\b/i ).and.match( /<html\b/i );
			} );
	} );

	it( "GETs /view/read", function() {
		return ctx.get( "/view/read" )
			.then( response => {
				response.should.have.value( "statusCode", 200 );
				response.should.be.json();
				response.data.session.should.be.String().and.equal( "instant session!" );
				Should( response.data.id ).be.undefined();
			} );
	} );

	it( "POSTs /view/read", function() {
		return ctx.post( "/view/read" )
			.then( response => {
				response.should.have.value( "statusCode", 200 );
				response.should.be.json();
				response.data.session.should.be.String().and.equal( "instant session!" );
				Should( response.data.id ).be.undefined();
			} );
	} );

	it( "GETs /view/read/1234", function() {
		return ctx.get( "/view/read/1234" )
			.then( response => {
				response.should.have.value( "statusCode", 200 );
				response.should.be.json();
				Should( response.data.session ).be.undefined();
				response.data.id.should.be.String().and.equal( "1234" );
			} );
	} );

	it( "POSTs /view/read/1234", function() {
		return ctx.post( "/view/read/1234" )
			.then( response => {
				response.should.have.value( "statusCode", 200 );
				response.should.be.json();
				response.data.session.should.be.String().and.equal( "promised session!" );
				response.data.id.should.be.String().and.equal( "1234" );
			} );
	} );

	it( "GETs /view/create", function() {
		return ctx.get( "/view/create" )
			.then( response => {
				response.should.have.value( "statusCode", 200 );
				response.should.be.json();
				response.data.session.should.be.String().and.equal( "promised session!" );
				Should( response.data.id ).be.undefined();
				Should( response.data.name ).be.undefined();
			} );
	} );

	it( "GETs /view/create/someId", function() {
		return ctx.get( "/view/create/someId" )
			.then( response => {
				response.should.have.value( "statusCode", 200 );
				response.should.be.json();
				response.data.session.should.be.String().and.equal( "promised session!" );
				Should( response.data.id ).be.undefined();
				response.data.name.should.be.String().and.equal( "someId" );
			} );
	} );

	it( "POSTs /view/create/someSimpleName?extra=1", function() {
		return ctx.post( "/view/create/someSimpleName?extra=1" )
			.then( response => {
				response.should.have.value( "statusCode", 200 );
				response.should.be.json();
				response.data.session.should.be.String().and.equal( "promised session!" );
				Should( response.data.id ).be.undefined();
				response.data.name.should.be.Array().and.eql( ["someSimpleName"] );
				response.data.extra.should.be.String().and.equal( "1" );
			} );
	} );

	it( "POSTs /view/create/some/complex/name?extra[]=foo&extra[]=bar", function() {
		return ctx.post( "/view/create/some/complex/name?extra[]=foo&extra[]=bar" )
			.then( response => {
				response.should.have.value( "statusCode", 200 );
				response.should.be.json();
				response.data.session.should.be.String().and.equal( "promised session!" );
				Should( response.data.id ).be.undefined();
				response.data.name.should.be.Array().and.eql( [ "some", "complex", "name" ] );
				response.data.extra.should.be.Array().and.eql( [ "foo", "bar" ] );
			} );
	} );
} );

describe( "Serving core-only project via expressjs w/ prefix w/ simple controllers and policies", function() {
	const ctx = {};

	before( Test.before( ctx, configWithArgs( { injector: "express", prefix: "/injected/hitchy" } ) ) );
	after( Test.after( ctx ) );

	it( "GETs /injected/hitchy/", function() {
		return ctx.get( "/injected/hitchy/" )
			.then( response => {
				response.should.have.status( 200 );
				response.should.be.html();
				response.text.should.be.String().and.match( /\bwelcome\b/i ).and.match( /<p>/i );
			} );
	} );

	it( "misses POSTing /injected/hitchy/", function() {
		return ctx.post( "/injected/hitchy/" )
			.then( response => {
				response.should.have.value( "statusCode", 404 );
				response.should.be.html();
				response.text.should.be.String().and.match( /\bCannot\s+POST\b/i ).and.match( /<html\b/i );
			} );
	} );

	it( "misses GETting /injected/hitchy/view", function() {
		return ctx.get( "/injected/hitchy/view" )
			.then( response => {
				response.should.have.value( "statusCode", 404 );
				response.should.be.html();
				response.text.should.be.String().and.match( /\bCannot\s+GET\b/i ).and.match( /<html\b/i );
			} );
	} );

	it( "misses POSTing /injected/hitchy/view", function() {
		return ctx.post( "/injected/hitchy/view" )
			.then( response => {
				response.should.have.value( "statusCode", 404 );
				response.should.be.html();
				response.text.should.be.String().and.match( /\bCannot\s+POST\b/i ).and.match( /<html\b/i );
			} );
	} );

	it( "GETs /injected/hitchy/view/read", function() {
		return ctx.get( "/injected/hitchy/view/read" )
			.then( response => {
				response.should.have.value( "statusCode", 200 );
				response.should.be.json();
				response.data.session.should.be.String().and.equal( "instant session!" );
				Should( response.data.id ).be.undefined();
			} );
	} );

	it( "POSTs /injected/hitchy/view/read", function() {
		return ctx.post( "/injected/hitchy/view/read" )
			.then( response => {
				response.should.have.value( "statusCode", 200 );
				response.should.be.json();
				response.data.session.should.be.String().and.equal( "instant session!" );
				Should( response.data.id ).be.undefined();
			} );
	} );

	it( "GETs /injected/hitchy/view/read/1234", function() {
		return ctx.get( "/injected/hitchy/view/read/1234" )
			.then( response => {
				response.should.have.value( "statusCode", 200 );
				response.should.be.json();
				Should( response.data.session ).be.undefined();
				response.data.id.should.be.String().and.equal( "1234" );
			} );
	} );

	it( "POSTs /injected/hitchy/view/read/1234", function() {
		return ctx.post( "/injected/hitchy/view/read/1234" )
			.then( response => {
				response.should.have.value( "statusCode", 200 );
				response.should.be.json();
				response.data.session.should.be.String().and.equal( "promised session!" );
				response.data.id.should.be.String().and.equal( "1234" );
			} );
	} );

	it( "GETs /injected/hitchy/view/create", function() {
		return ctx.get( "/injected/hitchy/view/create" )
			.then( response => {
				response.should.have.value( "statusCode", 200 );
				response.should.be.json();
				response.data.session.should.be.String().and.equal( "promised session!" );
				Should( response.data.id ).be.undefined();
				Should( response.data.name ).be.undefined();
			} );
	} );

	it( "GETs /injected/hitchy/view/create/someId", function() {
		return ctx.get( "/injected/hitchy/view/create/someId" )
			.then( response => {
				response.should.have.value( "statusCode", 200 );
				response.should.be.json();
				response.data.session.should.be.String().and.equal( "promised session!" );
				Should( response.data.id ).be.undefined();
				response.data.name.should.be.String().and.equal( "someId" );
			} );
	} );

	it( "POSTs /injected/hitchy/view/create/someSimpleName?extra=1", function() {
		return ctx.post( "/injected/hitchy/view/create/someSimpleName?extra=1" )
			.then( response => {
				response.should.have.value( "statusCode", 200 );
				response.should.be.json();
				response.data.session.should.be.String().and.equal( "promised session!" );
				Should( response.data.id ).be.undefined();
				response.data.name.should.be.Array().and.eql( ["someSimpleName"] );
				response.data.extra.should.be.String().and.equal( "1" );
			} );
	} );

	it( "POSTs /injected/hitchy/view/create/some/complex/name?extra[]=foo&extra[]=bar", function() {
		return ctx.post( "/injected/hitchy/view/create/some/complex/name?extra[]=foo&extra[]=bar" )
			.then( response => {
				response.should.have.value( "statusCode", 200 );
				response.should.be.json();
				response.data.session.should.be.String().and.equal( "promised session!" );
				Should( response.data.id ).be.undefined();
				response.data.name.should.be.Array().and.eql( [ "some", "complex", "name" ] );
				response.data.extra.should.be.Array().and.eql( [ "foo", "bar" ] );
			} );
	} );
} );
