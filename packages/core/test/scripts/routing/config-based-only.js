import { describe, it, after, before } from "mocha";
import "should";
import "should-http";
import SDT from "@hitchy/server-dev-tools";

import Core from "../../../sdt.js";

const Test = await SDT( Core );

const config = {
	projectFolder: "test/projects/core-only",
	options: {
		// debug: true,
	},
};

describe( "Serving core-only project w/ simple controllers and policies", () => {
	const ctx = {};

	before( Test.before( ctx, { ...config } ) );
	after( Test.after( ctx ) );

	it( "GETs /", async() => {
		const response = await ctx.get( "/" );

		response.should.have.status( 200 );
		response.should.be.html();
		response.text.should.be.String().and.match( /\bwelcome\b/i ).and.match( /<p>/i );
	} );

	it( "misses POSTing /", async() => {
		const response = await ctx.post( "/" );

		response.should.have.value( "statusCode", 404 );
		response.should.be.html();
		response.text.should.be.String().and.match( /\bnot\s+found\b/i ).and.match( /<html\b/i );
	} );

	it( "misses GETting /view", async() => {
		const response = await ctx.get( "/view" );

		response.should.have.value( "statusCode", 404 );
		response.should.be.html();
		response.text.should.be.String().and.match( /\bnot\s+found\b/i ).and.match( /<html\b/i );
	} );

	it( "misses POSTing /view", async() => {
		const response = await ctx.post( "/view" );

		response.should.have.value( "statusCode", 404 );
		response.should.be.html();
		response.text.should.be.String().and.match( /\bnot\s+found\b/i ).and.match( /<html\b/i );
	} );

	it( "GETs /view/read", async() => {
		const response = await ctx.get( "/view/read" );

		response.should.have.value( "statusCode", 200 );
		response.should.be.json();
		response.data.session.should.be.String().and.equal( "instant session!" );
		( response.data.id == null ).should.be.true();
	} );

	it( "POSTs /view/read", async() => {
		const response = await ctx.post( "/view/read" );

		response.should.have.value( "statusCode", 200 );
		response.should.be.json();
		response.data.session.should.be.String().and.equal( "instant session!" );
		( response.data.id == null ).should.be.true();
	} );

	it( "GETs /view/read/1234", async() => {
		const response = await ctx.get( "/view/read/1234" );

		response.should.have.value( "statusCode", 200 );
		response.should.be.json();
		( response.data.session == null ).should.be.true();
		response.data.id.should.be.String().and.equal( "1234" );
	} );

	it( "POSTs /view/read/1234", async() => {
		const response = await ctx.post( "/view/read/1234" );

		response.should.have.value( "statusCode", 200 );
		response.should.be.json();
		response.data.session.should.be.String().and.equal( "promised session!" );
		response.data.id.should.be.String().and.equal( "1234" );
	} );

	it( "GETs /view/create", async() => {
		const response = await ctx.get( "/view/create" );

		response.should.have.value( "statusCode", 200 );
		response.should.be.json();
		response.data.session.should.be.String().and.equal( "promised session!" );
		( response.data.id == null ).should.be.true();
		( response.data.name == null ).should.be.true();
	} );

	it( "GETs /view/create/someId", async() => {
		const response = await ctx.get( "/view/create/someId" );

		response.should.have.value( "statusCode", 200 );
		response.should.be.json();
		response.data.session.should.be.String().and.equal( "promised session!" );
		( response.data.id == null ).should.be.true();
		response.data.name.should.be.String().and.equal( "someId" );
	} );

	it( "POSTs /view/create/someSimpleName?extra=1", async() => {
		const response = await ctx.post( "/view/create/someSimpleName?extra=1" );

		response.should.have.value( "statusCode", 200 );
		response.should.be.json();
		response.data.session.should.be.String().and.equal( "promised session!" );
		( response.data.id == null ).should.be.true();
		response.data.name.should.be.Array().and.eql( ["someSimpleName"] );
		response.data.extra.should.be.String().and.equal( "1" );
	} );

	it( "POSTs /view/create/some/complex/name?extra[]=foo&extra[]=bar", async() => {
		const response = await ctx.post( "/view/create/some/complex/name?extra[]=foo&extra[]=bar" );

		response.should.have.value( "statusCode", 200 );
		response.should.be.json();
		response.data.session.should.be.String().and.equal( "promised session!" );
		( response.data.id == null ).should.be.true();
		response.data.name.should.be.Array().and.eql( [ "some", "complex", "name" ] );
		response.data.extra.should.be.Array().and.eql( [ "foo", "bar" ] );
	} );
} );
