import { describe, before, after, it } from "mocha";
import "should";
import SDT from "@hitchy/server-dev-tools";

import Core from "../../../sdt.js";

const Test = await SDT( Core );

describe( "Selecting controller by name", () => {
	const ctx = {};

	after( Test.after( ctx ) );
	before( Test.before( ctx, {
		tools: Test,
		projectFolder: false,
		files: {
			"package.json": `{"type":"commonjs"}`,
			"api/controller/foo.cjs": `class FooController {
				static a( req, res ) { this.b( req, res ); }
				static b( req, res ) { res.send( "bar " + typeof this.a + " " + this.a.name + " " + ( "/foo" in this.config.routes ) ); }

				// request context API is preferred over elements of controller with the same name   
				static config = { routes: {} }; 
			} module.exports = () => FooController;`,
			"api/policies/foo.cjs": `class FooPolicy { 
				static c( req, res, next ) { this.d( req, res, next ); } 
				static d( req, res, next ) { res.set( "x-baz", "bam " + typeof this.c + " " + this.c.name + " " + ( "/foo" in this.config.routes ) ); next(); } 

				// request context API is preferred over elements of controller with the same name   
				static config = { routes: {} }; 
			} module.exports = () => FooPolicy;`,
			"config/routes.js": `exports.routes = { "/foo": "foo::a" };`,
			"config/policies.js": `exports.policies = { "/": "foo::c" };`,
		},
		options: {},
	} ) );

	it( "merges request context with context of invoked controller/policy method", async() => {
		const { body, headers } = await ctx.get( "/foo" );

		body.toString( "utf8" ).should.be.equal( "bar function a true" );
		headers.should.have.property( "x-baz" ).which.is.equal( "bam function c true" );
	} );
} );
