import { describe, it, before, after } from "mocha";
import "should";
import SDT from "@hitchy/server-dev-tools";

import Core from "../../../sdt.js";

const Test = await SDT( Core );

const config = {
	projectFolder: "test/projects/merging-routings",
	options: {
		// debug: true,
	},
};

describe( "Merging policies", () => {
	const ctx = {};

	before( Test.before( ctx, { ...config } ) );
	after( Test.after( ctx ) );

	it( "should expose policies for either supported stage", () => {
		const { policies } = ctx.hitchy.api.router;

		policies.should.be.Object().which.has.properties( "before", "after" );

		const beforeScalars = policies.before.onMethod( "GET" ).onPrefix( "/scalar/stuff" );

		beforeScalars.map( i => i.handler() ).join( "," ).should.be.equal( "early,plugin-a,plugin-c,plugin-b,plugin-d,before" );

		const afterScalars = policies.after.onMethod( "GET" ).onPrefix( "/scalar/stuff" );

		afterScalars.should.be.Array().which.has.length( 4 );
		afterScalars.map( i => i.handler() ).join( "," ).should.be.equal( "after,plugin-d,plugin-b,late" );

		const beforeArrays = policies.before.onMethod( "GET" ).onPrefix( "/array/stuff" );

		beforeArrays.should.be.Array().which.has.length( 6 );
		beforeArrays.map( i => i.handler() ).join( "," ).should.be.equal( "early,plugin-a,plugin-c,plugin-b,plugin-d,before" );

		const afterArrays = policies.after.onMethod( "GET" ).onPrefix( "/array/stuff" );

		afterArrays.should.be.Array().which.has.length( 4 );
		afterArrays.map( i => i.handler() ).join( "," ).should.be.equal( "after,plugin-d,plugin-b,late" );
	} );

	it( "should expose subset of routes defined for either supported stage and plugin using actually required routes, only", () => {
		const { controllers } = ctx.hitchy.api.router;

		const scalars = controllers.onMethod( "GET" ).onPrefix( "/scalar" );

		scalars.should.be.Array().which.has.length( 1 );
		scalars.map( i => i.handler() ).join( "," ).should.be.equal( "early" );

		const arrays = controllers.onMethod( "GET" ).onPrefix( "/array" );

		arrays.should.be.Array().which.is.empty();
	} );
} );
