import { describe, it, after, before } from "mocha";
import "should";
import "should-http";
import SDT from "@hitchy/server-dev-tools";

import Core from "../../../sdt.js";

const Test = await SDT( Core );

const config = {
	projectFolder: "test/projects/invalid-responder-routes",
	options: {
		// debug: true,
	},
};

describe( "Serving project with invalid responder routes", () => {
	const ctx = {};

	before( Test.before( ctx, { ...config } ) );
	after( Test.after( ctx ) );

	it( "GETs /test", async() => {
		const response = await ctx.get( "/test" );

		response.should.have.status( 200 );
		response.should.be.json();
		response.data.mode.should.be.String().and.eql( "index" );
	} );

	it( "misses GETting /missing-controller", async() => {
		const response = await ctx.get( "/missing-controller" );

		response.should.have.status( 404 );
	} );

	it( "misses GETting /missing-method", async() => {
		const response = await ctx.get( "/missing-method" );

		response.should.have.status( 404 );
	} );

	it( "GETs /something", async() => {
		const response = await ctx.get( "/something" );

		response.should.have.status( 200 );
		response.should.be.json();
		response.data.mode.should.be.String().and.eql( "something" );
	} );

	it( "GETs /addon", async() => {
		const response = await ctx.get( "/addon" );

		response.should.have.status( 200 );
		response.should.be.json();
		response.data.mode.should.be.String().and.eql( "addon" );
	} );
} );
