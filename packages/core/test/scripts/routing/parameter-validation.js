import { describe, afterEach, it } from "mocha";
import "should";
import "should-http";
import SDT from "@hitchy/server-dev-tools";

import Core from "../../../sdt.js";

const Test = await SDT( Core );

const config = {
	tools: Test,
	projectFolder: false,
	options: {
		// debug: true,
	},
};

describe( "Routing parameter validation", () => {
	const ctx = {};

	const setup = type => Test.before( ctx, {
		...config,
		files: {
			"config/routes.cjs": `exports.routes = { '/foo/:"input.${type}"': ( req, res ) => res.send( "got " + req.params.input ) };`,
		}
	} );

	afterEach( Test.after( ctx ) );

	it( "skips handler on queries with parameter values mismatching required UUID format", async() => {
		await setup( "uuid" )();

		let res = await ctx.get( "/foo/12345678-1234-1234-1234-1234567890a" );
		res.should.have.status( 404 );

		res = await ctx.get( "/foo/12345678-1234-1234-1234-1234567890ab" );
		res.should.have.status( 200 );
		res.body.toString( "utf-8" ).should.be.equal( "got 12345678-1234-1234-1234-1234567890ab" );
	} );

	it( "skips handler on queries with parameter values mismatching required integer format", async() => {
		await setup( "integer" )();

		let res = await ctx.get( "/foo/bar" );
		res.should.have.status( 404 );

		res = await ctx.get( "/foo/0" );
		res.should.have.status( 200 );
		res.body.toString( "utf-8" ).should.be.equal( "got 0" );

		res = await ctx.get( "/foo/-0" );
		res.should.have.status( 200 );
		res.body.toString( "utf-8" ).should.be.equal( "got -0" );

		res = await ctx.get( "/foo/1234567890123456" );
		res.should.have.status( 200 );
		res.body.toString( "utf-8" ).should.be.equal( "got 1234567890123456" );

		res = await ctx.get( "/foo/-1234567890123456" );
		res.should.have.status( 200 );
		res.body.toString( "utf-8" ).should.be.equal( "got -1234567890123456" );

		res = await ctx.get( "/foo/+1234567890123456" );
		res.should.have.status( 200 );
		res.body.toString( "utf-8" ).should.be.equal( "got +1234567890123456" );

		res = await ctx.get( "/foo/12345678901234567" );
		res.should.have.status( 404 );
	} );

	it( "skips handler on queries with parameter values mismatching required number format", async() => {
		await setup( "number" )();

		let res = await ctx.get( "/foo/bar" );
		res.should.have.status( 404 );

		res = await ctx.get( "/foo/0" );
		res.should.have.status( 200 );
		res.body.toString( "utf-8" ).should.be.equal( "got 0" );

		res = await ctx.get( "/foo/-0" );
		res.should.have.status( 200 );
		res.body.toString( "utf-8" ).should.be.equal( "got -0" );

		res = await ctx.get( "/foo/1234567890123456" );
		res.should.have.status( 200 );
		res.body.toString( "utf-8" ).should.be.equal( "got 1234567890123456" );

		res = await ctx.get( "/foo/-1234567890123456" );
		res.should.have.status( 200 );
		res.body.toString( "utf-8" ).should.be.equal( "got -1234567890123456" );

		res = await ctx.get( "/foo/+1234567890123456" );
		res.should.have.status( 200 );
		res.body.toString( "utf-8" ).should.be.equal( "got +1234567890123456" );

		res = await ctx.get( "/foo/12345678901234567" );
		res.should.have.status( 404 );

		res = await ctx.get( "/foo/0.0" );
		res.should.have.status( 200 );
		res.body.toString( "utf-8" ).should.be.equal( "got 0.0" );

		res = await ctx.get( "/foo/.0" );
		res.should.have.status( 404 );

		res = await ctx.get( "/foo/1." );
		res.should.have.status( 404 );

		res = await ctx.get( "/foo/-0.0" );
		res.should.have.status( 200 );
		res.body.toString( "utf-8" ).should.be.equal( "got -0.0" );

		res = await ctx.get( "/foo/+0.0" );
		res.should.have.status( 200 );
		res.body.toString( "utf-8" ).should.be.equal( "got +0.0" );

		res = await ctx.get( "/foo/1234567890123456.1234567890123456" );
		res.should.have.status( 200 );
		res.body.toString( "utf-8" ).should.be.equal( "got 1234567890123456.1234567890123456" );

		res = await ctx.get( "/foo/12345678901234567.1234567890123456" );
		res.should.have.status( 404 );

		res = await ctx.get( "/foo/1234567890123456.12345678901234567" );
		res.should.have.status( 404 );
	} );

	it( "skips handler on queries with parameter values mismatching required keyword format", async() => {
		await setup( "keyword" )();

		let res = await ctx.get( "/foo/bar" );
		res.should.have.status( 200 );
		res.body.toString( "utf-8" ).should.be.equal( "got bar" );

		res = await ctx.get( "/foo/0" );
		res.should.have.status( 404 );

		res = await ctx.get( "/foo/-0" );
		res.should.have.status( 404 );

		res = await ctx.get( "/foo/1234567890123456" );
		res.should.have.status( 404 );

		res = await ctx.get( "/foo/.1234567890123456" );
		res.should.have.status( 404 );

		res = await ctx.get( "/foo/_1234567890123456" );
		res.should.have.status( 200 );
		res.body.toString( "utf-8" ).should.be.equal( "got _1234567890123456" );
	} );

	it( "skips handler on queries with parameter values mismatching required custom format lacking registered validator", async() => {
		await setup( "foo" )();

		let res = await ctx.get( "/foo/bar" );
		res.should.have.status( 404 );

		res = await ctx.get( "/foo/0" );
		res.should.have.status( 404 );

		res = await ctx.get( "/foo/foo" );
		res.should.have.status( 404 );

		res = await ctx.get( "/foo/123" );
		res.should.have.status( 404 );

		res = await ctx.get( "/foo/123.123" );
		res.should.have.status( 404 );
	} );
} );
