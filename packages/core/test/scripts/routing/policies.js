import { describe, it, beforeEach, afterEach } from "mocha";
import "should";
import "should-http";
import SDT from "@hitchy/server-dev-tools";

import Core from "../../../sdt.js";

const Test = await SDT( Core );

const config = {
	projectFolder: "test/projects/policies",
	options: {
		// debug: true,
	},
};

describe( "Policy routing", () => {
	const ctx = {};

	beforeEach( Test.before( ctx, { ...config } ) );
	afterEach( Test.after( ctx ) );

	for ( const method of [ "GET", "PUT", "POST", "PATCH", "DELETE", "HEAD", "OPTIONS", "TRACE" ] ) {
		for ( const path of [ "/prefix/deep/path", "/prefix/deep/", "/prefix/deep", "/prefix/", "/prefix" ] ) {
			it( `is involved on dispatching HTTP request ${method} ${path}`, async() => {
				const response = await ctx[method.toLowerCase()]( path );

				response.should.have.status( 200 );
				response.headers["x-policy"].should.be.String().and.equal( "responded" );
			} );
		}
	}
} );
