import { describe, it, beforeEach, afterEach } from "mocha";
import "should";
import "should-http";
import SDT from "@hitchy/server-dev-tools";

import Core from "../../sdt.js";

const Test = await SDT( Core );

const config = {
	projectFolder: "test/projects/empty",
	options: {
		// debug: true,
	},
};

describe( "Testing", () => {
	const ctx = {};

	beforeEach( Test.before( ctx, { ...config } ) );
	afterEach( Test.after( ctx ) );

	it( "is running test", async() => {
		const api = ctx.hitchy.api; // eslint-disable-line consistent-this

		// TODO put your test code relying on API here
		await Promise.resolve();
		api.service.should.be.ok();
	} );
} );
