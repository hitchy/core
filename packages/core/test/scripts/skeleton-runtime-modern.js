import { describe, before, after, it } from "mocha";
import "should";
import SDT from "@hitchy/server-dev-tools";

import Core from "../../sdt.js";

const Test = await SDT( Core );

const config = {
	tools: Test,
	projectFolder: false,
	files: {
		"config/routes.cjs": `exports.routes = { "/foo": ( _, res ) => res.send( "Hello!" ) };`,
	},
	options: {
		// debug: true,
	},
};

describe( "Testing with server-dev-tools faking project", () => {
	const ctx = {};

	before( Test.before( ctx, config ) );
	after( Test.after( ctx ) );

	it( "is supported", () => {
		return ctx.get( "/foo" )
			.then( response => {
				response.body.toString( "utf8" ).should.be.equal( "Hello!" );
			} );
	} );
} );
