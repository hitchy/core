import { describe, it, after, before } from "mocha";
import "should";
import "should-http";
import SDT from "@hitchy/server-dev-tools";

import Core from "../../sdt.js";

const Test = await SDT( Core );

const config = {
	projectFolder: "test/projects/core-only",
	options: {
		// debug: true,
	},
};

describe( "Testing runtime", function() {
	const ctx = {};

	before( Test.before( ctx, config, { /* injector: "express", prefix: "/mount/path" */ } ) );
	after( Test.after( ctx ) );

	it( "is assessing request", () => {
		return ctx.get( "/" )
			.then( response => { // eslint-disable-line no-unused-vars
				/*
				response.should.have.status( 200 );
				response.should.be.html();
				response.text.should.be.String().and.match( /\bwelcome\b/i ).and.match( /<p>/i );
				*/
			} );
	} );
} );
