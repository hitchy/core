#!/usr/bin/env node
import { parseArgs } from "node:util";
import prompts from "prompts";
import { ptnFolderName } from "./lib/data.js";
import { runner } from "./lib/runner.js";

const { values: args, positionals } = parseArgs( {
	args: process.argv.slice( 2 ),
	options: {},
	strict: false,
} );

const targetFolder = positionals[0];

const ifFrontend = ( ifYes, ifNo = null ) => ( _, { mode } ) => ( mode === "none" ? ifNo : ifYes );
const ifUseAuth = ( ifYes, ifNo = null ) => ( _, { auth } ) => ( auth ? ifYes : ifNo );
const ifUseOdem = ( ifYes, ifNo = null ) => ( _, { auth, odem } ) => ( auth || odem ? ifYes : ifNo );

const steps = [
	{
		type: targetFolder ? null : "text",
		name: "folder",
		message: "Provide name of sub-folder to contain your project:",
		initial: "my-app",
		validate: value => ptnFolderName.test( value ) || "Use letters, digits, dashes and underscores, only." ,
	},
	{
		type: "select",
		name: "mode",
		message: "Select frontend to integrate Hitchy with:",
		choices: [
			{ value: "none", title: "none", description: "Hitchy-only backend" },
			{ value: "vue", title: "Vue3", description: "Hitchy as backend, Vue3 as frontend" },
			// { value: "vitepress", title: "VitePress", description: "Hitchy as backend, VitePress as frontend" },
		],
	},
	{
		type: "toggle",
		name: "auth",
		message: "Hitchy: Do you intend to authenticate users?",
		initial: false,
		active: "Yes",
		inactive: "No",
	},
	{
		type: ifUseOdem( null, "toggle" ),
		name: "odem",
		message: "Hitchy: Do you intend to manage custom data models?",
		initial: true,
		active: "Yes",
		inactive: "No",
	},
	{
		type: ifUseOdem( "select" ),
		name: "odem-backend",
		message: "Hitchy: Where do you want to store the custom data models?",
		choices: [
			{ value: "subfolder", title: "subfolder", description: "data is stored in a local subfolder" },
			{ value: "etcd", title: "etcd", description: "data is stored in an etcd cluster" },
			{ value: "memory", title: "memory", description: "data is stored in volatile runtime memory, only" },
		],
	},
	{
		type: ifUseOdem( "toggle" ),
		name: "odem-rest",
		message: "Hitchy: Do you intend to expose custom data models via REST API?",
		initial: true,
		active: "Yes",
		inactive: "No",
	},
	{
		type: ifUseOdem( "toggle" ),
		name: "odem-websocket",
		message: "Hitchy: Do you intend to expose custom data models via web socket?",
		initial: false,
		active: "Yes",
		inactive: "No",
	},
	{
		type: ifUseAuth( null, "toggle" ),
		name: "session",
		message: "Hitchy: Do you intend to use server-side sessions?",
		initial: false,
		active: "Yes",
		inactive: "No",
	},
	{
		type: ifFrontend( null, "toggle" ),
		name: "static",
		message: "Hitchy: Do you intend to expose static files in a local folder?",
		initial: false,
		active: "Yes",
		inactive: "No",
	},
	{
		type: "toggle",
		name: "proxy",
		message: "Hitchy: Do you intend to locally expose assets and APIs of remote services?",
		initial: false,
		active: "Yes",
		inactive: "No",
	},
	{
		type: "toggle",
		name: "mailer",
		message: "Hitchy: Do you intend to send mails?",
		initial: false,
		active: "Yes",
		inactive: "No",
	},
	{
		type: ifFrontend( "toggle", null ),
		name: "shoelace",
		message: "Extra: Do you want to use Shoelace with the frontend?",
		initial: false,
		active: "Yes",
		inactive: "No",
	},
	{
		type: ifFrontend( "toggle", null ),
		name: "fontawesome",
		message: "Extra: Do you want to use FontAwesome with the frontend?",
		initial: false,
		active: "Yes",
		inactive: "No",
	},
	{
		type: "toggle",
		name: "eslint",
		message: "Development: Do you like to use ESLint with cepharum coding style?",
		initial: false,
		active: "Yes",
		inactive: "No",
	},
	{
		type: "toggle",
		name: "git",
		message: "Development: Do you intend to use git for versioning?",
		initial: true,
		active: "Yes",
		inactive: "No",
	},
];

const responses = {
	folder: targetFolder,
	...await prompts( steps )
};

if ( !responses.hasOwnProperty( steps[steps.length - 1].name ) ) {
	// user cancelled
	process.exit( 1 );
}

await runner( responses, positionals, args );
