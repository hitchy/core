export const ptnFolderName = /^(?:\.|(?:[a-z0-9_-]+\.)*[a-z0-9_-]+)$/i;

/**
 * Checks if provided name is suitable as name of folder to create.
 */
export function isValidFolderName( name ) {
	return ptnFolderName.test( name );
}
