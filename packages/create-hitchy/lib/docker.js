import { templateToFile } from "./template.js";

/**
 *
 */
export async function addDocker( config ) {
	await templateToFile( "docker/.dockerignore", config( ".dockerignore" ), config );
	await templateToFile( "docker/compose.yaml", config( "compose.yaml" ), config );

	if ( config.mode === "none" ) {
		await templateToFile( "docker/hitchy.Dockerfile", config( "Dockerfile" ), config );
	} else {
		await templateToFile( "docker/vue.Dockerfile", config( "Dockerfile" ), config );
	}
}
