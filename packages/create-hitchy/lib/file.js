import { mkdir, readFile, writeFile, stat } from "node:fs/promises";
import { resolve } from "node:path";

/**
 *
 */
export async function exists( file ) {
	try {
		await stat( file );
		return true;
	} catch ( cause ) {
		if ( cause.code === "ENOENT" ) {
			return false;
		}

		throw cause;
	}
}

/**
 *
 */
export function qualify( config, pathname ) {
	return pathname.replace( /^\$(HITCHY|BASE)(?:\/(.+))?$/, ( _, marker, relative ) => {
		switch ( marker ) {
			case "HITCHY" :
				return resolve( config.folder, config.hitchyBaseFolder, relative || "" );
			case "BASE" :
				return resolve( config.folder, relative || "" );

			default : return _;
		}
	} );
}

/**
 *
 */
export async function createFolder( folder, { enter = true } = {} ) {
	await mkdir( folder, { recursive: true } );

	if ( enter ) {
		process.chdir( folder );
	}
}

/**
 *
 */
export async function createTextFile( file, content ) {
	try {
		await stat( file );

		throw new Error( "file exists already: " + file );
	} catch ( cause ) {
		if ( cause.code === "ENOENT" ) {
			await writeFile( file, String( content ).replace( /(\S)\s*$/, "$1\n" ), "utf-8" );
		} else {
			throw cause;
		}
	}
}

/**
 *
 */
export async function writeTextFile( file, content ) {
	await writeFile( file, String( content ).replace( /(\S)[ \t]*$/, "$1\n" ), "utf-8" );
}

/**
 *
 */
export async function editTextFile( file, editFn ) {
	await writeFile( file, String( await editFn( await readFile( file, "utf-8" ) ) ).replace( /(\S)[ \t]*$/, "$1\n" ), "utf-8" );
}

/**
 *
 */
export async function editJsonFile( file, editFn ) {
	await writeFile( file, JSON.stringify( await editFn( JSON.parse( await readFile( file, "utf-8" ) ) ), null, "\t" ), "utf-8" );
}

/**
 *
 */
export async function appendTextFile( file, lines ) {
	lines = String( lines ).replace( /(\S)[ \t]*$/, "$1\n" );

	await editTextFile( file, content => ( content.endsWith( "\n" ) ? content + lines : content ? content + "\n" + lines : lines ) );
}

/**
 *
 */
export async function replaceTextFile( file, pattern, replacement ) {
	let occurrences = 0;

	await editTextFile( file, content => content.replace( pattern, ( ...match ) => {
		occurrences++;

		return typeof replacement === "function" ? replacement( ...match ) : replacement;
	} ) );

	return occurrences > 0;
}
