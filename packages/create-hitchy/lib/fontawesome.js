import { npmInstallDev } from "./process.js";
import { createTextFile, exists, replaceTextFile } from "./file.js";
import { ptnAllLeadingImports } from "./tools.js";

/**
 *
 */
export async function addFontAwesomeToVue( config ) {
	if ( !config.fontawesome ) {
		return;
	}

	await npmInstallDev(
		"@fortawesome/fontawesome-svg-core",
		"@fortawesome/free-brands-svg-icons",
		"@fortawesome/free-regular-svg-icons",
		"@fortawesome/free-solid-svg-icons",
		"@fortawesome/vue-fontawesome@latest-3",
	);

	if ( config.mode === "vue" ) {
		await replaceTextFile( config( "$BASE/src/main.js" ), ptnAllLeadingImports, prefix => `${prefix}

import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import "./fontawesome.js";` );

		await replaceTextFile( config( "$BASE/src/main.js" ), /([ \t]*)(\.mount)/,
			( _, indent, mount ) => `${indent}.component( "FontAwesomeIcon", FontAwesomeIcon )${indent}${mount}` );

		await createTextFile( config( "$BASE/src/fontawesome.js" ), `import { library } from "@fortawesome/fontawesome-svg-core";

// import every icon you use
import { faFolderOpen, faUserSecret } from "@fortawesome/free-solid-svg-icons";

// then add either item to the library
library.add( 
	faFolderOpen,
	faUserSecret,
);

// eventually use them in your templates e.g. like this:
// <FontAwesomeIcon icon="fa-solid fa-user-secret"/>
` );


		const vueDemoFile = config( "$BASE/src/views/AboutView.vue" );

		if ( await exists( vueDemoFile ) ) {
			await replaceTextFile( vueDemoFile, /<\/h1>/, match => `${match}
    <FontAwesomeIcon icon="fa-solid fa-folder-open"></FontAwesomeIcon>` );
		}
	}
}
