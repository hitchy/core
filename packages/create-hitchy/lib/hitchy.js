import { posix } from "node:path";

import { npmInstall, npmInstallDev } from "./process.js";
import { appendTextFile, createFolder, replaceTextFile } from "./file.js";
import { renderTemplate, templateToFile } from "./template.js";

/**
 *
 */
export async function installHitchyDependencies( config ) {
	const plugins = [];

	if ( config.auth ) {
		plugins.push( "auth" );
	}

	if ( config.odem || config.auth ) {
		plugins.push( "odem" );

		if ( config["odem-rest"] ) {
			plugins.push( "odem-rest" );
		}

		if ( config["odem-websocket"] ) {
			plugins.push( "odem-socket.io", "socket.io" );
		}

		if ( config["odem-backend"] === "etcd" ) {
			plugins.push( "odem-etcd" );
		}
	}

	if ( config.auth || config.session ) {
		plugins.push( "cookies", "session" );
	}

	if ( config.static || config.mode !== "none" ) {
		plugins.push( "static" );
	}

	if ( config.proxy ) {
		plugins.push( "proxy" );
	}

	if ( config.mailer ) {
		plugins.push( "mailer" );
	}

	await npmInstall( "@hitchy/core", ...plugins.map( name => "@hitchy/plugin-" + name ) );

	if ( !config.proxy && config.mode !== "none" ) {
		await npmInstallDev( "@hitchy/plugin-proxy" );
	}
}

/**
 *
 */
export async function createHitchyFilesystem( config ) {
	await createFolder( config( "$HITCHY/api/controllers" ), { enter: false } );
	await createFolder( config( "$HITCHY/api/models" ), { enter: false } );
	await createFolder( config( "$HITCHY/api/policies" ), { enter: false } );
	await createFolder( config( "$HITCHY/api/services" ), { enter: false } );
	await createFolder( config( "$HITCHY/config" ), { enter: false } );

	await templateToFile( "hitchy/local.config.js", "$HITCHY/config/local.js", config );
	await templateToFile( "hitchy/routes.config.js", "$HITCHY/config/routes.js", config );
	await templateToFile( "hitchy/policies.config.js", "$HITCHY/config/policies.js", config );

	if ( config.auth ) {
		await templateToFile( "hitchy/auth.config.js", "$HITCHY/config/auth.js", config );
	}

	await templateToFile( "hitchy/hello.controller.js", "$HITCHY/api/controllers/hello.js", config );
	await templateToFile( "hitchy/foe-filter.policy.js", "$HITCHY/api/policies/foe-filter.js", config );
	await templateToFile( "hitchy/post.model.js", "$HITCHY/api/models/post.js", config );
	await templateToFile( "hitchy/tools.service.js", "$HITCHY/api/services/tools.js", config );
}

/**
 *
 */
export async function configureOdem( config ) {
	if ( !config.odem && !config.auth ) {
		return;
	}

	switch ( config["odem-backend"] ) {
		case "subfolder" :
			await appendTextFile( config( "$BASE/.gitignore" ), posix.join( config.hitchyBaseFolder, "data" ) );
			await templateToFile( "hitchy/subfolder-database.config.js", "$HITCHY/config/database.js", config );
			await appendTextFile( config( "$HITCHY/.env.dist" ), await renderTemplate( "hitchy/.env.dist-file", { config } ) );
			break;

		case "etcd" :
			await appendTextFile( config( "$BASE/.gitignore" ), posix.join( config.hitchyBaseFolder, "data" ) ); // used as fallback in configuration written in next line
			await templateToFile( "hitchy/etcd-database.config.js", "$HITCHY/config/database.js", config );
			await appendTextFile( config( "$HITCHY/.env.dist" ), await renderTemplate( "hitchy/.env.dist-etcd", { config } ) );
			await appendTextFile( config( "$HITCHY/.env.dist" ), await renderTemplate( "hitchy/.env.dist-file", { config } ) );
			break;

		case "memory" :
			// that's the default, so nothing needs to be done here
	}
}

/**
 *
 */
export async function configureHitchyVueIntegration( config ) {
	await replaceTextFile( config( "$BASE/vite.config.js" ), /(\s*}\s*\);?\s*)$/, ( _, tail ) => `
	build: {
		outDir: "server/public"
	},
	server: {
		port: 8080,
		hmr: {
			host: "localhost",
			port: 8080
		}
	},${tail}` );

	await templateToFile( "hitchy/vue-proxy.config.js", "$HITCHY/config/proxy.js", config );
	await templateToFile( "hitchy/vue-static.config.js", "$HITCHY/config/static.js", config );
}
