import { spawn } from "node:child_process";

export const npm = process.platform === "win32" ? "npm.cmd" : "npm";

/**
 *
 */
async function _invoke( command, args, quiet = false ) {
	if ( process.env.DEBUG ) {
		console.error( `invoking: ${command} ${args.join( " " )}\nin: ${process.cwd()}` );
	}

	const result = await new Promise( ( resolve, reject ) => {
		const stdout = [];
		const stderr = [];

		const child = spawn( command, args, {
			shell: true,
			cwd: process.cwd(),
			stdio: quiet ? "pipe" : "inherit",
		} );

		if ( quiet ) {
			child.stdout.on( "data", chunk => stdout.push( chunk ) );
			child.stderr.on( "data", chunk => stderr.push( chunk ) );
		}

		child.once( "error", reject );
		child.once( "close", code => {
			const info = {
				stdout: Buffer.concat( stdout ),
				stderr: Buffer.concat( stderr ),
				code
			};

			if ( code === 0 ) {
				resolve( info );
			} else {
				if ( quiet ) {
					console.log( info.stdout.toString( "utf8" ) ); // eslint-disable-line no-console
					console.error( info.stderr.toString( "utf8" ) );
				}

				reject( new Error( `${command} exited with ${code} on running in ${process.cwd()} with ${args.map( arg => `'${arg}'` ) }` ) );
			}
		} );
	} );

	if ( process.env.DEBUG ) {
		console.error( `exit code is ${result.code}` );
	}

	return result;
}

/**
 *
 */
export function invoke( command, ...args ) {
	return _invoke( command, args, false );
}

/**
 *
 */
export function invokeQuiet( command, ...args ) {
	return _invoke( command, args, !process.env.DEBUG );
}

/**
 *
 */
export async function npmInstall( ...deps ) {
	await invokeQuiet( npm, "install", ...deps );
}

/**
 *
 */
export async function npmInstallDev( ...deps ) {
	await invokeQuiet( npm, "install", "-D", ...deps );
}

/**
 *
 */
export async function gitInitialize() {
	await invokeQuiet( "git", "init", "." );
	await invokeQuiet( "git", "add", "." );
}
