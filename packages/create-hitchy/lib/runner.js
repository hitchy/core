import { resolve } from "node:path";
import { createConfig } from "./tools.js";

/**
 * Creates project based on provided responses to questions prompted to the user
 * to customize the intended result.
 */
export async function runner( responses, positionals, args ) {
	responses.project = responses.folder;
	responses.folder = resolve( process.cwd(), responses.folder );

	if ( responses.project === "." ) {
		responses.project = basename( responses.folder );
	}

	const config = createConfig( responses );

	try {
		const nextSteps = await ( await import( `../modes/${config.mode}.js` ) ).handler( config, positionals, args );

		// eslint-disable-next-line no-console
		console.log( `
Project has been created. Next steps:

${nextSteps}` );
	} catch ( cause ) {
		console.error( cause.message );
	}
}
