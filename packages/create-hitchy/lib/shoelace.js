import { npmInstallDev } from "./process.js";
import { appendTextFile, exists, replaceTextFile } from "./file.js";
import { ptnAllLeadingImports } from "./tools.js";

/**
 *
 */
export async function addShoelaceToVue( config ) {
	if ( !config.shoelace ) {
		return;
	}

	await npmInstallDev( "@shoelace-style/shoelace", "vite-plugin-static-copy" );

	if ( config.mode === "vue" ) {
		await replaceTextFile( config( "$BASE/src/main.js" ), ptnAllLeadingImports, prefix => `${prefix}

import { setBasePath } from "@shoelace-style/shoelace/dist/utilities/base-path.js";
import "@shoelace-style/shoelace/dist/themes/light.css";
import "@shoelace-style/shoelace/dist/themes/dark.css";

setBasePath( import.meta.env.BASE_URL + "shoelace/" );` );

		await replaceTextFile( config( "$BASE/vite.config.js" ), ptnAllLeadingImports, prefix => `${prefix}

import { viteStaticCopy } from "vite-plugin-static-copy";` );

		await replaceTextFile( config( "$BASE/vite.config.js" ), /(plugins:\s*\[[^\]]*)([ \t]*])/, ( _, before, after ) => `${before}viteStaticCopy( { 
      targets: [ 
        { 
          src: "node_modules/@shoelace-style/shoelace/dist/assets/", 
          dest: "shoelace/", 
        } 
      ] 
    } ),
${after}` );

		await replaceTextFile( config( "$BASE/vite.config.js" ), /(vue\()(\))/, ( _, before, after ) => `${before}{
      template: {
        compilerOptions: {
          // treat all tags with a dash as custom elements
          isCustomElement: (tag) => tag.includes('-')
        }
      }
    }${after}` );


		const vueDemoFile = config( "$BASE/src/views/AboutView.vue" );

		if ( await exists( vueDemoFile ) ) {
			await replaceTextFile( vueDemoFile, /<\/h1>/, match => `${match}
    <sl-button variant="primary">Click me!</sl-button>` );

			await appendTextFile( vueDemoFile, `
<script>
import "@shoelace-style/shoelace/dist/components/button/button.js";
</script>` );
		}
	}
}
