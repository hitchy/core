import { resolve } from "node:path";
import { fileURLToPath } from "node:url";
import { readFile } from "node:fs/promises";
import { createTextFile } from "./file.js";

/**
 *
 */
export async function interpolate( string, data ) {
	const result = [];
	const buffer = [];

	for ( const token of string.split( /(\{\{|}})/ ) ) {
		switch ( token ) {
			case "{{" :
				buffer.unshift( buffer.length ? [token] : [] );
				break;

			case "}}" :
				switch ( buffer.length ) {
					case 0 :
						result.push( token );
						break;

					case 1 :
						const marker = buffer.shift().join( "" );

						if ( marker === "" ) {
							result.push( "{{}}" );
						} else {
							let context = data;

							const split = marker.indexOf( ":" );
							const options = split > -1 ? marker.slice( split + 1 ) : "";
							const segments = split > -1 ? marker.substring( 0, split ) : marker;

							for ( const segment of segments.trim().split( "." ) ) {
								context = context[segment.trim()];

								if ( !context ) {
									break;
								}
							}

							let replacement;

							if ( typeof context === "function" ) {
								replacement = await context( marker ); // eslint-disable-line no-await-in-loop
							} else {
								replacement = String( context ?? "" );
							}

							if ( options ) {
								for ( const option of options.split( ":" ) ) {
									const match = /^([^=]+)=(.*)$/.exec( option.trim() );

									if ( match ) {
										switch ( match[1].trim().toLowerCase() ) {
											case "prefix" :
												if ( replacement ) {
													replacement = match[2].trim() + replacement;
												}
												break;

											case "suffix" :
												if ( replacement ) {
													replacement = replacement + match[2].trim();
												}
												break;
										}
									}
								}
							}

							result.push( replacement );
						}
						break;

					default :
						const level = buffer.shift();
						buffer[0].push( ...level );
				}
				break;

			default :
				if ( buffer.length ) {
					buffer[0].push( token );
				} else {
					result.push( token );
				}
		}
	}

	while ( buffer.length > 1 ) {
		const level = buffer.shift();
		buffer[0].push( ...level );
	}

	if ( buffer.length > 0 ) {
		result.push( "{{" + buffer[0].join( "" ) );
	}

	return result.join( "" );
}

/**
 *
 */
export async function renderTemplate( file, data ) {
	const path = resolve( fileURLToPath( import.meta.url ), "../templates", file );
	const template = await readFile( path, "utf-8" );

	return await interpolate( template, data );
}

/**
 *
 */
export async function templateToFile( source, target, config, data = {} ) {
	await createTextFile( config( target ), await renderTemplate( source, { ...data, config } ) );
}
