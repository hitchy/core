FROM node:lts-alpine

WORKDIR /app

COPY ./ /app/
RUN npm ci --omit=dev && \
    mkdir {{config.hitchyBaseFolder:suffix=/}}data && \
    chown node:node . -R

VOLUME /app/{{config.hitchyBaseFolder:suffix=/}}data
EXPOSE 3000
USER node

CMD ["npm", "start"]
