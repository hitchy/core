FROM node:lts-alpine AS frontend-build

WORKDIR /app

COPY ./ /app/

RUN mkdir -p /app/server/public
RUN npm ci
RUN npm run build


FROM node:lts-alpine

WORKDIR /app

COPY ./server/ /app/server/
COPY ./package.json /app/
COPY ./package-lock.json /app/
COPY --from=frontend-build /app/server/public/ /app/server/public/

RUN npm ci --omit=dev && \
    mkdir {{config.hitchyBaseFolder:suffix=/}}data && \
    chown node:node . -R

VOLUME /app/{{config.hitchyBaseFolder:suffix=/}}data
EXPOSE 3000
USER node

CMD ["npm", "start"]
