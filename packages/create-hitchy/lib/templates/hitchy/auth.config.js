/**
 * Configures Hitchy's auth plugin.
 *
 * Official documentation is at https://auth.hitchy.org/
 */
export default function( options ) {
	return {
		auth: {
			// configures an administrator account that will be created automatically on first start
			admin: {
				name: "john.doe",
				password: "my5ecr3t"
			},
			// declares user roles application is going to support
			roles: [ "guest", "customer", "manager" ],
			// maps a (tree of) resource(s) into rules declaring who may access and who must not
			authorizations: {
				// some examples for custom resources your application might implement
				// - read more about e.g. at https://auth.hitchy.org/api/service/authorization.html
				backup: "@managers",
				"backup.import": "+@admins, -john.doe",

				// carefully configure access permissions on managed data, next
				// - read more about it at https://www.npmjs.com/package/@hitchy/plugin-odem-rest#authorization
				"@hitchy.odem": "*",
				"@hitchy.odem.model.User": [ "-*", "@admin" ],
				"@hitchy.odem.model.Role": [ "-*", "@admin" ],
				"@hitchy.odem.model.User.property.password": ["-*"],
			},
		},
	};
}
