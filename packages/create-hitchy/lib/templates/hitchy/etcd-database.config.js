import { resolve } from "node:path";

/**
 * Implements configuration for managing data in an etcd cluster as soon as some
 * nodes are provided on command line or in environment variables. Otherwise,
 * data is stored in a local subfolder named data unless backend has been
 * invoked with either --memory or --test.
 */
export default function( { arguments: args, environment: env, projectFolder } ) {
	const api = this;
	const { services } = api;

	const logDebug = api.log( "{{config.project}}:config:database:debug" );

	const database = {};
	const etcdNodes = String( args.backend || env.ETCD_NODES || "" )
		.split( /[,;\s]+/ )
		.filter( i => i.trim() );

	if ( etcdNodes.length > 0 ) {
		const etcdOptions = {
			hosts: etcdNodes,
			retry: true,
			prefix: "prefix-of-authenticated-user"
		};

		if ( env.ETCD_USER && env.ETCD_PASSWORD ) {
			etcdOptions.auth = {
				username: env.ETCD_USER,
				password: env.ETCD_PASSWORD,
			};
		}

		if ( env.ETCD_PREFIX ) {
			etcdOptions.prefix = env.ETCD_PREFIX;
		}

		database.default = new services.OdemAdapterEtcd( etcdOptions );

		logDebug( "using etcd backend with options %j", etcdOptions );
	} else if ( !args.memory && !args.test ) {
		const fileOptions = {
			dataSource: resolve( projectFolder, args["database-folder"] || env.DATABASE_FOLDER || "data" ),
		};

		database.default = new services.OdemAdapterFile( fileOptions );

		logDebug( "using file-based backend with options %j", fileOptions );
	}

	return {
		database,
	};
};
