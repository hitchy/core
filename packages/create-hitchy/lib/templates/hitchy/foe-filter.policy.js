/**
 * This module implements so-called policy handlers meant to be involved in
 * handling certain incoming requests.
 *
 * Read more about it at https://core.hitchy.org/internals/components.html#policies
 */
export default function( options ) {
	const api = this;

	const logInfo = api.log( "{{config.project}}:policy:foe-filter:info" );

	/**
	 *
	 */
	class FoeFilterPolicy {
		/**
		 * @param {Hitchy.Core.IncomingMessage} req request descriptor
		 * @param {Hitchy.Core.ServerResponse} res response manager
		 */
		static apply( req, res, next ) {
			logInfo( "testing for foe to be greeted" );

			if ( api.config.foes?.includes( api.service.Tools.normalize( req.params.name ) ) ) {
				next( new api.service.HttpException( 403, "You are not welcome!" ) );
			} else {
				next();
			}
		}
	}

	return FoeFilterPolicy;
}
