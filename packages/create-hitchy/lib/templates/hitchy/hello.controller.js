/**
 * This module implements a controller which is consisting of methods routes may
 * target for eventually handling incoming requests.
 *
 * Read more about it at https://core.hitchy.org/internals/components.html#controllers
 */
export default function( options ) {
	const api = this;

	const logDebug = api.log( "{{config.project}}:controller:hello:debug" );

	/**
	 *
	 */
	class HelloController {
		/**
		 * @param {Hitchy.Core.IncomingMessage} req request descriptor
		 * @param {Hitchy.Core.ServerResponse} res response manager
		 */
		static world( req, res ) {
			logDebug( "greeting a friend" );

			res.send( "Hello " + ( req.params.name || "world" ) + "!" );
		}
	}

	return HelloController;
}
