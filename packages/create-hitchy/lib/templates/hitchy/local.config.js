/**
 * Applies a local-only configuration.
 *
 * This configuration file is processed after all other configurations and thus
 * may replace any configuration found there. It is excluded from deployments by
 * intention to provide setup-specific customizations.
 */
export default function( options ) {
	const api = this;

	return {
		foes: [ "john", "peter", ...options.arguments.foes?.split( "," ) || [] ],
	};
}
