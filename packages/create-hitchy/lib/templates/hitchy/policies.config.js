/**
 * This file configures policies to consider on dispatching certain incoming
 * requests.
 *
 * Read more about it at https://core.hitchy.org/internals/components.html#policies
 */
export default {
	policies: {
		"GET /greet/:name": "FoeFilterPolicy.apply",
	},
};
