/**
 * This file illustrates definition of a server-side data model managed by
 * Hitchy's plugin-odem.
 *
 * Read more about it at https://odem.hitchy.org/guides/defining-models.html#defining-in-filesystem
 */
export default function( options ) {
	const api = this;

	return {
		props: {
			title: { require: true },
			content: {},
			createdAt: {
				type: "date",
				required: true,
				readonly: true,
			},
		}
	};
}
