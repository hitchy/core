# {{config.project}}

## About

This project is based on [Hitchy](https://core.hitchy.org).

## Installation

```sh
npm install
```

## Configuration

Create a copy of file **.env.dist** and name it **.env**. Then, use a text editor to adjust any parameter in there to match your environment.

In addition, files in folder **{{config.configFolder}}** are considered by Hitchy for configuration. Special file **local.js** can be used to provide custom settings there.

## Running

### Development

```sh
npm run dev
```

See [Hitchy's online manual](https://core.hitchy.org) for additional information.

### Production

```sh
npm run start
```

This command is running the Hitchy backend which is serving built frontend, too.

## Docker

Run production version with docker like this:

```sh
docker compose build
docker compose up -d
``` 

## Usage

The running Hitchy backend is handling HTTP requests on port 3000.
