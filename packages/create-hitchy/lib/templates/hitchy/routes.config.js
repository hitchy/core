/**
 * This configuration file associates incoming requests with controller methods
 * handling them.
 *
 * Read more about it at https://core.hitchy.org/internals/routing-basics.html
 */
export const routes = {
	"GET /greet": "Hello.world",
	"GET /greet/:name": "Hello.world",
};
