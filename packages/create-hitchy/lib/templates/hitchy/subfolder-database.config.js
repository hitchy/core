import { resolve } from "node:path";

/**
 * This file is configuring Hitchy's plugin-odem for persistently storing its
 * data in a local subfolder.
 *
 * Read more about it at https://odem.hitchy.org/api/adapter.html#storing-data-in-local-filesystem
 */
export default function( { arguments: args, environment: env, projectFolder } ) {
	return {
		database: {
			default: new this.services.OdemAdapterFile( {
				dataSource: resolve( projectFolder, args["database-folder"] || env.DATABASE_FOLDER || "data" ),
			} ),
		},
	};
}
