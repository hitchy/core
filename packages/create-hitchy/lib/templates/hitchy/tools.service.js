/**
 * This module implements a service meant to commonly provide code supporting
 * controllers, policies and models in doing their job. Services implement
 * anything that is neither specific to a single request nor to a single model
 * or one of its items.
 *
 * Read more about it at https://core.hitchy.org/internals/components.html#services
 */
export default function( options ) {
	const api = this;

	const logInfo = api.log( "{{config.project}}:service:tools:info" );

	/**
	 *
	 */
	class ToolsService {
		/**
		 *
		 */
		static normalize( value ) {
			return typeof value === "string" ? value.trim().toLowerCase() : null;
		}
	}

	return ToolsService;
}
