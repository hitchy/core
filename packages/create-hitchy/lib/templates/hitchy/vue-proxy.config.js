/**
 * This configuration file sets up Hitchy's plugin-proxy to forward requests to
 * locally running Vue development server while developing.
 */
export default function( options ) {
	return {
		proxy: options.arguments.development ? [{
			prefix: "/",
			target: "http://localhost:8080/",
			filters: {
				response: {
					header( res, req ) {
						if ( req.path.endsWith( ".json" ) ) {

							res.headers["content-type"] = "application/javascript";
						}

						return res;
					}
				}
			},
		}] : [],
	};
}
