### Backend configuration

Create a copy of file **server/.env.dist** and name it **server/.env**. Then, use a text editor to adjust any parameter in there to match your environment.

In addition, files in folder **server/config** are processed by Hitchy for configuration. Special file **local.js** can be used to provide custom settings there.

### Run Production Version

```sh
npm run start
```

This command is running the Hitchy backend which is serving built frontend, too. 
