import { qualify, writeTextFile } from "./file.js";

export const ptnAllLeadingImports = /(^[\s\S]+\nimport\b.+)/;

/**
 *
 */
export async function writeEditorConfig( config ) {
	await writeTextFile( config( "$BASE/.editorconfig" ), `root = true

[*]
charset = utf-8
end_of_line = lf

[*.{js,mjs,cjs,jsx,html,json,css,scss,ts,vue}]
indent_size = 4
indent_style = tab

ij_css_use_double_quotes = true
ij_css_enforce_quotes_on_format = true
ij_javascript_use_double_quotes = true
ij_javascript_force_quote_style = true

[{*.yml,*.yaml}]
indent_size = 2
indent_style = space
` );
}

/**
 *
 */
export function createConfig( responses ) {
	const config = value => qualify( config, value );

	for ( const key of Object.keys( responses ) ) {
		config[key] = responses[key];
	}

	return config;
}
