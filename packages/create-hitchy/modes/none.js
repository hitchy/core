import { createTextFile, createFolder, editJsonFile } from "../lib/file.js";
import { gitInitialize, invokeQuiet, npm, npmInstallDev } from "../lib/process.js";
import { configureOdem, createHitchyFilesystem, installHitchyDependencies } from "../lib/hitchy.js";
import { writeEditorConfig } from "../lib/tools.js";
import { templateToFile } from "../lib/template.js";
import { addDocker } from "../lib/docker.js";

/**
 *
 */
export async function handler( config ) {
	console.log( "Setting up Hitchy w/o frontend framework now ..." ); // eslint-disable-line no-console

	config.hitchyBaseFolder = "";
	config.configFolder = "config";

	await createFolder( config.folder );

	process.chdir( config.folder );

	await templateToFile( "hitchy/readme.md", config( "readme.md" ), config );
	await templateToFile( "hitchy/.env.dist", config( "$HITCHY/.env.dist" ), config );
	await createTextFile( config( "package.json" ), "{}" );
	await createTextFile( config( ".gitignore" ), `.idea
.env
.vscode
node_modules
coverage
config/local.js
` );

	await invokeQuiet( npm, "init", "-y" );

	await installHitchyDependencies( config );
	await createHitchyFilesystem( config );
	await configureOdem( config );

	await writeEditorConfig( config );

	await editJsonFile( config( "package.json" ), data => {
		data.type = "module";

		data.scripts.start = "hitchy --ip=0.0.0.0 --foes=fred,jane";
		data.scripts.dev = "hitchy --development --log-level=DEBUG";

		return data;
	} );

	if ( config.eslint ) {
		await npmInstallDev( "eslint", "eslint-config-cepharum" );
		await createTextFile( config( "$BASE/eslint.config.js" ), `import eslintConfigCepharum from "eslint-config-cepharum";

export default [
    ...eslintConfigCepharum,
];` );
	}

	await addDocker( config );

	if ( config.git ) {
		await gitInitialize();
	}

	return `  1. cd ${config.project}
  2. npm install
  3. Create .env file from .env.dist and adjust as necessary.
  4. npm run dev
       or
     npm run start
  5. Open browser e.g. at http://127.0.0.1:3000/greet/hugo`;
}
