import { createFolder } from "../lib/file.js";

/**
 *
 */
export async function handler( config ) {
	await createFolder( config.folder );
}
