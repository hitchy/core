import { appendTextFile, createFolder, createTextFile, editJsonFile, exists, replaceTextFile } from "../lib/file.js";
import { gitInitialize, invoke, npm, npmInstallDev } from "../lib/process.js";
import { configureHitchyVueIntegration, configureOdem, createHitchyFilesystem, installHitchyDependencies } from "../lib/hitchy.js";
import { writeEditorConfig } from "../lib/tools.js";
import { addShoelaceToVue } from "../lib/shoelace.js";
import { addFontAwesomeToVue } from "../lib/fontawesome.js";
import { addDocker } from "../lib/docker.js";
import { renderTemplate, templateToFile } from "../lib/template.js";

/**
 *
 */
export async function handler( config ) {	config.hitchyBaseFolder = config.folder;
	console.log( "Creating Vue3 project first ..." ); // eslint-disable-line no-console

	await invoke( npm, "create", "vue@latest", config.project );

	console.log( "Okay, that's all for Vue3 ... adding Hitchy as backend now ..." ); // eslint-disable-line no-console


	config.hitchyBaseFolder = "server";
	config.configFolder = config( "$BASE/server/config" );

	process.chdir( config.folder );

	await createFolder( config.hitchyBaseFolder, { enter: false } );

	await templateToFile( "hitchy/.env.dist", config( "$HITCHY/.env.dist" ), config );
	await appendTextFile( config( "$BASE/.gitignore" ), `server/public
server/config/local.js` );

	await installHitchyDependencies( config );
	await createHitchyFilesystem( config );
	await configureHitchyVueIntegration( config );
	await configureOdem( config );

	await writeEditorConfig( config );

	await npmInstallDev( "concurrently" );

	await editJsonFile( config( "$BASE/package.json" ), data => {
		data.scripts["dev:frontend"] = data.scripts.dev;
		data.scripts["dev:backend"] = "hitchy --project=server --plugins=. --development --debug";

		data.scripts.start = "hitchy --project=server --plugins=. --ip=0.0.0.0";
		data.scripts.dev = "concurrently -n vue,hitchy -k npm:dev:frontend npm:dev:backend";

		for ( const key of Object.keys( data.dependencies ) ) {
			if ( !key.startsWith( "@hitchy/" ) ) {
				data.devDependencies[key] = data.dependencies[key];
				delete data.dependencies[key];
			}
		}

		return data;
	} );

	await addShoelaceToVue( config );
	await addFontAwesomeToVue( config );

	if ( config.eslint ) {
		if ( await exists( config( "$BASE/eslint.config.js" ) ) ) {
			await npmInstallDev( "eslint-config-cepharum" );
			await replaceTextFile( config( "$BASE/eslint.config.js" ), /(?=import)/, `import eslintConfigCepharum from "eslint-config-cepharum";\n` );
			await replaceTextFile( config( "$BASE/eslint.config.js" ), /(\s*];?\s*$)/, ( _, tail ) => "\n    ...eslintConfigCepharum," + tail );
		} else {
			await npmInstallDev( "eslint", "eslint-config-cepharum" );
			await createTextFile( config( "$BASE/eslint.config.js" ), `import eslintConfigCepharum from "eslint-config-cepharum";

export default [
    ...eslintConfigCepharum,
];` );
		}
	}

	await addDocker( config );

	await appendTextFile( config( "$BASE/README.md" ), await renderTemplate( "hitchy/vue-readme.md", { config } ) );

	if ( config.git ) {
		await gitInitialize();
	}

	return `  1. cd ${config.project}
  2. npm run dev
       or
     npm run build && npm run start
  3. Open browser at http://127.0.0.1:3000`;
}
