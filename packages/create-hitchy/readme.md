# create-hitchy

This package is setting up a new project based on Hitchy.

## License

MIT

## Usage

1. Open terminal in context of a local folder to contain your new project.
2. Invoke

   ```sh
   npm create hitchy@latest my-app
   ```
   
   This is fetching and executing **create-hitchy** for setting up a new Hitchy-based project in sub-folder **my-app**.
