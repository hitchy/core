#!/usr/bin/env node
import { resolve } from "node:path";
import { fileURLToPath } from "node:url";
import { rm, writeFile } from "node:fs/promises";
import { runner } from "./lib/runner.js";

await rm( resolve( fileURLToPath( import.meta.url ), "../my-app" ), { recursive: true, force: true } );

console.log( "Test: create hitchy project using all available backend options" ); // eslint-disable-line no-console

await runner( {
	folder: "my-app",
	mode: "none",
	auth: true,
	"odem-backend": "etcd",
	"odem-rest": true,
	"odem-websocket": true,
	static: true,
	proxy: true,
	mailer: true,
	eslint: true,
	git: true
}, [], {} );

await writeFile( "api/services/shutdown.js", `
export default function() {
	setTimeout( () => {
		this.shutdown();
	}, 5000 );
}
`, "utf-8" );
