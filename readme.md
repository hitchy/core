# Hitchy's Core

## License

[MIT](packages/core/LICENSE)

## Packages

This project consists of multiple packages a.k.a. workspaces:

* [core](packages/core) is the actual implementation of [@hitchy/core](https://www.npmjs.com/package/@hitchy/core).
* [core-manual](packages/core-manual) implements the official online manual published at https://core.hitchy.org.
* [create-hitchy](packages/create-hitchy) provides a tool for quickly scaffolding new projects based on Hitchy.
